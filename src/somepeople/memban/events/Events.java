/*    */ package somepeople.memban.events;
/*    */ 
/*    */ import net.md_5.bungee.api.chat.TextComponent;
/*    */ import net.md_5.bungee.api.connection.ProxiedPlayer;
/*    */ import net.md_5.bungee.api.event.ChatEvent;
/*    */ import net.md_5.bungee.api.event.LoginEvent;
/*    */ import net.md_5.bungee.event.EventHandler;
/*    */ import org.joda.time.DateTime;
/*    */ import somepeople.memban.data.Ban;
/*    */ import somepeople.memban.data.Messages;
/*    */ import somepeople.memban.data.Mute;
/*    */ import somepeople.memban.data.MyTime;
/*    */ import somepeople.memban.main.CM;
/*    */ import somepeople.memban.main.Main;
/*    */ 
/*    */ public class Events implements net.md_5.bungee.api.plugin.Listener
/*    */ {
/*    */   @EventHandler(priority=32)
/*    */   public void onLogin(LoginEvent event)
/*    */   {
/* 21 */     String name = event.getConnection().getName();
/*    */     
/* 23 */     Ban ban = Ban.load(name);
/*    */     
/* 25 */     if (ban != null)
/*    */     {
/* 27 */       if ((!ban.value.equals("")) && (DateTime.parse(ban.enddate, MyTime.FORMAT_DATE).isBeforeNow())) {
/* 28 */         Ban.saveNull(name);
/* 29 */         event.setCancelled(false);
/*    */       } else {
/* 31 */         String msg = Main.CM.messages.playerBan(ban.byPlayer, ban.reason, ban.date, ban.getEndDate());
/*    */         
/* 33 */         event.setCancelReason(msg);
/* 34 */         event.setCancelled(true);
/*    */       }
/* 36 */     } else if (Main.CM.banIp.isBanned(event.getConnection().getAddress().getAddress().toString())) {
/* 37 */       event.setCancelReason(Main.CM.messages.playerBanIp());
/* 38 */       event.setCancelled(true);
/*    */     }
/*    */   }
/*    */   
/*    */   @EventHandler
/*    */   public void onChat(ChatEvent event) {
/* 44 */     ProxiedPlayer sender = (ProxiedPlayer)event.getSender();
/*    */     
/* 46 */     Mute mute = Mute.load(sender.getName());
/*    */     
/* 48 */     if (mute != null)
/*    */     {
/* 50 */       String command = event.getMessage().split(" ")[0];
/*    */       
/* 52 */       if (command.startsWith("/")) {
/* 53 */         if (Main.CM.blockCommands.isBlock(command)) {
/* 54 */           event.setCancelled(true);
/* 55 */           sender.sendMessage(new TextComponent(net.md_5.bungee.api.ChatColor.RED + "[MemBan] Эта команда запрещена"));
/* 56 */           return;
/*    */         }
/* 58 */         return;
/*    */       }
/* 60 */       if (mute.enddate.equals("")) {
/* 61 */         event.setCancelled(true);
/*    */         
/* 63 */         sender.sendMessage(new TextComponent(Main.CM.messages
/* 64 */           .playerMuteForever(mute.byPlayer, mute.reason)));
/*    */       }
/* 66 */       else if (DateTime.parse(mute.enddate, MyTime.FORMAT_DATE).isBeforeNow()) {
/* 67 */         Mute.saveNull(sender.getName());
/* 68 */         event.setCancelled(false);
/*    */       } else {
/* 70 */         event.setCancelled(true);
/*    */         
/* 72 */         sender.sendMessage(new TextComponent(Main.CM.messages
/* 73 */           .playerMuteTime(mute.byPlayer, mute.reason, mute.getEndDate())));
/*    */       }
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\somepeople\memban\events\Events.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */