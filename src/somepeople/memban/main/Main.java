/*    */ package somepeople.memban.main;
/*    */ 
/*    */ import com.google.gson.GsonBuilder;
/*    */ import net.md_5.bungee.api.ProxyServer;
/*    */ import net.md_5.bungee.api.plugin.Plugin;
/*    */ import net.md_5.bungee.api.plugin.PluginManager;
/*    */ import somepeople.memban.commands.MembanCommand;
/*    */ import somepeople.memban.commands.MemhelpCommand;
/*    */ import somepeople.memban.commands.MuteCommand;
/*    */ import somepeople.memban.commands.TimeBanCommand;
/*    */ import somepeople.memban.commands.UnbanCommand;
/*    */ import somepeople.memban.data.Limit;
/*    */ import somepeople.memban.events.Events;
/*    */ 
/*    */ public class Main extends Plugin
/*    */ {
/*    */   public static java.util.logging.Logger logger;
/*    */   public static CM CM;
/* 19 */   public static com.google.gson.Gson gson = new GsonBuilder().setPrettyPrinting().create();
/* 20 */   public static Limit LIMITS = new Limit();
/*    */   
/*    */ 
/*    */   public void onEnable()
/*    */   {
/* 25 */     logger = getLogger();
/* 26 */     CM = new CM();
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 34 */     getProxy().getPluginManager().registerListener(this, new Events());
/*    */     
/*    */ 
/* 37 */     getProxy().getPluginManager().registerCommand(this, new somepeople.memban.commands.BanCommand());
/* 38 */     getProxy().getPluginManager().registerCommand(this, new TimeBanCommand());
/* 39 */     getProxy().getPluginManager().registerCommand(this, new somepeople.memban.commands.BanipCommand());
/* 40 */     getProxy().getPluginManager().registerCommand(this, new UnbanCommand());
/*    */     
/*    */ 
/* 43 */     getProxy().getPluginManager().registerCommand(this, new somepeople.memban.commands.KickCommand());
/*    */     
/*    */ 
/* 46 */     getProxy().getPluginManager().registerCommand(this, new MuteCommand());
/* 47 */     getProxy().getPluginManager().registerCommand(this, new somepeople.memban.commands.UmuteCommand());
/*    */     
/*    */ 
/* 50 */     getProxy().getPluginManager().registerCommand(this, new somepeople.memban.commands.WarnCommand());
/*    */     
/*    */ 
/* 53 */     getProxy().getPluginManager().registerCommand(this, new somepeople.memban.commands.MuteinfoCommand());
/* 54 */     getProxy().getPluginManager().registerCommand(this, new somepeople.memban.commands.BaninfoCommand());
/*    */     
/*    */ 
/* 57 */     getProxy().getPluginManager().registerCommand(this, new MembanCommand());
/* 58 */     getProxy().getPluginManager().registerCommand(this, new MemhelpCommand());
/* 59 */     getProxy().getPluginManager().registerCommand(this, new somepeople.memban.commands.BanhelpCommand());
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\somepeople\memban\main\Main.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */