/*    */ package somepeople.memban.commands;
/*    */ 
/*    */ import net.md_5.bungee.api.CommandSender;
/*    */ import net.md_5.bungee.api.plugin.Command;
/*    */ import somepeople.memban.data.HelpList;
/*    */ 
/*    */ public class MemhelpCommand extends Command
/*    */ {
/*    */   public MemhelpCommand()
/*    */   {
/* 11 */     super("memhelp");
/*    */   }
/*    */   
/*    */   public void execute(CommandSender sender, String[] args)
/*    */   {
/* 16 */     sender.sendMessage(new net.md_5.bungee.api.chat.TextComponent(HelpList.getHelp(args)));
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\somepeople\memban\commands\MemhelpCommand.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */