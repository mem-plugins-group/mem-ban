/*    */ package somepeople.memban.commands;
/*    */ 
/*    */ import net.md_5.bungee.api.CommandSender;
/*    */ import net.md_5.bungee.api.ProxyServer;
/*    */ import net.md_5.bungee.api.chat.TextComponent;
/*    */ import net.md_5.bungee.api.connection.ProxiedPlayer;
/*    */ import somepeople.memban.data.Messages;
/*    */ import somepeople.memban.main.CM;
/*    */ import somepeople.memban.main.Main;
/*    */ 
/*    */ public class BanipCommand extends net.md_5.bungee.api.plugin.Command
/*    */ {
/*    */   public BanipCommand()
/*    */   {
/* 15 */     super("sbanip", "memban.sbanip", new String[0]);
/*    */   }
/*    */   
/*    */ 
/*    */   public void execute(CommandSender sender, String[] args)
/*    */   {
/* 21 */     switch (args.length) {
/*    */     case 0: 
/* 23 */       sender.sendMessage(new TextComponent(Messages.INVALID));
/* 24 */       break;
/*    */     case 1: 
/* 26 */       sender.sendMessage(new TextComponent(Messages.INVALID));
/* 27 */       break;
/*    */     default: 
/* 29 */       banIp(sender, args);
/*    */     }
/*    */     
/*    */   }
/*    */   
/*    */ 
/*    */   static void banIp(CommandSender sender, String[] args)
/*    */   {
/* 37 */     ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);
/*    */     
/* 39 */     if (target == null) {
/* 40 */       sender.sendMessage(new TextComponent(Messages.OFFLINE)); return;
/*    */     }
/*    */     
/*    */     String prefix;
/*    */     String prefix;
/* 45 */     if ((sender instanceof ProxiedPlayer)) {
/* 46 */       prefix = somepeople.memban.data.BPhook.getPrefix(sender.getName());
/*    */     } else {
/* 48 */       prefix = "Сервер";
/*    */     }
/*    */     
/* 51 */     String reason = somepeople.memban.data.Args.getReason(args);
/*    */     
/* 53 */     Main.CM.banIp.add(target.getAddress().getAddress().toString(), reason, prefix);
/*    */     
/* 55 */     target.disconnect(TextComponent.fromLegacyText(Main.CM.messages.playerBanIp()));
/*    */     
/* 57 */     String broadcast = Main.CM.messages.serverBanForever(somepeople.memban.data.BPhook.getPrefix(target.getName()), prefix, reason);
/* 58 */     Main.CM.messages.broadcast(target, broadcast);
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\somepeople\memban\commands\BanipCommand.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */