/*    */ package somepeople.memban.commands;
/*    */ 
/*    */ import net.md_5.bungee.api.CommandSender;
/*    */ import net.md_5.bungee.api.connection.ProxiedPlayer;
/*    */ import somepeople.memban.data.Args;
/*    */ import somepeople.memban.data.Messages;
/*    */ import somepeople.memban.data.Profile;
/*    */ import somepeople.memban.data.Warn;
/*    */ 
/*    */ public class WarnCommand extends net.md_5.bungee.api.plugin.Command
/*    */ {
/*    */   public WarnCommand()
/*    */   {
/* 14 */     super("warn", "memban.warn", new String[0]);
/*    */   }
/*    */   
/*    */ 
/*    */   public void execute(CommandSender sender, String[] args)
/*    */   {
/* 20 */     switch (args.length) {
/*    */     case 0: 
/* 22 */       sender.sendMessage(new net.md_5.bungee.api.chat.TextComponent(Messages.INVALID));
/* 23 */       break;
/*    */     case 1: 
/* 25 */       sender.sendMessage(new net.md_5.bungee.api.chat.TextComponent(Messages.INVALID));
/* 26 */       break;
/*    */     default: 
/* 28 */       warn(sender, args);
/*    */     }
/*    */   }
/*    */   
/*    */   static void warn(CommandSender sender, String[] args)
/*    */   {
/* 34 */     ProxiedPlayer target = net.md_5.bungee.api.ProxyServer.getInstance().getPlayer(args[0]);
/* 35 */     if (target == null) {
/* 36 */       sender.sendMessage(new net.md_5.bungee.api.chat.TextComponent(Messages.OFFLINE));
/* 37 */       return;
/*    */     }
/*    */     
/* 40 */     String reason = Args.getReason(args);
/* 41 */     String prefix = Args.getPrefix(sender);
/*    */     
/* 43 */     Warn warn = Warn.load(target.getName());
/* 44 */     warn.set(reason, prefix, Args.getServerName(sender));
/* 45 */     warn.save(target.getName());
/*    */     
/* 47 */     Profile profile = Profile.get(target.getName());
/* 48 */     profile.addWarn();
/* 49 */     profile.save(target.getName());
/*    */     
/* 51 */     String broadcast = somepeople.memban.main.Main.CM.messages.serverWarn(somepeople.memban.data.BPhook.getPrefix(target.getName()), prefix, reason);
/* 52 */     somepeople.memban.main.Main.CM.messages.broadcast(target, broadcast);
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\somepeople\memban\commands\WarnCommand.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */