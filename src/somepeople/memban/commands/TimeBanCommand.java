/*    */ package somepeople.memban.commands;
/*    */ 
/*    */ import net.md_5.bungee.api.CommandSender;
/*    */ import net.md_5.bungee.api.chat.TextComponent;
/*    */ import net.md_5.bungee.api.connection.ProxiedPlayer;
/*    */ import somepeople.memban.data.Ban;
/*    */ import somepeople.memban.data.Messages;
/*    */ import somepeople.memban.data.Profile;
/*    */ import somepeople.memban.main.CM;
/*    */ import somepeople.memban.main.Main;
/*    */ 
/*    */ public class TimeBanCommand extends net.md_5.bungee.api.plugin.Command
/*    */ {
/*    */   public TimeBanCommand()
/*    */   {
/* 16 */     super("stimeban", "memban.stimeban", new String[0]);
/*    */   }
/*    */   
/*    */   public void execute(CommandSender sender, String[] args)
/*    */   {
/* 21 */     if (args.length < 4) {
/* 22 */       sender.sendMessage(new TextComponent(Messages.INVALID));
/*    */     } else {
/* 24 */       banTime(sender, args);
/*    */     }
/*    */   }
/*    */   
/*    */   static void banTime(CommandSender sender, String[] args) {
/* 29 */     ProxiedPlayer target = net.md_5.bungee.api.ProxyServer.getInstance().getPlayer(args[0]);
/* 30 */     if (target == null) {
/* 31 */       sender.sendMessage(new TextComponent(Messages.OFFLINE));
/* 32 */       return;
/*    */     }
/*    */     
/* 35 */     String time = somepeople.memban.data.MyTime.convertTime(somepeople.memban.data.Args.getTime(args));
/* 36 */     if (time == null) {
/* 37 */       sender.sendMessage(new TextComponent(net.md_5.bungee.api.ChatColor.RED + "[MemBan] Неверный формат времени! Используйте: Y M D h m s"));
/* 38 */       return;
/*    */     }
/*    */     
/* 41 */     String reason = somepeople.memban.data.Args.getReason(args);
/*    */     String prefix;
/*    */     String prefix;
/* 44 */     if ((sender instanceof ProxiedPlayer)) {
/* 45 */       prefix = somepeople.memban.data.BPhook.getPrefix(sender.getName());
/*    */     } else {
/* 47 */       prefix = "Сервер";
/*    */     }
/*    */     
/* 50 */     Ban ban = new Ban(reason, prefix, time);
/* 51 */     ban.save(target.getName());
/*    */     
/* 53 */     String broadcast = Main.CM.messages.serverBanTime(somepeople.memban.data.BPhook.getPrefix(args[0]), prefix, time, reason);
/* 54 */     Main.CM.messages.broadcast(target, broadcast);
/*    */     
/* 56 */     String msg = Main.CM.messages.playerBan(prefix, reason, somepeople.memban.data.MyTime.FORMAT_DATE.print(org.joda.time.DateTime.now()), time);
/* 57 */     target.disconnect(TextComponent.fromLegacyText(msg));
/*    */     
/* 59 */     Profile profile = Profile.get(target.getName());
/* 60 */     profile.addBan();
/* 61 */     profile.save(target.getName());
/*    */     
/* 63 */     if (sender.getGroups().contains("helper")) {
/* 64 */       Main.LIMITS.add(sender.getName(), target.getName(), org.joda.time.DateTime.now());
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\somepeople\memban\commands\TimeBanCommand.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */