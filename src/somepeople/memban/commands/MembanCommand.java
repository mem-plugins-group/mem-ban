/*    */ package somepeople.memban.commands;
/*    */ 
/*    */ import net.md_5.bungee.api.ChatColor;
/*    */ import net.md_5.bungee.api.CommandSender;
/*    */ import net.md_5.bungee.api.chat.TextComponent;
/*    */ import net.md_5.bungee.api.plugin.Command;
/*    */ import somepeople.memban.data.BPhook;
/*    */ import somepeople.memban.data.Messages;
/*    */ import somepeople.memban.data.Profile;
/*    */ import somepeople.memban.main.CM;
/*    */ 
/*    */ public class MembanCommand extends Command
/*    */ {
/*    */   public MembanCommand()
/*    */   {
/* 16 */     super("memban");
/*    */   }
/*    */   
/*    */   public void execute(CommandSender sender, String[] args)
/*    */   {
/* 21 */     switch (args.length) {
/*    */     case 0: 
/* 23 */       sender.sendMessage(new TextComponent(ChatColor.AQUA + "Плагин MemBan. Автор: Some. Специально для " + ChatColor.RED + "Memas" + ChatColor.GOLD + "Gold" + ChatColor.AQUA + "\nДля справки используйте /banhelp или /memhelp"));
/*    */       
/*    */ 
/* 26 */       break;
/*    */     case 1: 
/* 28 */       if (args[0].equals("reload")) reload(sender); else
/* 29 */         sender.sendMessage(new TextComponent(ChatColor.RED + "[MemBan] Используйте: /memban reload"));
/* 30 */       break;
/*    */     case 2: 
/* 32 */       if (args[0].equals("stat")) { stat(sender, args[1]);
/*    */       } else
/* 34 */         sender.sendMessage(new TextComponent(ChatColor.RED + "[MemBan] Используйте: /memban stat <игрок>"));
/* 35 */       break;
/*    */     default: 
/* 37 */       sender.sendMessage(new TextComponent(Messages.INVALID));
/*    */     }
/*    */   }
/*    */   
/*    */   static void reload(CommandSender sender)
/*    */   {
/* 43 */     if (!sender.hasPermission("memban.reload")) {
/* 44 */       sender.sendMessage(new TextComponent(Messages.NO_PEX));
/* 45 */       return;
/*    */     }
/*    */     
/* 48 */     somepeople.memban.main.Main.CM.loadAllConfigs();
/* 49 */     sender.sendMessage(new TextComponent(ChatColor.GREEN + "[MemBan] Plugin reloaded"));
/*    */   }
/*    */   
/*    */   public static void stat(CommandSender sender, String name) {
/* 53 */     if (!sender.hasPermission("memban.stat")) {
/* 54 */       sender.sendMessage(new TextComponent(Messages.NO_PEX));
/* 55 */       return;
/*    */     }
/*    */     
/* 58 */     Profile profile = Profile.get(name);
/*    */     
/* 60 */     if (profile == null) {
/* 61 */       sender.sendMessage(new TextComponent(ChatColor.RED + "[MemBan] Профиль игрока не найден"));
/* 62 */       return;
/*    */     }
/*    */     
/* 65 */     String msg = "Статистика игрока: " + BPhook.getPrefix(name);
/* 66 */     msg = msg + "\nBan: " + profile.countBan;
/* 67 */     msg = msg + "\nMute: " + profile.countMute;
/* 68 */     msg = msg + "\nWarn: " + profile.countWarn;
/*    */     
/* 70 */     sender.sendMessage(new TextComponent(ChatColor.AQUA + msg));
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\somepeople\memban\commands\MembanCommand.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */