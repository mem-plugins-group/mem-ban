/*    */ package somepeople.memban.commands;
/*    */ 
/*    */ import net.md_5.bungee.api.ChatColor;
/*    */ import net.md_5.bungee.api.CommandSender;
/*    */ import net.md_5.bungee.api.chat.TextComponent;
/*    */ import net.md_5.bungee.api.plugin.Command;
/*    */ import somepeople.memban.data.BPhook;
/*    */ import somepeople.memban.data.Messages;
/*    */ import somepeople.memban.data.Mute;
/*    */ 
/*    */ public class MuteinfoCommand extends Command
/*    */ {
/*    */   public MuteinfoCommand()
/*    */   {
/* 15 */     super("muteinfo", "memban.muteinfo", new String[0]);
/*    */   }
/*    */   
/*    */   public void execute(CommandSender sender, String[] args)
/*    */   {
/* 20 */     switch (args.length) {
/*    */     case 1: 
/*    */       try {
/* 23 */         Mute mute = Mute.load(args[0]);
/* 24 */         String msg = somepeople.memban.main.Main.CM.messages.infoMute(BPhook.getPrefix(args[0]), mute.byPlayer, mute.date, mute.enddate, mute.reason);
/*    */         
/*    */ 
/*    */ 
/*    */ 
/* 29 */         sender.sendMessage(new TextComponent(msg));
/*    */       } catch (NullPointerException e) {
/* 31 */         sender.sendMessage(new TextComponent(ChatColor.RED + "[MemBan] У этого игрока нет Mute"));
/*    */       }
/*    */     
/*    */     default: 
/* 35 */       sender.sendMessage(new TextComponent(Messages.INVALID));
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\somepeople\memban\commands\MuteinfoCommand.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */