/*    */ package somepeople.memban.commands;
/*    */ 
/*    */ import net.md_5.bungee.api.ChatColor;
/*    */ import net.md_5.bungee.api.CommandSender;
/*    */ import net.md_5.bungee.api.chat.TextComponent;
/*    */ import net.md_5.bungee.api.plugin.Command;
/*    */ import somepeople.memban.data.Args;
/*    */ import somepeople.memban.data.BPhook;
/*    */ import somepeople.memban.data.Ban;
/*    */ import somepeople.memban.data.Limit;
/*    */ import somepeople.memban.data.Messages;
/*    */ import somepeople.memban.main.Main;
/*    */ 
/*    */ public class UnbanCommand extends Command
/*    */ {
/*    */   public UnbanCommand()
/*    */   {
/* 18 */     super("sunban", "memban.sunmute", new String[0]);
/*    */   }
/*    */   
/*    */ 
/*    */   public void execute(CommandSender sender, String[] args)
/*    */   {
/* 24 */     switch (args.length) {
/*    */     case 1: 
/* 26 */       unBan(sender, args[0]);
/* 27 */       break;
/*    */     default: 
/* 29 */       sender.sendMessage(new TextComponent(Messages.INVALID));
/*    */     }
/*    */     
/*    */   }
/*    */   
/*    */   static void unBan(CommandSender sender, String name)
/*    */   {
/* 36 */     if ((sender.getGroups().contains("helper")) && 
/* 37 */       (!Main.LIMITS.hasLimit(sender.getName(), name))) {
/* 38 */       sender.sendMessage(new TextComponent(ChatColor.RED + "[MemBan] Вы не можете разбанить этого игрока"));
/* 39 */       return;
/*    */     }
/*    */     
/* 42 */     Ban ban = Ban.load(name);
/*    */     
/* 44 */     if (ban == null) {
/* 45 */       sender.sendMessage(new TextComponent(ChatColor.RED + "[MemBan] У этого игрока нет бана"));
/* 46 */       return;
/*    */     }
/*    */     
/* 49 */     Ban.saveNull(name);
/*    */     
/* 51 */     String prefix = Args.getPrefix(sender);
/*    */     
/* 53 */     String broadcast = Main.CM.messages.serverUnBan(BPhook.getPrefix(name), prefix);
/* 54 */     sender.sendMessage(new TextComponent(broadcast));
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\somepeople\memban\commands\UnbanCommand.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */