/*    */ package somepeople.memban.commands;
/*    */ 
/*    */ import net.md_5.bungee.api.CommandSender;
/*    */ import net.md_5.bungee.api.chat.TextComponent;
/*    */ import net.md_5.bungee.api.connection.ProxiedPlayer;
/*    */ import somepeople.memban.data.BPhook;
/*    */ import somepeople.memban.data.Messages;
/*    */ import somepeople.memban.main.CM;
/*    */ import somepeople.memban.main.Main;
/*    */ 
/*    */ public class KickCommand extends net.md_5.bungee.api.plugin.Command
/*    */ {
/*    */   public KickCommand()
/*    */   {
/* 15 */     super("kick", "memban.kick", new String[0]);
/*    */   }
/*    */   
/*    */ 
/*    */   public void execute(CommandSender sender, String[] args)
/*    */   {
/* 21 */     switch (args.length) {
/*    */     case 0: 
/* 23 */       sender.sendMessage(new TextComponent(Messages.INVALID));
/* 24 */       break;
/*    */     case 1: 
/* 26 */       sender.sendMessage(new TextComponent(Messages.INVALID));
/* 27 */       break;
/*    */     default: 
/* 29 */       kick(sender, args);
/*    */     }
/*    */     
/*    */   }
/*    */   
/*    */   static void kick(CommandSender sender, String[] args)
/*    */   {
/* 36 */     ProxiedPlayer target = net.md_5.bungee.api.ProxyServer.getInstance().getPlayer(args[0]);
/*    */     
/* 38 */     if (target == null) {
/* 39 */       sender.sendMessage(new TextComponent(Messages.OFFLINE)); return;
/*    */     }
/*    */     
/*    */     String prefix;
/*    */     String prefix;
/* 44 */     if ((sender instanceof ProxiedPlayer)) {
/* 45 */       prefix = BPhook.getPrefix(sender.getName());
/*    */     } else {
/* 47 */       prefix = "Сервер";
/*    */     }
/*    */     
/* 50 */     String reason = somepeople.memban.data.Args.getReason(args);
/*    */     
/* 52 */     target.disconnect(TextComponent.fromLegacyText(Main.CM.messages.playerKick(prefix, reason)));
/*    */     
/* 54 */     String broadcast = Main.CM.messages.serverKick(BPhook.getPrefix(target.getName()), prefix, reason);
/* 55 */     Main.CM.messages.broadcast(target, broadcast);
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\somepeople\memban\commands\KickCommand.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */