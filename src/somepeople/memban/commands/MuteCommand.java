/*     */ package somepeople.memban.commands;
/*     */ 
/*     */ import net.md_5.bungee.api.CommandSender;
/*     */ import net.md_5.bungee.api.ProxyServer;
/*     */ import net.md_5.bungee.api.chat.TextComponent;
/*     */ import net.md_5.bungee.api.connection.ProxiedPlayer;
/*     */ import somepeople.memban.data.Args;
/*     */ import somepeople.memban.data.Messages;
/*     */ import somepeople.memban.data.Mute;
/*     */ import somepeople.memban.data.Profile;
/*     */ 
/*     */ public class MuteCommand extends net.md_5.bungee.api.plugin.Command
/*     */ {
/*     */   public MuteCommand()
/*     */   {
/*  16 */     super("smute", "memban.smute", new String[0]);
/*     */   }
/*     */   
/*     */ 
/*     */   public void execute(CommandSender sender, String[] args)
/*     */   {
/*  22 */     if (args.length > 1) {
/*  23 */       mute(sender, args);
/*     */     } else {
/*  25 */       sender.sendMessage(new TextComponent(Messages.INVALID));
/*     */     }
/*     */   }
/*     */   
/*     */   static void mute(CommandSender sender, String[] args)
/*     */   {
/*  31 */     for (int i = 1; i < args.length; i++) {
/*  32 */       args[i] = args[i].toLowerCase();
/*     */     }
/*     */     
/*  35 */     if (java.util.Arrays.asList(args).contains("time")) muteTime(sender, args); else {
/*  36 */       muteForever(sender, args);
/*     */     }
/*     */   }
/*     */   
/*     */   static void muteForever(CommandSender sender, String[] args)
/*     */   {
/*  42 */     ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);
/*  43 */     if (target == null) {
/*  44 */       sender.sendMessage(new TextComponent(Messages.OFFLINE));
/*  45 */       return;
/*     */     }
/*     */     
/*  48 */     Mute mute = Mute.load(args[0]);
/*     */     
/*  50 */     if (mute != null) {
/*  51 */       sender.sendMessage(new TextComponent(net.md_5.bungee.api.ChatColor.RED + "[MemBan] У этого игрока уже есть Mute"));
/*  52 */       return;
/*     */     }
/*     */     
/*  55 */     String prefix = Args.getPrefix(sender);
/*  56 */     String reason = Args.getReason(args);
/*     */     
/*  58 */     mute = new Mute(reason, prefix, "");
/*  59 */     mute.save(args[0]);
/*     */     
/*  61 */     Profile profile = Profile.get(target.getName());
/*  62 */     profile.addMute();
/*  63 */     profile.save(target.getName());
/*     */     
/*  65 */     String msg = somepeople.memban.main.Main.CM.messages.serverMuteForever(somepeople.memban.data.BPhook.getPrefix(args[0]), prefix, reason);
/*  66 */     somepeople.memban.main.Main.CM.messages.broadcast(target, msg);
/*     */   }
/*     */   
/*     */   static void muteTime(CommandSender sender, String[] args)
/*     */   {
/*  71 */     ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);
/*  72 */     if (target == null) {
/*  73 */       sender.sendMessage(new TextComponent(Messages.OFFLINE));
/*  74 */       return;
/*     */     }
/*     */     
/*  77 */     Mute mute = Mute.load(args[0]);
/*     */     
/*  79 */     if (mute != null) {
/*  80 */       sender.sendMessage(new TextComponent(net.md_5.bungee.api.ChatColor.RED + "[MemBan] У этого игрока уже есть Mute"));
/*  81 */       return;
/*     */     }
/*     */     
/*  84 */     String prefix = Args.getPrefix(sender);
/*  85 */     String reason = Args.getReason(args);
/*  86 */     String time = somepeople.memban.data.MyTime.convertTime(Args.getTime(args));
/*     */     
/*  88 */     if (time == null) {
/*  89 */       sender.sendMessage(new TextComponent(net.md_5.bungee.api.ChatColor.RED + "[MemBan] Неверный формат времени! Используйте: Y M D h m s"));
/*  90 */       return;
/*     */     }
/*     */     
/*  93 */     mute = new Mute(reason, prefix, time);
/*  94 */     mute.save(target.getName());
/*     */     
/*  96 */     Profile profile = Profile.get(target.getName());
/*  97 */     profile.addMute();
/*  98 */     profile.save(target.getName());
/*     */     
/* 100 */     String msg = somepeople.memban.main.Main.CM.messages.serverMuteTime(somepeople.memban.data.BPhook.getPrefix(args[0]), prefix, time, reason);
/* 101 */     somepeople.memban.main.Main.CM.messages.broadcast(target, msg);
/*     */   }
/*     */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\somepeople\memban\commands\MuteCommand.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */