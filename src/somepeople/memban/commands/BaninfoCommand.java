/*    */ package somepeople.memban.commands;
/*    */ 
/*    */ import net.md_5.bungee.api.ChatColor;
/*    */ import net.md_5.bungee.api.CommandSender;
/*    */ import net.md_5.bungee.api.chat.TextComponent;
/*    */ import net.md_5.bungee.api.plugin.Command;
/*    */ import somepeople.memban.data.BPhook;
/*    */ import somepeople.memban.data.Ban;
/*    */ import somepeople.memban.data.Messages;
/*    */ 
/*    */ public class BaninfoCommand extends Command
/*    */ {
/*    */   public BaninfoCommand()
/*    */   {
/* 15 */     super("baninfo", "memban.baninfo", new String[0]);
/*    */   }
/*    */   
/*    */ 
/*    */   public void execute(CommandSender sender, String[] args)
/*    */   {
/* 21 */     switch (args.length) {
/*    */     case 1: 
/*    */       try {
/* 24 */         Ban ban = Ban.load(args[0]);
/* 25 */         String msg = somepeople.memban.main.Main.CM.messages.infoBan(BPhook.getPrefix(args[0]), ban.byPlayer, ban.reason, ban.date, ban.enddate);
/*    */         
/*    */ 
/*    */ 
/*    */ 
/* 30 */         sender.sendMessage(new TextComponent(msg));
/*    */       } catch (NullPointerException e) {
/* 32 */         sender.sendMessage(new TextComponent(ChatColor.RED + "[MemBan] У этого игрока нет бана"));
/*    */       }
/*    */     
/*    */     default: 
/* 36 */       sender.sendMessage(new TextComponent(Messages.INVALID));
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\somepeople\memban\commands\BaninfoCommand.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */