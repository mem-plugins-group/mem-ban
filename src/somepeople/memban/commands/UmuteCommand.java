/*    */ package somepeople.memban.commands;
/*    */ 
/*    */ import net.md_5.bungee.api.CommandSender;
/*    */ import net.md_5.bungee.api.ProxyServer;
/*    */ import net.md_5.bungee.api.chat.TextComponent;
/*    */ import net.md_5.bungee.api.connection.ProxiedPlayer;
/*    */ import net.md_5.bungee.api.plugin.Command;
/*    */ import somepeople.memban.data.Args;
/*    */ import somepeople.memban.data.Messages;
/*    */ import somepeople.memban.data.Mute;
/*    */ import somepeople.memban.main.CM;
/*    */ import somepeople.memban.main.Main;
/*    */ 
/*    */ public class UmuteCommand extends Command
/*    */ {
/*    */   public UmuteCommand()
/*    */   {
/* 18 */     super("sunmute", "memban.sunmute", new String[0]);
/*    */   }
/*    */   
/*    */   public void execute(CommandSender sender, String[] args)
/*    */   {
/* 23 */     switch (args.length) {
/*    */     case 1: 
/* 25 */       unMute(sender, args[0]);
/* 26 */       break;
/*    */     default: 
/* 28 */       sender.sendMessage(new TextComponent(Messages.INVALID));
/*    */     }
/*    */     
/*    */   }
/*    */   
/*    */   static void unMute(CommandSender sender, String name)
/*    */   {
/* 35 */     ProxiedPlayer target = ProxyServer.getInstance().getPlayer(name);
/* 36 */     if (target == null) {
/* 37 */       sender.sendMessage(new TextComponent(Messages.OFFLINE));
/* 38 */       return;
/*    */     }
/*    */     
/* 41 */     Mute mute = Mute.load(name);
/*    */     
/* 43 */     if (mute == null) {
/* 44 */       sender.sendMessage(new TextComponent(net.md_5.bungee.api.ChatColor.RED + "[MemBan] У этого игрока нет Mute"));
/* 45 */       return;
/*    */     }
/*    */     
/* 48 */     Mute.saveNull(name);
/*    */     
/* 50 */     String prefix = Args.getPrefix(sender);
/*    */     
/* 52 */     String broadcast = Main.CM.messages.serverUnMute(somepeople.memban.data.BPhook.getPrefix(name), prefix);
/* 53 */     Main.CM.messages.broadcast(target, broadcast);
/* 54 */     target.sendMessage(new TextComponent(Main.CM.messages.playerUnMute()));
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\somepeople\memban\commands\UmuteCommand.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */