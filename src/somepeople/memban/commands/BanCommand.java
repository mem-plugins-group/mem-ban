/*     */ package somepeople.memban.commands;
/*     */ 
/*     */ import net.md_5.bungee.api.CommandSender;
/*     */ import net.md_5.bungee.api.connection.ProxiedPlayer;
/*     */ import somepeople.memban.data.Ban;
/*     */ import somepeople.memban.data.Messages;
/*     */ import somepeople.memban.data.Profile;
/*     */ import somepeople.memban.main.CM;
/*     */ import somepeople.memban.main.Main;
/*     */ 
/*     */ public class BanCommand extends net.md_5.bungee.api.plugin.Command
/*     */ {
/*     */   public BanCommand()
/*     */   {
/*  15 */     super("sban", "memban.sban", new String[0]);
/*     */   }
/*     */   
/*     */ 
/*     */   public void execute(CommandSender sender, String[] args)
/*     */   {
/*  21 */     switch (args.length) {
/*     */     case 0: 
/*  23 */       sender.sendMessage(new net.md_5.bungee.api.chat.TextComponent(Messages.INVALID));
/*  24 */       break;
/*     */     case 1: 
/*  26 */       ban2min(sender, args[0]);
/*  27 */       break;
/*     */     default: 
/*  29 */       banForever(sender, args);
/*     */     }
/*     */   }
/*     */   
/*     */   static void ban2min(CommandSender sender, final String target)
/*     */   {
/*  35 */     final ProxiedPlayer banplayer = net.md_5.bungee.api.ProxyServer.getInstance().getPlayer(target);
/*     */     
/*  37 */     if (banplayer == null) {
/*  38 */       sender.sendMessage(new net.md_5.bungee.api.chat.TextComponent(Messages.OFFLINE));
/*  39 */       return;
/*     */     }
/*     */     
/*  42 */     Thread twoMinBan = new Thread(new Runnable() {
/*     */       public void run() { String prefix;
/*     */         String prefix;
/*  45 */         if ((this.val$sender instanceof ProxiedPlayer)) {
/*  46 */           prefix = somepeople.memban.data.BPhook.getPrefix(this.val$sender.getName());
/*     */         } else {
/*  48 */           prefix = "Сервер";
/*     */         }
/*     */         
/*  51 */         Ban ban = new Ban("", prefix, "0000-00-00 00:02:00");
/*  52 */         ban.save(target);
/*     */         
/*  54 */         String msg = Main.CM.messages.playerBan(prefix, "", somepeople.memban.data.MyTime.FORMAT_DATE.print(org.joda.time.DateTime.now()), "0000-00-00 00:02:00");
/*  55 */         banplayer.disconnect(net.md_5.bungee.api.chat.TextComponent.fromLegacyText(msg));
/*     */         
/*  57 */         String broadcast = Main.CM.messages.serverBan2min(somepeople.memban.data.BPhook.getPrefix(target), prefix);
/*  58 */         Main.CM.messages.broadcast(banplayer, broadcast);
/*     */         
/*  60 */         Profile profile = Profile.get(target);
/*  61 */         profile.addBan();
/*  62 */         profile.save(target);
/*     */         try
/*     */         {
/*  65 */           Thread.sleep(2000L);
/*  66 */           Ban.saveNull(target);
/*     */         } catch (InterruptedException e) {
/*  68 */           e.getMessage();
/*     */         }
/*     */       }
/*     */     });
/*     */     
/*  73 */     if (sender.getGroups().contains("helper"))
/*  74 */       Main.LIMITS.add(sender.getName(), target, org.joda.time.DateTime.now());
/*  75 */     twoMinBan.start();
/*     */   }
/*     */   
/*     */   static void banForever(CommandSender sender, String[] args) {
/*  79 */     ProxiedPlayer target = net.md_5.bungee.api.ProxyServer.getInstance().getPlayer(args[0]);
/*     */     
/*  81 */     if (target == null) {
/*  82 */       sender.sendMessage(new net.md_5.bungee.api.chat.TextComponent(Messages.OFFLINE)); return;
/*     */     }
/*     */     
/*     */     String prefix;
/*     */     String prefix;
/*  87 */     if ((sender instanceof ProxiedPlayer)) {
/*  88 */       prefix = somepeople.memban.data.BPhook.getPrefix(sender.getName());
/*     */     } else {
/*  90 */       prefix = "Сервер";
/*     */     }
/*     */     
/*  93 */     String reason = somepeople.memban.data.Args.getReason(args);
/*     */     
/*  95 */     Ban ban = new Ban(reason, prefix, "");
/*     */     
/*  97 */     String msg = Main.CM.messages.playerBan(prefix, reason, somepeople.memban.data.MyTime.FORMAT_DATE.print(org.joda.time.DateTime.now()), "permanent");
/*  98 */     target.disconnect(net.md_5.bungee.api.chat.TextComponent.fromLegacyText(msg));
/*     */     
/* 100 */     String broadcast = Main.CM.messages.serverBanForever(somepeople.memban.data.BPhook.getPrefix(target.getName()), prefix, reason);
/* 101 */     Main.CM.messages.broadcast(target, broadcast);
/*     */     
/* 103 */     ban.save(args[0]);
/*     */     
/* 105 */     Profile profile = Profile.get(target.getName());
/* 106 */     profile.addBan();
/* 107 */     profile.save(target.getName());
/*     */     
/* 109 */     if (sender.getGroups().contains("helper")) {
/* 110 */       Main.LIMITS.add(sender.getName(), target.getName(), org.joda.time.DateTime.now());
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\somepeople\memban\commands\BanCommand.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */