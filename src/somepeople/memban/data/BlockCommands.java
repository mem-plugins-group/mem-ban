/*    */ package somepeople.memban.data;
/*    */ 
/*    */ import com.google.gson.Gson;
/*    */ import java.io.File;
/*    */ import java.io.IOException;
/*    */ import java.io.Reader;
/*    */ import java.io.Writer;
/*    */ import java.lang.reflect.Type;
/*    */ import java.util.ArrayList;
/*    */ import java.util.logging.Logger;
/*    */ import somepeople.memban.main.Main;
/*    */ 
/*    */ public class BlockCommands
/*    */ {
/* 15 */   public ArrayList<String> list = new ArrayList(0);
/* 16 */   public static final Type TYPE = new com.google.gson.reflect.TypeToken() {}.getType();
/* 17 */   public static final char sp = File.separatorChar;
/*    */   
/*    */   public BlockCommands() {
/* 20 */     File file = new File("plugins" + sp + "MemBan" + sp + "blockcommands.json");
/*    */     
/* 22 */     file.getParentFile().mkdirs();
/*    */     try
/*    */     {
/* 25 */       if (file.createNewFile()) {
/* 26 */         this.list.add("command1");
/* 27 */         this.list.add("command2");
/* 28 */         this.list.add("command3");
/* 29 */         Writer writer = new java.io.FileWriter(file);
/* 30 */         Main.gson.toJson(this.list, writer);
/* 31 */         writer.close();
/*    */       } else {
/* 33 */         Reader reader = new java.io.FileReader(file);
/* 34 */         this.list = ((ArrayList)Main.gson.fromJson(reader, TYPE));
/* 35 */         reader.close();
/*    */       }
/* 37 */       Main.logger.info("Block commands file loaded");
/*    */     } catch (IOException e) {
/* 39 */       Main.logger.warning("Block commands file not found");
/*    */     }
/*    */   }
/*    */   
/*    */   public boolean isBlock(String command) {
/* 44 */     command = command.toLowerCase().substring(1);
/*    */     
/* 46 */     return this.list.contains(command);
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\somepeople\memban\data\BlockCommands.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */