/*    */ package somepeople.memban.data;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.HashMap;
/*    */ import org.joda.time.DateTime;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class Limit
/*    */ {
/* 25 */   public HashMap<String, ArrayList<TLimit>> list = new HashMap(0);
/*    */   
/*    */   public void add(String sender, String target, DateTime date)
/*    */   {
/* 29 */     if (this.list.containsKey(sender)) {
/* 30 */       if (!containsTarget(sender, target)) ((ArrayList)this.list.get(sender)).add(new TLimit(target, date));
/*    */     } else {
/* 32 */       this.list.put(sender, new ArrayList(1));
/* 33 */       ((ArrayList)this.list.get(sender)).add(new TLimit(target, date));
/*    */     }
/*    */   }
/*    */   
/*    */   public boolean hasLimit(String sender, String target)
/*    */   {
/* 39 */     if (!this.list.containsKey(sender)) return false;
/* 40 */     return containsTarget(sender, target);
/*    */   }
/*    */   
/*    */   private boolean containsTarget(String sender, String target) {
/* 44 */     ArrayList<TLimit> listLimit = (ArrayList)this.list.get(sender);
/*    */     
/* 46 */     for (TLimit o : listLimit) {
/* 47 */       if (o.name.equalsIgnoreCase(target)) {
/* 48 */         if (o.date.plusMinutes(5).isBeforeNow()) {
/* 49 */           ((ArrayList)this.list.get(sender)).remove(o);
/* 50 */           return false; }
/* 51 */         return true;
/*    */       }
/*    */     }
/*    */     
/* 55 */     return false;
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\somepeople\memban\data\Limit.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */