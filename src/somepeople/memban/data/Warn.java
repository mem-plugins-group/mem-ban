/*    */ package somepeople.memban.data;
/*    */ 
/*    */ import com.google.gson.Gson;
/*    */ import com.google.gson.reflect.TypeToken;
/*    */ import java.io.File;
/*    */ import java.io.FileReader;
/*    */ import java.io.FileWriter;
/*    */ import java.io.IOException;
/*    */ import java.io.Reader;
/*    */ import java.io.Writer;
/*    */ import java.lang.reflect.Type;
/*    */ import java.util.TreeMap;
/*    */ import java.util.logging.Logger;
/*    */ import somepeople.memban.main.CM;
/*    */ import somepeople.memban.main.Main;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class Warn
/*    */ {
/* 38 */   public TreeMap<Integer, TWarn> list = new TreeMap();
/*    */   
/* 40 */   public static final Type TYPE = new TypeToken() {}.getType();
/* 41 */   public static final char sp = File.separatorChar;
/*    */   
/*    */   public static Warn load(String name)
/*    */   {
/* 45 */     File file = new File(Main.CM.config.getSource() + sp + name + sp + "MemBan" + sp + "warn.json");
/*    */     try
/*    */     {
/* 48 */       if (file.createNewFile()) {
/* 49 */         return new Warn();
/*    */       }
/*    */       
/* 52 */       Reader reader = new FileReader(file);
/* 53 */       Warn warn = (Warn)Main.gson.fromJson(reader, TYPE);
/* 54 */       reader.close();
/* 55 */       return warn;
/*    */     }
/*    */     catch (IOException e) {
/* 58 */       Main.logger.warning("Warn file '" + name + "' not found"); }
/* 59 */     return null;
/*    */   }
/*    */   
/*    */   public void set(String reason, String byPlayer, String server) {
/*    */     int id;
/*    */     int id;
/* 65 */     if (this.list.size() != 0) id = ((Integer)this.list.lastKey()).intValue(); else {
/* 66 */       id = 0;
/*    */     }
/* 68 */     this.list.put(Integer.valueOf(++id), new TWarn(reason, byPlayer, server));
/*    */   }
/*    */   
/*    */   public void save(String name) {
/* 72 */     File file = new File(Main.CM.config.getSource() + sp + name + sp + "MemBan" + sp + "warn.json");
/*    */     try
/*    */     {
/* 75 */       Writer writer = new FileWriter(file);
/* 76 */       Main.gson.toJson(this, writer);
/* 77 */       writer.close();
/*    */     } catch (IOException e) {
/* 79 */       e.getMessage();
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\somepeople\memban\data\Warn.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */