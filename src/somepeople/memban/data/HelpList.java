/*    */ package somepeople.memban.data;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class HelpList
/*    */ {
/*  8 */   static final String HEADER_1 = Messages.line + "\nСписок команд плагина MemBan. Страница ";
/*    */   static final String HEADER_2 = " из 3";
/* 10 */   static final String FOOTER = "\n" + Messages.line;
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   static final String HELP_1 = "\n/sban <игрок> <причина> - бан игрока с причиной или без\n/sbanip <игрок> <причина> - бан ip-адреса игрока\n/stimeban <игрок> <причина> time <время>- бан игрока на время, время в формате Y M D h m s\n/baninfo <игрок> - информация о бане игрока\n/sunban <игрок> - разбан игрока\n/smute <игрок> <причина> time <время> - mute игрока навсегда или на время\n/muteinfo <игрок> - информация о mute игрока";
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   static final String HELP_2 = "\n/sunmute <игрок> - разблокировка чата игрока\n/kick <игрок> <причина> - кик игрока\n/warn <игрок> <предупреждение> - выдать предупреждение игроку\n/memban stat <игрок> - статистика банов, mute, report, warn\n/memban reload - перезагрузка файлов плагина";
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public static String getHelp(String[] args)
/*    */   {
/* 28 */     if ((args.length == 0) || (args[0].equals("1"))) return getFromPart("1", "\n/sban <игрок> <причина> - бан игрока с причиной или без\n/sbanip <игрок> <причина> - бан ip-адреса игрока\n/stimeban <игрок> <причина> time <время>- бан игрока на время, время в формате Y M D h m s\n/baninfo <игрок> - информация о бане игрока\n/sunban <игрок> - разбан игрока\n/smute <игрок> <причина> time <время> - mute игрока навсегда или на время\n/muteinfo <игрок> - информация о mute игрока");
/* 29 */     if (args[0].equals("2")) return getFromPart("2", "\n/sunmute <игрок> - разблокировка чата игрока\n/kick <игрок> <причина> - кик игрока\n/warn <игрок> <предупреждение> - выдать предупреждение игроку\n/memban stat <игрок> - статистика банов, mute, report, warn\n/memban reload - перезагрузка файлов плагина");
/* 30 */     return getFromPart("1", "\n/sban <игрок> <причина> - бан игрока с причиной или без\n/sbanip <игрок> <причина> - бан ip-адреса игрока\n/stimeban <игрок> <причина> time <время>- бан игрока на время, время в формате Y M D h m s\n/baninfo <игрок> - информация о бане игрока\n/sunban <игрок> - разбан игрока\n/smute <игрок> <причина> time <время> - mute игрока навсегда или на время\n/muteinfo <игрок> - информация о mute игрока");
/*    */   }
/*    */   
/*    */   private static String getFromPart(String index, String part) {
/* 34 */     return HEADER_1 + index + " из 3" + part + FOOTER;
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\somepeople\memban\data\HelpList.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */