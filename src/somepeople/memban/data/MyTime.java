/*    */ package somepeople.memban.data;
/*    */ 
/*    */ import org.joda.time.Period;
/*    */ import org.joda.time.format.DateTimeFormat;
/*    */ import org.joda.time.format.DateTimeFormatter;
/*    */ import org.joda.time.format.PeriodFormatter;
/*    */ import org.joda.time.format.PeriodFormatterBuilder;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class MyTime
/*    */ {
/* 14 */   public static final PeriodFormatter FORMAT_PERIOD = new PeriodFormatterBuilder().printZeroAlways().minimumPrintedDigits(4).appendYears()
/* 15 */     .appendSuffix("-").minimumPrintedDigits(2).appendMonths().appendSuffix("-").appendDays()
/* 16 */     .appendSuffix(" ").appendHours().appendSuffix(":").appendMinutes().appendSuffix(":").appendSeconds()
/* 17 */     .appendSuffix("").toFormatter();
/* 18 */   public static final DateTimeFormatter FORMAT_DATE = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
/*    */   
/*    */   public static String convertTime(String time) {
/* 21 */     String[] args = time.split(" ");
/* 22 */     Period p = new Period();
/*    */     try {
/* 24 */       for (String item : args) {
/* 25 */         switch (item.charAt(item.length() - 1)) {
/*    */         case 'Y': 
/* 27 */           item = item.substring(0, item.length() - 1);
/* 28 */           p = p.plusYears(Integer.parseInt(item));
/* 29 */           break;
/*    */         case 'M': 
/* 31 */           item = item.substring(0, item.length() - 1);
/* 32 */           p = p.plusMonths(Integer.parseInt(item));
/* 33 */           break;
/*    */         case 'D': 
/* 35 */           item = item.substring(0, item.length() - 1);
/* 36 */           p = p.plusDays(Integer.parseInt(item));
/* 37 */           break;
/*    */         case 'h': 
/* 39 */           item = item.substring(0, item.length() - 1);
/* 40 */           p = p.plusHours(Integer.parseInt(item));
/* 41 */           break;
/*    */         case 'm': 
/* 43 */           item = item.substring(0, item.length() - 1);
/* 44 */           p = p.plusMinutes(Integer.parseInt(item));
/* 45 */           break;
/*    */         case 's': 
/* 47 */           item = item.substring(0, item.length() - 1);
/* 48 */           p = p.plusSeconds(Integer.parseInt(item));
/*    */         }
/*    */         
/*    */       }
/* 52 */       return FORMAT_PERIOD.print(p);
/*    */     } catch (IllegalArgumentException e) {}
/* 54 */     return null;
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\somepeople\memban\data\MyTime.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */