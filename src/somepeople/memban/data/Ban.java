/*    */ package somepeople.memban.data;
/*    */ 
/*    */ import com.google.gson.Gson;
/*    */ import com.google.gson.reflect.TypeToken;
/*    */ import java.io.File;
/*    */ import java.io.FileReader;
/*    */ import java.io.FileWriter;
/*    */ import java.io.IOException;
/*    */ import java.io.Reader;
/*    */ import java.io.Writer;
/*    */ import java.lang.reflect.Type;
/*    */ import java.util.logging.Logger;
/*    */ import net.md_5.bungee.api.ChatColor;
/*    */ import org.joda.time.DateTime;
/*    */ import org.joda.time.Interval;
/*    */ import org.joda.time.Period;
/*    */ import org.joda.time.format.DateTimeFormatter;
/*    */ import org.joda.time.format.PeriodFormatter;
/*    */ import somepeople.memban.main.CM;
/*    */ import somepeople.memban.main.Main;
/*    */ 
/*    */ 
/*    */ public class Ban
/*    */ {
/*    */   public String reason;
/*    */   public String byPlayer;
/*    */   public String date;
/*    */   public String enddate;
/*    */   public String value;
/* 30 */   public static final Type TYPE = new TypeToken() {}.getType();
/* 31 */   public static final char sp = File.separatorChar;
/*    */   
/*    */   public Ban(String reason, String byPlayer, String value) {
/* 34 */     DateTime now = DateTime.now();
/*    */     
/* 36 */     this.reason = reason;
/* 37 */     this.byPlayer = byPlayer;
/* 38 */     this.date = MyTime.FORMAT_DATE.print(now);
/* 39 */     if (value.equals("")) this.enddate = ""; else
/* 40 */       this.enddate = MyTime.FORMAT_DATE.print(now.plus(Period.parse(value, MyTime.FORMAT_PERIOD)));
/* 41 */     this.value = value;
/*    */   }
/*    */   
/*    */   public String getEndDate() {
/* 45 */     if (!this.enddate.equals(""))
/*    */     {
/* 47 */       DateTime now = new DateTime();
/* 48 */       DateTime unbanDate = DateTime.parse(this.enddate, MyTime.FORMAT_DATE);
/*    */       
/* 50 */       Interval intervaldate = new Interval(now, unbanDate);
/*    */       
/* 52 */       return MyTime.FORMAT_PERIOD.print(intervaldate.toPeriod());
/*    */     }
/* 54 */     return "permanent";
/*    */   }
/*    */   
/*    */   public static void saveNull(String name) {
/* 58 */     File file = new File(Main.CM.config.getSource() + sp + name + sp + "MemBan" + sp + "ban.json");
/*    */     try
/*    */     {
/* 61 */       Writer writer = new FileWriter(file);
/* 62 */       Main.gson.toJson(null, writer);
/* 63 */       writer.close();
/*    */     } catch (IOException e) {
/* 65 */       e.getMessage();
/*    */     }
/*    */   }
/*    */   
/*    */   public static Ban load(String name)
/*    */   {
/* 71 */     File file = new File(Main.CM.config.getSource() + sp + name + sp + "MemBan" + sp + "ban.json");
/*    */     try
/*    */     {
/* 74 */       Reader reader = new FileReader(file);
/* 75 */       Ban ban = (Ban)Main.gson.fromJson(reader, TYPE);
/* 76 */       reader.close();
/* 77 */       return ban;
/*    */     } catch (IOException e) {
/* 79 */       e.getMessage();
/* 80 */       Main.logger.warning(ChatColor.RED + "Ban file '" + name + "' not found"); }
/* 81 */     return null;
/*    */   }
/*    */   
/*    */   public void save(String name)
/*    */   {
/* 86 */     File file = new File(Main.CM.config.getSource() + sp + name + sp + "MemBan" + sp + "ban.json");
/*    */     try
/*    */     {
/* 89 */       Writer writer = new FileWriter(file);
/* 90 */       Main.gson.toJson(this, writer);
/* 91 */       writer.close();
/*    */     } catch (IOException e) {
/* 93 */       e.getMessage();
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\somepeople\memban\data\Ban.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */