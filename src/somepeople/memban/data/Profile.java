/*    */ package somepeople.memban.data;
/*    */ 
/*    */ import com.google.gson.Gson;
/*    */ import java.io.File;
/*    */ import java.io.FileWriter;
/*    */ import java.io.IOException;
/*    */ import java.io.Reader;
/*    */ import java.io.Writer;
/*    */ import java.lang.reflect.Type;
/*    */ import somepeople.memban.main.CM;
/*    */ import somepeople.memban.main.Main;
/*    */ 
/*    */ public class Profile
/*    */ {
/* 15 */   public static final Type TYPE = new com.google.gson.reflect.TypeToken() {}.getType();
/* 16 */   public static final char sp = File.separatorChar;
/*    */   
/* 18 */   public int countBan = 0;
/* 19 */   public int countMute = 0;
/* 20 */   public int countWarn = 0;
/*    */   
/*    */   public static Profile get(String name) {
/* 23 */     File file = new File(Main.CM.config.getSource() + sp + name + sp + "MemBan" + sp + "profile.json");
/*    */     try
/*    */     {
/* 26 */       if (file.createNewFile()) {
/* 27 */         Profile profile = new Profile();
/* 28 */         Writer writer = new FileWriter(file);
/* 29 */         Main.gson.toJson(profile, writer);
/* 30 */         writer.close();
/* 31 */         return profile;
/*    */       }
/* 33 */       Reader reader = new java.io.FileReader(file);
/* 34 */       Profile profile = (Profile)Main.gson.fromJson(reader, TYPE);
/* 35 */       reader.close();
/* 36 */       return profile;
/*    */     }
/*    */     catch (IOException e) {
/* 39 */       e.getMessage(); }
/* 40 */     return null;
/*    */   }
/*    */   
/*    */   public void save(String name)
/*    */   {
/* 45 */     File file = new File(Main.CM.config.getSource() + sp + name + sp + "MemBan" + sp + "profile.json");
/*    */     try
/*    */     {
/* 48 */       Writer writer = new FileWriter(file);
/* 49 */       Main.gson.toJson(this, writer);
/* 50 */       writer.close();
/*    */     } catch (IOException e) {
/* 52 */       e.getMessage();
/*    */     }
/*    */   }
/*    */   
/* 56 */   public void addBan() { this.countBan += 1; }
/*    */   
/* 58 */   public void addMute() { this.countMute += 1; }
/*    */   
/* 60 */   public void addWarn() { this.countWarn += 1; }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\somepeople\memban\data\Profile.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */