/*     */ package somepeople.memban.data;
/*     */ 
/*     */ import com.google.gson.Gson;
/*     */ import java.io.File;
/*     */ import java.io.IOException;
/*     */ import java.io.Reader;
/*     */ import java.io.Writer;
/*     */ import java.util.TreeMap;
/*     */ import java.util.logging.Logger;
/*     */ import net.md_5.bungee.api.ChatColor;
/*     */ import net.md_5.bungee.api.ProxyServer;
/*     */ import net.md_5.bungee.api.connection.ProxiedPlayer;
/*     */ import net.md_5.bungee.api.connection.Server;
/*     */ import somepeople.memban.main.Main;
/*     */ 
/*     */ public class Messages
/*     */ {
/*  18 */   public static final String line = ChatColor.WHITE + "------------------------------------------------------------------------";
/*     */   
/*     */   static TreeMap<String, String> config;
/*  21 */   public final java.lang.reflect.Type TYPE = new com.google.gson.reflect.TypeToken() {}.getType();
/*     */   
/*     */   static String message;
/*     */   
/*  25 */   public static final String NO_PEX = ChatColor.RED + "[MemBan] У Вас нет на это прав!";
/*  26 */   public static final String INVALID = ChatColor.RED + "[MemBan] Неверная команда. Используйте /banhelp или /memhelp для справки";
/*  27 */   public static final String OFFLINE = ChatColor.RED + "[MemBan] Этот игрок не в сети!";
/*     */   
/*     */   public Messages() {
/*  30 */     File file = new File("plugins/MemBan/messages.json");
/*     */     
/*  32 */     file.getParentFile().mkdirs();
/*     */     try
/*     */     {
/*  35 */       if (file.createNewFile()) {
/*  36 */         config = setMessageConfig();
/*  37 */         Writer writer = new java.io.FileWriter(file);
/*  38 */         Main.gson.toJson(config, writer);
/*  39 */         writer.close();
/*     */       } else {
/*  41 */         Reader reader = new java.io.FileReader(file);
/*  42 */         config = (TreeMap)Main.gson.fromJson(reader, this.TYPE);
/*  43 */         reader.close();
/*     */       }
/*  45 */       Main.logger.info("Messages file loaded");
/*     */     } catch (IOException e) {
/*  47 */       Main.logger.warning("Messages file not file");
/*     */     }
/*     */   }
/*     */   
/*     */   public void broadcast(ProxiedPlayer target, String msg) {
/*  52 */     for (ProxiedPlayer item : ProxyServer.getInstance().getPlayers()) {
/*  53 */       if (item.getServer().getAddress() == target.getServer().getAddress()) item.sendMessage(new net.md_5.bungee.api.chat.TextComponent(msg));
/*     */     }
/*     */   }
/*     */   
/*     */   public String infoBan(String player, String byplayer, String date, String enddate, String reason) {
/*  58 */     return 
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  63 */       ((String)config.get("info-ban")).replaceAll("%player%", player).replaceAll("%byplayer%", byplayer).replaceAll("%date%", date).replaceAll("%enddate%", enddate).replaceAll("%reason%", reason);
/*     */   }
/*     */   
/*     */   public String infoMute(String player, String byplayer, String date, String enddate, String reason) {
/*  67 */     return 
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  72 */       ((String)config.get("info-mute")).replaceAll("%player%", player).replaceAll("%byplayer%", byplayer).replaceAll("%date%", date).replaceAll("%enddate%", enddate).replaceAll("%reason%", reason);
/*     */   }
/*     */   
/*     */   public String playerBan(String byplayer, String reason, String date, String value) {
/*  76 */     return 
/*     */     
/*     */ 
/*     */ 
/*  80 */       ((String)config.get("player-ban")).replaceAll("%byplayer%", byplayer + ChatColor.RED).replaceAll("%date%", date).replaceAll("%value%", value).replaceAll("%reason%", reason);
/*     */   }
/*     */   
/*     */   public String playerBanIp() {
/*  84 */     return (String)config.get("player-banip");
/*     */   }
/*     */   
/*     */   public String playerKick(String byplayer, String reason) {
/*  88 */     return 
/*     */     
/*  90 */       ((String)config.get("player-kick")).replaceAll("%byplayer%", byplayer).replaceAll("%reason%", reason);
/*     */   }
/*     */   
/*     */   public String playerMuteForever(String byplayer, String reason) {
/*  94 */     return 
/*     */     
/*  96 */       ((String)config.get("player-mute-forever")).replaceAll("%byplayer%", byplayer).replaceAll("%reason%", reason);
/*     */   }
/*     */   
/*     */   public String playerMuteTime(String byplayer, String reason, String value) {
/* 100 */     return 
/*     */     
/*     */ 
/* 103 */       ((String)config.get("player-mute-time")).replaceAll("%byplayer%", byplayer).replaceAll("%reason%", reason).replaceAll("%value%", value);
/*     */   }
/*     */   
/*     */   public String playerUnMute() {
/* 107 */     return (String)config.get("player-unmute");
/*     */   }
/*     */   
/*     */   public String serverBan2min(String player, String byplayer) {
/* 111 */     return 
/*     */     
/* 113 */       ((String)config.get("server-ban-2min")).replaceAll("%player%", player).replaceAll("%byplayer%", byplayer);
/*     */   }
/*     */   
/*     */   public String serverBanForever(String player, String byplayer, String reason) {
/* 117 */     return 
/*     */     
/*     */ 
/* 120 */       ((String)config.get("server-ban-forever")).replaceAll("%player%", player).replaceAll("%byplayer%", byplayer).replaceAll("%reason%", reason);
/*     */   }
/*     */   
/*     */   public String serverBanTime(String player, String byplayer, String value, String reason) {
/* 124 */     return 
/*     */     
/*     */ 
/*     */ 
/* 128 */       ((String)config.get("server-ban-time")).replaceAll("%player%", player).replaceAll("%byplayer%", byplayer).replaceAll("%reason%", reason).replaceAll("%value%", value);
/*     */   }
/*     */   
/*     */   public String serverKick(String player, String byplayer, String reason) {
/* 132 */     return 
/*     */     
/*     */ 
/* 135 */       ((String)config.get("server-kick")).replaceAll("%player%", player).replaceAll("%byplayer%", byplayer).replaceAll("%reason%", reason);
/*     */   }
/*     */   
/*     */   public String serverMuteForever(String player, String byplayer, String reason) {
/* 139 */     return 
/*     */     
/*     */ 
/* 142 */       ((String)config.get("server-mute-forever")).replaceAll("%player%", player).replaceAll("%byplayer%", byplayer).replaceAll("%reason%", reason);
/*     */   }
/*     */   
/*     */   public String serverMuteTime(String player, String byplayer, String value, String reason) {
/* 146 */     return 
/*     */     
/*     */ 
/*     */ 
/* 150 */       ((String)config.get("server-mute-time")).replaceAll("%player%", player).replaceAll("%byplayer%", byplayer).replaceAll("%value%", value).replaceAll("%reason%", reason);
/*     */   }
/*     */   
/*     */   public String serverUnBan(String player, String byplayer) {
/* 154 */     return 
/*     */     
/* 156 */       ((String)config.get("server-unban")).replaceAll("%player%", player).replaceAll("%byplayer%", byplayer);
/*     */   }
/*     */   
/*     */   public String serverUnMute(String player, String byplayer) {
/* 160 */     return 
/*     */     
/* 162 */       ((String)config.get("server-unmute")).replaceAll("%player%", player).replaceAll("%byplayer%", byplayer);
/*     */   }
/*     */   
/*     */   public String serverWarn(String player, String byplayer, String reason) {
/* 166 */     return 
/*     */     
/*     */ 
/* 169 */       ((String)config.get("server-warn")).replaceAll("%player%", player).replaceAll("%byplayer%", byplayer).replaceAll("%reason%", reason);
/*     */   }
/*     */   
/*     */   public static TreeMap<String, String> setMessageConfig() {
/* 173 */     TreeMap<String, String> config = new TreeMap();
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 180 */     config.put("player-ban", "§cВы были забанены на сервере\nБан был выдан %byplayer%\nПричина бана %reason%\nБан выл выдан в %date%\nРазбан будет через %value%\n----------------------------------\n§aЕсли вы не согласны с данным баном или же вас забанили просо так\nВы можете подать §cапелляцию §aв нашем форуме на сайте memasgold.ru\nв случае если вам на форуме не выдали разбан и объяснили за ваше нарушение подробнее\nвозможно в качестве штрафа вы получите +3 дня\nС уважением\n(C) Memas Gold");
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 188 */     config.put("player-banip", "Ваш IP-адрес забанен");
/*     */     
/*     */ 
/* 191 */     config.put("player-kick", "Вас кикнули на сервере\nKick был выдан %byplayer%\nПричина Kick %reason%");
/*     */     
/*     */ 
/*     */ 
/* 195 */     config.put("player-mute-forever", "§c[Memas§6Gold] §cВам был выдан Mute игроком §e%byplayer% за §e%reason%");
/* 196 */     config.put("player-mute-time", "§c[Memas§6Gold] §cВам был выдан Mute игроком §e%byplayer% §cна §e%value% за §e%reason%");
/*     */     
/* 198 */     config.put("player-unmute", "§c[Memas§6Gold] §cВаш чат разблокирован");
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 205 */     config.put("server-kick", "§c[Memas§6Gold] §cИгрок §e%player% §cбыл кикнут с сервера за §e%reason%");
/*     */     
/*     */ 
/*     */ 
/* 209 */     config.put("server-ban-forever", "§c[Memas§6Gold] §cИгрок §e%player% §cбыл забанен игроком §e%byplayer% §cза §e%reason%");
/*     */     
/* 211 */     config.put("server-ban-2min", "§c[Memas§6Gold] §cИгрок §e%player% §cбыл забанен игроком §e%byplayer% §cна §e2 минуты");
/*     */     
/* 213 */     config.put("server-ban-time", "§c[Memas§6Gold] §cИгрок §e%player% §cбыл забанен игроком §e%byplayer% §cна §e%value% §cза §e%reason%");
/*     */     
/* 215 */     config.put("server-unban", "§c[Memas§6Gold] §cИгрок §e%player% §cбыл разбанен");
/*     */     
/*     */ 
/* 218 */     config.put("server-mute-forever", "§c[Memas§6Gold] §cИгроку §e%player% §cбыл был выдан Mute за §e%reason%");
/* 219 */     config.put("server-mute-time", "§c[Memas§6Gold] §cИгроку §e%player% §cбыл был выдан Mute на §e%value% §cза §e%reason%");
/*     */     
/* 221 */     config.put("server-unmute", "§c[Memas§6Gold] §cЧат игрока §e%player% §cразблокирован");
/*     */     
/*     */ 
/*     */ 
/* 225 */     config.put("server-warn", "§c[Memas§6Gold] §cИгроку §e%player% §cбыло выдано предупреждение от §e%byplayer% §cза §e%reason%");
/*     */     
/*     */ 
/* 228 */     config.put("info-ban", "§b%player%\nБан выдал %byplayer%\nБан был выдан %date%\nразбанен будет %enddate%\nПричина бана %reason%");
/*     */     
/* 230 */     config.put("info-mute", "§b%player%\nMute выдал %byplayer%\nMute был выдан %date%\nразблокировка будет %enddate%\nПричина Mute %reason%");
/*     */     
/*     */ 
/* 233 */     return config;
/*     */   }
/*     */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\somepeople\memban\data\Messages.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */