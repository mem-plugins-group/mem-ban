/*    */ package somepeople.memban.data;
/*    */ 
/*    */ import com.google.gson.Gson;
/*    */ import java.io.File;
/*    */ import java.io.IOException;
/*    */ import java.io.Reader;
/*    */ import java.io.Writer;
/*    */ import java.util.HashMap;
/*    */ import java.util.logging.Logger;
/*    */ import somepeople.memban.main.Main;
/*    */ 
/*    */ public class Config
/*    */ {
/*    */   private HashMap<String, String> CONFIG;
/* 15 */   public final java.lang.reflect.Type TYPE = new com.google.gson.reflect.TypeToken() {}.getType();
/*    */   
/*    */   public Config() {
/* 18 */     File file = new File("plugins/MemBan/config.json");
/*    */     
/* 20 */     file.getParentFile().mkdirs();
/*    */     try
/*    */     {
/* 23 */       if (file.createNewFile()) {
/* 24 */         this.CONFIG = setConfig();
/* 25 */         Writer writer = new java.io.FileWriter(file);
/* 26 */         Main.gson.toJson(this.CONFIG, writer);
/* 27 */         writer.close();
/*    */       } else {
/* 29 */         Reader reader = new java.io.FileReader(file);
/* 30 */         this.CONFIG = ((HashMap)Main.gson.fromJson(reader, this.TYPE));
/* 31 */         reader.close();
/*    */       }
/* 33 */       Main.logger.info("Config file loaded");
/*    */     } catch (IOException e) {
/* 35 */       Main.logger.warning("Config file not file");
/*    */     }
/*    */   }
/*    */   
/*    */   static HashMap<String, String> setConfig() {
/* 40 */     HashMap<String, String> config = new HashMap(1);
/*    */     
/* 42 */     config.put("source", "d:/players");
/*    */     
/* 44 */     return config;
/*    */   }
/*    */   
/*    */   public String getSource() {
/* 48 */     return (String)this.CONFIG.get("source");
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\somepeople\memban\data\Config.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */