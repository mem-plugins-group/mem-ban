/*    */ package somepeople.memban.data;
/*    */ 
/*    */ import com.google.gson.Gson;
/*    */ import com.google.gson.reflect.TypeToken;
/*    */ import java.io.File;
/*    */ import java.io.FileReader;
/*    */ import java.io.FileWriter;
/*    */ import java.io.IOException;
/*    */ import java.io.Reader;
/*    */ import java.io.Writer;
/*    */ import java.lang.reflect.Type;
/*    */ import java.util.HashMap;
/*    */ import java.util.logging.Logger;
/*    */ import somepeople.memban.main.Main;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class BanIp
/*    */ {
/* 31 */   public static final Type TYPE = new TypeToken() {}.getType();
/*    */   public HashMap<String, TIp> list;
/*    */   
/*    */   public BanIp() {
/* 35 */     load();
/*    */   }
/*    */   
/*    */   public void add(String ip, String reason, String byPlayer) {
/* 39 */     this.list.put(ip, new TIp(reason, byPlayer));
/* 40 */     save();
/*    */   }
/*    */   
/*    */   public void load() {
/* 44 */     File file = new File("plugins/MemBan/banip.json");
/*    */     try
/*    */     {
/* 47 */       if (file.createNewFile()) {
/* 48 */         this.list = new HashMap(0);
/*    */       } else {
/* 50 */         Reader reader = new FileReader(file);
/* 51 */         this.list = ((HashMap)Main.gson.fromJson(reader, TYPE));
/* 52 */         reader.close();
/* 53 */         if (this.list == null) this.list = new HashMap(0);
/*    */       }
/* 55 */       Main.logger.info("BanIp file loaded");
/*    */     } catch (IOException e) {
/* 57 */       Main.logger.warning("BanIp file not found");
/*    */     }
/*    */   }
/*    */   
/*    */   public void save() {
/*    */     try {
/* 63 */       Writer writer = new FileWriter(new File("plugins/MemBan/banip.json"));
/* 64 */       Main.gson.toJson(this.list, writer);
/* 65 */       writer.close();
/*    */     } catch (IOException e) {
/* 67 */       e.getMessage();
/*    */     }
/*    */   }
/*    */   
/*    */   public boolean isBanned(String ip) {
/* 72 */     return this.list.containsKey(ip);
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\somepeople\memban\data\BanIp.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */