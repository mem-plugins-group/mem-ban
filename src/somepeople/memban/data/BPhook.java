/*    */ package somepeople.memban.data;
/*    */ 
/*    */ import net.alpenblock.bungeeperms.BungeePerms;
/*    */ import net.alpenblock.bungeeperms.PermissionsManager;
/*    */ import net.alpenblock.bungeeperms.User;
/*    */ import net.md_5.bungee.api.ChatColor;
/*    */ import net.md_5.bungee.api.ProxyServer;
/*    */ 
/*    */ public class BPhook
/*    */ {
/*    */   public static BungeePerms bp_hook;
/*    */   
/*    */   public static String getPrefix(String name)
/*    */   {
/* 15 */     bp_hook = BungeePerms.getInstance();
/*    */     
/* 17 */     if (bp_hook == null) {
/* 18 */       ProxyServer.getInstance().getLogger().warning(ChatColor.RED + "ERROR > Error while trying to gain data from BungeePerms");
/* 19 */       return "";
/*    */     }
/* 21 */     PermissionsManager pm = bp_hook.getPermissionsManager();
/* 22 */     User user = pm.getUser(name);
/* 23 */     if (user == null) return "§e";
/* 24 */     return user.buildPrefix().replaceAll("&", "§") + name;
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\somepeople\memban\data\BPhook.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */