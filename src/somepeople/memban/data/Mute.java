/*     */ package somepeople.memban.data;
/*     */ 
/*     */ import com.google.gson.Gson;
/*     */ import com.google.gson.reflect.TypeToken;
/*     */ import java.io.File;
/*     */ import java.io.FileReader;
/*     */ import java.io.FileWriter;
/*     */ import java.io.IOException;
/*     */ import java.io.Reader;
/*     */ import java.io.Writer;
/*     */ import java.lang.reflect.Type;
/*     */ import java.util.logging.Logger;
/*     */ import net.md_5.bungee.api.ChatColor;
/*     */ import org.joda.time.DateTime;
/*     */ import org.joda.time.Interval;
/*     */ import org.joda.time.Period;
/*     */ import org.joda.time.format.DateTimeFormatter;
/*     */ import org.joda.time.format.PeriodFormatter;
/*     */ import somepeople.memban.main.CM;
/*     */ import somepeople.memban.main.Main;
/*     */ 
/*     */ 
/*     */ public class Mute
/*     */ {
/*     */   public String reason;
/*     */   public String byPlayer;
/*     */   public String date;
/*     */   public String enddate;
/*     */   public String value;
/*  30 */   public static final Type TYPE = new TypeToken() {}.getType();
/*  31 */   public static final char sp = File.separatorChar;
/*     */   
/*     */   public Mute(String reason, String byPlayer, String value)
/*     */   {
/*  35 */     DateTime now = DateTime.now();
/*     */     
/*  37 */     this.reason = reason;
/*  38 */     this.byPlayer = byPlayer;
/*  39 */     this.date = MyTime.FORMAT_DATE.print(now);
/*  40 */     if (value.equals("")) this.enddate = ""; else
/*  41 */       this.enddate = MyTime.FORMAT_DATE.print(now.plus(Period.parse(value, MyTime.FORMAT_PERIOD)));
/*  42 */     this.value = value;
/*     */   }
/*     */   
/*     */   public String getEndDate()
/*     */   {
/*  47 */     if (!this.enddate.equals(""))
/*     */     {
/*  49 */       DateTime now = new DateTime();
/*  50 */       DateTime unbanDate = DateTime.parse(this.enddate, MyTime.FORMAT_DATE);
/*     */       
/*  52 */       Interval intervaldate = new Interval(now, unbanDate);
/*     */       
/*  54 */       return MyTime.FORMAT_PERIOD.print(intervaldate.toPeriod());
/*     */     }
/*  56 */     return "permanent";
/*     */   }
/*     */   
/*     */   public static void saveNull(String name) {
/*  60 */     File file = new File(Main.CM.config.getSource() + sp + name + sp + "MemBan" + sp + "mute.json");
/*     */     try
/*     */     {
/*  63 */       Writer writer = new FileWriter(file);
/*  64 */       Main.gson.toJson(null, writer);
/*  65 */       writer.close();
/*     */     } catch (IOException e) {
/*  67 */       e.getMessage();
/*     */     }
/*     */   }
/*     */   
/*     */   public static Mute load(String name)
/*     */   {
/*  73 */     File file = new File(Main.CM.config.getSource() + sp + name + sp + "MemBan" + sp + "mute.json");
/*     */     try
/*     */     {
/*  76 */       Reader reader = new FileReader(file);
/*  77 */       Mute mute = (Mute)Main.gson.fromJson(reader, TYPE);
/*  78 */       reader.close();
/*  79 */       return mute;
/*     */     } catch (IOException e) {
/*  81 */       e.getMessage();
/*  82 */       Main.logger.warning(ChatColor.RED + "Mute file '" + name + "' not found"); }
/*  83 */     return null;
/*     */   }
/*     */   
/*     */   public void save(String name)
/*     */   {
/*  88 */     File file = new File(Main.CM.config.getSource() + sp + name + sp + "MemBan" + sp + "mute.json");
/*     */     try
/*     */     {
/*  91 */       Writer writer = new FileWriter(file);
/*  92 */       Main.gson.toJson(this, writer);
/*  93 */       writer.close();
/*     */     } catch (IOException e) {
/*  95 */       e.getMessage();
/*     */     }
/*     */   }
/*     */   
/*     */   public void set(String reason, String byPlayer, String value)
/*     */   {
/* 101 */     DateTime now = DateTime.now();
/*     */     
/* 103 */     this.reason = reason;
/* 104 */     this.byPlayer = byPlayer;
/* 105 */     this.date = MyTime.FORMAT_DATE.print(now);
/* 106 */     if (value.equals("")) this.enddate = ""; else
/* 107 */       this.enddate = MyTime.FORMAT_DATE.print(now.plus(Period.parse(value, MyTime.FORMAT_PERIOD)));
/* 108 */     this.value = value;
/*     */   }
/*     */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\somepeople\memban\data\Mute.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */