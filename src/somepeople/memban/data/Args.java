/*    */ package somepeople.memban.data;
/*    */ 
/*    */ import net.md_5.bungee.api.CommandSender;
/*    */ import net.md_5.bungee.api.config.ServerInfo;
/*    */ import net.md_5.bungee.api.connection.ProxiedPlayer;
/*    */ import net.md_5.bungee.api.connection.Server;
/*    */ 
/*    */ public class Args
/*    */ {
/*    */   public static String getReason(String[] args)
/*    */   {
/* 12 */     String reason = "";
/*    */     
/* 14 */     for (int i = 1; i < args.length; i++) {
/* 15 */       if (args[i].equalsIgnoreCase("time")) break;
/* 16 */       reason = reason + args[i] + " ";
/*    */     }
/*    */     
/* 19 */     return reason.trim().toLowerCase();
/*    */   }
/*    */   
/*    */   public static String getTime(String[] args) {
/* 23 */     String time = "";
/* 24 */     boolean find = false;
/* 25 */     for (String item : args) {
/* 26 */       if (item.equalsIgnoreCase("time")) {
/* 27 */         find = true;
/*    */ 
/*    */ 
/*    */       }
/* 31 */       else if (find) { time = time + item + " ";
/*    */       }
/*    */     }
/* 34 */     return time.trim();
/*    */   }
/*    */   
/*    */   public static String getPrefix(CommandSender sender) {
/* 38 */     if ((sender instanceof ProxiedPlayer)) return BPhook.getPrefix(sender.getName());
/* 39 */     return "Сервер";
/*    */   }
/*    */   
/*    */   public static String getServerName(CommandSender sender) {
/* 43 */     if ((sender instanceof ProxiedPlayer)) return ((ProxiedPlayer)sender).getServer().getInfo().getName();
/* 44 */     return "Сервер";
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\somepeople\memban\data\Args.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */