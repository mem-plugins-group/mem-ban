package org.joda.time.base;

public abstract class BaseLocal
  extends AbstractPartial
{
  private static final long serialVersionUID = 276453175381783L;
  
  protected abstract long getLocalMillis();
}


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\org\joda\time\base\BaseLocal.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */