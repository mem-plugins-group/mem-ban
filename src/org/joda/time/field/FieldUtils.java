/*     */ package org.joda.time.field;
/*     */ 
/*     */ import java.math.BigDecimal;
/*     */ import java.math.RoundingMode;
/*     */ import org.joda.time.DateTimeField;
/*     */ import org.joda.time.DateTimeFieldType;
/*     */ import org.joda.time.IllegalFieldValueException;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class FieldUtils
/*     */ {
/*     */   public static int safeNegate(int paramInt)
/*     */   {
/*  52 */     if (paramInt == Integer.MIN_VALUE) {
/*  53 */       throw new ArithmeticException("Integer.MIN_VALUE cannot be negated");
/*     */     }
/*  55 */     return -paramInt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static int safeAdd(int paramInt1, int paramInt2)
/*     */   {
/*  67 */     int i = paramInt1 + paramInt2;
/*     */     
/*  69 */     if (((paramInt1 ^ i) < 0) && ((paramInt1 ^ paramInt2) >= 0)) {
/*  70 */       throw new ArithmeticException("The calculation caused an overflow: " + paramInt1 + " + " + paramInt2);
/*     */     }
/*     */     
/*  73 */     return i;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static long safeAdd(long paramLong1, long paramLong2)
/*     */   {
/*  85 */     long l = paramLong1 + paramLong2;
/*     */     
/*  87 */     if (((paramLong1 ^ l) < 0L) && ((paramLong1 ^ paramLong2) >= 0L)) {
/*  88 */       throw new ArithmeticException("The calculation caused an overflow: " + paramLong1 + " + " + paramLong2);
/*     */     }
/*     */     
/*  91 */     return l;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static long safeSubtract(long paramLong1, long paramLong2)
/*     */   {
/* 103 */     long l = paramLong1 - paramLong2;
/*     */     
/* 105 */     if (((paramLong1 ^ l) < 0L) && ((paramLong1 ^ paramLong2) < 0L)) {
/* 106 */       throw new ArithmeticException("The calculation caused an overflow: " + paramLong1 + " - " + paramLong2);
/*     */     }
/*     */     
/* 109 */     return l;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static int safeMultiply(int paramInt1, int paramInt2)
/*     */   {
/* 122 */     long l = paramInt1 * paramInt2;
/* 123 */     if ((l < -2147483648L) || (l > 2147483647L)) {
/* 124 */       throw new ArithmeticException("Multiplication overflows an int: " + paramInt1 + " * " + paramInt2);
/*     */     }
/* 126 */     return (int)l;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static long safeMultiply(long paramLong, int paramInt)
/*     */   {
/* 139 */     switch (paramInt) {
/*     */     case -1: 
/* 141 */       if (paramLong == Long.MIN_VALUE) {
/* 142 */         throw new ArithmeticException("Multiplication overflows a long: " + paramLong + " * " + paramInt);
/*     */       }
/* 144 */       return -paramLong;
/*     */     case 0: 
/* 146 */       return 0L;
/*     */     case 1: 
/* 148 */       return paramLong;
/*     */     }
/* 150 */     long l = paramLong * paramInt;
/* 151 */     if (l / paramInt != paramLong) {
/* 152 */       throw new ArithmeticException("Multiplication overflows a long: " + paramLong + " * " + paramInt);
/*     */     }
/* 154 */     return l;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static long safeMultiply(long paramLong1, long paramLong2)
/*     */   {
/* 166 */     if (paramLong2 == 1L) {
/* 167 */       return paramLong1;
/*     */     }
/* 169 */     if (paramLong1 == 1L) {
/* 170 */       return paramLong2;
/*     */     }
/* 172 */     if ((paramLong1 == 0L) || (paramLong2 == 0L)) {
/* 173 */       return 0L;
/*     */     }
/* 175 */     long l = paramLong1 * paramLong2;
/* 176 */     if ((l / paramLong2 != paramLong1) || ((paramLong1 == Long.MIN_VALUE) && (paramLong2 == -1L)) || ((paramLong2 == Long.MIN_VALUE) && (paramLong1 == -1L))) {
/* 177 */       throw new ArithmeticException("Multiplication overflows a long: " + paramLong1 + " * " + paramLong2);
/*     */     }
/* 179 */     return l;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static long safeDivide(long paramLong1, long paramLong2)
/*     */   {
/* 192 */     if ((paramLong1 == Long.MIN_VALUE) && (paramLong2 == -1L)) {
/* 193 */       throw new ArithmeticException("Multiplication overflows a long: " + paramLong1 + " / " + paramLong2);
/*     */     }
/* 195 */     return paramLong1 / paramLong2;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static long safeDivide(long paramLong1, long paramLong2, RoundingMode paramRoundingMode)
/*     */   {
/* 209 */     if ((paramLong1 == Long.MIN_VALUE) && (paramLong2 == -1L)) {
/* 210 */       throw new ArithmeticException("Multiplication overflows a long: " + paramLong1 + " / " + paramLong2);
/*     */     }
/*     */     
/* 213 */     BigDecimal localBigDecimal1 = new BigDecimal(paramLong1);
/* 214 */     BigDecimal localBigDecimal2 = new BigDecimal(paramLong2);
/* 215 */     return localBigDecimal1.divide(localBigDecimal2, paramRoundingMode).longValue();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static int safeToInt(long paramLong)
/*     */   {
/* 226 */     if ((-2147483648L <= paramLong) && (paramLong <= 2147483647L)) {
/* 227 */       return (int)paramLong;
/*     */     }
/* 229 */     throw new ArithmeticException("Value cannot fit in an int: " + paramLong);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static int safeMultiplyToInt(long paramLong1, long paramLong2)
/*     */   {
/* 241 */     long l = safeMultiply(paramLong1, paramLong2);
/* 242 */     return safeToInt(l);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void verifyValueBounds(DateTimeField paramDateTimeField, int paramInt1, int paramInt2, int paramInt3)
/*     */   {
/* 256 */     if ((paramInt1 < paramInt2) || (paramInt1 > paramInt3)) {
/* 257 */       throw new IllegalFieldValueException(paramDateTimeField.getType(), Integer.valueOf(paramInt1), Integer.valueOf(paramInt2), Integer.valueOf(paramInt3));
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void verifyValueBounds(DateTimeFieldType paramDateTimeFieldType, int paramInt1, int paramInt2, int paramInt3)
/*     */   {
/* 274 */     if ((paramInt1 < paramInt2) || (paramInt1 > paramInt3)) {
/* 275 */       throw new IllegalFieldValueException(paramDateTimeFieldType, Integer.valueOf(paramInt1), Integer.valueOf(paramInt2), Integer.valueOf(paramInt3));
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void verifyValueBounds(String paramString, int paramInt1, int paramInt2, int paramInt3)
/*     */   {
/* 291 */     if ((paramInt1 < paramInt2) || (paramInt1 > paramInt3)) {
/* 292 */       throw new IllegalFieldValueException(paramString, Integer.valueOf(paramInt1), Integer.valueOf(paramInt2), Integer.valueOf(paramInt3));
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static int getWrappedValue(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
/*     */   {
/* 315 */     return getWrappedValue(paramInt1 + paramInt2, paramInt3, paramInt4);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static int getWrappedValue(int paramInt1, int paramInt2, int paramInt3)
/*     */   {
/* 331 */     if (paramInt2 >= paramInt3) {
/* 332 */       throw new IllegalArgumentException("MIN > MAX");
/*     */     }
/*     */     
/* 335 */     int i = paramInt3 - paramInt2 + 1;
/* 336 */     paramInt1 -= paramInt2;
/*     */     
/* 338 */     if (paramInt1 >= 0) {
/* 339 */       return paramInt1 % i + paramInt2;
/*     */     }
/*     */     
/* 342 */     int j = -paramInt1 % i;
/*     */     
/* 344 */     if (j == 0) {
/* 345 */       return 0 + paramInt2;
/*     */     }
/* 347 */     return i - j + paramInt2;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean equals(Object paramObject1, Object paramObject2)
/*     */   {
/* 360 */     if (paramObject1 == paramObject2) {
/* 361 */       return true;
/*     */     }
/* 363 */     if ((paramObject1 == null) || (paramObject2 == null)) {
/* 364 */       return false;
/*     */     }
/* 366 */     return paramObject1.equals(paramObject2);
/*     */   }
/*     */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\org\joda\time\field\FieldUtils.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */