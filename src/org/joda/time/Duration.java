/*     */ package org.joda.time;
/*     */ 
/*     */ import java.io.Serializable;
/*     */ import java.math.RoundingMode;
/*     */ import org.joda.convert.FromString;
/*     */ import org.joda.time.base.BaseDuration;
/*     */ import org.joda.time.field.FieldUtils;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public final class Duration
/*     */   extends BaseDuration
/*     */   implements ReadableDuration, Serializable
/*     */ {
/*  45 */   public static final Duration ZERO = new Duration(0L);
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static final long serialVersionUID = 2471658376918L;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   @FromString
/*     */   public static Duration parse(String paramString)
/*     */   {
/*  61 */     return new Duration(paramString);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static Duration standardDays(long paramLong)
/*     */   {
/*  83 */     if (paramLong == 0L) {
/*  84 */       return ZERO;
/*     */     }
/*  86 */     return new Duration(FieldUtils.safeMultiply(paramLong, 86400000));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static Duration standardHours(long paramLong)
/*     */   {
/* 106 */     if (paramLong == 0L) {
/* 107 */       return ZERO;
/*     */     }
/* 109 */     return new Duration(FieldUtils.safeMultiply(paramLong, 3600000));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static Duration standardMinutes(long paramLong)
/*     */   {
/* 129 */     if (paramLong == 0L) {
/* 130 */       return ZERO;
/*     */     }
/* 132 */     return new Duration(FieldUtils.safeMultiply(paramLong, 60000));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static Duration standardSeconds(long paramLong)
/*     */   {
/* 151 */     if (paramLong == 0L) {
/* 152 */       return ZERO;
/*     */     }
/* 154 */     return new Duration(FieldUtils.safeMultiply(paramLong, 1000));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static Duration millis(long paramLong)
/*     */   {
/* 165 */     if (paramLong == 0L) {
/* 166 */       return ZERO;
/*     */     }
/* 168 */     return new Duration(paramLong);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Duration(long paramLong)
/*     */   {
/* 178 */     super(paramLong);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Duration(long paramLong1, long paramLong2)
/*     */   {
/* 189 */     super(paramLong1, paramLong2);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Duration(ReadableInstant paramReadableInstant1, ReadableInstant paramReadableInstant2)
/*     */   {
/* 200 */     super(paramReadableInstant1, paramReadableInstant2);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Duration(Object paramObject)
/*     */   {
/* 211 */     super(paramObject);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public long getStandardDays()
/*     */   {
/* 231 */     return getMillis() / 86400000L;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public long getStandardHours()
/*     */   {
/* 249 */     return getMillis() / 3600000L;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public long getStandardMinutes()
/*     */   {
/* 267 */     return getMillis() / 60000L;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public long getStandardSeconds()
/*     */   {
/* 284 */     return getMillis() / 1000L;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Duration toDuration()
/*     */   {
/* 295 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Days toStandardDays()
/*     */   {
/* 312 */     long l = getStandardDays();
/* 313 */     return Days.days(FieldUtils.safeToInt(l));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Hours toStandardHours()
/*     */   {
/* 329 */     long l = getStandardHours();
/* 330 */     return Hours.hours(FieldUtils.safeToInt(l));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Minutes toStandardMinutes()
/*     */   {
/* 346 */     long l = getStandardMinutes();
/* 347 */     return Minutes.minutes(FieldUtils.safeToInt(l));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Seconds toStandardSeconds()
/*     */   {
/* 362 */     long l = getStandardSeconds();
/* 363 */     return Seconds.seconds(FieldUtils.safeToInt(l));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Duration withMillis(long paramLong)
/*     */   {
/* 374 */     if (paramLong == getMillis()) {
/* 375 */       return this;
/*     */     }
/* 377 */     return new Duration(paramLong);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Duration withDurationAdded(long paramLong, int paramInt)
/*     */   {
/* 391 */     if ((paramLong == 0L) || (paramInt == 0)) {
/* 392 */       return this;
/*     */     }
/* 394 */     long l1 = FieldUtils.safeMultiply(paramLong, paramInt);
/* 395 */     long l2 = FieldUtils.safeAdd(getMillis(), l1);
/* 396 */     return new Duration(l2);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Duration withDurationAdded(ReadableDuration paramReadableDuration, int paramInt)
/*     */   {
/* 410 */     if ((paramReadableDuration == null) || (paramInt == 0)) {
/* 411 */       return this;
/*     */     }
/* 413 */     return withDurationAdded(paramReadableDuration.getMillis(), paramInt);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Duration plus(long paramLong)
/*     */   {
/* 427 */     return withDurationAdded(paramLong, 1);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Duration plus(ReadableDuration paramReadableDuration)
/*     */   {
/* 440 */     if (paramReadableDuration == null) {
/* 441 */       return this;
/*     */     }
/* 443 */     return withDurationAdded(paramReadableDuration.getMillis(), 1);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Duration minus(long paramLong)
/*     */   {
/* 456 */     return withDurationAdded(paramLong, -1);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Duration minus(ReadableDuration paramReadableDuration)
/*     */   {
/* 469 */     if (paramReadableDuration == null) {
/* 470 */       return this;
/*     */     }
/* 472 */     return withDurationAdded(paramReadableDuration.getMillis(), -1);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Duration multipliedBy(long paramLong)
/*     */   {
/* 486 */     if (paramLong == 1L) {
/* 487 */       return this;
/*     */     }
/* 489 */     return new Duration(FieldUtils.safeMultiply(getMillis(), paramLong));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Duration dividedBy(long paramLong)
/*     */   {
/* 503 */     if (paramLong == 1L) {
/* 504 */       return this;
/*     */     }
/* 506 */     return new Duration(FieldUtils.safeDivide(getMillis(), paramLong));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Duration dividedBy(long paramLong, RoundingMode paramRoundingMode)
/*     */   {
/* 521 */     if (paramLong == 1L) {
/* 522 */       return this;
/*     */     }
/* 524 */     return new Duration(FieldUtils.safeDivide(getMillis(), paramLong, paramRoundingMode));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Duration negated()
/*     */   {
/* 534 */     if (getMillis() == Long.MIN_VALUE) {
/* 535 */       throw new ArithmeticException("Negation of this duration would overflow");
/*     */     }
/* 537 */     return new Duration(-getMillis());
/*     */   }
/*     */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\org\joda\time\Duration.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */