/*      */ package org.joda.time;
/*      */ 
/*      */ import java.io.File;
/*      */ import java.io.IOException;
/*      */ import java.io.ObjectInputStream;
/*      */ import java.io.ObjectOutputStream;
/*      */ import java.io.ObjectStreamException;
/*      */ import java.io.Serializable;
/*      */ import java.util.Collections;
/*      */ import java.util.HashMap;
/*      */ import java.util.Locale;
/*      */ import java.util.Map;
/*      */ import java.util.Set;
/*      */ import java.util.TimeZone;
/*      */ import java.util.concurrent.atomic.AtomicReference;
/*      */ import org.joda.convert.FromString;
/*      */ import org.joda.convert.ToString;
/*      */ import org.joda.time.chrono.BaseChronology;
/*      */ import org.joda.time.field.FieldUtils;
/*      */ import org.joda.time.format.DateTimeFormatter;
/*      */ import org.joda.time.format.DateTimeFormatterBuilder;
/*      */ import org.joda.time.format.FormatUtils;
/*      */ import org.joda.time.tz.DefaultNameProvider;
/*      */ import org.joda.time.tz.FixedDateTimeZone;
/*      */ import org.joda.time.tz.NameProvider;
/*      */ import org.joda.time.tz.Provider;
/*      */ import org.joda.time.tz.UTCProvider;
/*      */ import org.joda.time.tz.ZoneInfoProvider;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public abstract class DateTimeZone
/*      */   implements Serializable
/*      */ {
/*      */   private static final long serialVersionUID = 5546345482340108586L;
/*  108 */   public static final DateTimeZone UTC = UTCDateTimeZone.INSTANCE;
/*      */   
/*      */ 
/*      */ 
/*      */   private static final int MAX_MILLIS = 86399999;
/*      */   
/*      */ 
/*      */ 
/*  116 */   private static final AtomicReference<Provider> cProvider = new AtomicReference();
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  122 */   private static final AtomicReference<NameProvider> cNameProvider = new AtomicReference();
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  128 */   private static final AtomicReference<DateTimeZone> cDefault = new AtomicReference();
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private final String iID;
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static DateTimeZone getDefault()
/*      */   {
/*  145 */     DateTimeZone localDateTimeZone = (DateTimeZone)cDefault.get();
/*  146 */     if (localDateTimeZone == null) {
/*      */       try {
/*      */         try {
/*  149 */           String str = System.getProperty("user.timezone");
/*  150 */           if (str != null) {
/*  151 */             localDateTimeZone = forID(str);
/*      */           }
/*      */         }
/*      */         catch (RuntimeException localRuntimeException) {}
/*      */         
/*  156 */         if (localDateTimeZone == null) {
/*  157 */           localDateTimeZone = forTimeZone(TimeZone.getDefault());
/*      */         }
/*      */       }
/*      */       catch (IllegalArgumentException localIllegalArgumentException) {}
/*      */       
/*  162 */       if (localDateTimeZone == null) {
/*  163 */         localDateTimeZone = UTC;
/*      */       }
/*  165 */       if (!cDefault.compareAndSet(null, localDateTimeZone)) {
/*  166 */         localDateTimeZone = (DateTimeZone)cDefault.get();
/*      */       }
/*      */     }
/*  169 */     return localDateTimeZone;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void setDefault(DateTimeZone paramDateTimeZone)
/*      */     throws SecurityException
/*      */   {
/*  182 */     SecurityManager localSecurityManager = System.getSecurityManager();
/*  183 */     if (localSecurityManager != null) {
/*  184 */       localSecurityManager.checkPermission(new JodaTimePermission("DateTimeZone.setDefault"));
/*      */     }
/*  186 */     if (paramDateTimeZone == null) {
/*  187 */       throw new IllegalArgumentException("The datetime zone must not be null");
/*      */     }
/*  189 */     cDefault.set(paramDateTimeZone);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   @FromString
/*      */   public static DateTimeZone forID(String paramString)
/*      */   {
/*  210 */     if (paramString == null) {
/*  211 */       return getDefault();
/*      */     }
/*  213 */     if (paramString.equals("UTC")) {
/*  214 */       return UTC;
/*      */     }
/*  216 */     DateTimeZone localDateTimeZone = getProvider().getZone(paramString);
/*  217 */     if (localDateTimeZone != null) {
/*  218 */       return localDateTimeZone;
/*      */     }
/*  220 */     if ((paramString.startsWith("+")) || (paramString.startsWith("-"))) {
/*  221 */       int i = parseOffset(paramString);
/*  222 */       if (i == 0L) {
/*  223 */         return UTC;
/*      */       }
/*  225 */       paramString = printOffset(i);
/*  226 */       return fixedOffsetZone(paramString, i);
/*      */     }
/*      */     
/*  229 */     throw new IllegalArgumentException("The datetime zone id '" + paramString + "' is not recognised");
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static DateTimeZone forOffsetHours(int paramInt)
/*      */     throws IllegalArgumentException
/*      */   {
/*  243 */     return forOffsetHoursMinutes(paramInt, 0);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static DateTimeZone forOffsetHoursMinutes(int paramInt1, int paramInt2)
/*      */     throws IllegalArgumentException
/*      */   {
/*  278 */     if ((paramInt1 == 0) && (paramInt2 == 0)) {
/*  279 */       return UTC;
/*      */     }
/*  281 */     if ((paramInt1 < -23) || (paramInt1 > 23)) {
/*  282 */       throw new IllegalArgumentException("Hours out of range: " + paramInt1);
/*      */     }
/*  284 */     if ((paramInt2 < -59) || (paramInt2 > 59)) {
/*  285 */       throw new IllegalArgumentException("Minutes out of range: " + paramInt2);
/*      */     }
/*  287 */     if ((paramInt1 > 0) && (paramInt2 < 0)) {
/*  288 */       throw new IllegalArgumentException("Positive hours must not have negative minutes: " + paramInt2);
/*      */     }
/*  290 */     int i = 0;
/*      */     try {
/*  292 */       int j = paramInt1 * 60;
/*  293 */       if (j < 0) {
/*  294 */         paramInt2 = j - Math.abs(paramInt2);
/*      */       } else {
/*  296 */         paramInt2 = j + paramInt2;
/*      */       }
/*  298 */       i = FieldUtils.safeMultiply(paramInt2, 60000);
/*      */     } catch (ArithmeticException localArithmeticException) {
/*  300 */       throw new IllegalArgumentException("Offset is too large");
/*      */     }
/*  302 */     return forOffsetMillis(i);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static DateTimeZone forOffsetMillis(int paramInt)
/*      */   {
/*  312 */     if ((paramInt < -86399999) || (paramInt > 86399999)) {
/*  313 */       throw new IllegalArgumentException("Millis out of range: " + paramInt);
/*      */     }
/*  315 */     String str = printOffset(paramInt);
/*  316 */     return fixedOffsetZone(str, paramInt);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static DateTimeZone forTimeZone(TimeZone paramTimeZone)
/*      */   {
/*  335 */     if (paramTimeZone == null) {
/*  336 */       return getDefault();
/*      */     }
/*  338 */     String str1 = paramTimeZone.getID();
/*  339 */     if (str1 == null) {
/*  340 */       throw new IllegalArgumentException("The TimeZone id must not be null");
/*      */     }
/*  342 */     if (str1.equals("UTC")) {
/*  343 */       return UTC;
/*      */     }
/*      */     
/*      */ 
/*  347 */     DateTimeZone localDateTimeZone = null;
/*  348 */     String str2 = getConvertedId(str1);
/*  349 */     Provider localProvider = getProvider();
/*  350 */     if (str2 != null) {
/*  351 */       localDateTimeZone = localProvider.getZone(str2);
/*      */     }
/*  353 */     if (localDateTimeZone == null) {
/*  354 */       localDateTimeZone = localProvider.getZone(str1);
/*      */     }
/*  356 */     if (localDateTimeZone != null) {
/*  357 */       return localDateTimeZone;
/*      */     }
/*      */     
/*      */ 
/*  361 */     if (str2 == null) {
/*  362 */       str2 = str1;
/*  363 */       if ((str2.startsWith("GMT+")) || (str2.startsWith("GMT-"))) {
/*  364 */         str2 = str2.substring(3);
/*  365 */         if (str2.length() > 2) {
/*  366 */           char c = str2.charAt(1);
/*  367 */           if ((c > '9') && (Character.isDigit(c))) {
/*  368 */             str2 = convertToAsciiNumber(str2);
/*      */           }
/*      */         }
/*  371 */         int i = parseOffset(str2);
/*  372 */         if (i == 0L) {
/*  373 */           return UTC;
/*      */         }
/*  375 */         str2 = printOffset(i);
/*  376 */         return fixedOffsetZone(str2, i);
/*      */       }
/*      */     }
/*      */     
/*  380 */     throw new IllegalArgumentException("The datetime zone id '" + str1 + "' is not recognised");
/*      */   }
/*      */   
/*      */   private static String convertToAsciiNumber(String paramString) {
/*  384 */     StringBuilder localStringBuilder = new StringBuilder(paramString);
/*  385 */     for (int i = 0; i < localStringBuilder.length(); i++) {
/*  386 */       char c = localStringBuilder.charAt(i);
/*  387 */       int j = Character.digit(c, 10);
/*  388 */       if (j >= 0) {
/*  389 */         localStringBuilder.setCharAt(i, (char)(48 + j));
/*      */       }
/*      */     }
/*  392 */     return localStringBuilder.toString();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static DateTimeZone fixedOffsetZone(String paramString, int paramInt)
/*      */   {
/*  404 */     if (paramInt == 0) {
/*  405 */       return UTC;
/*      */     }
/*  407 */     return new FixedDateTimeZone(paramString, null, paramInt, paramInt);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static Set<String> getAvailableIDs()
/*      */   {
/*  416 */     return getProvider().getAvailableIDs();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static Provider getProvider()
/*      */   {
/*  429 */     Provider localProvider = (Provider)cProvider.get();
/*  430 */     if (localProvider == null) {
/*  431 */       localProvider = getDefaultProvider();
/*  432 */       if (!cProvider.compareAndSet(null, localProvider)) {
/*  433 */         localProvider = (Provider)cProvider.get();
/*      */       }
/*      */     }
/*  436 */     return localProvider;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void setProvider(Provider paramProvider)
/*      */     throws SecurityException
/*      */   {
/*  450 */     SecurityManager localSecurityManager = System.getSecurityManager();
/*  451 */     if (localSecurityManager != null) {
/*  452 */       localSecurityManager.checkPermission(new JodaTimePermission("DateTimeZone.setProvider"));
/*      */     }
/*  454 */     if (paramProvider == null) {
/*  455 */       paramProvider = getDefaultProvider();
/*      */     } else {
/*  457 */       validateProvider(paramProvider);
/*      */     }
/*  459 */     cProvider.set(paramProvider);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static Provider validateProvider(Provider paramProvider)
/*      */   {
/*  470 */     Set localSet = paramProvider.getAvailableIDs();
/*  471 */     if ((localSet == null) || (localSet.size() == 0)) {
/*  472 */       throw new IllegalArgumentException("The provider doesn't have any available ids");
/*      */     }
/*  474 */     if (!localSet.contains("UTC")) {
/*  475 */       throw new IllegalArgumentException("The provider doesn't support UTC");
/*      */     }
/*  477 */     if (!UTC.equals(paramProvider.getZone("UTC"))) {
/*  478 */       throw new IllegalArgumentException("Invalid UTC zone provided");
/*      */     }
/*  480 */     return paramProvider;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static Provider getDefaultProvider()
/*      */   {
/*      */     try
/*      */     {
/*  504 */       String str1 = System.getProperty("org.joda.time.DateTimeZone.Provider");
/*  505 */       if (str1 != null) {
/*      */         try {
/*  507 */           Provider localProvider = (Provider)Class.forName(str1).newInstance();
/*  508 */           return validateProvider(localProvider);
/*      */         } catch (Exception localException2) {
/*  510 */           throw new RuntimeException(localException2);
/*      */         }
/*      */       }
/*      */     }
/*      */     catch (SecurityException localSecurityException1) {}
/*      */     
/*      */     try
/*      */     {
/*  518 */       String str2 = System.getProperty("org.joda.time.DateTimeZone.Folder");
/*  519 */       if (str2 != null) {
/*      */         try {
/*  521 */           ZoneInfoProvider localZoneInfoProvider2 = new ZoneInfoProvider(new File(str2));
/*  522 */           return validateProvider(localZoneInfoProvider2);
/*      */         } catch (Exception localException3) {
/*  524 */           throw new RuntimeException(localException3);
/*      */         }
/*      */       }
/*      */     }
/*      */     catch (SecurityException localSecurityException2) {}
/*      */     
/*      */     try
/*      */     {
/*  532 */       ZoneInfoProvider localZoneInfoProvider1 = new ZoneInfoProvider("org/joda/time/tz/data");
/*  533 */       return validateProvider(localZoneInfoProvider1);
/*      */     } catch (Exception localException1) {
/*  535 */       localException1.printStackTrace();
/*      */     }
/*      */     
/*  538 */     return new UTCProvider();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static NameProvider getNameProvider()
/*      */   {
/*  551 */     NameProvider localNameProvider = (NameProvider)cNameProvider.get();
/*  552 */     if (localNameProvider == null) {
/*  553 */       localNameProvider = getDefaultNameProvider();
/*  554 */       if (!cNameProvider.compareAndSet(null, localNameProvider)) {
/*  555 */         localNameProvider = (NameProvider)cNameProvider.get();
/*      */       }
/*      */     }
/*  558 */     return localNameProvider;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void setNameProvider(NameProvider paramNameProvider)
/*      */     throws SecurityException
/*      */   {
/*  572 */     SecurityManager localSecurityManager = System.getSecurityManager();
/*  573 */     if (localSecurityManager != null) {
/*  574 */       localSecurityManager.checkPermission(new JodaTimePermission("DateTimeZone.setNameProvider"));
/*      */     }
/*  576 */     if (paramNameProvider == null) {
/*  577 */       paramNameProvider = getDefaultNameProvider();
/*      */     }
/*  579 */     cNameProvider.set(paramNameProvider);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static NameProvider getDefaultNameProvider()
/*      */   {
/*  591 */     Object localObject = null;
/*      */     try {
/*  593 */       String str = System.getProperty("org.joda.time.DateTimeZone.NameProvider");
/*  594 */       if (str != null) {
/*      */         try {
/*  596 */           localObject = (NameProvider)Class.forName(str).newInstance();
/*      */         } catch (Exception localException) {
/*  598 */           throw new RuntimeException(localException);
/*      */         }
/*      */       }
/*      */     }
/*      */     catch (SecurityException localSecurityException) {}
/*      */     
/*      */ 
/*  605 */     if (localObject == null) {
/*  606 */       localObject = new DefaultNameProvider();
/*      */     }
/*      */     
/*  609 */     return (NameProvider)localObject;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static String getConvertedId(String paramString)
/*      */   {
/*  620 */     return (String)LazyInit.CONVERSION_MAP.get(paramString);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static int parseOffset(String paramString)
/*      */   {
/*  630 */     return -(int)LazyInit.OFFSET_FORMATTER.parseMillis(paramString);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static String printOffset(int paramInt)
/*      */   {
/*  643 */     StringBuffer localStringBuffer = new StringBuffer();
/*  644 */     if (paramInt >= 0) {
/*  645 */       localStringBuffer.append('+');
/*      */     } else {
/*  647 */       localStringBuffer.append('-');
/*  648 */       paramInt = -paramInt;
/*      */     }
/*      */     
/*  651 */     int i = paramInt / 3600000;
/*  652 */     FormatUtils.appendPaddedInteger(localStringBuffer, i, 2);
/*  653 */     paramInt -= i * 3600000;
/*      */     
/*  655 */     int j = paramInt / 60000;
/*  656 */     localStringBuffer.append(':');
/*  657 */     FormatUtils.appendPaddedInteger(localStringBuffer, j, 2);
/*  658 */     paramInt -= j * 60000;
/*  659 */     if (paramInt == 0) {
/*  660 */       return localStringBuffer.toString();
/*      */     }
/*      */     
/*  663 */     int k = paramInt / 1000;
/*  664 */     localStringBuffer.append(':');
/*  665 */     FormatUtils.appendPaddedInteger(localStringBuffer, k, 2);
/*  666 */     paramInt -= k * 1000;
/*  667 */     if (paramInt == 0) {
/*  668 */       return localStringBuffer.toString();
/*      */     }
/*      */     
/*  671 */     localStringBuffer.append('.');
/*  672 */     FormatUtils.appendPaddedInteger(localStringBuffer, paramInt, 3);
/*  673 */     return localStringBuffer.toString();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   protected DateTimeZone(String paramString)
/*      */   {
/*  688 */     if (paramString == null) {
/*  689 */       throw new IllegalArgumentException("Id must not be null");
/*      */     }
/*  691 */     this.iID = paramString;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   @ToString
/*      */   public final String getID()
/*      */   {
/*  704 */     return this.iID;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public abstract String getNameKey(long paramLong);
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public final String getShortName(long paramLong)
/*      */   {
/*  727 */     return getShortName(paramLong, null);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getShortName(long paramLong, Locale paramLocale)
/*      */   {
/*  742 */     if (paramLocale == null) {
/*  743 */       paramLocale = Locale.getDefault();
/*      */     }
/*  745 */     String str1 = getNameKey(paramLong);
/*  746 */     if (str1 == null) {
/*  747 */       return this.iID;
/*      */     }
/*      */     
/*  750 */     NameProvider localNameProvider = getNameProvider();
/*  751 */     String str2; if ((localNameProvider instanceof DefaultNameProvider)) {
/*  752 */       str2 = ((DefaultNameProvider)localNameProvider).getShortName(paramLocale, this.iID, str1, isStandardOffset(paramLong));
/*      */     } else {
/*  754 */       str2 = localNameProvider.getShortName(paramLocale, this.iID, str1);
/*      */     }
/*  756 */     if (str2 != null) {
/*  757 */       return str2;
/*      */     }
/*  759 */     return printOffset(getOffset(paramLong));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public final String getName(long paramLong)
/*      */   {
/*  773 */     return getName(paramLong, null);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getName(long paramLong, Locale paramLocale)
/*      */   {
/*  788 */     if (paramLocale == null) {
/*  789 */       paramLocale = Locale.getDefault();
/*      */     }
/*  791 */     String str1 = getNameKey(paramLong);
/*  792 */     if (str1 == null) {
/*  793 */       return this.iID;
/*      */     }
/*      */     
/*  796 */     NameProvider localNameProvider = getNameProvider();
/*  797 */     String str2; if ((localNameProvider instanceof DefaultNameProvider)) {
/*  798 */       str2 = ((DefaultNameProvider)localNameProvider).getName(paramLocale, this.iID, str1, isStandardOffset(paramLong));
/*      */     } else {
/*  800 */       str2 = localNameProvider.getName(paramLocale, this.iID, str1);
/*      */     }
/*  802 */     if (str2 != null) {
/*  803 */       return str2;
/*      */     }
/*  805 */     return printOffset(getOffset(paramLong));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public abstract int getOffset(long paramLong);
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public final int getOffset(ReadableInstant paramReadableInstant)
/*      */   {
/*  823 */     if (paramReadableInstant == null) {
/*  824 */       return getOffset(DateTimeUtils.currentTimeMillis());
/*      */     }
/*  826 */     return getOffset(paramReadableInstant.getMillis());
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public abstract int getStandardOffset(long paramLong);
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean isStandardOffset(long paramLong)
/*      */   {
/*  854 */     return getOffset(paramLong) == getStandardOffset(paramLong);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int getOffsetFromLocal(long paramLong)
/*      */   {
/*  895 */     int i = getOffset(paramLong);
/*      */     
/*  897 */     long l1 = paramLong - i;
/*  898 */     int j = getOffset(l1);
/*      */     long l2;
/*  900 */     if (i != j)
/*      */     {
/*      */ 
/*  903 */       if (i - j < 0)
/*      */       {
/*      */ 
/*      */ 
/*  907 */         l2 = nextTransition(l1);
/*  908 */         if (l2 == paramLong - i) {
/*  909 */           l2 = Long.MAX_VALUE;
/*      */         }
/*  911 */         long l3 = nextTransition(paramLong - j);
/*  912 */         if (l3 == paramLong - j) {
/*  913 */           l3 = Long.MAX_VALUE;
/*      */         }
/*  915 */         if (l2 != l3) {
/*  916 */           return i;
/*      */         }
/*      */       }
/*  919 */     } else if (i >= 0) {
/*  920 */       l2 = previousTransition(l1);
/*  921 */       if (l2 < l1) {
/*  922 */         int k = getOffset(l2);
/*  923 */         int m = k - i;
/*  924 */         if (l1 - l2 <= m) {
/*  925 */           return k;
/*      */         }
/*      */       }
/*      */     }
/*  929 */     return j;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public long convertUTCToLocal(long paramLong)
/*      */   {
/*  943 */     int i = getOffset(paramLong);
/*  944 */     long l = paramLong + i;
/*      */     
/*  946 */     if (((paramLong ^ l) < 0L) && ((paramLong ^ i) >= 0L)) {
/*  947 */       throw new ArithmeticException("Adding time zone offset caused overflow");
/*      */     }
/*  949 */     return l;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public long convertLocalToUTC(long paramLong1, boolean paramBoolean, long paramLong2)
/*      */   {
/*  970 */     int i = getOffset(paramLong2);
/*  971 */     long l = paramLong1 - i;
/*  972 */     int j = getOffset(l);
/*  973 */     if (j == i) {
/*  974 */       return l;
/*      */     }
/*  976 */     return convertLocalToUTC(paramLong1, paramBoolean);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public long convertLocalToUTC(long paramLong, boolean paramBoolean)
/*      */   {
/*  993 */     int i = getOffset(paramLong);
/*      */     
/*  995 */     int j = getOffset(paramLong - i);
/*      */     
/*  997 */     if (i != j)
/*      */     {
/*      */ 
/*      */ 
/* 1001 */       if ((paramBoolean) || (i < 0))
/*      */       {
/* 1003 */         l1 = nextTransition(paramLong - i);
/* 1004 */         if (l1 == paramLong - i) {
/* 1005 */           l1 = Long.MAX_VALUE;
/*      */         }
/* 1007 */         long l2 = nextTransition(paramLong - j);
/* 1008 */         if (l2 == paramLong - j) {
/* 1009 */           l2 = Long.MAX_VALUE;
/*      */         }
/* 1011 */         if (l1 != l2)
/*      */         {
/* 1013 */           if (paramBoolean)
/*      */           {
/* 1015 */             throw new IllegalInstantException(paramLong, getID());
/*      */           }
/*      */           
/*      */ 
/*      */ 
/* 1020 */           j = i;
/*      */         }
/*      */       }
/*      */     }
/*      */     
/*      */ 
/* 1026 */     long l1 = paramLong - j;
/*      */     
/* 1028 */     if (((paramLong ^ l1) < 0L) && ((paramLong ^ j) < 0L)) {
/* 1029 */       throw new ArithmeticException("Subtracting time zone offset caused overflow");
/*      */     }
/* 1031 */     return l1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public long getMillisKeepLocal(DateTimeZone paramDateTimeZone, long paramLong)
/*      */   {
/* 1045 */     if (paramDateTimeZone == null) {
/* 1046 */       paramDateTimeZone = getDefault();
/*      */     }
/* 1048 */     if (paramDateTimeZone == this) {
/* 1049 */       return paramLong;
/*      */     }
/* 1051 */     long l = convertUTCToLocal(paramLong);
/* 1052 */     return paramDateTimeZone.convertLocalToUTC(l, false, paramLong);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean isLocalDateTimeGap(LocalDateTime paramLocalDateTime)
/*      */   {
/* 1162 */     if (isFixed()) {
/* 1163 */       return false;
/*      */     }
/*      */     try {
/* 1166 */       paramLocalDateTime.toDateTime(this);
/* 1167 */       return false;
/*      */     } catch (IllegalInstantException localIllegalInstantException) {}
/* 1169 */     return true;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public long adjustOffset(long paramLong, boolean paramBoolean)
/*      */   {
/* 1184 */     long l1 = paramLong - 10800000L;
/* 1185 */     long l2 = paramLong + 10800000L;
/* 1186 */     long l3 = getOffset(l1);
/* 1187 */     long l4 = getOffset(l2);
/* 1188 */     if (l3 <= l4) {
/* 1189 */       return paramLong;
/*      */     }
/*      */     
/*      */ 
/* 1193 */     long l5 = l3 - l4;
/* 1194 */     long l6 = nextTransition(l1);
/* 1195 */     long l7 = l6 - l5;
/* 1196 */     long l8 = l6 + l5;
/* 1197 */     if ((paramLong < l7) || (paramLong >= l8)) {
/* 1198 */       return paramLong;
/*      */     }
/*      */     
/*      */ 
/* 1202 */     long l9 = paramLong - l7;
/* 1203 */     if (l9 >= l5)
/*      */     {
/* 1205 */       return paramBoolean ? paramLong : paramLong - l5;
/*      */     }
/*      */     
/* 1208 */     return paramBoolean ? paramLong + l5 : paramLong;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public abstract boolean isFixed();
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public abstract long nextTransition(long paramLong);
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public abstract long previousTransition(long paramLong);
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public TimeZone toTimeZone()
/*      */   {
/* 1250 */     return TimeZone.getTimeZone(this.iID);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public abstract boolean equals(Object paramObject);
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int hashCode()
/*      */   {
/* 1267 */     return 57 + getID().hashCode();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public String toString()
/*      */   {
/* 1275 */     return getID();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   protected Object writeReplace()
/*      */     throws ObjectStreamException
/*      */   {
/* 1285 */     return new Stub(this.iID);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   private static final class Stub
/*      */     implements Serializable
/*      */   {
/*      */     private static final long serialVersionUID = -6471952376487863581L;
/*      */     
/*      */ 
/*      */     private transient String iID;
/*      */     
/*      */ 
/*      */ 
/*      */     Stub(String paramString)
/*      */     {
/* 1302 */       this.iID = paramString;
/*      */     }
/*      */     
/*      */     private void writeObject(ObjectOutputStream paramObjectOutputStream) throws IOException {
/* 1306 */       paramObjectOutputStream.writeUTF(this.iID);
/*      */     }
/*      */     
/*      */     private void readObject(ObjectInputStream paramObjectInputStream) throws IOException {
/* 1310 */       this.iID = paramObjectInputStream.readUTF();
/*      */     }
/*      */     
/*      */     private Object readResolve() throws ObjectStreamException {
/* 1314 */       return DateTimeZone.forID(this.iID);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   static final class LazyInit
/*      */   {
/* 1325 */     static final Map<String, String> CONVERSION_MAP = ;
/*      */     
/* 1327 */     static final DateTimeFormatter OFFSET_FORMATTER = buildFormatter();
/*      */     
/*      */ 
/*      */     private static DateTimeFormatter buildFormatter()
/*      */     {
/* 1332 */       BaseChronology local1 = new BaseChronology() {
/*      */         private static final long serialVersionUID = -3128740902654445468L;
/*      */         
/* 1335 */         public DateTimeZone getZone() { return null; }
/*      */         
/*      */         public Chronology withUTC() {
/* 1338 */           return this;
/*      */         }
/*      */         
/* 1341 */         public Chronology withZone(DateTimeZone paramAnonymousDateTimeZone) { return this; }
/*      */         
/*      */         public String toString() {
/* 1344 */           return getClass().getName();
/*      */         }
/* 1346 */       };
/* 1347 */       return new DateTimeFormatterBuilder().appendTimeZoneOffset(null, true, 2, 4).toFormatter().withChronology(local1);
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */     private static Map<String, String> buildMap()
/*      */     {
/* 1355 */       HashMap localHashMap = new HashMap();
/* 1356 */       localHashMap.put("GMT", "UTC");
/* 1357 */       localHashMap.put("WET", "WET");
/* 1358 */       localHashMap.put("CET", "CET");
/* 1359 */       localHashMap.put("MET", "CET");
/* 1360 */       localHashMap.put("ECT", "CET");
/* 1361 */       localHashMap.put("EET", "EET");
/* 1362 */       localHashMap.put("MIT", "Pacific/Apia");
/* 1363 */       localHashMap.put("HST", "Pacific/Honolulu");
/* 1364 */       localHashMap.put("AST", "America/Anchorage");
/* 1365 */       localHashMap.put("PST", "America/Los_Angeles");
/* 1366 */       localHashMap.put("MST", "America/Denver");
/* 1367 */       localHashMap.put("PNT", "America/Phoenix");
/* 1368 */       localHashMap.put("CST", "America/Chicago");
/* 1369 */       localHashMap.put("EST", "America/New_York");
/* 1370 */       localHashMap.put("IET", "America/Indiana/Indianapolis");
/* 1371 */       localHashMap.put("PRT", "America/Puerto_Rico");
/* 1372 */       localHashMap.put("CNT", "America/St_Johns");
/* 1373 */       localHashMap.put("AGT", "America/Argentina/Buenos_Aires");
/* 1374 */       localHashMap.put("BET", "America/Sao_Paulo");
/* 1375 */       localHashMap.put("ART", "Africa/Cairo");
/* 1376 */       localHashMap.put("CAT", "Africa/Harare");
/* 1377 */       localHashMap.put("EAT", "Africa/Addis_Ababa");
/* 1378 */       localHashMap.put("NET", "Asia/Yerevan");
/* 1379 */       localHashMap.put("PLT", "Asia/Karachi");
/* 1380 */       localHashMap.put("IST", "Asia/Kolkata");
/* 1381 */       localHashMap.put("BST", "Asia/Dhaka");
/* 1382 */       localHashMap.put("VST", "Asia/Ho_Chi_Minh");
/* 1383 */       localHashMap.put("CTT", "Asia/Shanghai");
/* 1384 */       localHashMap.put("JST", "Asia/Tokyo");
/* 1385 */       localHashMap.put("ACT", "Australia/Darwin");
/* 1386 */       localHashMap.put("AET", "Australia/Sydney");
/* 1387 */       localHashMap.put("SST", "Pacific/Guadalcanal");
/* 1388 */       localHashMap.put("NST", "Pacific/Auckland");
/* 1389 */       return Collections.unmodifiableMap(localHashMap);
/*      */     }
/*      */   }
/*      */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\org\joda\time\DateTimeZone.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */