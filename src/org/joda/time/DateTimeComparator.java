/*     */ package org.joda.time;
/*     */ 
/*     */ import java.io.Serializable;
/*     */ import java.util.Comparator;
/*     */ import org.joda.time.convert.ConverterManager;
/*     */ import org.joda.time.convert.InstantConverter;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class DateTimeComparator
/*     */   implements Comparator<Object>, Serializable
/*     */ {
/*     */   private static final long serialVersionUID = -6097339773320178364L;
/*  54 */   private static final DateTimeComparator ALL_INSTANCE = new DateTimeComparator(null, null);
/*     */   
/*  56 */   private static final DateTimeComparator DATE_INSTANCE = new DateTimeComparator(DateTimeFieldType.dayOfYear(), null);
/*     */   
/*  58 */   private static final DateTimeComparator TIME_INSTANCE = new DateTimeComparator(null, DateTimeFieldType.dayOfYear());
/*     */   
/*     */ 
/*     */ 
/*     */   private final DateTimeFieldType iLowerLimit;
/*     */   
/*     */ 
/*     */ 
/*     */   private final DateTimeFieldType iUpperLimit;
/*     */   
/*     */ 
/*     */ 
/*     */   public static DateTimeComparator getInstance()
/*     */   {
/*  72 */     return ALL_INSTANCE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static DateTimeComparator getInstance(DateTimeFieldType paramDateTimeFieldType)
/*     */   {
/*  87 */     return getInstance(paramDateTimeFieldType, null);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static DateTimeComparator getInstance(DateTimeFieldType paramDateTimeFieldType1, DateTimeFieldType paramDateTimeFieldType2)
/*     */   {
/* 106 */     if ((paramDateTimeFieldType1 == null) && (paramDateTimeFieldType2 == null)) {
/* 107 */       return ALL_INSTANCE;
/*     */     }
/* 109 */     if ((paramDateTimeFieldType1 == DateTimeFieldType.dayOfYear()) && (paramDateTimeFieldType2 == null)) {
/* 110 */       return DATE_INSTANCE;
/*     */     }
/* 112 */     if ((paramDateTimeFieldType1 == null) && (paramDateTimeFieldType2 == DateTimeFieldType.dayOfYear())) {
/* 113 */       return TIME_INSTANCE;
/*     */     }
/* 115 */     return new DateTimeComparator(paramDateTimeFieldType1, paramDateTimeFieldType2);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static DateTimeComparator getDateOnlyInstance()
/*     */   {
/* 130 */     return DATE_INSTANCE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static DateTimeComparator getTimeOnlyInstance()
/*     */   {
/* 145 */     return TIME_INSTANCE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected DateTimeComparator(DateTimeFieldType paramDateTimeFieldType1, DateTimeFieldType paramDateTimeFieldType2)
/*     */   {
/* 156 */     this.iLowerLimit = paramDateTimeFieldType1;
/* 157 */     this.iUpperLimit = paramDateTimeFieldType2;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public DateTimeFieldType getLowerLimit()
/*     */   {
/* 167 */     return this.iLowerLimit;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public DateTimeFieldType getUpperLimit()
/*     */   {
/* 176 */     return this.iUpperLimit;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int compare(Object paramObject1, Object paramObject2)
/*     */   {
/* 192 */     InstantConverter localInstantConverter = ConverterManager.getInstance().getInstantConverter(paramObject1);
/* 193 */     Chronology localChronology1 = localInstantConverter.getChronology(paramObject1, (Chronology)null);
/* 194 */     long l1 = localInstantConverter.getInstantMillis(paramObject1, localChronology1);
/*     */     
/*     */ 
/*     */ 
/* 198 */     if (paramObject1 == paramObject2) {
/* 199 */       return 0;
/*     */     }
/*     */     
/* 202 */     localInstantConverter = ConverterManager.getInstance().getInstantConverter(paramObject2);
/* 203 */     Chronology localChronology2 = localInstantConverter.getChronology(paramObject2, (Chronology)null);
/* 204 */     long l2 = localInstantConverter.getInstantMillis(paramObject2, localChronology2);
/*     */     
/* 206 */     if (this.iLowerLimit != null) {
/* 207 */       l1 = this.iLowerLimit.getField(localChronology1).roundFloor(l1);
/* 208 */       l2 = this.iLowerLimit.getField(localChronology2).roundFloor(l2);
/*     */     }
/*     */     
/* 211 */     if (this.iUpperLimit != null) {
/* 212 */       l1 = this.iUpperLimit.getField(localChronology1).remainder(l1);
/* 213 */       l2 = this.iUpperLimit.getField(localChronology2).remainder(l2);
/*     */     }
/*     */     
/* 216 */     if (l1 < l2)
/* 217 */       return -1;
/* 218 */     if (l1 > l2) {
/* 219 */       return 1;
/*     */     }
/* 221 */     return 0;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private Object readResolve()
/*     */   {
/* 232 */     return getInstance(this.iLowerLimit, this.iUpperLimit);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean equals(Object paramObject)
/*     */   {
/* 242 */     if ((paramObject instanceof DateTimeComparator)) {
/* 243 */       DateTimeComparator localDateTimeComparator = (DateTimeComparator)paramObject;
/* 244 */       return ((this.iLowerLimit == localDateTimeComparator.getLowerLimit()) || ((this.iLowerLimit != null) && (this.iLowerLimit.equals(localDateTimeComparator.getLowerLimit())))) && ((this.iUpperLimit == localDateTimeComparator.getUpperLimit()) || ((this.iUpperLimit != null) && (this.iUpperLimit.equals(localDateTimeComparator.getUpperLimit()))));
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 249 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int hashCode()
/*     */   {
/* 258 */     return (this.iLowerLimit == null ? 0 : this.iLowerLimit.hashCode()) + 123 * (this.iUpperLimit == null ? 0 : this.iUpperLimit.hashCode());
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String toString()
/*     */   {
/* 268 */     if (this.iLowerLimit == this.iUpperLimit) {
/* 269 */       return "DateTimeComparator[" + (this.iLowerLimit == null ? "" : this.iLowerLimit.getName()) + "]";
/*     */     }
/*     */     
/*     */ 
/* 273 */     return "DateTimeComparator[" + (this.iLowerLimit == null ? "" : this.iLowerLimit.getName()) + "-" + (this.iUpperLimit == null ? "" : this.iUpperLimit.getName()) + "]";
/*     */   }
/*     */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\org\joda\time\DateTimeComparator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */