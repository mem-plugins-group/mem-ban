package org.joda.time.convert;

public abstract interface Converter
{
  public abstract Class<?> getSupportedType();
}


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\org\joda\time\convert\Converter.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */