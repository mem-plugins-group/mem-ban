package org.joda.time.convert;

public abstract interface DurationConverter
  extends Converter
{
  public abstract long getDurationMillis(Object paramObject);
}


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\org\joda\time\convert\DurationConverter.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */