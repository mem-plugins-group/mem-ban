package org.joda.time.convert;

import org.joda.time.Chronology;
import org.joda.time.ReadWritableInterval;

public abstract interface IntervalConverter
  extends Converter
{
  public abstract boolean isReadableInterval(Object paramObject, Chronology paramChronology);
  
  public abstract void setInto(ReadWritableInterval paramReadWritableInterval, Object paramObject, Chronology paramChronology);
}


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\org\joda\time\convert\IntervalConverter.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */