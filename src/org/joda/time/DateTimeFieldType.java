/*     */ package org.joda.time;
/*     */ 
/*     */ import java.io.Serializable;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public abstract class DateTimeFieldType
/*     */   implements Serializable
/*     */ {
/*     */   private static final long serialVersionUID = -42615285973990L;
/*     */   static final byte ERA = 1;
/*     */   static final byte YEAR_OF_ERA = 2;
/*     */   static final byte CENTURY_OF_ERA = 3;
/*     */   static final byte YEAR_OF_CENTURY = 4;
/*     */   static final byte YEAR = 5;
/*     */   static final byte DAY_OF_YEAR = 6;
/*     */   static final byte MONTH_OF_YEAR = 7;
/*     */   static final byte DAY_OF_MONTH = 8;
/*     */   static final byte WEEKYEAR_OF_CENTURY = 9;
/*     */   static final byte WEEKYEAR = 10;
/*     */   static final byte WEEK_OF_WEEKYEAR = 11;
/*     */   static final byte DAY_OF_WEEK = 12;
/*     */   static final byte HALFDAY_OF_DAY = 13;
/*     */   static final byte HOUR_OF_HALFDAY = 14;
/*     */   static final byte CLOCKHOUR_OF_HALFDAY = 15;
/*     */   static final byte CLOCKHOUR_OF_DAY = 16;
/*     */   static final byte HOUR_OF_DAY = 17;
/*     */   static final byte MINUTE_OF_DAY = 18;
/*     */   static final byte MINUTE_OF_HOUR = 19;
/*     */   static final byte SECOND_OF_DAY = 20;
/*     */   static final byte SECOND_OF_MINUTE = 21;
/*     */   static final byte MILLIS_OF_DAY = 22;
/*     */   static final byte MILLIS_OF_SECOND = 23;
/*  73 */   private static final DateTimeFieldType ERA_TYPE = new StandardDateTimeFieldType("era", (byte)1, DurationFieldType.eras(), null);
/*     */   
/*     */ 
/*  76 */   private static final DateTimeFieldType YEAR_OF_ERA_TYPE = new StandardDateTimeFieldType("yearOfEra", (byte)2, DurationFieldType.years(), DurationFieldType.eras());
/*     */   
/*     */ 
/*  79 */   private static final DateTimeFieldType CENTURY_OF_ERA_TYPE = new StandardDateTimeFieldType("centuryOfEra", (byte)3, DurationFieldType.centuries(), DurationFieldType.eras());
/*     */   
/*     */ 
/*  82 */   private static final DateTimeFieldType YEAR_OF_CENTURY_TYPE = new StandardDateTimeFieldType("yearOfCentury", (byte)4, DurationFieldType.years(), DurationFieldType.centuries());
/*     */   
/*     */ 
/*  85 */   private static final DateTimeFieldType YEAR_TYPE = new StandardDateTimeFieldType("year", (byte)5, DurationFieldType.years(), null);
/*     */   
/*     */ 
/*  88 */   private static final DateTimeFieldType DAY_OF_YEAR_TYPE = new StandardDateTimeFieldType("dayOfYear", (byte)6, DurationFieldType.days(), DurationFieldType.years());
/*     */   
/*     */ 
/*  91 */   private static final DateTimeFieldType MONTH_OF_YEAR_TYPE = new StandardDateTimeFieldType("monthOfYear", (byte)7, DurationFieldType.months(), DurationFieldType.years());
/*     */   
/*     */ 
/*  94 */   private static final DateTimeFieldType DAY_OF_MONTH_TYPE = new StandardDateTimeFieldType("dayOfMonth", (byte)8, DurationFieldType.days(), DurationFieldType.months());
/*     */   
/*     */ 
/*  97 */   private static final DateTimeFieldType WEEKYEAR_OF_CENTURY_TYPE = new StandardDateTimeFieldType("weekyearOfCentury", (byte)9, DurationFieldType.weekyears(), DurationFieldType.centuries());
/*     */   
/*     */ 
/* 100 */   private static final DateTimeFieldType WEEKYEAR_TYPE = new StandardDateTimeFieldType("weekyear", (byte)10, DurationFieldType.weekyears(), null);
/*     */   
/*     */ 
/* 103 */   private static final DateTimeFieldType WEEK_OF_WEEKYEAR_TYPE = new StandardDateTimeFieldType("weekOfWeekyear", (byte)11, DurationFieldType.weeks(), DurationFieldType.weekyears());
/*     */   
/*     */ 
/* 106 */   private static final DateTimeFieldType DAY_OF_WEEK_TYPE = new StandardDateTimeFieldType("dayOfWeek", (byte)12, DurationFieldType.days(), DurationFieldType.weeks());
/*     */   
/*     */ 
/*     */ 
/* 110 */   private static final DateTimeFieldType HALFDAY_OF_DAY_TYPE = new StandardDateTimeFieldType("halfdayOfDay", (byte)13, DurationFieldType.halfdays(), DurationFieldType.days());
/*     */   
/*     */ 
/* 113 */   private static final DateTimeFieldType HOUR_OF_HALFDAY_TYPE = new StandardDateTimeFieldType("hourOfHalfday", (byte)14, DurationFieldType.hours(), DurationFieldType.halfdays());
/*     */   
/*     */ 
/* 116 */   private static final DateTimeFieldType CLOCKHOUR_OF_HALFDAY_TYPE = new StandardDateTimeFieldType("clockhourOfHalfday", (byte)15, DurationFieldType.hours(), DurationFieldType.halfdays());
/*     */   
/*     */ 
/* 119 */   private static final DateTimeFieldType CLOCKHOUR_OF_DAY_TYPE = new StandardDateTimeFieldType("clockhourOfDay", (byte)16, DurationFieldType.hours(), DurationFieldType.days());
/*     */   
/*     */ 
/* 122 */   private static final DateTimeFieldType HOUR_OF_DAY_TYPE = new StandardDateTimeFieldType("hourOfDay", (byte)17, DurationFieldType.hours(), DurationFieldType.days());
/*     */   
/*     */ 
/* 125 */   private static final DateTimeFieldType MINUTE_OF_DAY_TYPE = new StandardDateTimeFieldType("minuteOfDay", (byte)18, DurationFieldType.minutes(), DurationFieldType.days());
/*     */   
/*     */ 
/* 128 */   private static final DateTimeFieldType MINUTE_OF_HOUR_TYPE = new StandardDateTimeFieldType("minuteOfHour", (byte)19, DurationFieldType.minutes(), DurationFieldType.hours());
/*     */   
/*     */ 
/* 131 */   private static final DateTimeFieldType SECOND_OF_DAY_TYPE = new StandardDateTimeFieldType("secondOfDay", (byte)20, DurationFieldType.seconds(), DurationFieldType.days());
/*     */   
/*     */ 
/* 134 */   private static final DateTimeFieldType SECOND_OF_MINUTE_TYPE = new StandardDateTimeFieldType("secondOfMinute", (byte)21, DurationFieldType.seconds(), DurationFieldType.minutes());
/*     */   
/*     */ 
/* 137 */   private static final DateTimeFieldType MILLIS_OF_DAY_TYPE = new StandardDateTimeFieldType("millisOfDay", (byte)22, DurationFieldType.millis(), DurationFieldType.days());
/*     */   
/*     */ 
/* 140 */   private static final DateTimeFieldType MILLIS_OF_SECOND_TYPE = new StandardDateTimeFieldType("millisOfSecond", (byte)23, DurationFieldType.millis(), DurationFieldType.seconds());
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private final String iName;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected DateTimeFieldType(String paramString)
/*     */   {
/* 154 */     this.iName = paramString;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static DateTimeFieldType millisOfSecond()
/*     */   {
/* 164 */     return MILLIS_OF_SECOND_TYPE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static DateTimeFieldType millisOfDay()
/*     */   {
/* 177 */     return MILLIS_OF_DAY_TYPE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static DateTimeFieldType secondOfMinute()
/*     */   {
/* 186 */     return SECOND_OF_MINUTE_TYPE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static DateTimeFieldType secondOfDay()
/*     */   {
/* 199 */     return SECOND_OF_DAY_TYPE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static DateTimeFieldType minuteOfHour()
/*     */   {
/* 208 */     return MINUTE_OF_HOUR_TYPE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static DateTimeFieldType minuteOfDay()
/*     */   {
/* 221 */     return MINUTE_OF_DAY_TYPE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static DateTimeFieldType hourOfDay()
/*     */   {
/* 230 */     return HOUR_OF_DAY_TYPE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static DateTimeFieldType clockhourOfDay()
/*     */   {
/* 239 */     return CLOCKHOUR_OF_DAY_TYPE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static DateTimeFieldType hourOfHalfday()
/*     */   {
/* 248 */     return HOUR_OF_HALFDAY_TYPE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static DateTimeFieldType clockhourOfHalfday()
/*     */   {
/* 257 */     return CLOCKHOUR_OF_HALFDAY_TYPE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static DateTimeFieldType halfdayOfDay()
/*     */   {
/* 266 */     return HALFDAY_OF_DAY_TYPE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static DateTimeFieldType dayOfWeek()
/*     */   {
/* 276 */     return DAY_OF_WEEK_TYPE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static DateTimeFieldType dayOfMonth()
/*     */   {
/* 285 */     return DAY_OF_MONTH_TYPE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static DateTimeFieldType dayOfYear()
/*     */   {
/* 294 */     return DAY_OF_YEAR_TYPE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static DateTimeFieldType weekOfWeekyear()
/*     */   {
/* 303 */     return WEEK_OF_WEEKYEAR_TYPE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static DateTimeFieldType weekyear()
/*     */   {
/* 312 */     return WEEKYEAR_TYPE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static DateTimeFieldType weekyearOfCentury()
/*     */   {
/* 321 */     return WEEKYEAR_OF_CENTURY_TYPE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static DateTimeFieldType monthOfYear()
/*     */   {
/* 330 */     return MONTH_OF_YEAR_TYPE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static DateTimeFieldType year()
/*     */   {
/* 339 */     return YEAR_TYPE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static DateTimeFieldType yearOfEra()
/*     */   {
/* 348 */     return YEAR_OF_ERA_TYPE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static DateTimeFieldType yearOfCentury()
/*     */   {
/* 357 */     return YEAR_OF_CENTURY_TYPE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static DateTimeFieldType centuryOfEra()
/*     */   {
/* 366 */     return CENTURY_OF_ERA_TYPE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static DateTimeFieldType era()
/*     */   {
/* 375 */     return ERA_TYPE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getName()
/*     */   {
/* 390 */     return this.iName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public abstract DurationFieldType getDurationType();
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public abstract DurationFieldType getRangeDurationType();
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public abstract DateTimeField getField(Chronology paramChronology);
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean isSupported(Chronology paramChronology)
/*     */   {
/* 422 */     return getField(paramChronology).isSupported();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String toString()
/*     */   {
/* 431 */     return getName();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private static class StandardDateTimeFieldType
/*     */     extends DateTimeFieldType
/*     */   {
/*     */     private static final long serialVersionUID = -9937958251642L;
/*     */     
/*     */ 
/*     */ 
/*     */     private final byte iOrdinal;
/*     */     
/*     */ 
/*     */     private final transient DurationFieldType iUnitType;
/*     */     
/*     */ 
/*     */     private final transient DurationFieldType iRangeType;
/*     */     
/*     */ 
/*     */ 
/*     */     StandardDateTimeFieldType(String paramString, byte paramByte, DurationFieldType paramDurationFieldType1, DurationFieldType paramDurationFieldType2)
/*     */     {
/* 456 */       super();
/* 457 */       this.iOrdinal = paramByte;
/* 458 */       this.iUnitType = paramDurationFieldType1;
/* 459 */       this.iRangeType = paramDurationFieldType2;
/*     */     }
/*     */     
/*     */     public DurationFieldType getDurationType()
/*     */     {
/* 464 */       return this.iUnitType;
/*     */     }
/*     */     
/*     */     public DurationFieldType getRangeDurationType()
/*     */     {
/* 469 */       return this.iRangeType;
/*     */     }
/*     */     
/*     */ 
/*     */     public boolean equals(Object paramObject)
/*     */     {
/* 475 */       if (this == paramObject) {
/* 476 */         return true;
/*     */       }
/* 478 */       if ((paramObject instanceof StandardDateTimeFieldType)) {
/* 479 */         return this.iOrdinal == ((StandardDateTimeFieldType)paramObject).iOrdinal;
/*     */       }
/* 481 */       return false;
/*     */     }
/*     */     
/*     */ 
/*     */     public int hashCode()
/*     */     {
/* 487 */       return 1 << this.iOrdinal;
/*     */     }
/*     */     
/*     */     public DateTimeField getField(Chronology paramChronology)
/*     */     {
/* 492 */       paramChronology = DateTimeUtils.getChronology(paramChronology);
/*     */       
/* 494 */       switch (this.iOrdinal) {
/*     */       case 1: 
/* 496 */         return paramChronology.era();
/*     */       case 2: 
/* 498 */         return paramChronology.yearOfEra();
/*     */       case 3: 
/* 500 */         return paramChronology.centuryOfEra();
/*     */       case 4: 
/* 502 */         return paramChronology.yearOfCentury();
/*     */       case 5: 
/* 504 */         return paramChronology.year();
/*     */       case 6: 
/* 506 */         return paramChronology.dayOfYear();
/*     */       case 7: 
/* 508 */         return paramChronology.monthOfYear();
/*     */       case 8: 
/* 510 */         return paramChronology.dayOfMonth();
/*     */       case 9: 
/* 512 */         return paramChronology.weekyearOfCentury();
/*     */       case 10: 
/* 514 */         return paramChronology.weekyear();
/*     */       case 11: 
/* 516 */         return paramChronology.weekOfWeekyear();
/*     */       case 12: 
/* 518 */         return paramChronology.dayOfWeek();
/*     */       case 13: 
/* 520 */         return paramChronology.halfdayOfDay();
/*     */       case 14: 
/* 522 */         return paramChronology.hourOfHalfday();
/*     */       case 15: 
/* 524 */         return paramChronology.clockhourOfHalfday();
/*     */       case 16: 
/* 526 */         return paramChronology.clockhourOfDay();
/*     */       case 17: 
/* 528 */         return paramChronology.hourOfDay();
/*     */       case 18: 
/* 530 */         return paramChronology.minuteOfDay();
/*     */       case 19: 
/* 532 */         return paramChronology.minuteOfHour();
/*     */       case 20: 
/* 534 */         return paramChronology.secondOfDay();
/*     */       case 21: 
/* 536 */         return paramChronology.secondOfMinute();
/*     */       case 22: 
/* 538 */         return paramChronology.millisOfDay();
/*     */       case 23: 
/* 540 */         return paramChronology.millisOfSecond();
/*     */       }
/*     */       
/* 543 */       throw new InternalError();
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     private Object readResolve()
/*     */     {
/* 553 */       switch (this.iOrdinal) {
/*     */       case 1: 
/* 555 */         return DateTimeFieldType.ERA_TYPE;
/*     */       case 2: 
/* 557 */         return DateTimeFieldType.YEAR_OF_ERA_TYPE;
/*     */       case 3: 
/* 559 */         return DateTimeFieldType.CENTURY_OF_ERA_TYPE;
/*     */       case 4: 
/* 561 */         return DateTimeFieldType.YEAR_OF_CENTURY_TYPE;
/*     */       case 5: 
/* 563 */         return DateTimeFieldType.YEAR_TYPE;
/*     */       case 6: 
/* 565 */         return DateTimeFieldType.DAY_OF_YEAR_TYPE;
/*     */       case 7: 
/* 567 */         return DateTimeFieldType.MONTH_OF_YEAR_TYPE;
/*     */       case 8: 
/* 569 */         return DateTimeFieldType.DAY_OF_MONTH_TYPE;
/*     */       case 9: 
/* 571 */         return DateTimeFieldType.WEEKYEAR_OF_CENTURY_TYPE;
/*     */       case 10: 
/* 573 */         return DateTimeFieldType.WEEKYEAR_TYPE;
/*     */       case 11: 
/* 575 */         return DateTimeFieldType.WEEK_OF_WEEKYEAR_TYPE;
/*     */       case 12: 
/* 577 */         return DateTimeFieldType.DAY_OF_WEEK_TYPE;
/*     */       case 13: 
/* 579 */         return DateTimeFieldType.HALFDAY_OF_DAY_TYPE;
/*     */       case 14: 
/* 581 */         return DateTimeFieldType.HOUR_OF_HALFDAY_TYPE;
/*     */       case 15: 
/* 583 */         return DateTimeFieldType.CLOCKHOUR_OF_HALFDAY_TYPE;
/*     */       case 16: 
/* 585 */         return DateTimeFieldType.CLOCKHOUR_OF_DAY_TYPE;
/*     */       case 17: 
/* 587 */         return DateTimeFieldType.HOUR_OF_DAY_TYPE;
/*     */       case 18: 
/* 589 */         return DateTimeFieldType.MINUTE_OF_DAY_TYPE;
/*     */       case 19: 
/* 591 */         return DateTimeFieldType.MINUTE_OF_HOUR_TYPE;
/*     */       case 20: 
/* 593 */         return DateTimeFieldType.SECOND_OF_DAY_TYPE;
/*     */       case 21: 
/* 595 */         return DateTimeFieldType.SECOND_OF_MINUTE_TYPE;
/*     */       case 22: 
/* 597 */         return DateTimeFieldType.MILLIS_OF_DAY_TYPE;
/*     */       case 23: 
/* 599 */         return DateTimeFieldType.MILLIS_OF_SECOND_TYPE;
/*     */       }
/*     */       
/* 602 */       return this;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\org\joda\time\DateTimeFieldType.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */