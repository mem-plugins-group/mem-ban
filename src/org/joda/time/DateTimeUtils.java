/*     */ package org.joda.time;
/*     */ 
/*     */ import java.lang.reflect.Method;
/*     */ import java.text.DateFormatSymbols;
/*     */ import java.util.Collections;
/*     */ import java.util.HashMap;
/*     */ import java.util.LinkedHashMap;
/*     */ import java.util.Locale;
/*     */ import java.util.Map;
/*     */ import java.util.concurrent.atomic.AtomicReference;
/*     */ import org.joda.time.chrono.ISOChronology;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class DateTimeUtils
/*     */ {
/*  44 */   public static final MillisProvider SYSTEM_MILLIS_PROVIDER = new SystemMillisProvider();
/*     */   
/*     */ 
/*  47 */   private static volatile MillisProvider cMillisProvider = SYSTEM_MILLIS_PROVIDER;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*  52 */   private static final AtomicReference<Map<String, DateTimeZone>> cZoneNames = new AtomicReference();
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static final long currentTimeMillis()
/*     */   {
/*  72 */     return cMillisProvider.getMillis();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static final void setCurrentMillisSystem()
/*     */     throws SecurityException
/*     */   {
/*  84 */     checkPermission();
/*  85 */     cMillisProvider = SYSTEM_MILLIS_PROVIDER;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static final void setCurrentMillisFixed(long paramLong)
/*     */     throws SecurityException
/*     */   {
/*  98 */     checkPermission();
/*  99 */     cMillisProvider = new FixedMillisProvider(paramLong);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static final void setCurrentMillisOffset(long paramLong)
/*     */     throws SecurityException
/*     */   {
/*     */     
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 114 */     if (paramLong == 0L) {
/* 115 */       cMillisProvider = SYSTEM_MILLIS_PROVIDER;
/*     */     } else {
/* 117 */       cMillisProvider = new OffsetMillisProvider(paramLong);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static final void setCurrentMillisProvider(MillisProvider paramMillisProvider)
/*     */     throws SecurityException
/*     */   {
/* 132 */     if (paramMillisProvider == null) {
/* 133 */       throw new IllegalArgumentException("The MillisProvider must not be null");
/*     */     }
/* 135 */     checkPermission();
/* 136 */     cMillisProvider = paramMillisProvider;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void checkPermission()
/*     */     throws SecurityException
/*     */   {
/* 145 */     SecurityManager localSecurityManager = System.getSecurityManager();
/* 146 */     if (localSecurityManager != null) {
/* 147 */       localSecurityManager.checkPermission(new JodaTimePermission("CurrentTime.setProvider"));
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static final long getInstantMillis(ReadableInstant paramReadableInstant)
/*     */   {
/* 162 */     if (paramReadableInstant == null) {
/* 163 */       return currentTimeMillis();
/*     */     }
/* 165 */     return paramReadableInstant.getMillis();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static final Chronology getInstantChronology(ReadableInstant paramReadableInstant)
/*     */   {
/* 180 */     if (paramReadableInstant == null) {
/* 181 */       return ISOChronology.getInstance();
/*     */     }
/* 183 */     Chronology localChronology = paramReadableInstant.getChronology();
/* 184 */     if (localChronology == null) {
/* 185 */       return ISOChronology.getInstance();
/*     */     }
/* 187 */     return localChronology;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static final Chronology getIntervalChronology(ReadableInstant paramReadableInstant1, ReadableInstant paramReadableInstant2)
/*     */   {
/* 203 */     Object localObject = null;
/* 204 */     if (paramReadableInstant1 != null) {
/* 205 */       localObject = paramReadableInstant1.getChronology();
/* 206 */     } else if (paramReadableInstant2 != null) {
/* 207 */       localObject = paramReadableInstant2.getChronology();
/*     */     }
/* 209 */     if (localObject == null) {
/* 210 */       localObject = ISOChronology.getInstance();
/*     */     }
/* 212 */     return (Chronology)localObject;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static final Chronology getIntervalChronology(ReadableInterval paramReadableInterval)
/*     */   {
/* 227 */     if (paramReadableInterval == null) {
/* 228 */       return ISOChronology.getInstance();
/*     */     }
/* 230 */     Chronology localChronology = paramReadableInterval.getChronology();
/* 231 */     if (localChronology == null) {
/* 232 */       return ISOChronology.getInstance();
/*     */     }
/* 234 */     return localChronology;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static final ReadableInterval getReadableInterval(ReadableInterval paramReadableInterval)
/*     */   {
/* 250 */     if (paramReadableInterval == null) {
/* 251 */       long l = currentTimeMillis();
/* 252 */       paramReadableInterval = new Interval(l, l);
/*     */     }
/* 254 */     return paramReadableInterval;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static final Chronology getChronology(Chronology paramChronology)
/*     */   {
/* 268 */     if (paramChronology == null) {
/* 269 */       return ISOChronology.getInstance();
/*     */     }
/* 271 */     return paramChronology;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static final DateTimeZone getZone(DateTimeZone paramDateTimeZone)
/*     */   {
/* 285 */     if (paramDateTimeZone == null) {
/* 286 */       return DateTimeZone.getDefault();
/*     */     }
/* 288 */     return paramDateTimeZone;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static final PeriodType getPeriodType(PeriodType paramPeriodType)
/*     */   {
/* 302 */     if (paramPeriodType == null) {
/* 303 */       return PeriodType.standard();
/*     */     }
/* 305 */     return paramPeriodType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static final long getDurationMillis(ReadableDuration paramReadableDuration)
/*     */   {
/* 319 */     if (paramReadableDuration == null) {
/* 320 */       return 0L;
/*     */     }
/* 322 */     return paramReadableDuration.getMillis();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static final boolean isContiguous(ReadablePartial paramReadablePartial)
/*     */   {
/* 350 */     if (paramReadablePartial == null) {
/* 351 */       throw new IllegalArgumentException("Partial must not be null");
/*     */     }
/* 353 */     DurationFieldType localDurationFieldType = null;
/* 354 */     for (int i = 0; i < paramReadablePartial.size(); i++) {
/* 355 */       DateTimeField localDateTimeField = paramReadablePartial.getField(i);
/* 356 */       if ((i > 0) && (
/* 357 */         (localDateTimeField.getRangeDurationField() == null) || (localDateTimeField.getRangeDurationField().getType() != localDurationFieldType))) {
/* 358 */         return false;
/*     */       }
/*     */       
/* 361 */       localDurationFieldType = localDateTimeField.getDurationField().getType();
/*     */     }
/* 363 */     return true;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static final DateFormatSymbols getDateFormatSymbols(Locale paramLocale)
/*     */   {
/*     */     try
/*     */     {
/* 381 */       Method localMethod = DateFormatSymbols.class.getMethod("getInstance", new Class[] { Locale.class });
/* 382 */       return (DateFormatSymbols)localMethod.invoke(null, new Object[] { paramLocale });
/*     */     } catch (Exception localException) {}
/* 384 */     return new DateFormatSymbols(paramLocale);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static final Map<String, DateTimeZone> getDefaultTimeZoneNames()
/*     */   {
/* 413 */     Map localMap = (Map)cZoneNames.get();
/* 414 */     if (localMap == null) {
/* 415 */       localMap = buildDefaultTimeZoneNames();
/* 416 */       if (!cZoneNames.compareAndSet(null, localMap)) {
/* 417 */         localMap = (Map)cZoneNames.get();
/*     */       }
/*     */     }
/* 420 */     return localMap;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static final void setDefaultTimeZoneNames(Map<String, DateTimeZone> paramMap)
/*     */   {
/* 432 */     cZoneNames.set(Collections.unmodifiableMap(new HashMap(paramMap)));
/*     */   }
/*     */   
/*     */ 
/*     */   private static Map<String, DateTimeZone> buildDefaultTimeZoneNames()
/*     */   {
/* 438 */     LinkedHashMap localLinkedHashMap = new LinkedHashMap();
/* 439 */     localLinkedHashMap.put("UT", DateTimeZone.UTC);
/* 440 */     localLinkedHashMap.put("UTC", DateTimeZone.UTC);
/* 441 */     localLinkedHashMap.put("GMT", DateTimeZone.UTC);
/* 442 */     put(localLinkedHashMap, "EST", "America/New_York");
/* 443 */     put(localLinkedHashMap, "EDT", "America/New_York");
/* 444 */     put(localLinkedHashMap, "CST", "America/Chicago");
/* 445 */     put(localLinkedHashMap, "CDT", "America/Chicago");
/* 446 */     put(localLinkedHashMap, "MST", "America/Denver");
/* 447 */     put(localLinkedHashMap, "MDT", "America/Denver");
/* 448 */     put(localLinkedHashMap, "PST", "America/Los_Angeles");
/* 449 */     put(localLinkedHashMap, "PDT", "America/Los_Angeles");
/* 450 */     return Collections.unmodifiableMap(localLinkedHashMap);
/*     */   }
/*     */   
/*     */   private static void put(Map<String, DateTimeZone> paramMap, String paramString1, String paramString2) {
/* 454 */     try { paramMap.put(paramString1, DateTimeZone.forID(paramString2));
/*     */     }
/*     */     catch (RuntimeException localRuntimeException) {}
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static final double toJulianDay(long paramLong)
/*     */   {
/* 483 */     double d = paramLong / 8.64E7D;
/* 484 */     return d + 2440587.5D;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static final long toJulianDayNumber(long paramLong)
/*     */   {
/* 503 */     return Math.floor(toJulianDay(paramLong) + 0.5D);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static final long fromJulianDay(double paramDouble)
/*     */   {
/* 516 */     double d = paramDouble - 2440587.5D;
/* 517 */     return (d * 8.64E7D);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static abstract interface MillisProvider
/*     */   {
/*     */     public abstract long getMillis();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   static class SystemMillisProvider
/*     */     implements DateTimeUtils.MillisProvider
/*     */   {
/*     */     public long getMillis()
/*     */     {
/* 547 */       return System.currentTimeMillis();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   static class FixedMillisProvider
/*     */     implements DateTimeUtils.MillisProvider
/*     */   {
/*     */     private final long iMillis;
/*     */     
/*     */ 
/*     */ 
/*     */     FixedMillisProvider(long paramLong)
/*     */     {
/* 563 */       this.iMillis = paramLong;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */     public long getMillis()
/*     */     {
/* 571 */       return this.iMillis;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   static class OffsetMillisProvider
/*     */     implements DateTimeUtils.MillisProvider
/*     */   {
/*     */     private final long iMillis;
/*     */     
/*     */ 
/*     */ 
/*     */     OffsetMillisProvider(long paramLong)
/*     */     {
/* 587 */       this.iMillis = paramLong;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */     public long getMillis()
/*     */     {
/* 595 */       return System.currentTimeMillis() + this.iMillis;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\org\joda\time\DateTimeUtils.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */