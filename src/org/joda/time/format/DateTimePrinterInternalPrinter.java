/*    */ package org.joda.time.format;
/*    */ 
/*    */ import java.io.IOException;
/*    */ import java.io.Writer;
/*    */ import java.util.Locale;
/*    */ import org.joda.time.Chronology;
/*    */ import org.joda.time.DateTimeZone;
/*    */ import org.joda.time.ReadablePartial;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ class DateTimePrinterInternalPrinter
/*    */   implements InternalPrinter
/*    */ {
/*    */   private final DateTimePrinter underlying;
/*    */   
/*    */   static InternalPrinter of(DateTimePrinter paramDateTimePrinter)
/*    */   {
/* 37 */     if ((paramDateTimePrinter instanceof InternalPrinterDateTimePrinter)) {
/* 38 */       return (InternalPrinter)paramDateTimePrinter;
/*    */     }
/* 40 */     if (paramDateTimePrinter == null) {
/* 41 */       return null;
/*    */     }
/* 43 */     return new DateTimePrinterInternalPrinter(paramDateTimePrinter);
/*    */   }
/*    */   
/*    */   private DateTimePrinterInternalPrinter(DateTimePrinter paramDateTimePrinter) {
/* 47 */     this.underlying = paramDateTimePrinter;
/*    */   }
/*    */   
/*    */   DateTimePrinter getUnderlying()
/*    */   {
/* 52 */     return this.underlying;
/*    */   }
/*    */   
/*    */   public int estimatePrintedLength()
/*    */   {
/* 57 */     return this.underlying.estimatePrintedLength();
/*    */   }
/*    */   
/*    */   public void printTo(Appendable paramAppendable, long paramLong, Chronology paramChronology, int paramInt, DateTimeZone paramDateTimeZone, Locale paramLocale) throws IOException {
/*    */     Object localObject;
/* 62 */     if ((paramAppendable instanceof StringBuffer)) {
/* 63 */       localObject = (StringBuffer)paramAppendable;
/* 64 */       this.underlying.printTo((StringBuffer)localObject, paramLong, paramChronology, paramInt, paramDateTimeZone, paramLocale);
/* 65 */     } else if ((paramAppendable instanceof Writer)) {
/* 66 */       localObject = (Writer)paramAppendable;
/* 67 */       this.underlying.printTo((Writer)localObject, paramLong, paramChronology, paramInt, paramDateTimeZone, paramLocale);
/*    */     } else {
/* 69 */       localObject = new StringBuffer(estimatePrintedLength());
/* 70 */       this.underlying.printTo((StringBuffer)localObject, paramLong, paramChronology, paramInt, paramDateTimeZone, paramLocale);
/* 71 */       paramAppendable.append((CharSequence)localObject);
/*    */     }
/*    */   }
/*    */   
/*    */   public void printTo(Appendable paramAppendable, ReadablePartial paramReadablePartial, Locale paramLocale) throws IOException { Object localObject;
/* 76 */     if ((paramAppendable instanceof StringBuffer)) {
/* 77 */       localObject = (StringBuffer)paramAppendable;
/* 78 */       this.underlying.printTo((StringBuffer)localObject, paramReadablePartial, paramLocale);
/* 79 */     } else if ((paramAppendable instanceof Writer)) {
/* 80 */       localObject = (Writer)paramAppendable;
/* 81 */       this.underlying.printTo((Writer)localObject, paramReadablePartial, paramLocale);
/*    */     } else {
/* 83 */       localObject = new StringBuffer(estimatePrintedLength());
/* 84 */       this.underlying.printTo((StringBuffer)localObject, paramReadablePartial, paramLocale);
/* 85 */       paramAppendable.append((CharSequence)localObject);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\org\joda\time\format\DateTimePrinterInternalPrinter.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */