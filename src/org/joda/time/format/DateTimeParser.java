package org.joda.time.format;

public abstract interface DateTimeParser
{
  public abstract int estimateParsedLength();
  
  public abstract int parseInto(DateTimeParserBucket paramDateTimeParserBucket, String paramString, int paramInt);
}


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\org\joda\time\format\DateTimeParser.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */