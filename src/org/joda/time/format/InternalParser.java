package org.joda.time.format;

abstract interface InternalParser
{
  public abstract int estimateParsedLength();
  
  public abstract int parseInto(DateTimeParserBucket paramDateTimeParserBucket, CharSequence paramCharSequence, int paramInt);
}


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\org\joda\time\format\InternalParser.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */