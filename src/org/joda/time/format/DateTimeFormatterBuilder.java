/*      */ package org.joda.time.format;
/*      */ 
/*      */ import java.io.IOException;
/*      */ import java.util.ArrayList;
/*      */ import java.util.Collections;
/*      */ import java.util.HashMap;
/*      */ import java.util.List;
/*      */ import java.util.Locale;
/*      */ import java.util.Map;
/*      */ import java.util.concurrent.ConcurrentHashMap;
/*      */ import org.joda.time.Chronology;
/*      */ import org.joda.time.DateTimeField;
/*      */ import org.joda.time.DateTimeFieldType;
/*      */ import org.joda.time.DateTimeUtils;
/*      */ import org.joda.time.DateTimeZone;
/*      */ import org.joda.time.DurationField;
/*      */ import org.joda.time.MutableDateTime;
/*      */ import org.joda.time.MutableDateTime.Property;
/*      */ import org.joda.time.ReadablePartial;
/*      */ import org.joda.time.field.MillisDurationField;
/*      */ import org.joda.time.field.PreciseDateTimeField;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public class DateTimeFormatterBuilder
/*      */ {
/*      */   private ArrayList<Object> iElementPairs;
/*      */   private Object iFormatter;
/*      */   
/*      */   public DateTimeFormatterBuilder()
/*      */   {
/*   84 */     this.iElementPairs = new ArrayList();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatter toFormatter()
/*      */   {
/*  104 */     Object localObject = getFormatter();
/*  105 */     InternalPrinter localInternalPrinter = null;
/*  106 */     if (isPrinter(localObject)) {
/*  107 */       localInternalPrinter = (InternalPrinter)localObject;
/*      */     }
/*  109 */     InternalParser localInternalParser = null;
/*  110 */     if (isParser(localObject)) {
/*  111 */       localInternalParser = (InternalParser)localObject;
/*      */     }
/*  113 */     if ((localInternalPrinter != null) || (localInternalParser != null)) {
/*  114 */       return new DateTimeFormatter(localInternalPrinter, localInternalParser);
/*      */     }
/*  116 */     throw new UnsupportedOperationException("Both printing and parsing not supported");
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimePrinter toPrinter()
/*      */   {
/*  132 */     Object localObject = getFormatter();
/*  133 */     if (isPrinter(localObject)) {
/*  134 */       InternalPrinter localInternalPrinter = (InternalPrinter)localObject;
/*  135 */       return InternalPrinterDateTimePrinter.of(localInternalPrinter);
/*      */     }
/*  137 */     throw new UnsupportedOperationException("Printing is not supported");
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeParser toParser()
/*      */   {
/*  153 */     Object localObject = getFormatter();
/*  154 */     if (isParser(localObject)) {
/*  155 */       InternalParser localInternalParser = (InternalParser)localObject;
/*  156 */       return InternalParserDateTimeParser.of(localInternalParser);
/*      */     }
/*  158 */     throw new UnsupportedOperationException("Parsing is not supported");
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean canBuildFormatter()
/*      */   {
/*  169 */     return isFormatter(getFormatter());
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean canBuildPrinter()
/*      */   {
/*  179 */     return isPrinter(getFormatter());
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean canBuildParser()
/*      */   {
/*  189 */     return isParser(getFormatter());
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void clear()
/*      */   {
/*  198 */     this.iFormatter = null;
/*  199 */     this.iElementPairs.clear();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder append(DateTimeFormatter paramDateTimeFormatter)
/*      */   {
/*  218 */     if (paramDateTimeFormatter == null) {
/*  219 */       throw new IllegalArgumentException("No formatter supplied");
/*      */     }
/*  221 */     return append0(paramDateTimeFormatter.getPrinter0(), paramDateTimeFormatter.getParser0());
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder append(DateTimePrinter paramDateTimePrinter)
/*      */   {
/*  239 */     checkPrinter(paramDateTimePrinter);
/*  240 */     return append0(DateTimePrinterInternalPrinter.of(paramDateTimePrinter), null);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder append(DateTimeParser paramDateTimeParser)
/*      */   {
/*  258 */     checkParser(paramDateTimeParser);
/*  259 */     return append0(null, DateTimeParserInternalParser.of(paramDateTimeParser));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder append(DateTimePrinter paramDateTimePrinter, DateTimeParser paramDateTimeParser)
/*      */   {
/*  277 */     checkPrinter(paramDateTimePrinter);
/*  278 */     checkParser(paramDateTimeParser);
/*  279 */     return append0(DateTimePrinterInternalPrinter.of(paramDateTimePrinter), DateTimeParserInternalParser.of(paramDateTimeParser));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder append(DateTimePrinter paramDateTimePrinter, DateTimeParser[] paramArrayOfDateTimeParser)
/*      */   {
/*  306 */     if (paramDateTimePrinter != null) {
/*  307 */       checkPrinter(paramDateTimePrinter);
/*      */     }
/*  309 */     if (paramArrayOfDateTimeParser == null) {
/*  310 */       throw new IllegalArgumentException("No parsers supplied");
/*      */     }
/*  312 */     int i = paramArrayOfDateTimeParser.length;
/*  313 */     if (i == 1) {
/*  314 */       if (paramArrayOfDateTimeParser[0] == null) {
/*  315 */         throw new IllegalArgumentException("No parser supplied");
/*      */       }
/*  317 */       return append0(DateTimePrinterInternalPrinter.of(paramDateTimePrinter), DateTimeParserInternalParser.of(paramArrayOfDateTimeParser[0]));
/*      */     }
/*      */     
/*  320 */     InternalParser[] arrayOfInternalParser = new InternalParser[i];
/*      */     
/*  322 */     for (int j = 0; j < i - 1; j++) {
/*  323 */       if ((arrayOfInternalParser[j] = DateTimeParserInternalParser.of(paramArrayOfDateTimeParser[j])) == null) {
/*  324 */         throw new IllegalArgumentException("Incomplete parser array");
/*      */       }
/*      */     }
/*  327 */     arrayOfInternalParser[j] = DateTimeParserInternalParser.of(paramArrayOfDateTimeParser[j]);
/*      */     
/*  329 */     return append0(DateTimePrinterInternalPrinter.of(paramDateTimePrinter), new MatchingParser(arrayOfInternalParser));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendOptional(DateTimeParser paramDateTimeParser)
/*      */   {
/*  346 */     checkParser(paramDateTimeParser);
/*  347 */     InternalParser[] arrayOfInternalParser = { DateTimeParserInternalParser.of(paramDateTimeParser), null };
/*  348 */     return append0(null, new MatchingParser(arrayOfInternalParser));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void checkParser(DateTimeParser paramDateTimeParser)
/*      */   {
/*  358 */     if (paramDateTimeParser == null) {
/*  359 */       throw new IllegalArgumentException("No parser supplied");
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void checkPrinter(DateTimePrinter paramDateTimePrinter)
/*      */   {
/*  369 */     if (paramDateTimePrinter == null) {
/*  370 */       throw new IllegalArgumentException("No printer supplied");
/*      */     }
/*      */   }
/*      */   
/*      */   private DateTimeFormatterBuilder append0(Object paramObject) {
/*  375 */     this.iFormatter = null;
/*      */     
/*  377 */     this.iElementPairs.add(paramObject);
/*  378 */     this.iElementPairs.add(paramObject);
/*  379 */     return this;
/*      */   }
/*      */   
/*      */   private DateTimeFormatterBuilder append0(InternalPrinter paramInternalPrinter, InternalParser paramInternalParser)
/*      */   {
/*  384 */     this.iFormatter = null;
/*  385 */     this.iElementPairs.add(paramInternalPrinter);
/*  386 */     this.iElementPairs.add(paramInternalParser);
/*  387 */     return this;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendLiteral(char paramChar)
/*      */   {
/*  398 */     return append0(new CharacterLiteral(paramChar));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendLiteral(String paramString)
/*      */   {
/*  409 */     if (paramString == null) {
/*  410 */       throw new IllegalArgumentException("Literal must not be null");
/*      */     }
/*  412 */     switch (paramString.length()) {
/*      */     case 0: 
/*  414 */       return this;
/*      */     case 1: 
/*  416 */       return append0(new CharacterLiteral(paramString.charAt(0)));
/*      */     }
/*  418 */     return append0(new StringLiteral(paramString));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendDecimal(DateTimeFieldType paramDateTimeFieldType, int paramInt1, int paramInt2)
/*      */   {
/*  435 */     if (paramDateTimeFieldType == null) {
/*  436 */       throw new IllegalArgumentException("Field type must not be null");
/*      */     }
/*  438 */     if (paramInt2 < paramInt1) {
/*  439 */       paramInt2 = paramInt1;
/*      */     }
/*  441 */     if ((paramInt1 < 0) || (paramInt2 <= 0)) {
/*  442 */       throw new IllegalArgumentException();
/*      */     }
/*  444 */     if (paramInt1 <= 1) {
/*  445 */       return append0(new UnpaddedNumber(paramDateTimeFieldType, paramInt2, false));
/*      */     }
/*  447 */     return append0(new PaddedNumber(paramDateTimeFieldType, paramInt2, false, paramInt1));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendFixedDecimal(DateTimeFieldType paramDateTimeFieldType, int paramInt)
/*      */   {
/*  465 */     if (paramDateTimeFieldType == null) {
/*  466 */       throw new IllegalArgumentException("Field type must not be null");
/*      */     }
/*  468 */     if (paramInt <= 0) {
/*  469 */       throw new IllegalArgumentException("Illegal number of digits: " + paramInt);
/*      */     }
/*  471 */     return append0(new FixedNumber(paramDateTimeFieldType, paramInt, false));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendSignedDecimal(DateTimeFieldType paramDateTimeFieldType, int paramInt1, int paramInt2)
/*      */   {
/*  487 */     if (paramDateTimeFieldType == null) {
/*  488 */       throw new IllegalArgumentException("Field type must not be null");
/*      */     }
/*  490 */     if (paramInt2 < paramInt1) {
/*  491 */       paramInt2 = paramInt1;
/*      */     }
/*  493 */     if ((paramInt1 < 0) || (paramInt2 <= 0)) {
/*  494 */       throw new IllegalArgumentException();
/*      */     }
/*  496 */     if (paramInt1 <= 1) {
/*  497 */       return append0(new UnpaddedNumber(paramDateTimeFieldType, paramInt2, true));
/*      */     }
/*  499 */     return append0(new PaddedNumber(paramDateTimeFieldType, paramInt2, true, paramInt1));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendFixedSignedDecimal(DateTimeFieldType paramDateTimeFieldType, int paramInt)
/*      */   {
/*  517 */     if (paramDateTimeFieldType == null) {
/*  518 */       throw new IllegalArgumentException("Field type must not be null");
/*      */     }
/*  520 */     if (paramInt <= 0) {
/*  521 */       throw new IllegalArgumentException("Illegal number of digits: " + paramInt);
/*      */     }
/*  523 */     return append0(new FixedNumber(paramDateTimeFieldType, paramInt, true));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendText(DateTimeFieldType paramDateTimeFieldType)
/*      */   {
/*  535 */     if (paramDateTimeFieldType == null) {
/*  536 */       throw new IllegalArgumentException("Field type must not be null");
/*      */     }
/*  538 */     return append0(new TextField(paramDateTimeFieldType, false));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendShortText(DateTimeFieldType paramDateTimeFieldType)
/*      */   {
/*  550 */     if (paramDateTimeFieldType == null) {
/*  551 */       throw new IllegalArgumentException("Field type must not be null");
/*      */     }
/*  553 */     return append0(new TextField(paramDateTimeFieldType, true));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendFraction(DateTimeFieldType paramDateTimeFieldType, int paramInt1, int paramInt2)
/*      */   {
/*  571 */     if (paramDateTimeFieldType == null) {
/*  572 */       throw new IllegalArgumentException("Field type must not be null");
/*      */     }
/*  574 */     if (paramInt2 < paramInt1) {
/*  575 */       paramInt2 = paramInt1;
/*      */     }
/*  577 */     if ((paramInt1 < 0) || (paramInt2 <= 0)) {
/*  578 */       throw new IllegalArgumentException();
/*      */     }
/*  580 */     return append0(new Fraction(paramDateTimeFieldType, paramInt1, paramInt2));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendFractionOfSecond(int paramInt1, int paramInt2)
/*      */   {
/*  598 */     return appendFraction(DateTimeFieldType.secondOfDay(), paramInt1, paramInt2);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendFractionOfMinute(int paramInt1, int paramInt2)
/*      */   {
/*  615 */     return appendFraction(DateTimeFieldType.minuteOfDay(), paramInt1, paramInt2);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendFractionOfHour(int paramInt1, int paramInt2)
/*      */   {
/*  632 */     return appendFraction(DateTimeFieldType.hourOfDay(), paramInt1, paramInt2);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendFractionOfDay(int paramInt1, int paramInt2)
/*      */   {
/*  649 */     return appendFraction(DateTimeFieldType.dayOfYear(), paramInt1, paramInt2);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendMillisOfSecond(int paramInt)
/*      */   {
/*  666 */     return appendDecimal(DateTimeFieldType.millisOfSecond(), paramInt, 3);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendMillisOfDay(int paramInt)
/*      */   {
/*  676 */     return appendDecimal(DateTimeFieldType.millisOfDay(), paramInt, 8);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendSecondOfMinute(int paramInt)
/*      */   {
/*  686 */     return appendDecimal(DateTimeFieldType.secondOfMinute(), paramInt, 2);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendSecondOfDay(int paramInt)
/*      */   {
/*  696 */     return appendDecimal(DateTimeFieldType.secondOfDay(), paramInt, 5);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendMinuteOfHour(int paramInt)
/*      */   {
/*  706 */     return appendDecimal(DateTimeFieldType.minuteOfHour(), paramInt, 2);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendMinuteOfDay(int paramInt)
/*      */   {
/*  716 */     return appendDecimal(DateTimeFieldType.minuteOfDay(), paramInt, 4);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendHourOfDay(int paramInt)
/*      */   {
/*  726 */     return appendDecimal(DateTimeFieldType.hourOfDay(), paramInt, 2);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendClockhourOfDay(int paramInt)
/*      */   {
/*  736 */     return appendDecimal(DateTimeFieldType.clockhourOfDay(), paramInt, 2);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendHourOfHalfday(int paramInt)
/*      */   {
/*  746 */     return appendDecimal(DateTimeFieldType.hourOfHalfday(), paramInt, 2);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendClockhourOfHalfday(int paramInt)
/*      */   {
/*  756 */     return appendDecimal(DateTimeFieldType.clockhourOfHalfday(), paramInt, 2);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendDayOfWeek(int paramInt)
/*      */   {
/*  766 */     return appendDecimal(DateTimeFieldType.dayOfWeek(), paramInt, 1);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendDayOfMonth(int paramInt)
/*      */   {
/*  776 */     return appendDecimal(DateTimeFieldType.dayOfMonth(), paramInt, 2);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendDayOfYear(int paramInt)
/*      */   {
/*  786 */     return appendDecimal(DateTimeFieldType.dayOfYear(), paramInt, 3);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendWeekOfWeekyear(int paramInt)
/*      */   {
/*  796 */     return appendDecimal(DateTimeFieldType.weekOfWeekyear(), paramInt, 2);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendWeekyear(int paramInt1, int paramInt2)
/*      */   {
/*  808 */     return appendSignedDecimal(DateTimeFieldType.weekyear(), paramInt1, paramInt2);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendMonthOfYear(int paramInt)
/*      */   {
/*  818 */     return appendDecimal(DateTimeFieldType.monthOfYear(), paramInt, 2);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendYear(int paramInt1, int paramInt2)
/*      */   {
/*  830 */     return appendSignedDecimal(DateTimeFieldType.year(), paramInt1, paramInt2);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendTwoDigitYear(int paramInt)
/*      */   {
/*  852 */     return appendTwoDigitYear(paramInt, false);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendTwoDigitYear(int paramInt, boolean paramBoolean)
/*      */   {
/*  870 */     return append0(new TwoDigitYear(DateTimeFieldType.year(), paramInt, paramBoolean));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendTwoDigitWeekyear(int paramInt)
/*      */   {
/*  892 */     return appendTwoDigitWeekyear(paramInt, false);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendTwoDigitWeekyear(int paramInt, boolean paramBoolean)
/*      */   {
/*  910 */     return append0(new TwoDigitYear(DateTimeFieldType.weekyear(), paramInt, paramBoolean));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendYearOfEra(int paramInt1, int paramInt2)
/*      */   {
/*  922 */     return appendDecimal(DateTimeFieldType.yearOfEra(), paramInt1, paramInt2);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendYearOfCentury(int paramInt1, int paramInt2)
/*      */   {
/*  934 */     return appendDecimal(DateTimeFieldType.yearOfCentury(), paramInt1, paramInt2);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendCenturyOfEra(int paramInt1, int paramInt2)
/*      */   {
/*  946 */     return appendSignedDecimal(DateTimeFieldType.centuryOfEra(), paramInt1, paramInt2);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendHalfdayOfDayText()
/*      */   {
/*  956 */     return appendText(DateTimeFieldType.halfdayOfDay());
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendDayOfWeekText()
/*      */   {
/*  966 */     return appendText(DateTimeFieldType.dayOfWeek());
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendDayOfWeekShortText()
/*      */   {
/*  977 */     return appendShortText(DateTimeFieldType.dayOfWeek());
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendMonthOfYearText()
/*      */   {
/*  988 */     return appendText(DateTimeFieldType.monthOfYear());
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendMonthOfYearShortText()
/*      */   {
/*  998 */     return appendShortText(DateTimeFieldType.monthOfYear());
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendEraText()
/*      */   {
/* 1008 */     return appendText(DateTimeFieldType.era());
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendTimeZoneName()
/*      */   {
/* 1019 */     return append0(new TimeZoneName(0, null), null);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendTimeZoneName(Map<String, DateTimeZone> paramMap)
/*      */   {
/* 1032 */     TimeZoneName localTimeZoneName = new TimeZoneName(0, paramMap);
/* 1033 */     return append0(localTimeZoneName, localTimeZoneName);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendTimeZoneShortName()
/*      */   {
/* 1044 */     return append0(new TimeZoneName(1, null), null);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendTimeZoneShortName(Map<String, DateTimeZone> paramMap)
/*      */   {
/* 1058 */     TimeZoneName localTimeZoneName = new TimeZoneName(1, paramMap);
/* 1059 */     return append0(localTimeZoneName, localTimeZoneName);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendTimeZoneId()
/*      */   {
/* 1069 */     return append0(TimeZoneId.INSTANCE, TimeZoneId.INSTANCE);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendTimeZoneOffset(String paramString, boolean paramBoolean, int paramInt1, int paramInt2)
/*      */   {
/* 1092 */     return append0(new TimeZoneOffset(paramString, paramString, paramBoolean, paramInt1, paramInt2));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendTimeZoneOffset(String paramString1, String paramString2, boolean paramBoolean, int paramInt1, int paramInt2)
/*      */   {
/* 1119 */     return append0(new TimeZoneOffset(paramString1, paramString2, paramBoolean, paramInt1, paramInt2));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeFormatterBuilder appendPattern(String paramString)
/*      */   {
/* 1133 */     DateTimeFormat.appendPatternTo(this, paramString);
/* 1134 */     return this;
/*      */   }
/*      */   
/*      */   private Object getFormatter()
/*      */   {
/* 1139 */     Object localObject1 = this.iFormatter;
/*      */     
/* 1141 */     if (localObject1 == null) {
/* 1142 */       if (this.iElementPairs.size() == 2) {
/* 1143 */         Object localObject2 = this.iElementPairs.get(0);
/* 1144 */         Object localObject3 = this.iElementPairs.get(1);
/*      */         
/* 1146 */         if (localObject2 != null) {
/* 1147 */           if ((localObject2 == localObject3) || (localObject3 == null)) {
/* 1148 */             localObject1 = localObject2;
/*      */           }
/*      */         } else {
/* 1151 */           localObject1 = localObject3;
/*      */         }
/*      */       }
/*      */       
/* 1155 */       if (localObject1 == null) {
/* 1156 */         localObject1 = new Composite(this.iElementPairs);
/*      */       }
/*      */       
/* 1159 */       this.iFormatter = localObject1;
/*      */     }
/*      */     
/* 1162 */     return localObject1;
/*      */   }
/*      */   
/*      */   private boolean isPrinter(Object paramObject) {
/* 1166 */     if ((paramObject instanceof InternalPrinter)) {
/* 1167 */       if ((paramObject instanceof Composite)) {
/* 1168 */         return ((Composite)paramObject).isPrinter();
/*      */       }
/* 1170 */       return true;
/*      */     }
/* 1172 */     return false;
/*      */   }
/*      */   
/*      */   private boolean isParser(Object paramObject) {
/* 1176 */     if ((paramObject instanceof InternalParser)) {
/* 1177 */       if ((paramObject instanceof Composite)) {
/* 1178 */         return ((Composite)paramObject).isParser();
/*      */       }
/* 1180 */       return true;
/*      */     }
/* 1182 */     return false;
/*      */   }
/*      */   
/*      */   private boolean isFormatter(Object paramObject) {
/* 1186 */     return (isPrinter(paramObject)) || (isParser(paramObject));
/*      */   }
/*      */   
/*      */   static void appendUnknownString(Appendable paramAppendable, int paramInt) throws IOException {
/* 1190 */     int i = paramInt; for (;;) { i--; if (i < 0) break;
/* 1191 */       paramAppendable.append(65533);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   static class CharacterLiteral
/*      */     implements InternalPrinter, InternalParser
/*      */   {
/*      */     private final char iValue;
/*      */     
/*      */     CharacterLiteral(char paramChar)
/*      */     {
/* 1203 */       this.iValue = paramChar;
/*      */     }
/*      */     
/*      */     public int estimatePrintedLength() {
/* 1207 */       return 1;
/*      */     }
/*      */     
/*      */     public void printTo(Appendable paramAppendable, long paramLong, Chronology paramChronology, int paramInt, DateTimeZone paramDateTimeZone, Locale paramLocale)
/*      */       throws IOException
/*      */     {
/* 1213 */       paramAppendable.append(this.iValue);
/*      */     }
/*      */     
/*      */     public void printTo(Appendable paramAppendable, ReadablePartial paramReadablePartial, Locale paramLocale) throws IOException {
/* 1217 */       paramAppendable.append(this.iValue);
/*      */     }
/*      */     
/*      */     public int estimateParsedLength() {
/* 1221 */       return 1;
/*      */     }
/*      */     
/*      */     public int parseInto(DateTimeParserBucket paramDateTimeParserBucket, CharSequence paramCharSequence, int paramInt) {
/* 1225 */       if (paramInt >= paramCharSequence.length()) {
/* 1226 */         return paramInt ^ 0xFFFFFFFF;
/*      */       }
/*      */       
/* 1229 */       char c1 = paramCharSequence.charAt(paramInt);
/* 1230 */       char c2 = this.iValue;
/*      */       
/* 1232 */       if (c1 != c2) {
/* 1233 */         c1 = Character.toUpperCase(c1);
/* 1234 */         c2 = Character.toUpperCase(c2);
/* 1235 */         if (c1 != c2) {
/* 1236 */           c1 = Character.toLowerCase(c1);
/* 1237 */           c2 = Character.toLowerCase(c2);
/* 1238 */           if (c1 != c2) {
/* 1239 */             return paramInt ^ 0xFFFFFFFF;
/*      */           }
/*      */         }
/*      */       }
/*      */       
/* 1244 */       return paramInt + 1;
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   static class StringLiteral
/*      */     implements InternalPrinter, InternalParser
/*      */   {
/*      */     private final String iValue;
/*      */     
/*      */     StringLiteral(String paramString)
/*      */     {
/* 1256 */       this.iValue = paramString;
/*      */     }
/*      */     
/*      */     public int estimatePrintedLength() {
/* 1260 */       return this.iValue.length();
/*      */     }
/*      */     
/*      */     public void printTo(Appendable paramAppendable, long paramLong, Chronology paramChronology, int paramInt, DateTimeZone paramDateTimeZone, Locale paramLocale)
/*      */       throws IOException
/*      */     {
/* 1266 */       paramAppendable.append(this.iValue);
/*      */     }
/*      */     
/*      */     public void printTo(Appendable paramAppendable, ReadablePartial paramReadablePartial, Locale paramLocale) throws IOException {
/* 1270 */       paramAppendable.append(this.iValue);
/*      */     }
/*      */     
/*      */     public int estimateParsedLength() {
/* 1274 */       return this.iValue.length();
/*      */     }
/*      */     
/*      */     public int parseInto(DateTimeParserBucket paramDateTimeParserBucket, CharSequence paramCharSequence, int paramInt) {
/* 1278 */       if (DateTimeFormatterBuilder.csStartsWithIgnoreCase(paramCharSequence, paramInt, this.iValue)) {
/* 1279 */         return paramInt + this.iValue.length();
/*      */       }
/* 1281 */       return paramInt ^ 0xFFFFFFFF;
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   static abstract class NumberFormatter
/*      */     implements InternalPrinter, InternalParser
/*      */   {
/*      */     protected final DateTimeFieldType iFieldType;
/*      */     protected final int iMaxParsedDigits;
/*      */     protected final boolean iSigned;
/*      */     
/*      */     NumberFormatter(DateTimeFieldType paramDateTimeFieldType, int paramInt, boolean paramBoolean)
/*      */     {
/* 1295 */       this.iFieldType = paramDateTimeFieldType;
/* 1296 */       this.iMaxParsedDigits = paramInt;
/* 1297 */       this.iSigned = paramBoolean;
/*      */     }
/*      */     
/*      */     public int estimateParsedLength() {
/* 1301 */       return this.iMaxParsedDigits;
/*      */     }
/*      */     
/*      */     public int parseInto(DateTimeParserBucket paramDateTimeParserBucket, CharSequence paramCharSequence, int paramInt) {
/* 1305 */       int i = Math.min(this.iMaxParsedDigits, paramCharSequence.length() - paramInt);
/*      */       
/* 1307 */       int j = 0;
/* 1308 */       int k = 0;
/* 1309 */       int m = 0;
/* 1310 */       int n; while (m < i) {
/* 1311 */         n = paramCharSequence.charAt(paramInt + m);
/* 1312 */         if ((m == 0) && ((n == 45) || (n == 43)) && (this.iSigned)) {
/* 1313 */           j = n == 45 ? 1 : 0;
/* 1314 */           k = n == 43 ? 1 : 0;
/*      */           
/*      */ 
/* 1317 */           if ((m + 1 >= i) || ((n = paramCharSequence.charAt(paramInt + m + 1)) < '0') || (n > 57)) {
/*      */             break;
/*      */           }
/*      */           
/* 1321 */           m++;
/*      */           
/*      */ 
/* 1324 */           i = Math.min(i + 1, paramCharSequence.length() - paramInt);
/*      */         }
/*      */         else {
/* 1327 */           if ((n < 48) || (n > 57)) {
/*      */             break;
/*      */           }
/* 1330 */           m++;
/*      */         }
/*      */       }
/* 1333 */       if (m == 0) {
/* 1334 */         return paramInt ^ 0xFFFFFFFF;
/*      */       }
/*      */       
/*      */ 
/* 1338 */       if (m >= 9)
/*      */       {
/*      */ 
/* 1341 */         if (k != 0) {
/* 1342 */           n = Integer.parseInt(paramCharSequence.subSequence(paramInt + 1, paramInt += m).toString());
/*      */         } else {
/* 1344 */           n = Integer.parseInt(paramCharSequence.subSequence(paramInt, paramInt += m).toString());
/*      */         }
/*      */       }
/*      */       else {
/* 1348 */         int i1 = paramInt;
/* 1349 */         if ((j != 0) || (k != 0)) {
/* 1350 */           i1++;
/*      */         }
/*      */         try {
/* 1353 */           n = paramCharSequence.charAt(i1++) - '0';
/*      */         } catch (StringIndexOutOfBoundsException localStringIndexOutOfBoundsException) {
/* 1355 */           return paramInt ^ 0xFFFFFFFF;
/*      */         }
/* 1357 */         paramInt += m;
/* 1358 */         while (i1 < paramInt) {
/* 1359 */           n = (n << 3) + (n << 1) + paramCharSequence.charAt(i1++) - 48;
/*      */         }
/* 1361 */         if (j != 0) {
/* 1362 */           n = -n;
/*      */         }
/*      */       }
/*      */       
/* 1366 */       paramDateTimeParserBucket.saveField(this.iFieldType, n);
/* 1367 */       return paramInt;
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   static class UnpaddedNumber
/*      */     extends DateTimeFormatterBuilder.NumberFormatter
/*      */   {
/*      */     protected UnpaddedNumber(DateTimeFieldType paramDateTimeFieldType, int paramInt, boolean paramBoolean)
/*      */     {
/* 1377 */       super(paramInt, paramBoolean);
/*      */     }
/*      */     
/*      */     public int estimatePrintedLength() {
/* 1381 */       return this.iMaxParsedDigits;
/*      */     }
/*      */     
/*      */     public void printTo(Appendable paramAppendable, long paramLong, Chronology paramChronology, int paramInt, DateTimeZone paramDateTimeZone, Locale paramLocale) throws IOException
/*      */     {
/*      */       try
/*      */       {
/* 1388 */         DateTimeField localDateTimeField = this.iFieldType.getField(paramChronology);
/* 1389 */         FormatUtils.appendUnpaddedInteger(paramAppendable, localDateTimeField.get(paramLong));
/*      */       } catch (RuntimeException localRuntimeException) {
/* 1391 */         paramAppendable.append(65533);
/*      */       }
/*      */     }
/*      */     
/*      */     public void printTo(Appendable paramAppendable, ReadablePartial paramReadablePartial, Locale paramLocale) throws IOException {
/* 1396 */       if (paramReadablePartial.isSupported(this.iFieldType)) {
/*      */         try {
/* 1398 */           FormatUtils.appendUnpaddedInteger(paramAppendable, paramReadablePartial.get(this.iFieldType));
/*      */         } catch (RuntimeException localRuntimeException) {
/* 1400 */           paramAppendable.append(65533);
/*      */         }
/*      */       } else {
/* 1403 */         paramAppendable.append(65533);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   static class PaddedNumber
/*      */     extends DateTimeFormatterBuilder.NumberFormatter
/*      */   {
/*      */     protected final int iMinPrintedDigits;
/*      */     
/*      */     protected PaddedNumber(DateTimeFieldType paramDateTimeFieldType, int paramInt1, boolean paramBoolean, int paramInt2)
/*      */     {
/* 1416 */       super(paramInt1, paramBoolean);
/* 1417 */       this.iMinPrintedDigits = paramInt2;
/*      */     }
/*      */     
/*      */     public int estimatePrintedLength() {
/* 1421 */       return this.iMaxParsedDigits;
/*      */     }
/*      */     
/*      */     public void printTo(Appendable paramAppendable, long paramLong, Chronology paramChronology, int paramInt, DateTimeZone paramDateTimeZone, Locale paramLocale) throws IOException
/*      */     {
/*      */       try
/*      */       {
/* 1428 */         DateTimeField localDateTimeField = this.iFieldType.getField(paramChronology);
/* 1429 */         FormatUtils.appendPaddedInteger(paramAppendable, localDateTimeField.get(paramLong), this.iMinPrintedDigits);
/*      */       } catch (RuntimeException localRuntimeException) {
/* 1431 */         DateTimeFormatterBuilder.appendUnknownString(paramAppendable, this.iMinPrintedDigits);
/*      */       }
/*      */     }
/*      */     
/*      */     public void printTo(Appendable paramAppendable, ReadablePartial paramReadablePartial, Locale paramLocale) throws IOException {
/* 1436 */       if (paramReadablePartial.isSupported(this.iFieldType)) {
/*      */         try {
/* 1438 */           FormatUtils.appendPaddedInteger(paramAppendable, paramReadablePartial.get(this.iFieldType), this.iMinPrintedDigits);
/*      */         } catch (RuntimeException localRuntimeException) {
/* 1440 */           DateTimeFormatterBuilder.appendUnknownString(paramAppendable, this.iMinPrintedDigits);
/*      */         }
/*      */       } else {
/* 1443 */         DateTimeFormatterBuilder.appendUnknownString(paramAppendable, this.iMinPrintedDigits);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   static class FixedNumber extends DateTimeFormatterBuilder.PaddedNumber
/*      */   {
/*      */     protected FixedNumber(DateTimeFieldType paramDateTimeFieldType, int paramInt, boolean paramBoolean)
/*      */     {
/* 1452 */       super(paramInt, paramBoolean, paramInt);
/*      */     }
/*      */     
/*      */     public int parseInto(DateTimeParserBucket paramDateTimeParserBucket, CharSequence paramCharSequence, int paramInt)
/*      */     {
/* 1457 */       int i = super.parseInto(paramDateTimeParserBucket, paramCharSequence, paramInt);
/* 1458 */       if (i < 0) {
/* 1459 */         return i;
/*      */       }
/* 1461 */       int j = paramInt + this.iMaxParsedDigits;
/* 1462 */       if (i != j) {
/* 1463 */         if (this.iSigned) {
/* 1464 */           int k = paramCharSequence.charAt(paramInt);
/* 1465 */           if ((k == 45) || (k == 43)) {
/* 1466 */             j++;
/*      */           }
/*      */         }
/* 1469 */         if (i > j)
/*      */         {
/* 1471 */           return j + 1 ^ 0xFFFFFFFF; }
/* 1472 */         if (i < j)
/*      */         {
/* 1474 */           return i ^ 0xFFFFFFFF;
/*      */         }
/*      */       }
/* 1477 */       return i;
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   static class TwoDigitYear
/*      */     implements InternalPrinter, InternalParser
/*      */   {
/*      */     private final DateTimeFieldType iType;
/*      */     
/*      */     private final int iPivot;
/*      */     
/*      */     private final boolean iLenientParse;
/*      */     
/*      */     TwoDigitYear(DateTimeFieldType paramDateTimeFieldType, int paramInt, boolean paramBoolean)
/*      */     {
/* 1493 */       this.iType = paramDateTimeFieldType;
/* 1494 */       this.iPivot = paramInt;
/* 1495 */       this.iLenientParse = paramBoolean;
/*      */     }
/*      */     
/*      */     public int estimateParsedLength() {
/* 1499 */       return this.iLenientParse ? 4 : 2;
/*      */     }
/*      */     
/*      */     public int parseInto(DateTimeParserBucket paramDateTimeParserBucket, CharSequence paramCharSequence, int paramInt) {
/* 1503 */       int i = paramCharSequence.length() - paramInt;
/*      */       int i1;
/* 1505 */       if (!this.iLenientParse) {
/* 1506 */         i = Math.min(2, i);
/* 1507 */         if (i < 2) {
/* 1508 */           return paramInt ^ 0xFFFFFFFF;
/*      */         }
/*      */       } else {
/* 1511 */         j = 0;
/* 1512 */         k = 0;
/* 1513 */         m = 0;
/* 1514 */         while (m < i) {
/* 1515 */           n = paramCharSequence.charAt(paramInt + m);
/* 1516 */           if ((m == 0) && ((n == 45) || (n == 43))) {
/* 1517 */             j = 1;
/* 1518 */             k = n == 45 ? 1 : 0;
/* 1519 */             if (k != 0) {
/* 1520 */               m++;
/*      */             }
/*      */             else {
/* 1523 */               paramInt++;
/* 1524 */               i--;
/*      */             }
/*      */           }
/*      */           else {
/* 1528 */             if ((n < 48) || (n > 57)) {
/*      */               break;
/*      */             }
/* 1531 */             m++;
/*      */           }
/*      */         }
/* 1534 */         if (m == 0) {
/* 1535 */           return paramInt ^ 0xFFFFFFFF;
/*      */         }
/*      */         
/* 1538 */         if ((j != 0) || (m != 2))
/*      */         {
/* 1540 */           if (m >= 9)
/*      */           {
/*      */ 
/* 1543 */             n = Integer.parseInt(paramCharSequence.subSequence(paramInt, paramInt += m).toString());
/*      */           } else {
/* 1545 */             i1 = paramInt;
/* 1546 */             if (k != 0) {
/* 1547 */               i1++;
/*      */             }
/*      */             try {
/* 1550 */               n = paramCharSequence.charAt(i1++) - '0';
/*      */             } catch (StringIndexOutOfBoundsException localStringIndexOutOfBoundsException) {
/* 1552 */               return paramInt ^ 0xFFFFFFFF;
/*      */             }
/* 1554 */             paramInt += m;
/* 1555 */             while (i1 < paramInt) {
/* 1556 */               n = (n << 3) + (n << 1) + paramCharSequence.charAt(i1++) - 48;
/*      */             }
/* 1558 */             if (k != 0) {
/* 1559 */               n = -n;
/*      */             }
/*      */           }
/*      */           
/* 1563 */           paramDateTimeParserBucket.saveField(this.iType, n);
/* 1564 */           return paramInt;
/*      */         }
/*      */       }
/*      */       
/*      */ 
/* 1569 */       int k = paramCharSequence.charAt(paramInt);
/* 1570 */       if ((k < 48) || (k > 57)) {
/* 1571 */         return paramInt ^ 0xFFFFFFFF;
/*      */       }
/* 1573 */       int j = k - 48;
/* 1574 */       k = paramCharSequence.charAt(paramInt + 1);
/* 1575 */       if ((k < 48) || (k > 57)) {
/* 1576 */         return paramInt ^ 0xFFFFFFFF;
/*      */       }
/* 1578 */       j = (j << 3) + (j << 1) + k - 48;
/*      */       
/* 1580 */       int m = this.iPivot;
/*      */       
/* 1582 */       if (paramDateTimeParserBucket.getPivotYear() != null) {
/* 1583 */         m = paramDateTimeParserBucket.getPivotYear().intValue();
/*      */       }
/*      */       
/* 1586 */       int n = m - 50;
/*      */       
/*      */ 
/* 1589 */       if (n >= 0) {
/* 1590 */         i1 = n % 100;
/*      */       } else {
/* 1592 */         i1 = 99 + (n + 1) % 100;
/*      */       }
/*      */       
/* 1595 */       j += n + (j < i1 ? 100 : 0) - i1;
/*      */       
/* 1597 */       paramDateTimeParserBucket.saveField(this.iType, j);
/* 1598 */       return paramInt + 2;
/*      */     }
/*      */     
/*      */     public int estimatePrintedLength() {
/* 1602 */       return 2;
/*      */     }
/*      */     
/*      */     public void printTo(Appendable paramAppendable, long paramLong, Chronology paramChronology, int paramInt, DateTimeZone paramDateTimeZone, Locale paramLocale)
/*      */       throws IOException
/*      */     {
/* 1608 */       int i = getTwoDigitYear(paramLong, paramChronology);
/* 1609 */       if (i < 0) {
/* 1610 */         paramAppendable.append(65533);
/* 1611 */         paramAppendable.append(65533);
/*      */       } else {
/* 1613 */         FormatUtils.appendPaddedInteger(paramAppendable, i, 2);
/*      */       }
/*      */     }
/*      */     
/*      */     private int getTwoDigitYear(long paramLong, Chronology paramChronology) {
/*      */       try {
/* 1619 */         int i = this.iType.getField(paramChronology).get(paramLong);
/* 1620 */         if (i < 0) {
/* 1621 */           i = -i;
/*      */         }
/* 1623 */         return i % 100;
/*      */       } catch (RuntimeException localRuntimeException) {}
/* 1625 */       return -1;
/*      */     }
/*      */     
/*      */     public void printTo(Appendable paramAppendable, ReadablePartial paramReadablePartial, Locale paramLocale) throws IOException
/*      */     {
/* 1630 */       int i = getTwoDigitYear(paramReadablePartial);
/* 1631 */       if (i < 0) {
/* 1632 */         paramAppendable.append(65533);
/* 1633 */         paramAppendable.append(65533);
/*      */       } else {
/* 1635 */         FormatUtils.appendPaddedInteger(paramAppendable, i, 2);
/*      */       }
/*      */     }
/*      */     
/*      */     private int getTwoDigitYear(ReadablePartial paramReadablePartial) {
/* 1640 */       if (paramReadablePartial.isSupported(this.iType)) {
/*      */         try {
/* 1642 */           int i = paramReadablePartial.get(this.iType);
/* 1643 */           if (i < 0) {
/* 1644 */             i = -i;
/*      */           }
/* 1646 */           return i % 100;
/*      */         } catch (RuntimeException localRuntimeException) {}
/*      */       }
/* 1649 */       return -1;
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   static class TextField
/*      */     implements InternalPrinter, InternalParser
/*      */   {
/* 1657 */     private static Map<Locale, Map<DateTimeFieldType, Object[]>> cParseCache = new ConcurrentHashMap();
/*      */     
/*      */     private final DateTimeFieldType iFieldType;
/*      */     private final boolean iShort;
/*      */     
/*      */     TextField(DateTimeFieldType paramDateTimeFieldType, boolean paramBoolean)
/*      */     {
/* 1664 */       this.iFieldType = paramDateTimeFieldType;
/* 1665 */       this.iShort = paramBoolean;
/*      */     }
/*      */     
/*      */     public int estimatePrintedLength() {
/* 1669 */       return this.iShort ? 6 : 20;
/*      */     }
/*      */     
/*      */     public void printTo(Appendable paramAppendable, long paramLong, Chronology paramChronology, int paramInt, DateTimeZone paramDateTimeZone, Locale paramLocale) throws IOException
/*      */     {
/*      */       try
/*      */       {
/* 1676 */         paramAppendable.append(print(paramLong, paramChronology, paramLocale));
/*      */       } catch (RuntimeException localRuntimeException) {
/* 1678 */         paramAppendable.append(65533);
/*      */       }
/*      */     }
/*      */     
/*      */     public void printTo(Appendable paramAppendable, ReadablePartial paramReadablePartial, Locale paramLocale) throws IOException {
/*      */       try {
/* 1684 */         paramAppendable.append(print(paramReadablePartial, paramLocale));
/*      */       } catch (RuntimeException localRuntimeException) {
/* 1686 */         paramAppendable.append(65533);
/*      */       }
/*      */     }
/*      */     
/*      */     private String print(long paramLong, Chronology paramChronology, Locale paramLocale) {
/* 1691 */       DateTimeField localDateTimeField = this.iFieldType.getField(paramChronology);
/* 1692 */       if (this.iShort) {
/* 1693 */         return localDateTimeField.getAsShortText(paramLong, paramLocale);
/*      */       }
/* 1695 */       return localDateTimeField.getAsText(paramLong, paramLocale);
/*      */     }
/*      */     
/*      */     private String print(ReadablePartial paramReadablePartial, Locale paramLocale)
/*      */     {
/* 1700 */       if (paramReadablePartial.isSupported(this.iFieldType)) {
/* 1701 */         DateTimeField localDateTimeField = this.iFieldType.getField(paramReadablePartial.getChronology());
/* 1702 */         if (this.iShort) {
/* 1703 */           return localDateTimeField.getAsShortText(paramReadablePartial, paramLocale);
/*      */         }
/* 1705 */         return localDateTimeField.getAsText(paramReadablePartial, paramLocale);
/*      */       }
/*      */       
/* 1708 */       return "�";
/*      */     }
/*      */     
/*      */     public int estimateParsedLength()
/*      */     {
/* 1713 */       return estimatePrintedLength();
/*      */     }
/*      */     
/*      */     public int parseInto(DateTimeParserBucket paramDateTimeParserBucket, CharSequence paramCharSequence, int paramInt)
/*      */     {
/* 1718 */       Locale localLocale = paramDateTimeParserBucket.getLocale();
/*      */       
/*      */ 
/* 1721 */       Object localObject1 = null;
/* 1722 */       int i = 0;
/* 1723 */       Object localObject2 = (Map)cParseCache.get(localLocale);
/* 1724 */       if (localObject2 == null) {
/* 1725 */         localObject2 = new ConcurrentHashMap();
/* 1726 */         cParseCache.put(localLocale, localObject2);
/*      */       }
/* 1728 */       Object[] arrayOfObject = (Object[])((Map)localObject2).get(this.iFieldType);
/* 1729 */       if (arrayOfObject == null) {
/* 1730 */         localObject1 = new ConcurrentHashMap(32);
/* 1731 */         MutableDateTime localMutableDateTime = new MutableDateTime(0L, DateTimeZone.UTC);
/* 1732 */         localProperty2 = localMutableDateTime.property(this.iFieldType);
/* 1733 */         int j = localProperty2.getMinimumValueOverall();
/* 1734 */         int k = localProperty2.getMaximumValueOverall();
/* 1735 */         if (k - j > 32) {
/* 1736 */           return paramInt ^ 0xFFFFFFFF;
/*      */         }
/* 1738 */         i = localProperty2.getMaximumTextLength(localLocale);
/* 1739 */         for (int m = j; m <= k; m++) {
/* 1740 */           localProperty2.set(m);
/* 1741 */           ((Map)localObject1).put(localProperty2.getAsShortText(localLocale), Boolean.TRUE);
/* 1742 */           ((Map)localObject1).put(localProperty2.getAsShortText(localLocale).toLowerCase(localLocale), Boolean.TRUE);
/* 1743 */           ((Map)localObject1).put(localProperty2.getAsShortText(localLocale).toUpperCase(localLocale), Boolean.TRUE);
/* 1744 */           ((Map)localObject1).put(localProperty2.getAsText(localLocale), Boolean.TRUE);
/* 1745 */           ((Map)localObject1).put(localProperty2.getAsText(localLocale).toLowerCase(localLocale), Boolean.TRUE);
/* 1746 */           ((Map)localObject1).put(localProperty2.getAsText(localLocale).toUpperCase(localLocale), Boolean.TRUE);
/*      */         }
/* 1748 */         if (("en".equals(localLocale.getLanguage())) && (this.iFieldType == DateTimeFieldType.era()))
/*      */         {
/* 1750 */           ((Map)localObject1).put("BCE", Boolean.TRUE);
/* 1751 */           ((Map)localObject1).put("bce", Boolean.TRUE);
/* 1752 */           ((Map)localObject1).put("CE", Boolean.TRUE);
/* 1753 */           ((Map)localObject1).put("ce", Boolean.TRUE);
/* 1754 */           i = 3;
/*      */         }
/* 1756 */         arrayOfObject = new Object[] { localObject1, Integer.valueOf(i) };
/* 1757 */         ((Map)localObject2).put(this.iFieldType, arrayOfObject);
/*      */       } else {
/* 1759 */         localObject1 = (Map)arrayOfObject[0];
/* 1760 */         i = ((Integer)arrayOfObject[1]).intValue();
/*      */       }
/*      */       
/* 1763 */       MutableDateTime.Property localProperty1 = Math.min(paramCharSequence.length(), paramInt + i);
/* 1764 */       for (MutableDateTime.Property localProperty2 = localProperty1; localProperty2 > paramInt; localProperty2--) {
/* 1765 */         String str = paramCharSequence.subSequence(paramInt, localProperty2).toString();
/* 1766 */         if (((Map)localObject1).containsKey(str)) {
/* 1767 */           paramDateTimeParserBucket.saveField(this.iFieldType, str, localLocale);
/* 1768 */           return localProperty2;
/*      */         }
/*      */       }
/* 1771 */       return paramInt ^ 0xFFFFFFFF;
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   static class Fraction
/*      */     implements InternalPrinter, InternalParser
/*      */   {
/*      */     private final DateTimeFieldType iFieldType;
/*      */     protected int iMinDigits;
/*      */     protected int iMaxDigits;
/*      */     
/*      */     protected Fraction(DateTimeFieldType paramDateTimeFieldType, int paramInt1, int paramInt2)
/*      */     {
/* 1785 */       this.iFieldType = paramDateTimeFieldType;
/*      */       
/* 1787 */       if (paramInt2 > 18) {
/* 1788 */         paramInt2 = 18;
/*      */       }
/* 1790 */       this.iMinDigits = paramInt1;
/* 1791 */       this.iMaxDigits = paramInt2;
/*      */     }
/*      */     
/*      */     public int estimatePrintedLength() {
/* 1795 */       return this.iMaxDigits;
/*      */     }
/*      */     
/*      */     public void printTo(Appendable paramAppendable, long paramLong, Chronology paramChronology, int paramInt, DateTimeZone paramDateTimeZone, Locale paramLocale)
/*      */       throws IOException
/*      */     {
/* 1801 */       printTo(paramAppendable, paramLong, paramChronology);
/*      */     }
/*      */     
/*      */     public void printTo(Appendable paramAppendable, ReadablePartial paramReadablePartial, Locale paramLocale)
/*      */       throws IOException
/*      */     {
/* 1807 */       long l = paramReadablePartial.getChronology().set(paramReadablePartial, 0L);
/* 1808 */       printTo(paramAppendable, l, paramReadablePartial.getChronology());
/*      */     }
/*      */     
/*      */     protected void printTo(Appendable paramAppendable, long paramLong, Chronology paramChronology)
/*      */       throws IOException
/*      */     {
/* 1814 */       DateTimeField localDateTimeField = this.iFieldType.getField(paramChronology);
/* 1815 */       int i = this.iMinDigits;
/*      */       long l1;
/*      */       try
/*      */       {
/* 1819 */         l1 = localDateTimeField.remainder(paramLong);
/*      */       } catch (RuntimeException localRuntimeException) {
/* 1821 */         DateTimeFormatterBuilder.appendUnknownString(paramAppendable, i);
/* 1822 */         return;
/*      */       }
/*      */       
/* 1825 */       if (l1 == 0L) {
/* 1826 */         for (;;) { i--; if (i < 0) break;
/* 1827 */           paramAppendable.append('0');
/*      */         }
/* 1829 */         return;
/*      */       }
/*      */       
/*      */ 
/* 1833 */       long[] arrayOfLong = getFractionData(l1, localDateTimeField);
/* 1834 */       long l2 = arrayOfLong[0];
/* 1835 */       int j = (int)arrayOfLong[1];
/*      */       String str;
/* 1837 */       if ((l2 & 0x7FFFFFFF) == l2) {
/* 1838 */         str = Integer.toString((int)l2);
/*      */       } else {
/* 1840 */         str = Long.toString(l2);
/*      */       }
/*      */       
/* 1843 */       int k = str.length();
/* 1844 */       int m = j;
/* 1845 */       while (k < m) {
/* 1846 */         paramAppendable.append('0');
/* 1847 */         i--;
/* 1848 */         m--;
/*      */       }
/*      */       
/* 1851 */       if (i < m)
/*      */       {
/* 1853 */         while ((i < m) && 
/* 1854 */           (k > 1) && (str.charAt(k - 1) == '0'))
/*      */         {
/*      */ 
/* 1857 */           m--;
/* 1858 */           k--;
/*      */         }
/* 1860 */         if (k < str.length()) {
/* 1861 */           for (int n = 0; n < k; n++) {
/* 1862 */             paramAppendable.append(str.charAt(n));
/*      */           }
/* 1864 */           return;
/*      */         }
/*      */       }
/*      */       
/* 1868 */       paramAppendable.append(str);
/*      */     }
/*      */     
/*      */     private long[] getFractionData(long paramLong, DateTimeField paramDateTimeField) {
/* 1872 */       long l1 = paramDateTimeField.getDurationField().getUnitMillis();
/*      */       
/* 1874 */       int i = this.iMaxDigits;
/*      */       long l2;
/* 1876 */       for (;;) { switch (i) {
/* 1877 */         default:  l2 = 1L; break;
/* 1878 */         case 1:  l2 = 10L; break;
/* 1879 */         case 2:  l2 = 100L; break;
/* 1880 */         case 3:  l2 = 1000L; break;
/* 1881 */         case 4:  l2 = 10000L; break;
/* 1882 */         case 5:  l2 = 100000L; break;
/* 1883 */         case 6:  l2 = 1000000L; break;
/* 1884 */         case 7:  l2 = 10000000L; break;
/* 1885 */         case 8:  l2 = 100000000L; break;
/* 1886 */         case 9:  l2 = 1000000000L; break;
/* 1887 */         case 10:  l2 = 10000000000L; break;
/* 1888 */         case 11:  l2 = 100000000000L; break;
/* 1889 */         case 12:  l2 = 1000000000000L; break;
/* 1890 */         case 13:  l2 = 10000000000000L; break;
/* 1891 */         case 14:  l2 = 100000000000000L; break;
/* 1892 */         case 15:  l2 = 1000000000000000L; break;
/* 1893 */         case 16:  l2 = 10000000000000000L; break;
/* 1894 */         case 17:  l2 = 100000000000000000L; break;
/* 1895 */         case 18:  l2 = 1000000000000000000L;
/*      */         }
/* 1897 */         if (l1 * l2 / l2 == l1) {
/*      */           break;
/*      */         }
/*      */         
/* 1901 */         i--;
/*      */       }
/*      */       
/* 1904 */       return new long[] { paramLong * l2 / l1, i };
/*      */     }
/*      */     
/*      */     public int estimateParsedLength() {
/* 1908 */       return this.iMaxDigits;
/*      */     }
/*      */     
/*      */     public int parseInto(DateTimeParserBucket paramDateTimeParserBucket, CharSequence paramCharSequence, int paramInt) {
/* 1912 */       DateTimeField localDateTimeField = this.iFieldType.getField(paramDateTimeParserBucket.getChronology());
/*      */       
/* 1914 */       int i = Math.min(this.iMaxDigits, paramCharSequence.length() - paramInt);
/*      */       
/* 1916 */       long l1 = 0L;
/* 1917 */       long l2 = localDateTimeField.getDurationField().getUnitMillis() * 10L;
/* 1918 */       int j = 0;
/* 1919 */       while (j < i) {
/* 1920 */         int k = paramCharSequence.charAt(paramInt + j);
/* 1921 */         if ((k < 48) || (k > 57)) {
/*      */           break;
/*      */         }
/* 1924 */         j++;
/* 1925 */         long l3 = l2 / 10L;
/* 1926 */         l1 += (k - 48) * l3;
/* 1927 */         l2 = l3;
/*      */       }
/*      */       
/* 1930 */       l1 /= 10L;
/*      */       
/* 1932 */       if (j == 0) {
/* 1933 */         return paramInt ^ 0xFFFFFFFF;
/*      */       }
/*      */       
/* 1936 */       if (l1 > 2147483647L) {
/* 1937 */         return paramInt ^ 0xFFFFFFFF;
/*      */       }
/*      */       
/* 1940 */       PreciseDateTimeField localPreciseDateTimeField = new PreciseDateTimeField(DateTimeFieldType.millisOfSecond(), MillisDurationField.INSTANCE, localDateTimeField.getDurationField());
/*      */       
/*      */ 
/*      */ 
/*      */ 
/* 1945 */       paramDateTimeParserBucket.saveField(localPreciseDateTimeField, (int)l1);
/*      */       
/* 1947 */       return paramInt + j;
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   static class TimeZoneOffset
/*      */     implements InternalPrinter, InternalParser
/*      */   {
/*      */     private final String iZeroOffsetPrintText;
/*      */     
/*      */     private final String iZeroOffsetParseText;
/*      */     
/*      */     private final boolean iShowSeparators;
/*      */     
/*      */     private final int iMinFields;
/*      */     private final int iMaxFields;
/*      */     
/*      */     TimeZoneOffset(String paramString1, String paramString2, boolean paramBoolean, int paramInt1, int paramInt2)
/*      */     {
/* 1966 */       this.iZeroOffsetPrintText = paramString1;
/* 1967 */       this.iZeroOffsetParseText = paramString2;
/* 1968 */       this.iShowSeparators = paramBoolean;
/* 1969 */       if ((paramInt1 <= 0) || (paramInt2 < paramInt1)) {
/* 1970 */         throw new IllegalArgumentException();
/*      */       }
/* 1972 */       if (paramInt1 > 4) {
/* 1973 */         paramInt1 = 4;
/* 1974 */         paramInt2 = 4;
/*      */       }
/* 1976 */       this.iMinFields = paramInt1;
/* 1977 */       this.iMaxFields = paramInt2;
/*      */     }
/*      */     
/*      */     public int estimatePrintedLength() {
/* 1981 */       int i = 1 + this.iMinFields << 1;
/* 1982 */       if (this.iShowSeparators) {
/* 1983 */         i += this.iMinFields - 1;
/*      */       }
/* 1985 */       if ((this.iZeroOffsetPrintText != null) && (this.iZeroOffsetPrintText.length() > i)) {
/* 1986 */         i = this.iZeroOffsetPrintText.length();
/*      */       }
/* 1988 */       return i;
/*      */     }
/*      */     
/*      */     public void printTo(Appendable paramAppendable, long paramLong, Chronology paramChronology, int paramInt, DateTimeZone paramDateTimeZone, Locale paramLocale)
/*      */       throws IOException
/*      */     {
/* 1994 */       if (paramDateTimeZone == null) {
/* 1995 */         return;
/*      */       }
/* 1997 */       if ((paramInt == 0) && (this.iZeroOffsetPrintText != null)) {
/* 1998 */         paramAppendable.append(this.iZeroOffsetPrintText);
/* 1999 */         return;
/*      */       }
/* 2001 */       if (paramInt >= 0) {
/* 2002 */         paramAppendable.append('+');
/*      */       } else {
/* 2004 */         paramAppendable.append('-');
/* 2005 */         paramInt = -paramInt;
/*      */       }
/*      */       
/* 2008 */       int i = paramInt / 3600000;
/* 2009 */       FormatUtils.appendPaddedInteger(paramAppendable, i, 2);
/* 2010 */       if (this.iMaxFields == 1) {
/* 2011 */         return;
/*      */       }
/* 2013 */       paramInt -= i * 3600000;
/* 2014 */       if ((paramInt == 0) && (this.iMinFields <= 1)) {
/* 2015 */         return;
/*      */       }
/*      */       
/* 2018 */       int j = paramInt / 60000;
/* 2019 */       if (this.iShowSeparators) {
/* 2020 */         paramAppendable.append(':');
/*      */       }
/* 2022 */       FormatUtils.appendPaddedInteger(paramAppendable, j, 2);
/* 2023 */       if (this.iMaxFields == 2) {
/* 2024 */         return;
/*      */       }
/* 2026 */       paramInt -= j * 60000;
/* 2027 */       if ((paramInt == 0) && (this.iMinFields <= 2)) {
/* 2028 */         return;
/*      */       }
/*      */       
/* 2031 */       int k = paramInt / 1000;
/* 2032 */       if (this.iShowSeparators) {
/* 2033 */         paramAppendable.append(':');
/*      */       }
/* 2035 */       FormatUtils.appendPaddedInteger(paramAppendable, k, 2);
/* 2036 */       if (this.iMaxFields == 3) {
/* 2037 */         return;
/*      */       }
/* 2039 */       paramInt -= k * 1000;
/* 2040 */       if ((paramInt == 0) && (this.iMinFields <= 3)) {
/* 2041 */         return;
/*      */       }
/*      */       
/* 2044 */       if (this.iShowSeparators) {
/* 2045 */         paramAppendable.append('.');
/*      */       }
/* 2047 */       FormatUtils.appendPaddedInteger(paramAppendable, paramInt, 3);
/*      */     }
/*      */     
/*      */     public void printTo(Appendable paramAppendable, ReadablePartial paramReadablePartial, Locale paramLocale) throws IOException
/*      */     {}
/*      */     
/*      */     public int estimateParsedLength()
/*      */     {
/* 2055 */       return estimatePrintedLength();
/*      */     }
/*      */     
/*      */     public int parseInto(DateTimeParserBucket paramDateTimeParserBucket, CharSequence paramCharSequence, int paramInt) {
/* 2059 */       int i = paramCharSequence.length() - paramInt;
/*      */       
/*      */       int j;
/* 2062 */       if (this.iZeroOffsetParseText != null) {
/* 2063 */         if (this.iZeroOffsetParseText.length() == 0)
/*      */         {
/* 2065 */           if (i > 0) {
/* 2066 */             j = paramCharSequence.charAt(paramInt);
/* 2067 */             if ((j == 45) || (j == 43)) {}
/*      */           }
/*      */           else
/*      */           {
/* 2071 */             paramDateTimeParserBucket.setOffset(Integer.valueOf(0));
/* 2072 */             return paramInt;
/*      */           }
/* 2074 */         } else if (DateTimeFormatterBuilder.csStartsWithIgnoreCase(paramCharSequence, paramInt, this.iZeroOffsetParseText)) {
/* 2075 */           paramDateTimeParserBucket.setOffset(Integer.valueOf(0));
/* 2076 */           return paramInt + this.iZeroOffsetParseText.length();
/*      */         }
/*      */       }
/*      */       
/*      */ 
/*      */ 
/* 2082 */       if (i <= 1) {
/* 2083 */         return paramInt ^ 0xFFFFFFFF;
/*      */       }
/*      */       
/*      */ 
/* 2087 */       int k = paramCharSequence.charAt(paramInt);
/* 2088 */       if (k == 45) {
/* 2089 */         j = 1;
/* 2090 */       } else if (k == 43) {
/* 2091 */         j = 0;
/*      */       } else {
/* 2093 */         return paramInt ^ 0xFFFFFFFF;
/*      */       }
/*      */       
/* 2096 */       i--;
/* 2097 */       paramInt++;
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 2111 */       if (digitCount(paramCharSequence, paramInt, 2) < 2)
/*      */       {
/* 2113 */         return paramInt ^ 0xFFFFFFFF;
/*      */       }
/*      */       
/*      */ 
/*      */ 
/* 2118 */       int n = FormatUtils.parseTwoDigits(paramCharSequence, paramInt);
/* 2119 */       if (n > 23) {
/* 2120 */         return paramInt ^ 0xFFFFFFFF;
/*      */       }
/* 2122 */       int m = n * 3600000;
/* 2123 */       i -= 2;
/* 2124 */       paramInt += 2;
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 2130 */       if (i > 0)
/*      */       {
/*      */ 
/*      */ 
/*      */ 
/* 2135 */         k = paramCharSequence.charAt(paramInt);
/* 2136 */         int i1; if (k == 58) {
/* 2137 */           i1 = 1;
/* 2138 */           i--;
/* 2139 */           paramInt++;
/* 2140 */         } else { if ((k < 48) || (k > 57)) break label569;
/* 2141 */           i1 = 0;
/*      */         }
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 2148 */         int i2 = digitCount(paramCharSequence, paramInt, 2);
/* 2149 */         if ((i2 != 0) || (i1 != 0))
/*      */         {
/* 2151 */           if (i2 < 2)
/*      */           {
/* 2153 */             return paramInt ^ 0xFFFFFFFF;
/*      */           }
/*      */           
/* 2156 */           int i3 = FormatUtils.parseTwoDigits(paramCharSequence, paramInt);
/* 2157 */           if (i3 > 59) {
/* 2158 */             return paramInt ^ 0xFFFFFFFF;
/*      */           }
/* 2160 */           m += i3 * 60000;
/* 2161 */           i -= 2;
/* 2162 */           paramInt += 2;
/*      */           
/*      */ 
/*      */ 
/* 2166 */           if (i > 0)
/*      */           {
/*      */ 
/*      */ 
/* 2170 */             if (i1 != 0) {
/* 2171 */               if (paramCharSequence.charAt(paramInt) == ':')
/*      */               {
/*      */ 
/* 2174 */                 i--;
/* 2175 */                 paramInt++;
/*      */               }
/*      */             } else {
/* 2178 */               i2 = digitCount(paramCharSequence, paramInt, 2);
/* 2179 */               if ((i2 != 0) || (i1 != 0))
/*      */               {
/* 2181 */                 if (i2 < 2)
/*      */                 {
/* 2183 */                   return paramInt ^ 0xFFFFFFFF;
/*      */                 }
/*      */                 
/* 2186 */                 int i4 = FormatUtils.parseTwoDigits(paramCharSequence, paramInt);
/* 2187 */                 if (i4 > 59) {
/* 2188 */                   return paramInt ^ 0xFFFFFFFF;
/*      */                 }
/* 2190 */                 m += i4 * 1000;
/* 2191 */                 i -= 2;
/* 2192 */                 paramInt += 2;
/*      */                 
/*      */ 
/*      */ 
/* 2196 */                 if (i > 0)
/*      */                 {
/*      */ 
/*      */ 
/* 2200 */                   if (i1 != 0) {
/* 2201 */                     if ((paramCharSequence.charAt(paramInt) == '.') || (paramCharSequence.charAt(paramInt) == ','))
/*      */                     {
/*      */ 
/* 2204 */                       i--;
/* 2205 */                       paramInt++;
/*      */                     }
/*      */                   } else {
/* 2208 */                     i2 = digitCount(paramCharSequence, paramInt, 3);
/* 2209 */                     if ((i2 != 0) || (i1 != 0))
/*      */                     {
/* 2211 */                       if (i2 < 1)
/*      */                       {
/* 2213 */                         return paramInt ^ 0xFFFFFFFF;
/*      */                       }
/*      */                       
/* 2216 */                       m += (paramCharSequence.charAt(paramInt++) - '0') * 100;
/* 2217 */                       if (i2 > 1) {
/* 2218 */                         m += (paramCharSequence.charAt(paramInt++) - '0') * 10;
/* 2219 */                         if (i2 > 2)
/* 2220 */                           m += paramCharSequence.charAt(paramInt++) - '0';
/*      */                       }
/*      */                     }
/*      */                   } } } } } } }
/*      */       label569:
/* 2225 */       paramDateTimeParserBucket.setOffset(Integer.valueOf(j != 0 ? -m : m));
/* 2226 */       return paramInt;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */     private int digitCount(CharSequence paramCharSequence, int paramInt1, int paramInt2)
/*      */     {
/* 2234 */       int i = Math.min(paramCharSequence.length() - paramInt1, paramInt2);
/* 2235 */       paramInt2 = 0;
/* 2236 */       for (; i > 0; i--) {
/* 2237 */         int j = paramCharSequence.charAt(paramInt1 + paramInt2);
/* 2238 */         if ((j < 48) || (j > 57)) {
/*      */           break;
/*      */         }
/* 2241 */         paramInt2++;
/*      */       }
/* 2243 */       return paramInt2;
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   static class TimeZoneName
/*      */     implements InternalPrinter, InternalParser
/*      */   {
/*      */     static final int LONG_NAME = 0;
/*      */     
/*      */     static final int SHORT_NAME = 1;
/*      */     private final Map<String, DateTimeZone> iParseLookup;
/*      */     private final int iType;
/*      */     
/*      */     TimeZoneName(int paramInt, Map<String, DateTimeZone> paramMap)
/*      */     {
/* 2259 */       this.iType = paramInt;
/* 2260 */       this.iParseLookup = paramMap;
/*      */     }
/*      */     
/*      */     public int estimatePrintedLength() {
/* 2264 */       return this.iType == 1 ? 4 : 20;
/*      */     }
/*      */     
/*      */     public void printTo(Appendable paramAppendable, long paramLong, Chronology paramChronology, int paramInt, DateTimeZone paramDateTimeZone, Locale paramLocale)
/*      */       throws IOException
/*      */     {
/* 2270 */       paramAppendable.append(print(paramLong - paramInt, paramDateTimeZone, paramLocale));
/*      */     }
/*      */     
/*      */     private String print(long paramLong, DateTimeZone paramDateTimeZone, Locale paramLocale) {
/* 2274 */       if (paramDateTimeZone == null) {
/* 2275 */         return "";
/*      */       }
/* 2277 */       switch (this.iType) {
/*      */       case 0: 
/* 2279 */         return paramDateTimeZone.getName(paramLong, paramLocale);
/*      */       case 1: 
/* 2281 */         return paramDateTimeZone.getShortName(paramLong, paramLocale);
/*      */       }
/* 2283 */       return "";
/*      */     }
/*      */     
/*      */     public void printTo(Appendable paramAppendable, ReadablePartial paramReadablePartial, Locale paramLocale) throws IOException
/*      */     {}
/*      */     
/*      */     public int estimateParsedLength()
/*      */     {
/* 2291 */       return this.iType == 1 ? 4 : 20;
/*      */     }
/*      */     
/*      */     public int parseInto(DateTimeParserBucket paramDateTimeParserBucket, CharSequence paramCharSequence, int paramInt) {
/* 2295 */       Map localMap = this.iParseLookup;
/* 2296 */       localMap = localMap != null ? localMap : DateTimeUtils.getDefaultTimeZoneNames();
/* 2297 */       Object localObject = null;
/* 2298 */       for (String str : localMap.keySet()) {
/* 2299 */         if ((DateTimeFormatterBuilder.csStartsWith(paramCharSequence, paramInt, str)) && (
/* 2300 */           (localObject == null) || (str.length() > ((String)localObject).length()))) {
/* 2301 */           localObject = str;
/*      */         }
/*      */       }
/*      */       
/* 2305 */       if (localObject != null) {
/* 2306 */         paramDateTimeParserBucket.setZone((DateTimeZone)localMap.get(localObject));
/* 2307 */         return paramInt + ((String)localObject).length();
/*      */       }
/* 2309 */       return paramInt ^ 0xFFFFFFFF;
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   static enum TimeZoneId
/*      */     implements InternalPrinter, InternalParser
/*      */   {
/* 2317 */     INSTANCE;
/*      */     
/*      */     private static final List<String> ALL_IDS;
/*      */     private static final Map<String, List<String>> GROUPED_IDS;
/*      */     
/* 2322 */     static { BASE_GROUPED_IDS = new ArrayList();
/*      */       
/*      */ 
/*      */ 
/* 2326 */       ALL_IDS = new ArrayList(DateTimeZone.getAvailableIDs());
/* 2327 */       Collections.sort(ALL_IDS);
/* 2328 */       GROUPED_IDS = new HashMap();
/* 2329 */       int i = 0;
/* 2330 */       int j = 0;
/* 2331 */       for (String str1 : ALL_IDS) {
/* 2332 */         int k = str1.indexOf('/');
/* 2333 */         if (k >= 0) {
/* 2334 */           if (k < str1.length()) {
/* 2335 */             k++;
/*      */           }
/* 2337 */           j = Math.max(j, k);
/* 2338 */           String str2 = str1.substring(0, k + 1);
/* 2339 */           String str3 = str1.substring(k);
/* 2340 */           if (!GROUPED_IDS.containsKey(str2)) {
/* 2341 */             GROUPED_IDS.put(str2, new ArrayList());
/*      */           }
/* 2343 */           ((List)GROUPED_IDS.get(str2)).add(str3);
/*      */         } else {
/* 2345 */           BASE_GROUPED_IDS.add(str1);
/*      */         }
/* 2347 */         i = Math.max(i, str1.length());
/*      */       }
/* 2349 */       MAX_LENGTH = i;
/* 2350 */       MAX_PREFIX_LENGTH = j;
/*      */     }
/*      */     
/*      */     public int estimatePrintedLength() {
/* 2354 */       return MAX_LENGTH;
/*      */     }
/*      */     
/*      */     public void printTo(Appendable paramAppendable, long paramLong, Chronology paramChronology, int paramInt, DateTimeZone paramDateTimeZone, Locale paramLocale)
/*      */       throws IOException
/*      */     {
/* 2360 */       paramAppendable.append(paramDateTimeZone != null ? paramDateTimeZone.getID() : "");
/*      */     }
/*      */     
/*      */     private static final List<String> BASE_GROUPED_IDS;
/*      */     static final int MAX_LENGTH;
/*      */     static final int MAX_PREFIX_LENGTH;
/*      */     public int estimateParsedLength()
/*      */     {
/* 2368 */       return MAX_LENGTH;
/*      */     }
/*      */     
/*      */     public int parseInto(DateTimeParserBucket paramDateTimeParserBucket, CharSequence paramCharSequence, int paramInt)
/*      */     {
/* 2373 */       List localList = BASE_GROUPED_IDS;
/*      */       
/* 2375 */       int i = paramCharSequence.length();
/* 2376 */       int j = Math.min(i, paramInt + MAX_PREFIX_LENGTH);
/* 2377 */       int k = paramInt;
/* 2378 */       String str1 = "";
/* 2379 */       for (int m = k; m < j; m++) {
/* 2380 */         if (paramCharSequence.charAt(m) == '/')
/*      */         {
/* 2382 */           str1 = paramCharSequence.subSequence(k, m + 1).toString();
/* 2383 */           k += str1.length();
/* 2384 */           String str2 = str1;
/* 2385 */           if (m < i) {
/* 2386 */             str2 = str2 + paramCharSequence.charAt(m + 1);
/*      */           }
/* 2388 */           localList = (List)GROUPED_IDS.get(str2);
/* 2389 */           if (localList != null) break;
/* 2390 */           return paramInt ^ 0xFFFFFFFF;
/*      */         }
/*      */       }
/*      */       
/*      */ 
/*      */ 
/* 2396 */       Object localObject = null;
/* 2397 */       for (int n = 0; n < localList.size(); n++) {
/* 2398 */         String str3 = (String)localList.get(n);
/* 2399 */         if ((DateTimeFormatterBuilder.csStartsWith(paramCharSequence, k, str3)) && (
/* 2400 */           (localObject == null) || (str3.length() > ((String)localObject).length()))) {
/* 2401 */           localObject = str3;
/*      */         }
/*      */       }
/*      */       
/*      */ 
/* 2406 */       if (localObject != null) {
/* 2407 */         paramDateTimeParserBucket.setZone(DateTimeZone.forID(str1 + (String)localObject));
/* 2408 */         return k + ((String)localObject).length();
/*      */       }
/* 2410 */       return paramInt ^ 0xFFFFFFFF;
/*      */     }
/*      */     
/*      */     private TimeZoneId() {}
/*      */     
/*      */     public void printTo(Appendable paramAppendable, ReadablePartial paramReadablePartial, Locale paramLocale) throws IOException
/*      */     {}
/*      */   }
/*      */   
/*      */   static class Composite implements InternalPrinter, InternalParser
/*      */   {
/*      */     private final InternalPrinter[] iPrinters;
/*      */     private final InternalParser[] iParsers;
/*      */     private final int iPrintedLengthEstimate;
/*      */     private final int iParsedLengthEstimate;
/*      */     
/*      */     Composite(List<Object> paramList)
/*      */     {
/* 2428 */       ArrayList localArrayList1 = new ArrayList();
/* 2429 */       ArrayList localArrayList2 = new ArrayList();
/*      */       
/* 2431 */       decompose(paramList, localArrayList1, localArrayList2);
/*      */       int i;
/* 2433 */       int j; int k; Object localObject; if ((localArrayList1.contains(null)) || (localArrayList1.isEmpty())) {
/* 2434 */         this.iPrinters = null;
/* 2435 */         this.iPrintedLengthEstimate = 0;
/*      */       } else {
/* 2437 */         i = localArrayList1.size();
/* 2438 */         this.iPrinters = new InternalPrinter[i];
/* 2439 */         j = 0;
/* 2440 */         for (k = 0; k < i; k++) {
/* 2441 */           localObject = (InternalPrinter)localArrayList1.get(k);
/* 2442 */           j += ((InternalPrinter)localObject).estimatePrintedLength();
/* 2443 */           this.iPrinters[k] = localObject;
/*      */         }
/* 2445 */         this.iPrintedLengthEstimate = j;
/*      */       }
/*      */       
/* 2448 */       if ((localArrayList2.contains(null)) || (localArrayList2.isEmpty())) {
/* 2449 */         this.iParsers = null;
/* 2450 */         this.iParsedLengthEstimate = 0;
/*      */       } else {
/* 2452 */         i = localArrayList2.size();
/* 2453 */         this.iParsers = new InternalParser[i];
/* 2454 */         j = 0;
/* 2455 */         for (k = 0; k < i; k++) {
/* 2456 */           localObject = (InternalParser)localArrayList2.get(k);
/* 2457 */           j += ((InternalParser)localObject).estimateParsedLength();
/* 2458 */           this.iParsers[k] = localObject;
/*      */         }
/* 2460 */         this.iParsedLengthEstimate = j;
/*      */       }
/*      */     }
/*      */     
/*      */     public int estimatePrintedLength() {
/* 2465 */       return this.iPrintedLengthEstimate;
/*      */     }
/*      */     
/*      */     public void printTo(Appendable paramAppendable, long paramLong, Chronology paramChronology, int paramInt, DateTimeZone paramDateTimeZone, Locale paramLocale)
/*      */       throws IOException
/*      */     {
/* 2471 */       InternalPrinter[] arrayOfInternalPrinter = this.iPrinters;
/* 2472 */       if (arrayOfInternalPrinter == null) {
/* 2473 */         throw new UnsupportedOperationException();
/*      */       }
/*      */       
/* 2476 */       if (paramLocale == null)
/*      */       {
/* 2478 */         paramLocale = Locale.getDefault();
/*      */       }
/*      */       
/* 2481 */       int i = arrayOfInternalPrinter.length;
/* 2482 */       for (int j = 0; j < i; j++) {
/* 2483 */         arrayOfInternalPrinter[j].printTo(paramAppendable, paramLong, paramChronology, paramInt, paramDateTimeZone, paramLocale);
/*      */       }
/*      */     }
/*      */     
/*      */     public void printTo(Appendable paramAppendable, ReadablePartial paramReadablePartial, Locale paramLocale) throws IOException {
/* 2488 */       InternalPrinter[] arrayOfInternalPrinter = this.iPrinters;
/* 2489 */       if (arrayOfInternalPrinter == null) {
/* 2490 */         throw new UnsupportedOperationException();
/*      */       }
/*      */       
/* 2493 */       if (paramLocale == null)
/*      */       {
/* 2495 */         paramLocale = Locale.getDefault();
/*      */       }
/*      */       
/* 2498 */       int i = arrayOfInternalPrinter.length;
/* 2499 */       for (int j = 0; j < i; j++) {
/* 2500 */         arrayOfInternalPrinter[j].printTo(paramAppendable, paramReadablePartial, paramLocale);
/*      */       }
/*      */     }
/*      */     
/*      */     public int estimateParsedLength() {
/* 2505 */       return this.iParsedLengthEstimate;
/*      */     }
/*      */     
/*      */     public int parseInto(DateTimeParserBucket paramDateTimeParserBucket, CharSequence paramCharSequence, int paramInt) {
/* 2509 */       InternalParser[] arrayOfInternalParser = this.iParsers;
/* 2510 */       if (arrayOfInternalParser == null) {
/* 2511 */         throw new UnsupportedOperationException();
/*      */       }
/*      */       
/* 2514 */       int i = arrayOfInternalParser.length;
/* 2515 */       for (int j = 0; (j < i) && (paramInt >= 0); j++) {
/* 2516 */         paramInt = arrayOfInternalParser[j].parseInto(paramDateTimeParserBucket, paramCharSequence, paramInt);
/*      */       }
/* 2518 */       return paramInt;
/*      */     }
/*      */     
/*      */     boolean isPrinter() {
/* 2522 */       return this.iPrinters != null;
/*      */     }
/*      */     
/*      */     boolean isParser() {
/* 2526 */       return this.iParsers != null;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */     private void decompose(List<Object> paramList1, List<Object> paramList2, List<Object> paramList3)
/*      */     {
/* 2534 */       int i = paramList1.size();
/* 2535 */       for (int j = 0; j < i; j += 2) {
/* 2536 */         Object localObject = paramList1.get(j);
/* 2537 */         if ((localObject instanceof Composite)) {
/* 2538 */           addArrayToList(paramList2, ((Composite)localObject).iPrinters);
/*      */         } else {
/* 2540 */           paramList2.add(localObject);
/*      */         }
/*      */         
/* 2543 */         localObject = paramList1.get(j + 1);
/* 2544 */         if ((localObject instanceof Composite)) {
/* 2545 */           addArrayToList(paramList3, ((Composite)localObject).iParsers);
/*      */         } else {
/* 2547 */           paramList3.add(localObject);
/*      */         }
/*      */       }
/*      */     }
/*      */     
/*      */     private void addArrayToList(List<Object> paramList, Object[] paramArrayOfObject) {
/* 2553 */       if (paramArrayOfObject != null) {
/* 2554 */         for (int i = 0; i < paramArrayOfObject.length; i++) {
/* 2555 */           paramList.add(paramArrayOfObject[i]);
/*      */         }
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   static class MatchingParser
/*      */     implements InternalParser
/*      */   {
/*      */     private final InternalParser[] iParsers;
/*      */     private final int iParsedLengthEstimate;
/*      */     
/*      */     MatchingParser(InternalParser[] paramArrayOfInternalParser)
/*      */     {
/* 2570 */       this.iParsers = paramArrayOfInternalParser;
/* 2571 */       int i = 0;
/* 2572 */       int j = paramArrayOfInternalParser.length; for (;;) { j--; if (j < 0) break;
/* 2573 */         InternalParser localInternalParser = paramArrayOfInternalParser[j];
/* 2574 */         if (localInternalParser != null) {
/* 2575 */           int k = localInternalParser.estimateParsedLength();
/* 2576 */           if (k > i) {
/* 2577 */             i = k;
/*      */           }
/*      */         }
/*      */       }
/* 2581 */       this.iParsedLengthEstimate = i;
/*      */     }
/*      */     
/*      */     public int estimateParsedLength() {
/* 2585 */       return this.iParsedLengthEstimate;
/*      */     }
/*      */     
/*      */     public int parseInto(DateTimeParserBucket paramDateTimeParserBucket, CharSequence paramCharSequence, int paramInt) {
/* 2589 */       InternalParser[] arrayOfInternalParser = this.iParsers;
/* 2590 */       int i = arrayOfInternalParser.length;
/*      */       
/* 2592 */       Object localObject1 = paramDateTimeParserBucket.saveState();
/* 2593 */       int j = 0;
/*      */       
/* 2595 */       int k = paramInt;
/* 2596 */       Object localObject2 = null;
/*      */       
/* 2598 */       int m = paramInt;
/*      */       
/* 2600 */       for (int n = 0; n < i; n++) {
/* 2601 */         InternalParser localInternalParser = arrayOfInternalParser[n];
/* 2602 */         if (localInternalParser == null)
/*      */         {
/* 2604 */           if (k <= paramInt) {
/* 2605 */             return paramInt;
/*      */           }
/* 2607 */           j = 1;
/* 2608 */           break;
/*      */         }
/* 2610 */         int i1 = localInternalParser.parseInto(paramDateTimeParserBucket, paramCharSequence, paramInt);
/* 2611 */         if (i1 >= paramInt) {
/* 2612 */           if (i1 > k) {
/* 2613 */             if ((i1 >= paramCharSequence.length()) || (n + 1 >= i) || (arrayOfInternalParser[(n + 1)] == null))
/*      */             {
/*      */ 
/*      */ 
/*      */ 
/* 2618 */               return i1;
/*      */             }
/* 2620 */             k = i1;
/* 2621 */             localObject2 = paramDateTimeParserBucket.saveState();
/*      */           }
/*      */         }
/* 2624 */         else if (i1 < 0) {
/* 2625 */           i1 ^= 0xFFFFFFFF;
/* 2626 */           if (i1 > m) {
/* 2627 */             m = i1;
/*      */           }
/*      */         }
/*      */         
/* 2631 */         paramDateTimeParserBucket.restoreState(localObject1);
/*      */       }
/*      */       
/* 2634 */       if ((k > paramInt) || ((k == paramInt) && (j != 0)))
/*      */       {
/* 2636 */         if (localObject2 != null) {
/* 2637 */           paramDateTimeParserBucket.restoreState(localObject2);
/*      */         }
/* 2639 */         return k;
/*      */       }
/*      */       
/* 2642 */       return m ^ 0xFFFFFFFF;
/*      */     }
/*      */   }
/*      */   
/*      */   static boolean csStartsWith(CharSequence paramCharSequence, int paramInt, String paramString) {
/* 2647 */     int i = paramString.length();
/* 2648 */     if (paramCharSequence.length() - paramInt < i) {
/* 2649 */       return false;
/*      */     }
/* 2651 */     for (int j = 0; j < i; j++) {
/* 2652 */       if (paramCharSequence.charAt(paramInt + j) != paramString.charAt(j)) {
/* 2653 */         return false;
/*      */       }
/*      */     }
/* 2656 */     return true;
/*      */   }
/*      */   
/*      */   static boolean csStartsWithIgnoreCase(CharSequence paramCharSequence, int paramInt, String paramString) {
/* 2660 */     int i = paramString.length();
/* 2661 */     if (paramCharSequence.length() - paramInt < i) {
/* 2662 */       return false;
/*      */     }
/* 2664 */     for (int j = 0; j < i; j++) {
/* 2665 */       char c1 = paramCharSequence.charAt(paramInt + j);
/* 2666 */       char c2 = paramString.charAt(j);
/* 2667 */       if (c1 != c2) {
/* 2668 */         char c3 = Character.toUpperCase(c1);
/* 2669 */         char c4 = Character.toUpperCase(c2);
/* 2670 */         if ((c3 != c4) && (Character.toLowerCase(c3) != Character.toLowerCase(c4))) {
/* 2671 */           return false;
/*      */         }
/*      */       }
/*      */     }
/* 2675 */     return true;
/*      */   }
/*      */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\org\joda\time\format\DateTimeFormatterBuilder.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */