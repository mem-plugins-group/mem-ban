/*      */ package org.joda.time.tz;
/*      */ 
/*      */ import java.io.DataInput;
/*      */ import java.io.DataInputStream;
/*      */ import java.io.DataOutput;
/*      */ import java.io.DataOutputStream;
/*      */ import java.io.IOException;
/*      */ import java.io.InputStream;
/*      */ import java.io.OutputStream;
/*      */ import java.io.PrintStream;
/*      */ import java.text.DateFormatSymbols;
/*      */ import java.util.ArrayList;
/*      */ import java.util.Arrays;
/*      */ import java.util.HashSet;
/*      */ import java.util.Iterator;
/*      */ import java.util.Locale;
/*      */ import java.util.Set;
/*      */ import org.joda.time.Chronology;
/*      */ import org.joda.time.DateTime;
/*      */ import org.joda.time.DateTimeField;
/*      */ import org.joda.time.DateTimeUtils;
/*      */ import org.joda.time.DateTimeZone;
/*      */ import org.joda.time.Period;
/*      */ import org.joda.time.PeriodType;
/*      */ import org.joda.time.chrono.ISOChronology;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public class DateTimeZoneBuilder
/*      */ {
/*      */   private final ArrayList<RuleSet> iRuleSets;
/*      */   
/*      */   public static DateTimeZone readFrom(InputStream paramInputStream, String paramString)
/*      */     throws IOException
/*      */   {
/*   95 */     if ((paramInputStream instanceof DataInput)) {
/*   96 */       return readFrom((DataInput)paramInputStream, paramString);
/*      */     }
/*   98 */     return readFrom(new DataInputStream(paramInputStream), paramString);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static DateTimeZone readFrom(DataInput paramDataInput, String paramString)
/*      */     throws IOException
/*      */   {
/*  110 */     switch (paramDataInput.readUnsignedByte()) {
/*      */     case 70: 
/*  112 */       Object localObject = new FixedDateTimeZone(paramString, paramDataInput.readUTF(), (int)readMillis(paramDataInput), (int)readMillis(paramDataInput));
/*      */       
/*  114 */       if (((DateTimeZone)localObject).equals(DateTimeZone.UTC)) {
/*  115 */         localObject = DateTimeZone.UTC;
/*      */       }
/*  117 */       return (DateTimeZone)localObject;
/*      */     case 67: 
/*  119 */       return CachedDateTimeZone.forZone(PrecalculatedZone.readFrom(paramDataInput, paramString));
/*      */     case 80: 
/*  121 */       return PrecalculatedZone.readFrom(paramDataInput, paramString);
/*      */     }
/*  123 */     throw new IOException("Invalid encoding");
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   static void writeMillis(DataOutput paramDataOutput, long paramLong)
/*      */     throws IOException
/*      */   {
/*      */     long l;
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  140 */     if (paramLong % 1800000L == 0L)
/*      */     {
/*  142 */       l = paramLong / 1800000L;
/*  143 */       if (l << 58 >> 58 == l)
/*      */       {
/*  145 */         paramDataOutput.writeByte((int)(l & 0x3F));
/*  146 */         return;
/*      */       }
/*      */     }
/*      */     
/*  150 */     if (paramLong % 60000L == 0L)
/*      */     {
/*  152 */       l = paramLong / 60000L;
/*  153 */       if (l << 34 >> 34 == l)
/*      */       {
/*  155 */         paramDataOutput.writeInt(0x40000000 | (int)(l & 0x3FFFFFFF));
/*  156 */         return;
/*      */       }
/*      */     }
/*      */     
/*  160 */     if (paramLong % 1000L == 0L)
/*      */     {
/*  162 */       l = paramLong / 1000L;
/*  163 */       if (l << 26 >> 26 == l)
/*      */       {
/*  165 */         paramDataOutput.writeByte(0x80 | (int)(l >> 32 & 0x3F));
/*  166 */         paramDataOutput.writeInt((int)(l & 0xFFFFFFFFFFFFFFFF));
/*  167 */         return;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  175 */     paramDataOutput.writeByte(paramLong < 0L ? 255 : 192);
/*  176 */     paramDataOutput.writeLong(paramLong);
/*      */   }
/*      */   
/*      */ 
/*      */   static long readMillis(DataInput paramDataInput)
/*      */     throws IOException
/*      */   {
/*  183 */     int i = paramDataInput.readUnsignedByte();
/*  184 */     switch (i >> 6) {
/*      */     case 0: 
/*      */     default: 
/*  187 */       i = i << 26 >> 26;
/*  188 */       return i * 1800000L;
/*      */     
/*      */ 
/*      */     case 1: 
/*  192 */       i = i << 26 >> 2;
/*  193 */       i |= paramDataInput.readUnsignedByte() << 16;
/*  194 */       i |= paramDataInput.readUnsignedByte() << 8;
/*  195 */       i |= paramDataInput.readUnsignedByte();
/*  196 */       return i * 60000L;
/*      */     
/*      */ 
/*      */     case 2: 
/*  200 */       long l = i << 58 >> 26;
/*  201 */       l |= paramDataInput.readUnsignedByte() << 24;
/*  202 */       l |= paramDataInput.readUnsignedByte() << 16;
/*  203 */       l |= paramDataInput.readUnsignedByte() << 8;
/*  204 */       l |= paramDataInput.readUnsignedByte();
/*  205 */       return l * 1000L;
/*      */     }
/*      */     
/*      */     
/*  209 */     return paramDataInput.readLong();
/*      */   }
/*      */   
/*      */ 
/*      */   private static DateTimeZone buildFixedZone(String paramString1, String paramString2, int paramInt1, int paramInt2)
/*      */   {
/*  215 */     if (("UTC".equals(paramString1)) && (paramString1.equals(paramString2)) && (paramInt1 == 0) && (paramInt2 == 0))
/*      */     {
/*  217 */       return DateTimeZone.UTC;
/*      */     }
/*  219 */     return new FixedDateTimeZone(paramString1, paramString2, paramInt1, paramInt2);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public DateTimeZoneBuilder()
/*      */   {
/*  226 */     this.iRuleSets = new ArrayList(10);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeZoneBuilder addCutover(int paramInt1, char paramChar, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean, int paramInt5)
/*      */   {
/*  252 */     if (this.iRuleSets.size() > 0) {
/*  253 */       OfYear localOfYear = new OfYear(paramChar, paramInt2, paramInt3, paramInt4, paramBoolean, paramInt5);
/*      */       
/*  255 */       RuleSet localRuleSet = (RuleSet)this.iRuleSets.get(this.iRuleSets.size() - 1);
/*  256 */       localRuleSet.setUpperLimit(paramInt1, localOfYear);
/*      */     }
/*  258 */     this.iRuleSets.add(new RuleSet());
/*  259 */     return this;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeZoneBuilder setStandardOffset(int paramInt)
/*      */   {
/*  268 */     getLastRuleSet().setStandardOffset(paramInt);
/*  269 */     return this;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public DateTimeZoneBuilder setFixedSavings(String paramString, int paramInt)
/*      */   {
/*  276 */     getLastRuleSet().setFixedSavings(paramString, paramInt);
/*  277 */     return this;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeZoneBuilder addRecurringSavings(String paramString, int paramInt1, int paramInt2, int paramInt3, char paramChar, int paramInt4, int paramInt5, int paramInt6, boolean paramBoolean, int paramInt7)
/*      */   {
/*  309 */     if (paramInt2 <= paramInt3) {
/*  310 */       OfYear localOfYear = new OfYear(paramChar, paramInt4, paramInt5, paramInt6, paramBoolean, paramInt7);
/*      */       
/*  312 */       Recurrence localRecurrence = new Recurrence(localOfYear, paramString, paramInt1);
/*  313 */       Rule localRule = new Rule(localRecurrence, paramInt2, paramInt3);
/*  314 */       getLastRuleSet().addRule(localRule);
/*      */     }
/*  316 */     return this;
/*      */   }
/*      */   
/*      */   private RuleSet getLastRuleSet() {
/*  320 */     if (this.iRuleSets.size() == 0) {
/*  321 */       addCutover(Integer.MIN_VALUE, 'w', 1, 1, 0, false, 0);
/*      */     }
/*  323 */     return (RuleSet)this.iRuleSets.get(this.iRuleSets.size() - 1);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateTimeZone toDateTimeZone(String paramString, boolean paramBoolean)
/*      */   {
/*  333 */     if (paramString == null) {
/*  334 */       throw new IllegalArgumentException();
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  339 */     ArrayList localArrayList = new ArrayList();
/*      */     
/*      */ 
/*      */ 
/*  343 */     DSTZone localDSTZone = null;
/*      */     
/*  345 */     long l = Long.MIN_VALUE;
/*  346 */     int i = 0;
/*      */     
/*  348 */     int j = this.iRuleSets.size();
/*  349 */     for (int k = 0; k < j; k++) {
/*  350 */       RuleSet localRuleSet = (RuleSet)this.iRuleSets.get(k);
/*  351 */       Transition localTransition = localRuleSet.firstTransition(l);
/*  352 */       if (localTransition != null)
/*      */       {
/*      */ 
/*  355 */         addTransition(localArrayList, localTransition);
/*  356 */         l = localTransition.getMillis();
/*  357 */         i = localTransition.getSaveMillis();
/*      */         
/*      */ 
/*  360 */         localRuleSet = new RuleSet(localRuleSet);
/*      */         
/*  362 */         while (((localTransition = localRuleSet.nextTransition(l, i)) != null) && (
/*  363 */           (!addTransition(localArrayList, localTransition)) || (localDSTZone == null)))
/*      */         {
/*      */ 
/*      */ 
/*  367 */           l = localTransition.getMillis();
/*  368 */           i = localTransition.getSaveMillis();
/*  369 */           if ((localDSTZone == null) && (k == j - 1)) {
/*  370 */             localDSTZone = localRuleSet.buildTailZone(paramString);
/*      */           }
/*      */         }
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*  377 */         l = localRuleSet.getUpperLimit(i);
/*      */       }
/*      */     }
/*      */     
/*  381 */     if (localArrayList.size() == 0) {
/*  382 */       if (localDSTZone != null)
/*      */       {
/*  384 */         return localDSTZone;
/*      */       }
/*  386 */       return buildFixedZone(paramString, "UTC", 0, 0);
/*      */     }
/*  388 */     if ((localArrayList.size() == 1) && (localDSTZone == null)) {
/*  389 */       localObject = (Transition)localArrayList.get(0);
/*  390 */       return buildFixedZone(paramString, ((Transition)localObject).getNameKey(), ((Transition)localObject).getWallOffset(), ((Transition)localObject).getStandardOffset());
/*      */     }
/*      */     
/*      */ 
/*  394 */     Object localObject = PrecalculatedZone.create(paramString, paramBoolean, localArrayList, localDSTZone);
/*  395 */     if (((PrecalculatedZone)localObject).isCachable()) {
/*  396 */       return CachedDateTimeZone.forZone((DateTimeZone)localObject);
/*      */     }
/*  398 */     return (DateTimeZone)localObject;
/*      */   }
/*      */   
/*      */   private boolean addTransition(ArrayList<Transition> paramArrayList, Transition paramTransition) {
/*  402 */     int i = paramArrayList.size();
/*  403 */     if (i == 0)
/*      */     {
/*  405 */       paramArrayList.add(paramTransition);
/*  406 */       return true;
/*      */     }
/*      */     
/*  409 */     Transition localTransition1 = (Transition)paramArrayList.get(i - 1);
/*  410 */     if (!paramTransition.isTransitionFrom(localTransition1))
/*      */     {
/*  412 */       return false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  417 */     int j = 0;
/*  418 */     if (i >= 2) {
/*  419 */       j = ((Transition)paramArrayList.get(i - 2)).getWallOffset();
/*      */     }
/*  421 */     int k = localTransition1.getWallOffset();
/*      */     
/*  423 */     long l1 = localTransition1.getMillis() + j;
/*  424 */     long l2 = paramTransition.getMillis() + k;
/*      */     
/*  426 */     if (l2 != l1) {
/*  427 */       paramArrayList.add(paramTransition);
/*      */       
/*  429 */       return true;
/*      */     }
/*  431 */     Transition localTransition2 = (Transition)paramArrayList.remove(i - 1);
/*  432 */     Transition localTransition3 = paramTransition.withMillis(localTransition2.getMillis());
/*      */     
/*      */ 
/*      */ 
/*  436 */     return addTransition(paramArrayList, localTransition3);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void writeTo(String paramString, OutputStream paramOutputStream)
/*      */     throws IOException
/*      */   {
/*  447 */     if ((paramOutputStream instanceof DataOutput)) {
/*  448 */       writeTo(paramString, (DataOutput)paramOutputStream);
/*      */     } else {
/*  450 */       DataOutputStream localDataOutputStream = new DataOutputStream(paramOutputStream);
/*  451 */       writeTo(paramString, localDataOutputStream);
/*  452 */       localDataOutputStream.flush();
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void writeTo(String paramString, DataOutput paramDataOutput)
/*      */     throws IOException
/*      */   {
/*  465 */     DateTimeZone localDateTimeZone = toDateTimeZone(paramString, false);
/*      */     
/*  467 */     if ((localDateTimeZone instanceof FixedDateTimeZone)) {
/*  468 */       paramDataOutput.writeByte(70);
/*  469 */       paramDataOutput.writeUTF(localDateTimeZone.getNameKey(0L));
/*  470 */       writeMillis(paramDataOutput, localDateTimeZone.getOffset(0L));
/*  471 */       writeMillis(paramDataOutput, localDateTimeZone.getStandardOffset(0L));
/*      */     } else {
/*  473 */       if ((localDateTimeZone instanceof CachedDateTimeZone)) {
/*  474 */         paramDataOutput.writeByte(67);
/*  475 */         localDateTimeZone = ((CachedDateTimeZone)localDateTimeZone).getUncachedZone();
/*      */       } else {
/*  477 */         paramDataOutput.writeByte(80);
/*      */       }
/*  479 */       ((PrecalculatedZone)localDateTimeZone).writeTo(paramDataOutput); } }
/*      */   
/*      */   private static final class OfYear { final char iMode;
/*      */     final int iMonthOfYear;
/*      */     final int iDayOfMonth;
/*      */     final int iDayOfWeek;
/*      */     final boolean iAdvance;
/*      */     final int iMillisOfDay;
/*      */     
/*  488 */     static OfYear readFrom(DataInput paramDataInput) throws IOException { return new OfYear((char)paramDataInput.readUnsignedByte(), paramDataInput.readUnsignedByte(), paramDataInput.readByte(), paramDataInput.readUnsignedByte(), paramDataInput.readBoolean(), (int)DateTimeZoneBuilder.readMillis(paramDataInput)); }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     OfYear(char paramChar, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean, int paramInt4)
/*      */     {
/*  511 */       if ((paramChar != 'u') && (paramChar != 'w') && (paramChar != 's')) {
/*  512 */         throw new IllegalArgumentException("Unknown mode: " + paramChar);
/*      */       }
/*      */       
/*  515 */       this.iMode = paramChar;
/*  516 */       this.iMonthOfYear = paramInt1;
/*  517 */       this.iDayOfMonth = paramInt2;
/*  518 */       this.iDayOfWeek = paramInt3;
/*  519 */       this.iAdvance = paramBoolean;
/*  520 */       this.iMillisOfDay = paramInt4;
/*      */     }
/*      */     
/*      */ 
/*      */     public long setInstant(int paramInt1, int paramInt2, int paramInt3)
/*      */     {
/*      */       int i;
/*      */       
/*  528 */       if (this.iMode == 'w') {
/*  529 */         i = paramInt2 + paramInt3;
/*  530 */       } else if (this.iMode == 's') {
/*  531 */         i = paramInt2;
/*      */       } else {
/*  533 */         i = 0;
/*      */       }
/*      */       
/*  536 */       ISOChronology localISOChronology = ISOChronology.getInstanceUTC();
/*  537 */       long l = localISOChronology.year().set(0L, paramInt1);
/*  538 */       l = localISOChronology.monthOfYear().set(l, this.iMonthOfYear);
/*  539 */       l = localISOChronology.millisOfDay().set(l, this.iMillisOfDay);
/*  540 */       l = setDayOfMonth(localISOChronology, l);
/*      */       
/*  542 */       if (this.iDayOfWeek != 0) {
/*  543 */         l = setDayOfWeek(localISOChronology, l);
/*      */       }
/*      */       
/*      */ 
/*  547 */       return l - i;
/*      */     }
/*      */     
/*      */ 
/*      */     public long next(long paramLong, int paramInt1, int paramInt2)
/*      */     {
/*      */       int i;
/*      */       
/*  555 */       if (this.iMode == 'w') {
/*  556 */         i = paramInt1 + paramInt2;
/*  557 */       } else if (this.iMode == 's') {
/*  558 */         i = paramInt1;
/*      */       } else {
/*  560 */         i = 0;
/*      */       }
/*      */       
/*      */ 
/*  564 */       paramLong += i;
/*      */       
/*  566 */       ISOChronology localISOChronology = ISOChronology.getInstanceUTC();
/*  567 */       long l = localISOChronology.monthOfYear().set(paramLong, this.iMonthOfYear);
/*      */       
/*  569 */       l = localISOChronology.millisOfDay().set(l, 0);
/*  570 */       l = localISOChronology.millisOfDay().add(l, this.iMillisOfDay);
/*  571 */       l = setDayOfMonthNext(localISOChronology, l);
/*      */       
/*  573 */       if (this.iDayOfWeek == 0) {
/*  574 */         if (l <= paramLong) {
/*  575 */           l = localISOChronology.year().add(l, 1);
/*  576 */           l = setDayOfMonthNext(localISOChronology, l);
/*      */         }
/*      */       } else {
/*  579 */         l = setDayOfWeek(localISOChronology, l);
/*  580 */         if (l <= paramLong) {
/*  581 */           l = localISOChronology.year().add(l, 1);
/*  582 */           l = localISOChronology.monthOfYear().set(l, this.iMonthOfYear);
/*  583 */           l = setDayOfMonthNext(localISOChronology, l);
/*  584 */           l = setDayOfWeek(localISOChronology, l);
/*      */         }
/*      */       }
/*      */       
/*      */ 
/*  589 */       return l - i;
/*      */     }
/*      */     
/*      */ 
/*      */     public long previous(long paramLong, int paramInt1, int paramInt2)
/*      */     {
/*      */       int i;
/*      */       
/*  597 */       if (this.iMode == 'w') {
/*  598 */         i = paramInt1 + paramInt2;
/*  599 */       } else if (this.iMode == 's') {
/*  600 */         i = paramInt1;
/*      */       } else {
/*  602 */         i = 0;
/*      */       }
/*      */       
/*      */ 
/*  606 */       paramLong += i;
/*      */       
/*  608 */       ISOChronology localISOChronology = ISOChronology.getInstanceUTC();
/*  609 */       long l = localISOChronology.monthOfYear().set(paramLong, this.iMonthOfYear);
/*      */       
/*  611 */       l = localISOChronology.millisOfDay().set(l, 0);
/*  612 */       l = localISOChronology.millisOfDay().add(l, this.iMillisOfDay);
/*  613 */       l = setDayOfMonthPrevious(localISOChronology, l);
/*      */       
/*  615 */       if (this.iDayOfWeek == 0) {
/*  616 */         if (l >= paramLong) {
/*  617 */           l = localISOChronology.year().add(l, -1);
/*  618 */           l = setDayOfMonthPrevious(localISOChronology, l);
/*      */         }
/*      */       } else {
/*  621 */         l = setDayOfWeek(localISOChronology, l);
/*  622 */         if (l >= paramLong) {
/*  623 */           l = localISOChronology.year().add(l, -1);
/*  624 */           l = localISOChronology.monthOfYear().set(l, this.iMonthOfYear);
/*  625 */           l = setDayOfMonthPrevious(localISOChronology, l);
/*  626 */           l = setDayOfWeek(localISOChronology, l);
/*      */         }
/*      */       }
/*      */       
/*      */ 
/*  631 */       return l - i;
/*      */     }
/*      */     
/*      */     public boolean equals(Object paramObject) {
/*  635 */       if (this == paramObject) {
/*  636 */         return true;
/*      */       }
/*  638 */       if ((paramObject instanceof OfYear)) {
/*  639 */         OfYear localOfYear = (OfYear)paramObject;
/*  640 */         return (this.iMode == localOfYear.iMode) && (this.iMonthOfYear == localOfYear.iMonthOfYear) && (this.iDayOfMonth == localOfYear.iDayOfMonth) && (this.iDayOfWeek == localOfYear.iDayOfWeek) && (this.iAdvance == localOfYear.iAdvance) && (this.iMillisOfDay == localOfYear.iMillisOfDay);
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  648 */       return false;
/*      */     }
/*      */     
/*      */     public String toString() {
/*  652 */       return "[OfYear]\nMode: " + this.iMode + '\n' + "MonthOfYear: " + this.iMonthOfYear + '\n' + "DayOfMonth: " + this.iDayOfMonth + '\n' + "DayOfWeek: " + this.iDayOfWeek + '\n' + "AdvanceDayOfWeek: " + this.iAdvance + '\n' + "MillisOfDay: " + this.iMillisOfDay + '\n';
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     public void writeTo(DataOutput paramDataOutput)
/*      */       throws IOException
/*      */     {
/*  663 */       paramDataOutput.writeByte(this.iMode);
/*  664 */       paramDataOutput.writeByte(this.iMonthOfYear);
/*  665 */       paramDataOutput.writeByte(this.iDayOfMonth);
/*  666 */       paramDataOutput.writeByte(this.iDayOfWeek);
/*  667 */       paramDataOutput.writeBoolean(this.iAdvance);
/*  668 */       DateTimeZoneBuilder.writeMillis(paramDataOutput, this.iMillisOfDay);
/*      */     }
/*      */     
/*      */ 
/*      */     private long setDayOfMonthNext(Chronology paramChronology, long paramLong)
/*      */     {
/*      */       try
/*      */       {
/*  676 */         paramLong = setDayOfMonth(paramChronology, paramLong);
/*      */       } catch (IllegalArgumentException localIllegalArgumentException) {
/*  678 */         if ((this.iMonthOfYear == 2) && (this.iDayOfMonth == 29)) {
/*  679 */           while (!paramChronology.year().isLeap(paramLong)) {
/*  680 */             paramLong = paramChronology.year().add(paramLong, 1);
/*      */           }
/*  682 */           paramLong = setDayOfMonth(paramChronology, paramLong);
/*      */         } else {
/*  684 */           throw localIllegalArgumentException;
/*      */         }
/*      */       }
/*  687 */       return paramLong;
/*      */     }
/*      */     
/*      */ 
/*      */     private long setDayOfMonthPrevious(Chronology paramChronology, long paramLong)
/*      */     {
/*      */       try
/*      */       {
/*  695 */         paramLong = setDayOfMonth(paramChronology, paramLong);
/*      */       } catch (IllegalArgumentException localIllegalArgumentException) {
/*  697 */         if ((this.iMonthOfYear == 2) && (this.iDayOfMonth == 29)) {
/*  698 */           while (!paramChronology.year().isLeap(paramLong)) {
/*  699 */             paramLong = paramChronology.year().add(paramLong, -1);
/*      */           }
/*  701 */           paramLong = setDayOfMonth(paramChronology, paramLong);
/*      */         } else {
/*  703 */           throw localIllegalArgumentException;
/*      */         }
/*      */       }
/*  706 */       return paramLong;
/*      */     }
/*      */     
/*      */     private long setDayOfMonth(Chronology paramChronology, long paramLong) {
/*  710 */       if (this.iDayOfMonth >= 0) {
/*  711 */         paramLong = paramChronology.dayOfMonth().set(paramLong, this.iDayOfMonth);
/*      */       } else {
/*  713 */         paramLong = paramChronology.dayOfMonth().set(paramLong, 1);
/*  714 */         paramLong = paramChronology.monthOfYear().add(paramLong, 1);
/*  715 */         paramLong = paramChronology.dayOfMonth().add(paramLong, this.iDayOfMonth);
/*      */       }
/*  717 */       return paramLong;
/*      */     }
/*      */     
/*      */     private long setDayOfWeek(Chronology paramChronology, long paramLong) {
/*  721 */       int i = paramChronology.dayOfWeek().get(paramLong);
/*  722 */       int j = this.iDayOfWeek - i;
/*  723 */       if (j != 0) {
/*  724 */         if (this.iAdvance) {
/*  725 */           if (j < 0) {
/*  726 */             j += 7;
/*      */           }
/*      */         }
/*  729 */         else if (j > 0) {
/*  730 */           j -= 7;
/*      */         }
/*      */         
/*  733 */         paramLong = paramChronology.dayOfWeek().add(paramLong, j);
/*      */       }
/*  735 */       return paramLong;
/*      */     }
/*      */   }
/*      */   
/*      */   private static final class Recurrence {
/*      */     final DateTimeZoneBuilder.OfYear iOfYear;
/*      */     final String iNameKey;
/*      */     final int iSaveMillis;
/*      */     
/*  744 */     static Recurrence readFrom(DataInput paramDataInput) throws IOException { return new Recurrence(DateTimeZoneBuilder.OfYear.readFrom(paramDataInput), paramDataInput.readUTF(), (int)DateTimeZoneBuilder.readMillis(paramDataInput)); }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     Recurrence(DateTimeZoneBuilder.OfYear paramOfYear, String paramString, int paramInt)
/*      */     {
/*  752 */       this.iOfYear = paramOfYear;
/*  753 */       this.iNameKey = paramString;
/*  754 */       this.iSaveMillis = paramInt;
/*      */     }
/*      */     
/*      */     public DateTimeZoneBuilder.OfYear getOfYear() {
/*  758 */       return this.iOfYear;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */     public long next(long paramLong, int paramInt1, int paramInt2)
/*      */     {
/*  765 */       return this.iOfYear.next(paramLong, paramInt1, paramInt2);
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */     public long previous(long paramLong, int paramInt1, int paramInt2)
/*      */     {
/*  772 */       return this.iOfYear.previous(paramLong, paramInt1, paramInt2);
/*      */     }
/*      */     
/*      */     public String getNameKey() {
/*  776 */       return this.iNameKey;
/*      */     }
/*      */     
/*      */     public int getSaveMillis() {
/*  780 */       return this.iSaveMillis;
/*      */     }
/*      */     
/*      */     public boolean equals(Object paramObject) {
/*  784 */       if (this == paramObject) {
/*  785 */         return true;
/*      */       }
/*  787 */       if ((paramObject instanceof Recurrence)) {
/*  788 */         Recurrence localRecurrence = (Recurrence)paramObject;
/*  789 */         return (this.iSaveMillis == localRecurrence.iSaveMillis) && (this.iNameKey.equals(localRecurrence.iNameKey)) && (this.iOfYear.equals(localRecurrence.iOfYear));
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*  794 */       return false;
/*      */     }
/*      */     
/*      */     public void writeTo(DataOutput paramDataOutput) throws IOException {
/*  798 */       this.iOfYear.writeTo(paramDataOutput);
/*  799 */       paramDataOutput.writeUTF(this.iNameKey);
/*  800 */       DateTimeZoneBuilder.writeMillis(paramDataOutput, this.iSaveMillis);
/*      */     }
/*      */     
/*      */     Recurrence rename(String paramString) {
/*  804 */       return new Recurrence(this.iOfYear, paramString, this.iSaveMillis);
/*      */     }
/*      */     
/*      */     Recurrence renameAppend(String paramString) {
/*  808 */       return rename((this.iNameKey + paramString).intern());
/*      */     }
/*      */     
/*      */     public String toString()
/*      */     {
/*  813 */       return this.iOfYear + " named " + this.iNameKey + " at " + this.iSaveMillis;
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   private static final class Rule
/*      */   {
/*      */     final DateTimeZoneBuilder.Recurrence iRecurrence;
/*      */     final int iFromYear;
/*      */     final int iToYear;
/*      */     
/*      */     Rule(DateTimeZoneBuilder.Recurrence paramRecurrence, int paramInt1, int paramInt2)
/*      */     {
/*  826 */       this.iRecurrence = paramRecurrence;
/*  827 */       this.iFromYear = paramInt1;
/*  828 */       this.iToYear = paramInt2;
/*      */     }
/*      */     
/*      */     public int getFromYear()
/*      */     {
/*  833 */       return this.iFromYear;
/*      */     }
/*      */     
/*      */     public int getToYear() {
/*  837 */       return this.iToYear;
/*      */     }
/*      */     
/*      */     public DateTimeZoneBuilder.OfYear getOfYear()
/*      */     {
/*  842 */       return this.iRecurrence.getOfYear();
/*      */     }
/*      */     
/*      */     public String getNameKey() {
/*  846 */       return this.iRecurrence.getNameKey();
/*      */     }
/*      */     
/*      */     public int getSaveMillis() {
/*  850 */       return this.iRecurrence.getSaveMillis();
/*      */     }
/*      */     
/*      */     public long next(long paramLong, int paramInt1, int paramInt2) {
/*  854 */       ISOChronology localISOChronology = ISOChronology.getInstanceUTC();
/*      */       
/*  856 */       int i = paramInt1 + paramInt2;
/*  857 */       long l1 = paramLong;
/*      */       
/*      */       int j;
/*  860 */       if (paramLong == Long.MIN_VALUE) {
/*  861 */         j = Integer.MIN_VALUE;
/*      */       } else {
/*  863 */         j = localISOChronology.year().get(paramLong + i);
/*      */       }
/*      */       
/*  866 */       if (j < this.iFromYear)
/*      */       {
/*  868 */         l1 = localISOChronology.year().set(0L, this.iFromYear) - i;
/*      */         
/*      */ 
/*  871 */         l1 -= 1L;
/*      */       }
/*      */       
/*  874 */       long l2 = this.iRecurrence.next(l1, paramInt1, paramInt2);
/*      */       
/*  876 */       if (l2 > paramLong) {
/*  877 */         j = localISOChronology.year().get(l2 + i);
/*  878 */         if (j > this.iToYear)
/*      */         {
/*  880 */           l2 = paramLong;
/*      */         }
/*      */       }
/*      */       
/*  884 */       return l2;
/*      */     }
/*      */     
/*      */     public String toString()
/*      */     {
/*  889 */       return this.iFromYear + " to " + this.iToYear + " using " + this.iRecurrence;
/*      */     }
/*      */   }
/*      */   
/*      */   private static final class Transition {
/*      */     private final long iMillis;
/*      */     private final String iNameKey;
/*      */     private final int iWallOffset;
/*      */     private final int iStandardOffset;
/*      */     
/*      */     Transition(long paramLong, Transition paramTransition) {
/*  900 */       this.iMillis = paramLong;
/*  901 */       this.iNameKey = paramTransition.iNameKey;
/*  902 */       this.iWallOffset = paramTransition.iWallOffset;
/*  903 */       this.iStandardOffset = paramTransition.iStandardOffset;
/*      */     }
/*      */     
/*      */     Transition(long paramLong, DateTimeZoneBuilder.Rule paramRule, int paramInt) {
/*  907 */       this.iMillis = paramLong;
/*  908 */       this.iNameKey = paramRule.getNameKey();
/*  909 */       this.iWallOffset = (paramInt + paramRule.getSaveMillis());
/*  910 */       this.iStandardOffset = paramInt;
/*      */     }
/*      */     
/*      */     Transition(long paramLong, String paramString, int paramInt1, int paramInt2)
/*      */     {
/*  915 */       this.iMillis = paramLong;
/*  916 */       this.iNameKey = paramString;
/*  917 */       this.iWallOffset = paramInt1;
/*  918 */       this.iStandardOffset = paramInt2;
/*      */     }
/*      */     
/*      */     public long getMillis() {
/*  922 */       return this.iMillis;
/*      */     }
/*      */     
/*      */     public String getNameKey() {
/*  926 */       return this.iNameKey;
/*      */     }
/*      */     
/*      */     public int getWallOffset() {
/*  930 */       return this.iWallOffset;
/*      */     }
/*      */     
/*      */     public int getStandardOffset() {
/*  934 */       return this.iStandardOffset;
/*      */     }
/*      */     
/*      */     public int getSaveMillis() {
/*  938 */       return this.iWallOffset - this.iStandardOffset;
/*      */     }
/*      */     
/*      */     public Transition withMillis(long paramLong) {
/*  942 */       return new Transition(paramLong, this.iNameKey, this.iWallOffset, this.iStandardOffset);
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */     public boolean isTransitionFrom(Transition paramTransition)
/*      */     {
/*  949 */       if (paramTransition == null) {
/*  950 */         return true;
/*      */       }
/*  952 */       return (this.iMillis > paramTransition.iMillis) && ((this.iWallOffset != paramTransition.iWallOffset) || (this.iStandardOffset != paramTransition.iStandardOffset) || (!this.iNameKey.equals(paramTransition.iNameKey)));
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  960 */     public String toString() { return new DateTime(this.iMillis, DateTimeZone.UTC) + " " + this.iStandardOffset + " " + this.iWallOffset; }
/*      */   }
/*      */   
/*      */   private static final class RuleSet {
/*      */     private static final int YEAR_LIMIT;
/*      */     private int iStandardOffset;
/*      */     private ArrayList<DateTimeZoneBuilder.Rule> iRules;
/*      */     private String iInitialNameKey;
/*      */     private int iInitialSaveMillis;
/*      */     private int iUpperYear;
/*      */     private DateTimeZoneBuilder.OfYear iUpperOfYear;
/*      */     
/*      */     static {
/*  973 */       long l = DateTimeUtils.currentTimeMillis();
/*  974 */       YEAR_LIMIT = ISOChronology.getInstanceUTC().year().get(l) + 100;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     RuleSet()
/*      */     {
/*  989 */       this.iRules = new ArrayList(10);
/*  990 */       this.iUpperYear = Integer.MAX_VALUE;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */     RuleSet(RuleSet paramRuleSet)
/*      */     {
/*  997 */       this.iStandardOffset = paramRuleSet.iStandardOffset;
/*  998 */       this.iRules = new ArrayList(paramRuleSet.iRules);
/*  999 */       this.iInitialNameKey = paramRuleSet.iInitialNameKey;
/* 1000 */       this.iInitialSaveMillis = paramRuleSet.iInitialSaveMillis;
/* 1001 */       this.iUpperYear = paramRuleSet.iUpperYear;
/* 1002 */       this.iUpperOfYear = paramRuleSet.iUpperOfYear;
/*      */     }
/*      */     
/*      */     public int getStandardOffset()
/*      */     {
/* 1007 */       return this.iStandardOffset;
/*      */     }
/*      */     
/*      */     public void setStandardOffset(int paramInt) {
/* 1011 */       this.iStandardOffset = paramInt;
/*      */     }
/*      */     
/*      */     public void setFixedSavings(String paramString, int paramInt) {
/* 1015 */       this.iInitialNameKey = paramString;
/* 1016 */       this.iInitialSaveMillis = paramInt;
/*      */     }
/*      */     
/*      */     public void addRule(DateTimeZoneBuilder.Rule paramRule) {
/* 1020 */       if (!this.iRules.contains(paramRule)) {
/* 1021 */         this.iRules.add(paramRule);
/*      */       }
/*      */     }
/*      */     
/*      */     public void setUpperLimit(int paramInt, DateTimeZoneBuilder.OfYear paramOfYear) {
/* 1026 */       this.iUpperYear = paramInt;
/* 1027 */       this.iUpperOfYear = paramOfYear;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     public DateTimeZoneBuilder.Transition firstTransition(long paramLong)
/*      */     {
/* 1037 */       if (this.iInitialNameKey != null)
/*      */       {
/* 1039 */         return new DateTimeZoneBuilder.Transition(paramLong, this.iInitialNameKey, this.iStandardOffset + this.iInitialSaveMillis, this.iStandardOffset);
/*      */       }
/*      */       
/*      */ 
/*      */ 
/* 1044 */       ArrayList localArrayList = new ArrayList(this.iRules);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1050 */       long l = Long.MIN_VALUE;
/* 1051 */       int i = 0;
/* 1052 */       DateTimeZoneBuilder.Transition localTransition1 = null;
/*      */       
/*      */       DateTimeZoneBuilder.Transition localTransition2;
/* 1055 */       while ((localTransition2 = nextTransition(l, i)) != null) {
/* 1056 */         l = localTransition2.getMillis();
/*      */         
/* 1058 */         if (l == paramLong) {
/* 1059 */           localTransition1 = new DateTimeZoneBuilder.Transition(paramLong, localTransition2);
/* 1060 */           break;
/*      */         }
/*      */         
/* 1063 */         if (l > paramLong) {
/* 1064 */           if (localTransition1 == null)
/*      */           {
/*      */ 
/*      */ 
/* 1068 */             for (DateTimeZoneBuilder.Rule localRule : localArrayList) {
/* 1069 */               if (localRule.getSaveMillis() == 0) {
/* 1070 */                 localTransition1 = new DateTimeZoneBuilder.Transition(paramLong, localRule, this.iStandardOffset);
/* 1071 */                 break;
/*      */               }
/*      */             }
/*      */           }
/* 1075 */           if (localTransition1 != null) {
/*      */             break;
/*      */           }
/*      */           
/* 1079 */           localTransition1 = new DateTimeZoneBuilder.Transition(paramLong, localTransition2.getNameKey(), this.iStandardOffset, this.iStandardOffset); break;
/*      */         }
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1087 */         localTransition1 = new DateTimeZoneBuilder.Transition(paramLong, localTransition2);
/*      */         
/* 1089 */         i = localTransition2.getSaveMillis();
/*      */       }
/*      */       
/* 1092 */       this.iRules = localArrayList;
/* 1093 */       return localTransition1;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     public DateTimeZoneBuilder.Transition nextTransition(long paramLong, int paramInt)
/*      */     {
/* 1108 */       ISOChronology localISOChronology = ISOChronology.getInstanceUTC();
/*      */       
/*      */ 
/* 1111 */       Object localObject = null;
/* 1112 */       long l1 = Long.MAX_VALUE;
/*      */       
/* 1114 */       Iterator localIterator = this.iRules.iterator();
/* 1115 */       while (localIterator.hasNext()) {
/* 1116 */         DateTimeZoneBuilder.Rule localRule = (DateTimeZoneBuilder.Rule)localIterator.next();
/* 1117 */         long l3 = localRule.next(paramLong, this.iStandardOffset, paramInt);
/* 1118 */         if (l3 <= paramLong) {
/* 1119 */           localIterator.remove();
/*      */ 
/*      */ 
/*      */ 
/*      */         }
/* 1124 */         else if (l3 <= l1)
/*      */         {
/* 1126 */           localObject = localRule;
/* 1127 */           l1 = l3;
/*      */         }
/*      */       }
/*      */       
/* 1131 */       if (localObject == null) {
/* 1132 */         return null;
/*      */       }
/*      */       
/*      */ 
/* 1136 */       if (localISOChronology.year().get(l1) >= YEAR_LIMIT) {
/* 1137 */         return null;
/*      */       }
/*      */       
/*      */ 
/* 1141 */       if (this.iUpperYear < Integer.MAX_VALUE) {
/* 1142 */         long l2 = this.iUpperOfYear.setInstant(this.iUpperYear, this.iStandardOffset, paramInt);
/*      */         
/* 1144 */         if (l1 >= l2)
/*      */         {
/* 1146 */           return null;
/*      */         }
/*      */       }
/*      */       
/* 1150 */       return new DateTimeZoneBuilder.Transition(l1, (DateTimeZoneBuilder.Rule)localObject, this.iStandardOffset);
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */     public long getUpperLimit(int paramInt)
/*      */     {
/* 1157 */       if (this.iUpperYear == Integer.MAX_VALUE) {
/* 1158 */         return Long.MAX_VALUE;
/*      */       }
/* 1160 */       return this.iUpperOfYear.setInstant(this.iUpperYear, this.iStandardOffset, paramInt);
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */     public DateTimeZoneBuilder.DSTZone buildTailZone(String paramString)
/*      */     {
/* 1167 */       if (this.iRules.size() == 2) {
/* 1168 */         DateTimeZoneBuilder.Rule localRule1 = (DateTimeZoneBuilder.Rule)this.iRules.get(0);
/* 1169 */         DateTimeZoneBuilder.Rule localRule2 = (DateTimeZoneBuilder.Rule)this.iRules.get(1);
/* 1170 */         if ((localRule1.getToYear() == Integer.MAX_VALUE) && (localRule2.getToYear() == Integer.MAX_VALUE))
/*      */         {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1180 */           return new DateTimeZoneBuilder.DSTZone(paramString, this.iStandardOffset, localRule1.iRecurrence, localRule2.iRecurrence);
/*      */         }
/*      */       }
/*      */       
/* 1184 */       return null;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/* 1189 */     public String toString() { return this.iInitialNameKey + " initial: " + this.iInitialSaveMillis + " std: " + this.iStandardOffset + " upper: " + this.iUpperYear + " " + this.iUpperOfYear + " " + this.iRules; }
/*      */   }
/*      */   
/*      */   private static final class DSTZone extends DateTimeZone {
/*      */     private static final long serialVersionUID = 6941492635554961361L;
/*      */     final int iStandardOffset;
/*      */     final DateTimeZoneBuilder.Recurrence iStartRecurrence;
/*      */     final DateTimeZoneBuilder.Recurrence iEndRecurrence;
/*      */     
/* 1198 */     static DSTZone readFrom(DataInput paramDataInput, String paramString) throws IOException { return new DSTZone(paramString, (int)DateTimeZoneBuilder.readMillis(paramDataInput), DateTimeZoneBuilder.Recurrence.readFrom(paramDataInput), DateTimeZoneBuilder.Recurrence.readFrom(paramDataInput)); }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     DSTZone(String paramString, int paramInt, DateTimeZoneBuilder.Recurrence paramRecurrence1, DateTimeZoneBuilder.Recurrence paramRecurrence2)
/*      */     {
/* 1208 */       super();
/* 1209 */       this.iStandardOffset = paramInt;
/* 1210 */       this.iStartRecurrence = paramRecurrence1;
/* 1211 */       this.iEndRecurrence = paramRecurrence2;
/*      */     }
/*      */     
/*      */     public String getNameKey(long paramLong) {
/* 1215 */       return findMatchingRecurrence(paramLong).getNameKey();
/*      */     }
/*      */     
/*      */     public int getOffset(long paramLong) {
/* 1219 */       return this.iStandardOffset + findMatchingRecurrence(paramLong).getSaveMillis();
/*      */     }
/*      */     
/*      */     public int getStandardOffset(long paramLong) {
/* 1223 */       return this.iStandardOffset;
/*      */     }
/*      */     
/*      */     public boolean isFixed() {
/* 1227 */       return false;
/*      */     }
/*      */     
/*      */     public long nextTransition(long paramLong) {
/* 1231 */       int i = this.iStandardOffset;
/* 1232 */       DateTimeZoneBuilder.Recurrence localRecurrence1 = this.iStartRecurrence;
/* 1233 */       DateTimeZoneBuilder.Recurrence localRecurrence2 = this.iEndRecurrence;
/*      */       
/*      */       long l1;
/*      */       try
/*      */       {
/* 1238 */         l1 = localRecurrence1.next(paramLong, i, localRecurrence2.getSaveMillis());
/*      */         
/* 1240 */         if ((paramLong > 0L) && (l1 < 0L))
/*      */         {
/* 1242 */           l1 = paramLong;
/*      */         }
/*      */       }
/*      */       catch (IllegalArgumentException localIllegalArgumentException1) {
/* 1246 */         l1 = paramLong;
/*      */       }
/*      */       catch (ArithmeticException localArithmeticException1) {
/* 1249 */         l1 = paramLong;
/*      */       }
/*      */       long l2;
/*      */       try {
/* 1253 */         l2 = localRecurrence2.next(paramLong, i, localRecurrence1.getSaveMillis());
/*      */         
/* 1255 */         if ((paramLong > 0L) && (l2 < 0L))
/*      */         {
/* 1257 */           l2 = paramLong;
/*      */         }
/*      */       }
/*      */       catch (IllegalArgumentException localIllegalArgumentException2) {
/* 1261 */         l2 = paramLong;
/*      */       }
/*      */       catch (ArithmeticException localArithmeticException2) {
/* 1264 */         l2 = paramLong;
/*      */       }
/*      */       
/* 1267 */       return l1 > l2 ? l2 : l1;
/*      */     }
/*      */     
/*      */ 
/*      */     public long previousTransition(long paramLong)
/*      */     {
/* 1273 */       paramLong += 1L;
/*      */       
/* 1275 */       int i = this.iStandardOffset;
/* 1276 */       DateTimeZoneBuilder.Recurrence localRecurrence1 = this.iStartRecurrence;
/* 1277 */       DateTimeZoneBuilder.Recurrence localRecurrence2 = this.iEndRecurrence;
/*      */       
/*      */       long l1;
/*      */       try
/*      */       {
/* 1282 */         l1 = localRecurrence1.previous(paramLong, i, localRecurrence2.getSaveMillis());
/*      */         
/* 1284 */         if ((paramLong < 0L) && (l1 > 0L))
/*      */         {
/* 1286 */           l1 = paramLong;
/*      */         }
/*      */       }
/*      */       catch (IllegalArgumentException localIllegalArgumentException1) {
/* 1290 */         l1 = paramLong;
/*      */       }
/*      */       catch (ArithmeticException localArithmeticException1) {
/* 1293 */         l1 = paramLong;
/*      */       }
/*      */       long l2;
/*      */       try {
/* 1297 */         l2 = localRecurrence2.previous(paramLong, i, localRecurrence1.getSaveMillis());
/*      */         
/* 1299 */         if ((paramLong < 0L) && (l2 > 0L))
/*      */         {
/* 1301 */           l2 = paramLong;
/*      */         }
/*      */       }
/*      */       catch (IllegalArgumentException localIllegalArgumentException2) {
/* 1305 */         l2 = paramLong;
/*      */       }
/*      */       catch (ArithmeticException localArithmeticException2) {
/* 1308 */         l2 = paramLong;
/*      */       }
/*      */       
/* 1311 */       return (l1 > l2 ? l1 : l2) - 1L;
/*      */     }
/*      */     
/*      */     public boolean equals(Object paramObject) {
/* 1315 */       if (this == paramObject) {
/* 1316 */         return true;
/*      */       }
/* 1318 */       if ((paramObject instanceof DSTZone)) {
/* 1319 */         DSTZone localDSTZone = (DSTZone)paramObject;
/* 1320 */         return (getID().equals(localDSTZone.getID())) && (this.iStandardOffset == localDSTZone.iStandardOffset) && (this.iStartRecurrence.equals(localDSTZone.iStartRecurrence)) && (this.iEndRecurrence.equals(localDSTZone.iEndRecurrence));
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/* 1326 */       return false;
/*      */     }
/*      */     
/*      */     public void writeTo(DataOutput paramDataOutput) throws IOException {
/* 1330 */       DateTimeZoneBuilder.writeMillis(paramDataOutput, this.iStandardOffset);
/* 1331 */       this.iStartRecurrence.writeTo(paramDataOutput);
/* 1332 */       this.iEndRecurrence.writeTo(paramDataOutput);
/*      */     }
/*      */     
/*      */     private DateTimeZoneBuilder.Recurrence findMatchingRecurrence(long paramLong) {
/* 1336 */       int i = this.iStandardOffset;
/* 1337 */       DateTimeZoneBuilder.Recurrence localRecurrence1 = this.iStartRecurrence;
/* 1338 */       DateTimeZoneBuilder.Recurrence localRecurrence2 = this.iEndRecurrence;
/*      */       
/*      */       long l1;
/*      */       try
/*      */       {
/* 1343 */         l1 = localRecurrence1.next(paramLong, i, localRecurrence2.getSaveMillis());
/*      */       }
/*      */       catch (IllegalArgumentException localIllegalArgumentException1)
/*      */       {
/* 1347 */         l1 = paramLong;
/*      */       }
/*      */       catch (ArithmeticException localArithmeticException1) {
/* 1350 */         l1 = paramLong;
/*      */       }
/*      */       long l2;
/*      */       try {
/* 1354 */         l2 = localRecurrence2.next(paramLong, i, localRecurrence1.getSaveMillis());
/*      */       }
/*      */       catch (IllegalArgumentException localIllegalArgumentException2)
/*      */       {
/* 1358 */         l2 = paramLong;
/*      */       }
/*      */       catch (ArithmeticException localArithmeticException2) {
/* 1361 */         l2 = paramLong;
/*      */       }
/*      */       
/* 1364 */       return l1 > l2 ? localRecurrence1 : localRecurrence2; } }
/*      */   
/*      */   private static final class PrecalculatedZone extends DateTimeZone { private static final long serialVersionUID = 7811976468055766265L;
/*      */     private final long[] iTransitions;
/*      */     private final int[] iWallOffsets;
/*      */     private final int[] iStandardOffsets;
/*      */     private final String[] iNameKeys;
/*      */     private final DateTimeZoneBuilder.DSTZone iTailZone;
/*      */     
/* 1373 */     static PrecalculatedZone readFrom(DataInput paramDataInput, String paramString) throws IOException { int i = paramDataInput.readUnsignedShort();
/* 1374 */       String[] arrayOfString1 = new String[i];
/* 1375 */       for (int j = 0; j < i; j++) {
/* 1376 */         arrayOfString1[j] = paramDataInput.readUTF();
/*      */       }
/*      */       
/* 1379 */       j = paramDataInput.readInt();
/* 1380 */       long[] arrayOfLong = new long[j];
/* 1381 */       int[] arrayOfInt1 = new int[j];
/* 1382 */       int[] arrayOfInt2 = new int[j];
/* 1383 */       String[] arrayOfString2 = new String[j];
/*      */       
/* 1385 */       for (int k = 0; k < j; k++) {
/* 1386 */         arrayOfLong[k] = DateTimeZoneBuilder.readMillis(paramDataInput);
/* 1387 */         arrayOfInt1[k] = ((int)DateTimeZoneBuilder.readMillis(paramDataInput));
/* 1388 */         arrayOfInt2[k] = ((int)DateTimeZoneBuilder.readMillis(paramDataInput));
/*      */         try {
/*      */           int m;
/* 1391 */           if (i < 256) {
/* 1392 */             m = paramDataInput.readUnsignedByte();
/*      */           } else {
/* 1394 */             m = paramDataInput.readUnsignedShort();
/*      */           }
/* 1396 */           arrayOfString2[k] = arrayOfString1[m];
/*      */         } catch (ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException) {
/* 1398 */           throw new IOException("Invalid encoding");
/*      */         }
/*      */       }
/*      */       
/* 1402 */       DateTimeZoneBuilder.DSTZone localDSTZone = null;
/* 1403 */       if (paramDataInput.readBoolean()) {
/* 1404 */         localDSTZone = DateTimeZoneBuilder.DSTZone.readFrom(paramDataInput, paramString);
/*      */       }
/*      */       
/* 1407 */       return new PrecalculatedZone(paramString, arrayOfLong, arrayOfInt1, arrayOfInt2, arrayOfString2, localDSTZone);
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     static PrecalculatedZone create(String paramString, boolean paramBoolean, ArrayList<DateTimeZoneBuilder.Transition> paramArrayList, DateTimeZoneBuilder.DSTZone paramDSTZone)
/*      */     {
/* 1421 */       int i = paramArrayList.size();
/* 1422 */       if (i == 0) {
/* 1423 */         throw new IllegalArgumentException();
/*      */       }
/*      */       
/* 1426 */       long[] arrayOfLong = new long[i];
/* 1427 */       int[] arrayOfInt1 = new int[i];
/* 1428 */       int[] arrayOfInt2 = new int[i];
/* 1429 */       String[] arrayOfString = new String[i];
/*      */       
/* 1431 */       Object localObject1 = null;
/* 1432 */       for (int j = 0; j < i; j++) {
/* 1433 */         localObject3 = (DateTimeZoneBuilder.Transition)paramArrayList.get(j);
/*      */         
/* 1435 */         if (!((DateTimeZoneBuilder.Transition)localObject3).isTransitionFrom((DateTimeZoneBuilder.Transition)localObject1)) {
/* 1436 */           throw new IllegalArgumentException(paramString);
/*      */         }
/*      */         
/* 1439 */         arrayOfLong[j] = ((DateTimeZoneBuilder.Transition)localObject3).getMillis();
/* 1440 */         arrayOfInt1[j] = ((DateTimeZoneBuilder.Transition)localObject3).getWallOffset();
/* 1441 */         arrayOfInt2[j] = ((DateTimeZoneBuilder.Transition)localObject3).getStandardOffset();
/* 1442 */         arrayOfString[j] = ((DateTimeZoneBuilder.Transition)localObject3).getNameKey();
/*      */         
/* 1444 */         localObject1 = localObject3;
/*      */       }
/*      */       
/*      */ 
/*      */ 
/* 1449 */       Object localObject2 = new String[5];
/* 1450 */       Object localObject3 = new DateFormatSymbols(Locale.ENGLISH).getZoneStrings();
/* 1451 */       for (int k = 0; k < localObject3.length; k++) {
/* 1452 */         Object localObject4 = localObject3[k];
/* 1453 */         if ((localObject4 != null) && (localObject4.length == 5) && (paramString.equals(localObject4[0]))) {
/* 1454 */           localObject2 = localObject4;
/*      */         }
/*      */       }
/*      */       
/* 1458 */       ISOChronology localISOChronology = ISOChronology.getInstanceUTC();
/*      */       
/* 1460 */       for (int m = 0; m < arrayOfString.length - 1; m++) {
/* 1461 */         String str1 = arrayOfString[m];
/* 1462 */         String str2 = arrayOfString[(m + 1)];
/* 1463 */         long l1 = arrayOfInt1[m];
/* 1464 */         long l2 = arrayOfInt1[(m + 1)];
/* 1465 */         long l3 = arrayOfInt2[m];
/* 1466 */         long l4 = arrayOfInt2[(m + 1)];
/* 1467 */         Period localPeriod = new Period(arrayOfLong[m], arrayOfLong[(m + 1)], PeriodType.yearMonthDay(), localISOChronology);
/* 1468 */         if ((l1 != l2) && (l3 == l4) && (str1.equals(str2)) && (localPeriod.getYears() == 0) && (localPeriod.getMonths() > 4) && (localPeriod.getMonths() < 8) && (str1.equals(localObject2[2])) && (str1.equals(localObject2[4])))
/*      */         {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1475 */           if (ZoneInfoLogger.verbose()) {
/* 1476 */             System.out.println("Fixing duplicate name key - " + str2);
/* 1477 */             System.out.println("     - " + new DateTime(arrayOfLong[m], localISOChronology) + " - " + new DateTime(arrayOfLong[(m + 1)], localISOChronology));
/*      */           }
/*      */           
/* 1480 */           if (l1 > l2) {
/* 1481 */             arrayOfString[m] = (str1 + "-Summer").intern();
/* 1482 */           } else if (l1 < l2) {
/* 1483 */             arrayOfString[(m + 1)] = (str2 + "-Summer").intern();
/* 1484 */             m++;
/*      */           }
/*      */         }
/*      */       }
/*      */       
/* 1489 */       if ((paramDSTZone != null) && 
/* 1490 */         (paramDSTZone.iStartRecurrence.getNameKey().equals(paramDSTZone.iEndRecurrence.getNameKey())))
/*      */       {
/* 1492 */         if (ZoneInfoLogger.verbose()) {
/* 1493 */           System.out.println("Fixing duplicate recurrent name key - " + paramDSTZone.iStartRecurrence.getNameKey());
/*      */         }
/*      */         
/* 1496 */         if (paramDSTZone.iStartRecurrence.getSaveMillis() > 0) {
/* 1497 */           paramDSTZone = new DateTimeZoneBuilder.DSTZone(paramDSTZone.getID(), paramDSTZone.iStandardOffset, paramDSTZone.iStartRecurrence.renameAppend("-Summer"), paramDSTZone.iEndRecurrence);
/*      */ 
/*      */         }
/*      */         else
/*      */         {
/*      */ 
/* 1503 */           paramDSTZone = new DateTimeZoneBuilder.DSTZone(paramDSTZone.getID(), paramDSTZone.iStandardOffset, paramDSTZone.iStartRecurrence, paramDSTZone.iEndRecurrence.renameAppend("-Summer"));
/*      */         }
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1512 */       return new PrecalculatedZone(paramBoolean ? paramString : "", arrayOfLong, arrayOfInt1, arrayOfInt2, arrayOfString, paramDSTZone);
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     private PrecalculatedZone(String paramString, long[] paramArrayOfLong, int[] paramArrayOfInt1, int[] paramArrayOfInt2, String[] paramArrayOfString, DateTimeZoneBuilder.DSTZone paramDSTZone)
/*      */     {
/* 1532 */       super();
/* 1533 */       this.iTransitions = paramArrayOfLong;
/* 1534 */       this.iWallOffsets = paramArrayOfInt1;
/* 1535 */       this.iStandardOffsets = paramArrayOfInt2;
/* 1536 */       this.iNameKeys = paramArrayOfString;
/* 1537 */       this.iTailZone = paramDSTZone;
/*      */     }
/*      */     
/*      */     public String getNameKey(long paramLong) {
/* 1541 */       long[] arrayOfLong = this.iTransitions;
/* 1542 */       int i = Arrays.binarySearch(arrayOfLong, paramLong);
/* 1543 */       if (i >= 0) {
/* 1544 */         return this.iNameKeys[i];
/*      */       }
/* 1546 */       i ^= 0xFFFFFFFF;
/* 1547 */       if (i < arrayOfLong.length) {
/* 1548 */         if (i > 0) {
/* 1549 */           return this.iNameKeys[(i - 1)];
/*      */         }
/* 1551 */         return "UTC";
/*      */       }
/* 1553 */       if (this.iTailZone == null) {
/* 1554 */         return this.iNameKeys[(i - 1)];
/*      */       }
/* 1556 */       return this.iTailZone.getNameKey(paramLong);
/*      */     }
/*      */     
/*      */     public int getOffset(long paramLong) {
/* 1560 */       long[] arrayOfLong = this.iTransitions;
/* 1561 */       int i = Arrays.binarySearch(arrayOfLong, paramLong);
/* 1562 */       if (i >= 0) {
/* 1563 */         return this.iWallOffsets[i];
/*      */       }
/* 1565 */       i ^= 0xFFFFFFFF;
/* 1566 */       if (i < arrayOfLong.length) {
/* 1567 */         if (i > 0) {
/* 1568 */           return this.iWallOffsets[(i - 1)];
/*      */         }
/* 1570 */         return 0;
/*      */       }
/* 1572 */       if (this.iTailZone == null) {
/* 1573 */         return this.iWallOffsets[(i - 1)];
/*      */       }
/* 1575 */       return this.iTailZone.getOffset(paramLong);
/*      */     }
/*      */     
/*      */     public int getStandardOffset(long paramLong) {
/* 1579 */       long[] arrayOfLong = this.iTransitions;
/* 1580 */       int i = Arrays.binarySearch(arrayOfLong, paramLong);
/* 1581 */       if (i >= 0) {
/* 1582 */         return this.iStandardOffsets[i];
/*      */       }
/* 1584 */       i ^= 0xFFFFFFFF;
/* 1585 */       if (i < arrayOfLong.length) {
/* 1586 */         if (i > 0) {
/* 1587 */           return this.iStandardOffsets[(i - 1)];
/*      */         }
/* 1589 */         return 0;
/*      */       }
/* 1591 */       if (this.iTailZone == null) {
/* 1592 */         return this.iStandardOffsets[(i - 1)];
/*      */       }
/* 1594 */       return this.iTailZone.getStandardOffset(paramLong);
/*      */     }
/*      */     
/*      */     public boolean isFixed() {
/* 1598 */       return false;
/*      */     }
/*      */     
/*      */     public long nextTransition(long paramLong) {
/* 1602 */       long[] arrayOfLong = this.iTransitions;
/* 1603 */       int i = Arrays.binarySearch(arrayOfLong, paramLong);
/* 1604 */       i = i >= 0 ? i + 1 : i ^ 0xFFFFFFFF;
/* 1605 */       if (i < arrayOfLong.length) {
/* 1606 */         return arrayOfLong[i];
/*      */       }
/* 1608 */       if (this.iTailZone == null) {
/* 1609 */         return paramLong;
/*      */       }
/* 1611 */       long l = arrayOfLong[(arrayOfLong.length - 1)];
/* 1612 */       if (paramLong < l) {
/* 1613 */         paramLong = l;
/*      */       }
/* 1615 */       return this.iTailZone.nextTransition(paramLong);
/*      */     }
/*      */     
/*      */     public long previousTransition(long paramLong) {
/* 1619 */       long[] arrayOfLong = this.iTransitions;
/* 1620 */       int i = Arrays.binarySearch(arrayOfLong, paramLong);
/* 1621 */       if (i >= 0) {
/* 1622 */         if (paramLong > Long.MIN_VALUE) {
/* 1623 */           return paramLong - 1L;
/*      */         }
/* 1625 */         return paramLong;
/*      */       }
/* 1627 */       i ^= 0xFFFFFFFF;
/* 1628 */       if (i < arrayOfLong.length) {
/* 1629 */         if (i > 0) {
/* 1630 */           l = arrayOfLong[(i - 1)];
/* 1631 */           if (l > Long.MIN_VALUE) {
/* 1632 */             return l - 1L;
/*      */           }
/*      */         }
/* 1635 */         return paramLong;
/*      */       }
/* 1637 */       if (this.iTailZone != null) {
/* 1638 */         l = this.iTailZone.previousTransition(paramLong);
/* 1639 */         if (l < paramLong) {
/* 1640 */           return l;
/*      */         }
/*      */       }
/* 1643 */       long l = arrayOfLong[(i - 1)];
/* 1644 */       if (l > Long.MIN_VALUE) {
/* 1645 */         return l - 1L;
/*      */       }
/* 1647 */       return paramLong;
/*      */     }
/*      */     
/*      */     public boolean equals(Object paramObject) {
/* 1651 */       if (this == paramObject) {
/* 1652 */         return true;
/*      */       }
/* 1654 */       if ((paramObject instanceof PrecalculatedZone)) {
/* 1655 */         PrecalculatedZone localPrecalculatedZone = (PrecalculatedZone)paramObject;
/* 1656 */         return (getID().equals(localPrecalculatedZone.getID())) && (Arrays.equals(this.iTransitions, localPrecalculatedZone.iTransitions)) && (Arrays.equals(this.iNameKeys, localPrecalculatedZone.iNameKeys)) && (Arrays.equals(this.iWallOffsets, localPrecalculatedZone.iWallOffsets)) && (Arrays.equals(this.iStandardOffsets, localPrecalculatedZone.iStandardOffsets)) && (this.iTailZone == null ? null == localPrecalculatedZone.iTailZone : this.iTailZone.equals(localPrecalculatedZone.iTailZone));
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1666 */       return false;
/*      */     }
/*      */     
/*      */     public void writeTo(DataOutput paramDataOutput) throws IOException {
/* 1670 */       int i = this.iTransitions.length;
/*      */       
/*      */ 
/* 1673 */       HashSet localHashSet = new HashSet();
/* 1674 */       for (int j = 0; j < i; j++) {
/* 1675 */         localHashSet.add(this.iNameKeys[j]);
/*      */       }
/*      */       
/* 1678 */       j = localHashSet.size();
/* 1679 */       if (j > 65535) {
/* 1680 */         throw new UnsupportedOperationException("String pool is too large");
/*      */       }
/* 1682 */       String[] arrayOfString = new String[j];
/* 1683 */       Iterator localIterator = localHashSet.iterator();
/* 1684 */       for (int k = 0; localIterator.hasNext(); k++) {
/* 1685 */         arrayOfString[k] = ((String)localIterator.next());
/*      */       }
/*      */       
/*      */ 
/* 1689 */       paramDataOutput.writeShort(j);
/* 1690 */       for (k = 0; k < j; k++) {
/* 1691 */         paramDataOutput.writeUTF(arrayOfString[k]);
/*      */       }
/*      */       
/* 1694 */       paramDataOutput.writeInt(i);
/*      */       
/* 1696 */       for (k = 0; k < i; k++) {
/* 1697 */         DateTimeZoneBuilder.writeMillis(paramDataOutput, this.iTransitions[k]);
/* 1698 */         DateTimeZoneBuilder.writeMillis(paramDataOutput, this.iWallOffsets[k]);
/* 1699 */         DateTimeZoneBuilder.writeMillis(paramDataOutput, this.iStandardOffsets[k]);
/*      */         
/*      */ 
/* 1702 */         String str = this.iNameKeys[k];
/* 1703 */         for (int m = 0; m < j; m++) {
/* 1704 */           if (arrayOfString[m].equals(str)) {
/* 1705 */             if (j < 256) {
/* 1706 */               paramDataOutput.writeByte(m); break;
/*      */             }
/* 1708 */             paramDataOutput.writeShort(m);
/*      */             
/* 1710 */             break;
/*      */           }
/*      */         }
/*      */       }
/*      */       
/* 1715 */       paramDataOutput.writeBoolean(this.iTailZone != null);
/* 1716 */       if (this.iTailZone != null) {
/* 1717 */         this.iTailZone.writeTo(paramDataOutput);
/*      */       }
/*      */     }
/*      */     
/*      */     public boolean isCachable() {
/* 1722 */       if (this.iTailZone != null) {
/* 1723 */         return true;
/*      */       }
/* 1725 */       long[] arrayOfLong = this.iTransitions;
/* 1726 */       if (arrayOfLong.length <= 1) {
/* 1727 */         return false;
/*      */       }
/*      */       
/*      */ 
/*      */ 
/* 1732 */       double d1 = 0.0D;
/* 1733 */       int i = 0;
/*      */       
/* 1735 */       for (int j = 1; j < arrayOfLong.length; j++) {
/* 1736 */         long l = arrayOfLong[j] - arrayOfLong[(j - 1)];
/* 1737 */         if (l < 63158400000L) {
/* 1738 */           d1 += l;
/* 1739 */           i++;
/*      */         }
/*      */       }
/*      */       
/* 1743 */       if (i > 0) {
/* 1744 */         double d2 = d1 / i;
/* 1745 */         d2 /= 8.64E7D;
/* 1746 */         if (d2 >= 25.0D)
/*      */         {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1753 */           return true;
/*      */         }
/*      */       }
/*      */       
/* 1757 */       return false;
/*      */     }
/*      */   }
/*      */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\org\joda\time\tz\DateTimeZoneBuilder.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */