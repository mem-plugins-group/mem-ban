package org.joda.time.tz;

import java.util.Set;
import org.joda.time.DateTimeZone;

public abstract interface Provider
{
  public abstract DateTimeZone getZone(String paramString);
  
  public abstract Set<String> getAvailableIDs();
}


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\org\joda\time\tz\Provider.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */