/*    */ package org.joda.time.tz;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ZoneInfoLogger
/*    */ {
/* 23 */   static ThreadLocal<Boolean> cVerbose = new ThreadLocal() {
/*    */     protected Boolean initialValue() {
/* 25 */       return Boolean.FALSE;
/*    */     }
/*    */   };
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public static boolean verbose()
/*    */   {
/* 34 */     return ((Boolean)cVerbose.get()).booleanValue();
/*    */   }
/*    */   
/*    */   public static void set(boolean paramBoolean) {
/* 38 */     cVerbose.set(Boolean.valueOf(paramBoolean));
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\org\joda\time\tz\ZoneInfoLogger.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */