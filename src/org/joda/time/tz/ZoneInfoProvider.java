/*     */ package org.joda.time.tz;
/*     */ 
/*     */ import java.io.DataInputStream;
/*     */ import java.io.File;
/*     */ import java.io.FileInputStream;
/*     */ import java.io.IOException;
/*     */ import java.io.InputStream;
/*     */ import java.lang.ref.SoftReference;
/*     */ import java.security.AccessController;
/*     */ import java.security.PrivilegedAction;
/*     */ import java.util.Collections;
/*     */ import java.util.Map;
/*     */ import java.util.Set;
/*     */ import java.util.TreeSet;
/*     */ import java.util.concurrent.ConcurrentHashMap;
/*     */ import org.joda.time.DateTimeZone;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class ZoneInfoProvider
/*     */   implements Provider
/*     */ {
/*     */   private final File iFileDir;
/*     */   private final String iResourcePath;
/*     */   private final ClassLoader iLoader;
/*     */   private final Map<String, Object> iZoneInfoMap;
/*     */   private final Set<String> iZoneInfoKeys;
/*     */   
/*     */   public ZoneInfoProvider(File paramFile)
/*     */     throws IOException
/*     */   {
/*  62 */     if (paramFile == null) {
/*  63 */       throw new IllegalArgumentException("No file directory provided");
/*     */     }
/*  65 */     if (!paramFile.exists()) {
/*  66 */       throw new IOException("File directory doesn't exist: " + paramFile);
/*     */     }
/*  68 */     if (!paramFile.isDirectory()) {
/*  69 */       throw new IOException("File doesn't refer to a directory: " + paramFile);
/*     */     }
/*     */     
/*  72 */     this.iFileDir = paramFile;
/*  73 */     this.iResourcePath = null;
/*  74 */     this.iLoader = null;
/*     */     
/*  76 */     this.iZoneInfoMap = loadZoneInfoMap(openResource("ZoneInfoMap"));
/*  77 */     this.iZoneInfoKeys = Collections.unmodifiableSortedSet(new TreeSet(this.iZoneInfoMap.keySet()));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public ZoneInfoProvider(String paramString)
/*     */     throws IOException
/*     */   {
/*  88 */     this(paramString, null, false);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public ZoneInfoProvider(String paramString, ClassLoader paramClassLoader)
/*     */     throws IOException
/*     */   {
/* 102 */     this(paramString, paramClassLoader, true);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private ZoneInfoProvider(String paramString, ClassLoader paramClassLoader, boolean paramBoolean)
/*     */     throws IOException
/*     */   {
/* 113 */     if (paramString == null) {
/* 114 */       throw new IllegalArgumentException("No resource path provided");
/*     */     }
/* 116 */     if (!paramString.endsWith("/")) {
/* 117 */       paramString = paramString + '/';
/*     */     }
/*     */     
/* 120 */     this.iFileDir = null;
/* 121 */     this.iResourcePath = paramString;
/*     */     
/* 123 */     if ((paramClassLoader == null) && (!paramBoolean)) {
/* 124 */       paramClassLoader = getClass().getClassLoader();
/*     */     }
/*     */     
/* 127 */     this.iLoader = paramClassLoader;
/*     */     
/* 129 */     this.iZoneInfoMap = loadZoneInfoMap(openResource("ZoneInfoMap"));
/* 130 */     this.iZoneInfoKeys = Collections.unmodifiableSortedSet(new TreeSet(this.iZoneInfoMap.keySet()));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public DateTimeZone getZone(String paramString)
/*     */   {
/* 142 */     if (paramString == null) {
/* 143 */       return null;
/*     */     }
/*     */     
/* 146 */     Object localObject = this.iZoneInfoMap.get(paramString);
/* 147 */     if (localObject == null) {
/* 148 */       return null;
/*     */     }
/*     */     
/* 151 */     if ((localObject instanceof SoftReference))
/*     */     {
/* 153 */       SoftReference localSoftReference = (SoftReference)localObject;
/* 154 */       DateTimeZone localDateTimeZone = (DateTimeZone)localSoftReference.get();
/* 155 */       if (localDateTimeZone != null) {
/* 156 */         return localDateTimeZone;
/*     */       }
/*     */       
/* 159 */       return loadZoneData(paramString); }
/* 160 */     if (paramString.equals(localObject))
/*     */     {
/* 162 */       return loadZoneData(paramString);
/*     */     }
/*     */     
/*     */ 
/* 166 */     return getZone((String)localObject);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Set<String> getAvailableIDs()
/*     */   {
/* 175 */     return this.iZoneInfoKeys;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected void uncaughtException(Exception paramException)
/*     */   {
/* 184 */     paramException.printStackTrace();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private InputStream openResource(String paramString)
/*     */     throws IOException
/*     */   {
/*     */     Object localObject;
/*     */     
/*     */ 
/*     */ 
/* 197 */     if (this.iFileDir != null) {
/* 198 */       localObject = new FileInputStream(new File(this.iFileDir, paramString));
/*     */     } else {
/* 200 */       final String str = this.iResourcePath.concat(paramString);
/* 201 */       localObject = (InputStream)AccessController.doPrivileged(new PrivilegedAction() {
/*     */         public InputStream run() {
/* 203 */           if (ZoneInfoProvider.this.iLoader != null) {
/* 204 */             return ZoneInfoProvider.this.iLoader.getResourceAsStream(str);
/*     */           }
/* 206 */           return ClassLoader.getSystemResourceAsStream(str);
/*     */         }
/*     */       });
/*     */       
/* 210 */       if (localObject == null) {
/* 211 */         StringBuilder localStringBuilder = new StringBuilder(40).append("Resource not found: \"").append(str).append("\" ClassLoader: ").append(this.iLoader != null ? this.iLoader.toString() : "system");
/*     */         
/*     */ 
/*     */ 
/*     */ 
/* 216 */         throw new IOException(localStringBuilder.toString());
/*     */       }
/*     */     }
/* 219 */     return (InputStream)localObject;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private DateTimeZone loadZoneData(String paramString)
/*     */   {
/* 229 */     InputStream localInputStream = null;
/*     */     try {
/* 231 */       localInputStream = openResource(paramString);
/* 232 */       DateTimeZone localDateTimeZone1 = DateTimeZoneBuilder.readFrom(localInputStream, paramString);
/* 233 */       this.iZoneInfoMap.put(paramString, new SoftReference(localDateTimeZone1));
/* 234 */       return localDateTimeZone1;
/*     */     } catch (IOException localIOException1) { DateTimeZone localDateTimeZone2;
/* 236 */       uncaughtException(localIOException1);
/* 237 */       this.iZoneInfoMap.remove(paramString);
/* 238 */       return null;
/*     */     } finally {
/*     */       try {
/* 241 */         if (localInputStream != null) {
/* 242 */           localInputStream.close();
/*     */         }
/*     */       }
/*     */       catch (IOException localIOException4) {}
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static Map<String, Object> loadZoneInfoMap(InputStream paramInputStream)
/*     */     throws IOException
/*     */   {
/* 257 */     ConcurrentHashMap localConcurrentHashMap = new ConcurrentHashMap();
/* 258 */     DataInputStream localDataInputStream = new DataInputStream(paramInputStream);
/*     */     try {
/* 260 */       readZoneInfoMap(localDataInputStream, localConcurrentHashMap);
/*     */       try
/*     */       {
/* 263 */         localDataInputStream.close();
/*     */       }
/*     */       catch (IOException localIOException1) {}
/*     */       
/* 267 */       localConcurrentHashMap.put("UTC", new SoftReference(DateTimeZone.UTC));
/*     */     }
/*     */     finally
/*     */     {
/*     */       try
/*     */       {
/* 263 */         localDataInputStream.close();
/*     */       }
/*     */       catch (IOException localIOException2) {}
/*     */     }
/*     */     
/* 268 */     return localConcurrentHashMap;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void readZoneInfoMap(DataInputStream paramDataInputStream, Map<String, Object> paramMap)
/*     */     throws IOException
/*     */   {
/* 279 */     int i = paramDataInputStream.readUnsignedShort();
/* 280 */     String[] arrayOfString = new String[i];
/* 281 */     for (int j = 0; j < i; j++) {
/* 282 */       arrayOfString[j] = paramDataInputStream.readUTF().intern();
/*     */     }
/*     */     
/*     */ 
/* 286 */     i = paramDataInputStream.readUnsignedShort();
/* 287 */     for (j = 0; j < i; j++) {
/*     */       try {
/* 289 */         paramMap.put(arrayOfString[paramDataInputStream.readUnsignedShort()], arrayOfString[paramDataInputStream.readUnsignedShort()]);
/*     */       } catch (ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException) {
/* 291 */         throw new IOException("Corrupt zone info map");
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\org\joda\time\tz\ZoneInfoProvider.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */