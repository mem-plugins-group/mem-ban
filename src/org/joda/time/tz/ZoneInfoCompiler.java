/*     */ package org.joda.time.tz;
/*     */ 
/*     */ import java.io.BufferedReader;
/*     */ import java.io.DataOutputStream;
/*     */ import java.io.File;
/*     */ import java.io.FileInputStream;
/*     */ import java.io.FileOutputStream;
/*     */ import java.io.FileReader;
/*     */ import java.io.IOException;
/*     */ import java.io.InputStream;
/*     */ import java.io.OutputStream;
/*     */ import java.io.PrintStream;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Collection;
/*     */ import java.util.HashMap;
/*     */ import java.util.Iterator;
/*     */ import java.util.List;
/*     */ import java.util.Locale;
/*     */ import java.util.Map;
/*     */ import java.util.Map.Entry;
/*     */ import java.util.Set;
/*     */ import java.util.StringTokenizer;
/*     */ import java.util.TreeMap;
/*     */ import org.joda.time.Chronology;
/*     */ import org.joda.time.DateTime;
/*     */ import org.joda.time.DateTimeField;
/*     */ import org.joda.time.DateTimeZone;
/*     */ import org.joda.time.LocalDate;
/*     */ import org.joda.time.MutableDateTime;
/*     */ import org.joda.time.chrono.ISOChronology;
/*     */ import org.joda.time.chrono.LenientChronology;
/*     */ import org.joda.time.format.DateTimeFormatter;
/*     */ import org.joda.time.format.ISODateTimeFormat;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class ZoneInfoCompiler
/*     */ {
/*     */   static DateTimeOfYear cStartOfYear;
/*     */   static Chronology cLenientISO;
/*     */   private Map<String, RuleSet> iRuleSets;
/*     */   private List<Zone> iZones;
/*     */   private List<String> iGoodLinks;
/*     */   private List<String> iBackLinks;
/*     */   
/*     */   public static void main(String[] paramArrayOfString)
/*     */     throws Exception
/*     */   {
/*  81 */     if (paramArrayOfString.length == 0) {
/*  82 */       printUsage();
/*  83 */       return;
/*     */     }
/*     */     
/*  86 */     File localFile1 = null;
/*  87 */     File localFile2 = null;
/*  88 */     boolean bool = false;
/*     */     
/*     */ 
/*  91 */     for (int i = 0; i < paramArrayOfString.length; i++) {
/*     */       try {
/*  93 */         if ("-src".equals(paramArrayOfString[i])) {
/*  94 */           localFile1 = new File(paramArrayOfString[(++i)]);
/*  95 */         } else if ("-dst".equals(paramArrayOfString[i])) {
/*  96 */           localFile2 = new File(paramArrayOfString[(++i)]);
/*  97 */         } else if ("-verbose".equals(paramArrayOfString[i])) {
/*  98 */           bool = true;
/*  99 */         } else { if ("-?".equals(paramArrayOfString[i])) {
/* 100 */             printUsage();
/* 101 */             return;
/*     */           }
/* 103 */           break;
/*     */         }
/*     */       } catch (IndexOutOfBoundsException localIndexOutOfBoundsException) {
/* 106 */         printUsage();
/* 107 */         return;
/*     */       }
/*     */     }
/*     */     
/* 111 */     if (i >= paramArrayOfString.length) {
/* 112 */       printUsage();
/* 113 */       return;
/*     */     }
/*     */     
/* 116 */     File[] arrayOfFile = new File[paramArrayOfString.length - i];
/* 117 */     for (int j = 0; i < paramArrayOfString.length; j++) {
/* 118 */       arrayOfFile[j] = (localFile1 == null ? new File(paramArrayOfString[i]) : new File(localFile1, paramArrayOfString[i]));i++;
/*     */     }
/*     */     
/* 121 */     ZoneInfoLogger.set(bool);
/* 122 */     ZoneInfoCompiler localZoneInfoCompiler = new ZoneInfoCompiler();
/* 123 */     localZoneInfoCompiler.compile(localFile2, arrayOfFile);
/*     */   }
/*     */   
/*     */   private static void printUsage() {
/* 127 */     System.out.println("Usage: java org.joda.time.tz.ZoneInfoCompiler <options> <source files>");
/* 128 */     System.out.println("where possible options include:");
/* 129 */     System.out.println("  -src <directory>    Specify where to read source files");
/* 130 */     System.out.println("  -dst <directory>    Specify where to write generated files");
/* 131 */     System.out.println("  -verbose            Output verbosely (default false)");
/*     */   }
/*     */   
/*     */   static DateTimeOfYear getStartOfYear() {
/* 135 */     if (cStartOfYear == null) {
/* 136 */       cStartOfYear = new DateTimeOfYear();
/*     */     }
/* 138 */     return cStartOfYear;
/*     */   }
/*     */   
/*     */   static Chronology getLenientISOChronology() {
/* 142 */     if (cLenientISO == null) {
/* 143 */       cLenientISO = LenientChronology.getInstance(ISOChronology.getInstanceUTC());
/*     */     }
/* 145 */     return cLenientISO;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   static void writeZoneInfoMap(DataOutputStream paramDataOutputStream, Map<String, DateTimeZone> paramMap)
/*     */     throws IOException
/*     */   {
/* 153 */     HashMap localHashMap = new HashMap(paramMap.size());
/* 154 */     TreeMap localTreeMap = new TreeMap();
/*     */     
/* 156 */     short s = 0;
/* 157 */     for (Iterator localIterator = paramMap.entrySet().iterator(); localIterator.hasNext();) { localObject = (Map.Entry)localIterator.next();
/* 158 */       str = (String)((Map.Entry)localObject).getKey();
/* 159 */       Short localShort; if (!localHashMap.containsKey(str)) {
/* 160 */         localShort = Short.valueOf(s);
/* 161 */         localHashMap.put(str, localShort);
/* 162 */         localTreeMap.put(localShort, str);
/* 163 */         s = (short)(s + 1); if (s == 0) {
/* 164 */           throw new InternalError("Too many time zone ids");
/*     */         }
/*     */       }
/* 167 */       str = ((DateTimeZone)((Map.Entry)localObject).getValue()).getID();
/* 168 */       if (!localHashMap.containsKey(str)) {
/* 169 */         localShort = Short.valueOf(s);
/* 170 */         localHashMap.put(str, localShort);
/* 171 */         localTreeMap.put(localShort, str);
/* 172 */         s = (short)(s + 1); if (s == 0) {
/* 173 */           throw new InternalError("Too many time zone ids");
/*     */         }
/*     */       }
/*     */     }
/*     */     Object localObject;
/*     */     String str;
/* 179 */     paramDataOutputStream.writeShort(localTreeMap.size());
/* 180 */     for (localIterator = localTreeMap.values().iterator(); localIterator.hasNext();) { localObject = (String)localIterator.next();
/* 181 */       paramDataOutputStream.writeUTF((String)localObject);
/*     */     }
/*     */     
/*     */ 
/* 185 */     paramDataOutputStream.writeShort(paramMap.size());
/* 186 */     for (localIterator = paramMap.entrySet().iterator(); localIterator.hasNext();) { localObject = (Map.Entry)localIterator.next();
/* 187 */       str = (String)((Map.Entry)localObject).getKey();
/* 188 */       paramDataOutputStream.writeShort(((Short)localHashMap.get(str)).shortValue());
/* 189 */       str = ((DateTimeZone)((Map.Entry)localObject).getValue()).getID();
/* 190 */       paramDataOutputStream.writeShort(((Short)localHashMap.get(str)).shortValue());
/*     */     }
/*     */   }
/*     */   
/*     */   static int parseYear(String paramString, int paramInt) {
/* 195 */     paramString = paramString.toLowerCase(Locale.ENGLISH);
/* 196 */     if ((paramString.equals("minimum")) || (paramString.equals("min")))
/* 197 */       return Integer.MIN_VALUE;
/* 198 */     if ((paramString.equals("maximum")) || (paramString.equals("max")))
/* 199 */       return Integer.MAX_VALUE;
/* 200 */     if (paramString.equals("only")) {
/* 201 */       return paramInt;
/*     */     }
/* 203 */     return Integer.parseInt(paramString);
/*     */   }
/*     */   
/*     */   static int parseMonth(String paramString) {
/* 207 */     DateTimeField localDateTimeField = ISOChronology.getInstanceUTC().monthOfYear();
/* 208 */     return localDateTimeField.get(localDateTimeField.set(0L, paramString, Locale.ENGLISH));
/*     */   }
/*     */   
/*     */   static int parseDayOfWeek(String paramString) {
/* 212 */     DateTimeField localDateTimeField = ISOChronology.getInstanceUTC().dayOfWeek();
/* 213 */     return localDateTimeField.get(localDateTimeField.set(0L, paramString, Locale.ENGLISH));
/*     */   }
/*     */   
/*     */   static String parseOptional(String paramString) {
/* 217 */     return paramString.equals("-") ? null : paramString;
/*     */   }
/*     */   
/*     */   static int parseTime(String paramString) {
/* 221 */     DateTimeFormatter localDateTimeFormatter = ISODateTimeFormat.hourMinuteSecondFraction();
/* 222 */     MutableDateTime localMutableDateTime = new MutableDateTime(0L, getLenientISOChronology());
/* 223 */     int i = 0;
/* 224 */     if (paramString.startsWith("-")) {
/* 225 */       i = 1;
/*     */     }
/* 227 */     int j = localDateTimeFormatter.parseInto(localMutableDateTime, paramString, i);
/* 228 */     if (j == (i ^ 0xFFFFFFFF)) {
/* 229 */       throw new IllegalArgumentException(paramString);
/*     */     }
/* 231 */     int k = (int)localMutableDateTime.getMillis();
/* 232 */     if (i == 1) {
/* 233 */       k = -k;
/*     */     }
/* 235 */     return k;
/*     */   }
/*     */   
/*     */   static char parseZoneChar(char paramChar) {
/* 239 */     switch (paramChar) {
/*     */     case 'S': 
/*     */     case 's': 
/* 242 */       return 's';
/*     */     case 'G': case 'U': case 'Z': 
/*     */     case 'g': case 'u': case 'z': 
/* 245 */       return 'u';
/*     */     }
/*     */     
/* 248 */     return 'w';
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   static boolean test(String paramString, DateTimeZone paramDateTimeZone)
/*     */   {
/* 256 */     if (!paramString.equals(paramDateTimeZone.getID())) {
/* 257 */       return true;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 262 */     long l1 = ISOChronology.getInstanceUTC().year().set(0L, 1850);
/* 263 */     long l2 = ISOChronology.getInstanceUTC().year().set(0L, 2050);
/*     */     
/* 265 */     int i = paramDateTimeZone.getOffset(l1);
/* 266 */     int j = paramDateTimeZone.getStandardOffset(l1);
/* 267 */     Object localObject = paramDateTimeZone.getNameKey(l1);
/*     */     
/* 269 */     ArrayList localArrayList = new ArrayList();
/*     */     for (;;)
/*     */     {
/* 272 */       long l3 = paramDateTimeZone.nextTransition(l1);
/* 273 */       if ((l3 == l1) || (l3 > l2)) {
/*     */         break;
/*     */       }
/*     */       
/* 277 */       l1 = l3;
/*     */       
/* 279 */       int m = paramDateTimeZone.getOffset(l1);
/* 280 */       int n = paramDateTimeZone.getStandardOffset(l1);
/* 281 */       String str = paramDateTimeZone.getNameKey(l1);
/*     */       
/* 283 */       if ((i == m) && (j == n) && (((String)localObject).equals(str))) {
/* 284 */         System.out.println("*d* Error in " + paramDateTimeZone.getID() + " " + new DateTime(l1, ISOChronology.getInstanceUTC()));
/*     */         
/*     */ 
/* 287 */         return false;
/*     */       }
/*     */       
/* 290 */       if ((str == null) || ((str.length() < 3) && (!"??".equals(str)))) {
/* 291 */         System.out.println("*s* Error in " + paramDateTimeZone.getID() + " " + new DateTime(l1, ISOChronology.getInstanceUTC()) + ", nameKey=" + str);
/*     */         
/*     */ 
/*     */ 
/* 295 */         return false;
/*     */       }
/*     */       
/* 298 */       localArrayList.add(Long.valueOf(l1));
/*     */       
/* 300 */       i = m;
/* 301 */       localObject = str;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 306 */     l1 = ISOChronology.getInstanceUTC().year().set(0L, 2050);
/* 307 */     l2 = ISOChronology.getInstanceUTC().year().set(0L, 1850);
/*     */     
/* 309 */     int k = localArrayList.size(); for (;;) { k--; if (k < 0) break;
/* 310 */       long l4 = paramDateTimeZone.previousTransition(l1);
/* 311 */       if ((l4 == l1) || (l4 < l2)) {
/*     */         break;
/*     */       }
/*     */       
/* 315 */       l1 = l4;
/*     */       
/* 317 */       long l5 = ((Long)localArrayList.get(k)).longValue();
/*     */       
/* 319 */       if (l5 - 1L != l1) {
/* 320 */         System.out.println("*r* Error in " + paramDateTimeZone.getID() + " " + new DateTime(l1, ISOChronology.getInstanceUTC()) + " != " + new DateTime(l5 - 1L, ISOChronology.getInstanceUTC()));
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 326 */         return false;
/*     */       }
/*     */     }
/*     */     
/* 330 */     return true;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public ZoneInfoCompiler()
/*     */   {
/* 346 */     this.iRuleSets = new HashMap();
/* 347 */     this.iZones = new ArrayList();
/* 348 */     this.iGoodLinks = new ArrayList();
/* 349 */     this.iBackLinks = new ArrayList();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Map<String, DateTimeZone> compile(File paramFile, File[] paramArrayOfFile)
/*     */     throws IOException
/*     */   {
/* 359 */     if (paramArrayOfFile != null) {
/* 360 */       for (int i = 0; i < paramArrayOfFile.length; i++) {
/* 361 */         localObject1 = null;
/*     */         try {
/* 363 */           localObject1 = new BufferedReader(new FileReader(paramArrayOfFile[i]));
/* 364 */           parseDataFile((BufferedReader)localObject1, "backward".equals(paramArrayOfFile[i].getName()));
/*     */         } finally {
/* 366 */           if (localObject1 != null) {
/* 367 */             ((BufferedReader)localObject1).close();
/*     */           }
/*     */         }
/*     */       }
/*     */     }
/*     */     
/* 373 */     if (paramFile != null) {
/* 374 */       if ((!paramFile.exists()) && (!paramFile.mkdirs())) {
/* 375 */         throw new IOException("Destination directory doesn't exist and cannot be created: " + paramFile);
/*     */       }
/* 377 */       if (!paramFile.isDirectory()) {
/* 378 */         throw new IOException("Destination is not a directory: " + paramFile);
/*     */       }
/*     */     }
/*     */     
/* 382 */     TreeMap localTreeMap = new TreeMap();
/* 383 */     Object localObject1 = new TreeMap();
/*     */     
/* 385 */     System.out.println("Writing zoneinfo files");
/*     */     Object localObject3;
/* 387 */     Object localObject4; Object localObject5; for (int j = 0; j < this.iZones.size(); j++) {
/* 388 */       localObject3 = (Zone)this.iZones.get(j);
/* 389 */       localObject4 = new DateTimeZoneBuilder();
/* 390 */       ((Zone)localObject3).addToBuilder((DateTimeZoneBuilder)localObject4, this.iRuleSets);
/* 391 */       localObject5 = ((DateTimeZoneBuilder)localObject4).toDateTimeZone(((Zone)localObject3).iName, true);
/* 392 */       if (test(((DateTimeZone)localObject5).getID(), (DateTimeZone)localObject5)) {
/* 393 */         localTreeMap.put(((DateTimeZone)localObject5).getID(), localObject5);
/* 394 */         ((Map)localObject1).put(((DateTimeZone)localObject5).getID(), localObject3);
/* 395 */         if (paramFile != null) {
/* 396 */           writeZone(paramFile, (DateTimeZoneBuilder)localObject4, (DateTimeZone)localObject5);
/*     */         }
/*     */       }
/*     */     }
/*     */     
/*     */     Object localObject6;
/* 402 */     for (j = 0; j < this.iGoodLinks.size(); j += 2) {
/* 403 */       localObject3 = (String)this.iGoodLinks.get(j);
/* 404 */       localObject4 = (String)this.iGoodLinks.get(j + 1);
/* 405 */       localObject5 = (Zone)((Map)localObject1).get(localObject3);
/* 406 */       if (localObject5 == null) {
/* 407 */         System.out.println("Cannot find source zone '" + (String)localObject3 + "' to link alias '" + (String)localObject4 + "' to");
/*     */       } else {
/* 409 */         localObject6 = new DateTimeZoneBuilder();
/* 410 */         ((Zone)localObject5).addToBuilder((DateTimeZoneBuilder)localObject6, this.iRuleSets);
/* 411 */         DateTimeZone localDateTimeZone = ((DateTimeZoneBuilder)localObject6).toDateTimeZone((String)localObject4, true);
/* 412 */         if (test(localDateTimeZone.getID(), localDateTimeZone)) {
/* 413 */           localTreeMap.put(localDateTimeZone.getID(), localDateTimeZone);
/* 414 */           if (paramFile != null) {
/* 415 */             writeZone(paramFile, (DateTimeZoneBuilder)localObject6, localDateTimeZone);
/*     */           }
/*     */         }
/* 418 */         localTreeMap.put(localDateTimeZone.getID(), localDateTimeZone);
/* 419 */         if (ZoneInfoLogger.verbose()) {
/* 420 */           System.out.println("Good link: " + (String)localObject4 + " -> " + (String)localObject3 + " revived");
/*     */         }
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 426 */     for (j = 0; j < 2; j++) {
/* 427 */       for (int k = 0; k < this.iBackLinks.size(); k += 2) {
/* 428 */         localObject4 = (String)this.iBackLinks.get(k);
/* 429 */         localObject5 = (String)this.iBackLinks.get(k + 1);
/* 430 */         localObject6 = (DateTimeZone)localTreeMap.get(localObject4);
/* 431 */         if (localObject6 == null) {
/* 432 */           if (j > 0) {
/* 433 */             System.out.println("Cannot find time zone '" + (String)localObject4 + "' to link alias '" + (String)localObject5 + "' to");
/*     */           }
/*     */         } else {
/* 436 */           localTreeMap.put(localObject5, localObject6);
/* 437 */           if (ZoneInfoLogger.verbose()) {
/* 438 */             System.out.println("Back link: " + (String)localObject5 + " -> " + ((DateTimeZone)localObject6).getID());
/*     */           }
/*     */         }
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 445 */     if (paramFile != null) {
/* 446 */       System.out.println("Writing ZoneInfoMap");
/* 447 */       File localFile = new File(paramFile, "ZoneInfoMap");
/* 448 */       if (!localFile.getParentFile().exists()) {
/* 449 */         localFile.getParentFile().mkdirs();
/*     */       }
/*     */       
/* 452 */       FileOutputStream localFileOutputStream = new FileOutputStream(localFile);
/* 453 */       localObject4 = new DataOutputStream(localFileOutputStream);
/*     */       try
/*     */       {
/* 456 */         localObject5 = new TreeMap(String.CASE_INSENSITIVE_ORDER);
/* 457 */         ((Map)localObject5).putAll(localTreeMap);
/* 458 */         writeZoneInfoMap((DataOutputStream)localObject4, (Map)localObject5);
/*     */       } finally {
/* 460 */         ((DataOutputStream)localObject4).close();
/*     */       }
/*     */     }
/*     */     
/* 464 */     return localTreeMap;
/*     */   }
/*     */   
/*     */   private void writeZone(File paramFile, DateTimeZoneBuilder paramDateTimeZoneBuilder, DateTimeZone paramDateTimeZone) throws IOException {
/* 468 */     if (ZoneInfoLogger.verbose()) {
/* 469 */       System.out.println("Writing " + paramDateTimeZone.getID());
/*     */     }
/* 471 */     File localFile = new File(paramFile, paramDateTimeZone.getID());
/* 472 */     if (!localFile.getParentFile().exists()) {
/* 473 */       localFile.getParentFile().mkdirs();
/*     */     }
/* 475 */     FileOutputStream localFileOutputStream = new FileOutputStream(localFile);
/*     */     try {
/* 477 */       paramDateTimeZoneBuilder.writeTo(paramDateTimeZone.getID(), localFileOutputStream);
/*     */     } finally {
/* 479 */       localFileOutputStream.close();
/*     */     }
/*     */     
/*     */ 
/* 483 */     FileInputStream localFileInputStream = new FileInputStream(localFile);
/* 484 */     DateTimeZone localDateTimeZone = DateTimeZoneBuilder.readFrom(localFileInputStream, paramDateTimeZone.getID());
/* 485 */     localFileInputStream.close();
/*     */     
/* 487 */     if (!paramDateTimeZone.equals(localDateTimeZone)) {
/* 488 */       System.out.println("*e* Error in " + paramDateTimeZone.getID() + ": Didn't read properly from file");
/*     */     }
/*     */   }
/*     */   
/*     */   public void parseDataFile(BufferedReader paramBufferedReader, boolean paramBoolean) throws IOException
/*     */   {
/* 494 */     Object localObject1 = null;
/*     */     String str1;
/* 496 */     while ((str1 = paramBufferedReader.readLine()) != null) {
/* 497 */       String str2 = str1.trim();
/* 498 */       if ((str2.length() != 0) && (str2.charAt(0) != '#'))
/*     */       {
/*     */ 
/*     */ 
/* 502 */         int i = str1.indexOf('#');
/* 503 */         if (i >= 0) {
/* 504 */           str1 = str1.substring(0, i);
/*     */         }
/*     */         
/*     */ 
/*     */ 
/* 509 */         StringTokenizer localStringTokenizer = new StringTokenizer(str1, " \t");
/*     */         
/* 511 */         if ((Character.isWhitespace(str1.charAt(0))) && (localStringTokenizer.hasMoreTokens())) {
/* 512 */           if (localObject1 != null)
/*     */           {
/* 514 */             ((Zone)localObject1).chain(localStringTokenizer);
/*     */           }
/*     */         }
/*     */         else {
/* 518 */           if (localObject1 != null) {
/* 519 */             this.iZones.add(localObject1);
/*     */           }
/* 521 */           localObject1 = null;
/*     */           
/*     */ 
/* 524 */           if (localStringTokenizer.hasMoreTokens()) {
/* 525 */             String str3 = localStringTokenizer.nextToken();
/* 526 */             Object localObject2; Object localObject3; if (str3.equalsIgnoreCase("Rule")) {
/* 527 */               localObject2 = new Rule(localStringTokenizer);
/* 528 */               localObject3 = (RuleSet)this.iRuleSets.get(((Rule)localObject2).iName);
/* 529 */               if (localObject3 == null) {
/* 530 */                 localObject3 = new RuleSet((Rule)localObject2);
/* 531 */                 this.iRuleSets.put(((Rule)localObject2).iName, localObject3);
/*     */               } else {
/* 533 */                 ((RuleSet)localObject3).addRule((Rule)localObject2);
/*     */               }
/* 535 */             } else if (str3.equalsIgnoreCase("Zone")) {
/* 536 */               if (localStringTokenizer.countTokens() < 4) {
/* 537 */                 throw new IllegalArgumentException("Attempting to create a Zone from an incomplete tokenizer");
/*     */               }
/* 539 */               localObject1 = new Zone(localStringTokenizer);
/* 540 */             } else if (str3.equalsIgnoreCase("Link")) {
/* 541 */               localObject2 = localStringTokenizer.nextToken();
/* 542 */               localObject3 = localStringTokenizer.nextToken();
/*     */               
/*     */ 
/*     */ 
/* 546 */               if ((paramBoolean) || (((String)localObject3).equals("US/Pacific-New")) || (((String)localObject3).startsWith("Etc/")) || (((String)localObject3).equals("GMT"))) {
/* 547 */                 this.iBackLinks.add(localObject2);
/* 548 */                 this.iBackLinks.add(localObject3);
/*     */               } else {
/* 550 */                 this.iGoodLinks.add(localObject2);
/* 551 */                 this.iGoodLinks.add(localObject3);
/*     */               }
/*     */             } else {
/* 554 */               System.out.println("Unknown line: " + str1);
/*     */             }
/*     */           }
/*     */         }
/*     */       } }
/* 559 */     if (localObject1 != null) {
/* 560 */       this.iZones.add(localObject1);
/*     */     }
/*     */   }
/*     */   
/*     */   static class DateTimeOfYear {
/*     */     public final int iMonthOfYear;
/*     */     public final int iDayOfMonth;
/*     */     public final int iDayOfWeek;
/*     */     public final boolean iAdvanceDayOfWeek;
/*     */     public final int iMillisOfDay;
/*     */     public final char iZoneChar;
/*     */     
/*     */     DateTimeOfYear() {
/* 573 */       this.iMonthOfYear = 1;
/* 574 */       this.iDayOfMonth = 1;
/* 575 */       this.iDayOfWeek = 0;
/* 576 */       this.iAdvanceDayOfWeek = false;
/* 577 */       this.iMillisOfDay = 0;
/* 578 */       this.iZoneChar = 'w';
/*     */     }
/*     */     
/*     */     DateTimeOfYear(StringTokenizer paramStringTokenizer) {
/* 582 */       int i = 1;
/* 583 */       int j = 1;
/* 584 */       int k = 0;
/* 585 */       int m = 0;
/* 586 */       boolean bool = false;
/* 587 */       char c = 'w';
/*     */       
/* 589 */       if (paramStringTokenizer.hasMoreTokens()) {
/* 590 */         i = ZoneInfoCompiler.parseMonth(paramStringTokenizer.nextToken());
/*     */         
/* 592 */         if (paramStringTokenizer.hasMoreTokens()) {
/* 593 */           String str = paramStringTokenizer.nextToken();
/* 594 */           if (str.startsWith("last")) {
/* 595 */             j = -1;
/* 596 */             k = ZoneInfoCompiler.parseDayOfWeek(str.substring(4));
/* 597 */             bool = false;
/*     */           } else {
/*     */             try {
/* 600 */               j = Integer.parseInt(str);
/* 601 */               k = 0;
/* 602 */               bool = false;
/*     */             } catch (NumberFormatException localNumberFormatException) {
/* 604 */               int n = str.indexOf(">=");
/* 605 */               if (n > 0) {
/* 606 */                 j = Integer.parseInt(str.substring(n + 2));
/* 607 */                 k = ZoneInfoCompiler.parseDayOfWeek(str.substring(0, n));
/* 608 */                 bool = true;
/*     */               } else {
/* 610 */                 n = str.indexOf("<=");
/* 611 */                 if (n > 0) {
/* 612 */                   j = Integer.parseInt(str.substring(n + 2));
/* 613 */                   k = ZoneInfoCompiler.parseDayOfWeek(str.substring(0, n));
/* 614 */                   bool = false;
/*     */                 } else {
/* 616 */                   throw new IllegalArgumentException(str);
/*     */                 }
/*     */               }
/*     */             }
/*     */           }
/*     */           
/* 622 */           if (paramStringTokenizer.hasMoreTokens()) {
/* 623 */             str = paramStringTokenizer.nextToken();
/* 624 */             c = ZoneInfoCompiler.parseZoneChar(str.charAt(str.length() - 1));
/* 625 */             if (str.equals("24:00"))
/*     */             {
/* 627 */               if ((i == 12) && (j == 31)) {
/* 628 */                 m = ZoneInfoCompiler.parseTime("23:59:59.999");
/*     */               } else {
/* 630 */                 LocalDate localLocalDate = j == -1 ? new LocalDate(2001, i, 1).plusMonths(1) : new LocalDate(2001, i, j).plusDays(1);
/*     */                 
/*     */ 
/* 633 */                 bool = (j != -1) && (k != 0);
/* 634 */                 i = localLocalDate.getMonthOfYear();
/* 635 */                 j = localLocalDate.getDayOfMonth();
/* 636 */                 if (k != 0) {
/* 637 */                   k = (k - 1 + 1) % 7 + 1;
/*     */                 }
/*     */               }
/*     */             } else {
/* 641 */               m = ZoneInfoCompiler.parseTime(str);
/*     */             }
/*     */           }
/*     */         }
/*     */       }
/*     */       
/* 647 */       this.iMonthOfYear = i;
/* 648 */       this.iDayOfMonth = j;
/* 649 */       this.iDayOfWeek = k;
/* 650 */       this.iAdvanceDayOfWeek = bool;
/* 651 */       this.iMillisOfDay = m;
/* 652 */       this.iZoneChar = c;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     public void addRecurring(DateTimeZoneBuilder paramDateTimeZoneBuilder, String paramString, int paramInt1, int paramInt2, int paramInt3)
/*     */     {
/* 661 */       paramDateTimeZoneBuilder.addRecurringSavings(paramString, paramInt1, paramInt2, paramInt3, this.iZoneChar, this.iMonthOfYear, this.iDayOfMonth, this.iDayOfWeek, this.iAdvanceDayOfWeek, this.iMillisOfDay);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     public void addCutover(DateTimeZoneBuilder paramDateTimeZoneBuilder, int paramInt)
/*     */     {
/* 675 */       paramDateTimeZoneBuilder.addCutover(paramInt, this.iZoneChar, this.iMonthOfYear, this.iDayOfMonth, this.iDayOfWeek, this.iAdvanceDayOfWeek, this.iMillisOfDay);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     public String toString()
/*     */     {
/* 685 */       return "MonthOfYear: " + this.iMonthOfYear + "\n" + "DayOfMonth: " + this.iDayOfMonth + "\n" + "DayOfWeek: " + this.iDayOfWeek + "\n" + "AdvanceDayOfWeek: " + this.iAdvanceDayOfWeek + "\n" + "MillisOfDay: " + this.iMillisOfDay + "\n" + "ZoneChar: " + this.iZoneChar + "\n";
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   private static class Rule
/*     */   {
/*     */     public final String iName;
/*     */     
/*     */     public final int iFromYear;
/*     */     
/*     */     public final int iToYear;
/*     */     
/*     */     public final String iType;
/*     */     public final ZoneInfoCompiler.DateTimeOfYear iDateTimeOfYear;
/*     */     public final int iSaveMillis;
/*     */     public final String iLetterS;
/*     */     
/*     */     Rule(StringTokenizer paramStringTokenizer)
/*     */     {
/* 705 */       if (paramStringTokenizer.countTokens() < 6) {
/* 706 */         throw new IllegalArgumentException("Attempting to create a Rule from an incomplete tokenizer");
/*     */       }
/* 708 */       this.iName = paramStringTokenizer.nextToken().intern();
/* 709 */       this.iFromYear = ZoneInfoCompiler.parseYear(paramStringTokenizer.nextToken(), 0);
/* 710 */       this.iToYear = ZoneInfoCompiler.parseYear(paramStringTokenizer.nextToken(), this.iFromYear);
/* 711 */       if (this.iToYear < this.iFromYear) {
/* 712 */         throw new IllegalArgumentException();
/*     */       }
/* 714 */       this.iType = ZoneInfoCompiler.parseOptional(paramStringTokenizer.nextToken());
/* 715 */       this.iDateTimeOfYear = new ZoneInfoCompiler.DateTimeOfYear(paramStringTokenizer);
/* 716 */       this.iSaveMillis = ZoneInfoCompiler.parseTime(paramStringTokenizer.nextToken());
/* 717 */       this.iLetterS = ZoneInfoCompiler.parseOptional(paramStringTokenizer.nextToken());
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */     public void addRecurring(DateTimeZoneBuilder paramDateTimeZoneBuilder, String paramString)
/*     */     {
/* 724 */       String str = formatName(paramString);
/* 725 */       this.iDateTimeOfYear.addRecurring(paramDateTimeZoneBuilder, str, this.iSaveMillis, this.iFromYear, this.iToYear);
/*     */     }
/*     */     
/*     */     private String formatName(String paramString)
/*     */     {
/* 730 */       int i = paramString.indexOf('/');
/* 731 */       if (i > 0) {
/* 732 */         if (this.iSaveMillis == 0)
/*     */         {
/* 734 */           return paramString.substring(0, i).intern();
/*     */         }
/* 736 */         return paramString.substring(i + 1).intern();
/*     */       }
/*     */       
/* 739 */       i = paramString.indexOf("%s");
/* 740 */       if (i < 0) {
/* 741 */         return paramString;
/*     */       }
/* 743 */       String str1 = paramString.substring(0, i);
/* 744 */       String str2 = paramString.substring(i + 2);
/*     */       String str3;
/* 746 */       if (this.iLetterS == null) {
/* 747 */         str3 = str1.concat(str2);
/*     */       } else {
/* 749 */         str3 = str1 + this.iLetterS + str2;
/*     */       }
/* 751 */       return str3.intern();
/*     */     }
/*     */     
/*     */     public String toString() {
/* 755 */       return "[Rule]\nName: " + this.iName + "\n" + "FromYear: " + this.iFromYear + "\n" + "ToYear: " + this.iToYear + "\n" + "Type: " + this.iType + "\n" + this.iDateTimeOfYear + "SaveMillis: " + this.iSaveMillis + "\n" + "LetterS: " + this.iLetterS + "\n";
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private static class RuleSet
/*     */   {
/*     */     private List<ZoneInfoCompiler.Rule> iRules;
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */     RuleSet(ZoneInfoCompiler.Rule paramRule)
/*     */     {
/* 771 */       this.iRules = new ArrayList();
/* 772 */       this.iRules.add(paramRule);
/*     */     }
/*     */     
/*     */     void addRule(ZoneInfoCompiler.Rule paramRule) {
/* 776 */       if (!paramRule.iName.equals(((ZoneInfoCompiler.Rule)this.iRules.get(0)).iName)) {
/* 777 */         throw new IllegalArgumentException("Rule name mismatch");
/*     */       }
/* 779 */       this.iRules.add(paramRule);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */     public void addRecurring(DateTimeZoneBuilder paramDateTimeZoneBuilder, String paramString)
/*     */     {
/* 786 */       for (int i = 0; i < this.iRules.size(); i++) {
/* 787 */         ZoneInfoCompiler.Rule localRule = (ZoneInfoCompiler.Rule)this.iRules.get(i);
/* 788 */         localRule.addRecurring(paramDateTimeZoneBuilder, paramString);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private static class Zone
/*     */   {
/*     */     public final String iName;
/*     */     public final int iOffsetMillis;
/*     */     public final String iRules;
/*     */     public final String iFormat;
/*     */     public final int iUntilYear;
/*     */     public final ZoneInfoCompiler.DateTimeOfYear iUntilDateTimeOfYear;
/*     */     private Zone iNext;
/*     */     
/*     */     Zone(StringTokenizer paramStringTokenizer) {
/* 804 */       this(paramStringTokenizer.nextToken(), paramStringTokenizer);
/*     */     }
/*     */     
/*     */     private Zone(String paramString, StringTokenizer paramStringTokenizer) {
/* 808 */       this.iName = paramString.intern();
/* 809 */       this.iOffsetMillis = ZoneInfoCompiler.parseTime(paramStringTokenizer.nextToken());
/* 810 */       this.iRules = ZoneInfoCompiler.parseOptional(paramStringTokenizer.nextToken());
/* 811 */       this.iFormat = paramStringTokenizer.nextToken().intern();
/*     */       
/* 813 */       int i = Integer.MAX_VALUE;
/* 814 */       ZoneInfoCompiler.DateTimeOfYear localDateTimeOfYear = ZoneInfoCompiler.getStartOfYear();
/*     */       
/* 816 */       if (paramStringTokenizer.hasMoreTokens()) {
/* 817 */         i = Integer.parseInt(paramStringTokenizer.nextToken());
/* 818 */         if (paramStringTokenizer.hasMoreTokens()) {
/* 819 */           localDateTimeOfYear = new ZoneInfoCompiler.DateTimeOfYear(paramStringTokenizer);
/*     */         }
/*     */       }
/*     */       
/* 823 */       this.iUntilYear = i;
/* 824 */       this.iUntilDateTimeOfYear = localDateTimeOfYear;
/*     */     }
/*     */     
/*     */     void chain(StringTokenizer paramStringTokenizer) {
/* 828 */       if (this.iNext != null) {
/* 829 */         this.iNext.chain(paramStringTokenizer);
/*     */       } else {
/* 831 */         this.iNext = new Zone(this.iName, paramStringTokenizer);
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     public void addToBuilder(DateTimeZoneBuilder paramDateTimeZoneBuilder, Map<String, ZoneInfoCompiler.RuleSet> paramMap)
/*     */     {
/* 847 */       addToBuilder(this, paramDateTimeZoneBuilder, paramMap);
/*     */     }
/*     */     
/*     */     private static void addToBuilder(Zone paramZone, DateTimeZoneBuilder paramDateTimeZoneBuilder, Map<String, ZoneInfoCompiler.RuleSet> paramMap)
/*     */     {
/* 854 */       for (; 
/*     */           
/* 854 */           paramZone != null; paramZone = paramZone.iNext) {
/* 855 */         paramDateTimeZoneBuilder.setStandardOffset(paramZone.iOffsetMillis);
/*     */         
/* 857 */         if (paramZone.iRules == null) {
/* 858 */           paramDateTimeZoneBuilder.setFixedSavings(paramZone.iFormat, 0);
/*     */         } else {
/*     */           try
/*     */           {
/* 862 */             int i = ZoneInfoCompiler.parseTime(paramZone.iRules);
/* 863 */             paramDateTimeZoneBuilder.setFixedSavings(paramZone.iFormat, i);
/*     */           }
/*     */           catch (Exception localException) {
/* 866 */             ZoneInfoCompiler.RuleSet localRuleSet = (ZoneInfoCompiler.RuleSet)paramMap.get(paramZone.iRules);
/* 867 */             if (localRuleSet == null) {
/* 868 */               throw new IllegalArgumentException("Rules not found: " + paramZone.iRules);
/*     */             }
/*     */             
/* 871 */             localRuleSet.addRecurring(paramDateTimeZoneBuilder, paramZone.iFormat);
/*     */           }
/*     */         }
/*     */         
/* 875 */         if (paramZone.iUntilYear == Integer.MAX_VALUE) {
/*     */           break;
/*     */         }
/*     */         
/* 879 */         paramZone.iUntilDateTimeOfYear.addCutover(paramDateTimeZoneBuilder, paramZone.iUntilYear);
/*     */       }
/*     */     }
/*     */     
/*     */     public String toString() {
/* 884 */       String str = "[Zone]\nName: " + this.iName + "\n" + "OffsetMillis: " + this.iOffsetMillis + "\n" + "Rules: " + this.iRules + "\n" + "Format: " + this.iFormat + "\n" + "UntilYear: " + this.iUntilYear + "\n" + this.iUntilDateTimeOfYear;
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 893 */       if (this.iNext == null) {
/* 894 */         return str;
/*     */       }
/*     */       
/* 897 */       return str + "...\n" + this.iNext.toString();
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\org\joda\time\tz\ZoneInfoCompiler.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */