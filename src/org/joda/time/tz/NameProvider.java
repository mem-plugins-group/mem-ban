package org.joda.time.tz;

import java.util.Locale;

public abstract interface NameProvider
{
  public abstract String getShortName(Locale paramLocale, String paramString1, String paramString2);
  
  public abstract String getName(Locale paramLocale, String paramString1, String paramString2);
}


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\org\joda\time\tz\NameProvider.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */