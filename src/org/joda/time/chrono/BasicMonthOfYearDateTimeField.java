/*     */ package org.joda.time.chrono;
/*     */ 
/*     */ import org.joda.time.DateTimeField;
/*     */ import org.joda.time.DateTimeFieldType;
/*     */ import org.joda.time.DateTimeUtils;
/*     */ import org.joda.time.DurationField;
/*     */ import org.joda.time.ReadablePartial;
/*     */ import org.joda.time.field.FieldUtils;
/*     */ import org.joda.time.field.ImpreciseDateTimeField;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ class BasicMonthOfYearDateTimeField
/*     */   extends ImpreciseDateTimeField
/*     */ {
/*     */   private static final long serialVersionUID = -8258715387168736L;
/*     */   private static final int MIN = 1;
/*     */   private final BasicChronology iChronology;
/*     */   private final int iMax;
/*     */   private final int iLeapMonth;
/*     */   
/*     */   BasicMonthOfYearDateTimeField(BasicChronology paramBasicChronology, int paramInt)
/*     */   {
/*  52 */     super(DateTimeFieldType.monthOfYear(), paramBasicChronology.getAverageMillisPerMonth());
/*  53 */     this.iChronology = paramBasicChronology;
/*  54 */     this.iMax = this.iChronology.getMaxMonth();
/*  55 */     this.iLeapMonth = paramInt;
/*     */   }
/*     */   
/*     */   public boolean isLenient()
/*     */   {
/*  60 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int get(long paramLong)
/*     */   {
/*  73 */     return this.iChronology.getMonthOfYear(paramLong);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public long add(long paramLong, int paramInt)
/*     */   {
/*  93 */     if (paramInt == 0) {
/*  94 */       return paramLong;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*  99 */     long l1 = this.iChronology.getMillisOfDay(paramLong);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 104 */     int i = this.iChronology.getYear(paramLong);
/* 105 */     int j = this.iChronology.getMonthOfYear(paramLong, i);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 111 */     int k = i;
/*     */     
/* 113 */     int m = j - 1 + paramInt;
/* 114 */     if ((j > 0) && (m < 0)) {
/* 115 */       if (Math.signum(paramInt + this.iMax) == Math.signum(paramInt)) {
/* 116 */         k--;
/* 117 */         paramInt += this.iMax;
/*     */       } else {
/* 119 */         k++;
/* 120 */         paramInt -= this.iMax;
/*     */       }
/* 122 */       m = j - 1 + paramInt;
/*     */     }
/* 124 */     if (m >= 0) {
/* 125 */       k += m / this.iMax;
/* 126 */       m = m % this.iMax + 1;
/*     */     } else {
/* 128 */       k = k + m / this.iMax - 1;
/* 129 */       m = Math.abs(m);
/* 130 */       n = m % this.iMax;
/*     */       
/* 132 */       if (n == 0) {
/* 133 */         n = this.iMax;
/*     */       }
/* 135 */       m = this.iMax - n + 1;
/*     */       
/* 137 */       if (m == 1) {
/* 138 */         k++;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 147 */     int n = this.iChronology.getDayOfMonth(paramLong, i, j);
/* 148 */     int i1 = this.iChronology.getDaysInYearMonth(k, m);
/* 149 */     if (n > i1) {
/* 150 */       n = i1;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 155 */     long l2 = this.iChronology.getYearMonthDayMillis(k, m, n);
/*     */     
/* 157 */     return l2 + l1;
/*     */   }
/*     */   
/*     */   public long add(long paramLong1, long paramLong2)
/*     */   {
/* 162 */     int i = (int)paramLong2;
/* 163 */     if (i == paramLong2) {
/* 164 */       return add(paramLong1, i);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 169 */     long l1 = this.iChronology.getMillisOfDay(paramLong1);
/*     */     
/* 171 */     int j = this.iChronology.getYear(paramLong1);
/* 172 */     int k = this.iChronology.getMonthOfYear(paramLong1, j);
/*     */     
/*     */ 
/* 175 */     long l3 = k - 1 + paramLong2;
/* 176 */     long l2; if (l3 >= 0L) {
/* 177 */       l2 = j + l3 / this.iMax;
/* 178 */       l3 = l3 % this.iMax + 1L;
/*     */     } else {
/* 180 */       l2 = j + l3 / this.iMax - 1L;
/* 181 */       l3 = Math.abs(l3);
/* 182 */       m = (int)(l3 % this.iMax);
/* 183 */       if (m == 0) {
/* 184 */         m = this.iMax;
/*     */       }
/* 186 */       l3 = this.iMax - m + 1;
/* 187 */       if (l3 == 1L) {
/* 188 */         l2 += 1L;
/*     */       }
/*     */     }
/*     */     
/* 192 */     if ((l2 < this.iChronology.getMinYear()) || (l2 > this.iChronology.getMaxYear()))
/*     */     {
/*     */ 
/* 195 */       throw new IllegalArgumentException("Magnitude of add amount is too large: " + paramLong2);
/*     */     }
/*     */     
/*     */ 
/* 199 */     int m = (int)l2;
/* 200 */     int n = (int)l3;
/*     */     
/* 202 */     int i1 = this.iChronology.getDayOfMonth(paramLong1, j, k);
/* 203 */     int i2 = this.iChronology.getDaysInYearMonth(m, n);
/* 204 */     if (i1 > i2) {
/* 205 */       i1 = i2;
/*     */     }
/*     */     
/* 208 */     long l4 = this.iChronology.getYearMonthDayMillis(m, n, i1);
/*     */     
/* 210 */     return l4 + l1;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public int[] add(ReadablePartial paramReadablePartial, int paramInt1, int[] paramArrayOfInt, int paramInt2)
/*     */   {
/* 217 */     if (paramInt2 == 0) {
/* 218 */       return paramArrayOfInt;
/*     */     }
/* 220 */     if ((paramReadablePartial.size() > 0) && (paramReadablePartial.getFieldType(0).equals(DateTimeFieldType.monthOfYear())) && (paramInt1 == 0))
/*     */     {
/* 222 */       int i = paramArrayOfInt[0] - 1;
/* 223 */       int j = (i + paramInt2 % 12 + 12) % 12 + 1;
/* 224 */       return set(paramReadablePartial, 0, paramArrayOfInt, j);
/*     */     }
/* 226 */     if (DateTimeUtils.isContiguous(paramReadablePartial)) {
/* 227 */       long l = 0L;
/* 228 */       int k = 0; for (int m = paramReadablePartial.size(); k < m; k++) {
/* 229 */         l = paramReadablePartial.getFieldType(k).getField(this.iChronology).set(l, paramArrayOfInt[k]);
/*     */       }
/* 231 */       l = add(l, paramInt2);
/* 232 */       return this.iChronology.get(paramReadablePartial, l);
/*     */     }
/* 234 */     return super.add(paramReadablePartial, paramInt1, paramArrayOfInt, paramInt2);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public long addWrapField(long paramLong, int paramInt)
/*     */   {
/* 249 */     return set(paramLong, FieldUtils.getWrappedValue(get(paramLong), paramInt, 1, this.iMax));
/*     */   }
/*     */   
/*     */   public long getDifferenceAsLong(long paramLong1, long paramLong2)
/*     */   {
/* 254 */     if (paramLong1 < paramLong2) {
/* 255 */       return -getDifference(paramLong2, paramLong1);
/*     */     }
/*     */     
/* 258 */     int i = this.iChronology.getYear(paramLong1);
/* 259 */     int j = this.iChronology.getMonthOfYear(paramLong1, i);
/* 260 */     int k = this.iChronology.getYear(paramLong2);
/* 261 */     int m = this.iChronology.getMonthOfYear(paramLong2, k);
/*     */     
/* 263 */     long l1 = (i - k) * this.iMax + j - m;
/*     */     
/*     */ 
/*     */ 
/* 267 */     int n = this.iChronology.getDayOfMonth(paramLong1, i, j);
/*     */     
/* 269 */     if (n == this.iChronology.getDaysInYearMonth(i, j))
/*     */     {
/* 271 */       int i1 = this.iChronology.getDayOfMonth(paramLong2, k, m);
/*     */       
/* 273 */       if (i1 > n)
/*     */       {
/*     */ 
/*     */ 
/* 277 */         paramLong2 = this.iChronology.dayOfMonth().set(paramLong2, n);
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 282 */     long l2 = paramLong1 - this.iChronology.getYearMonthMillis(i, j);
/*     */     
/* 284 */     long l3 = paramLong2 - this.iChronology.getYearMonthMillis(k, m);
/*     */     
/*     */ 
/* 287 */     if (l2 < l3) {
/* 288 */       l1 -= 1L;
/*     */     }
/*     */     
/* 291 */     return l1;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public long set(long paramLong, int paramInt)
/*     */   {
/* 309 */     FieldUtils.verifyValueBounds(this, paramInt, 1, this.iMax);
/*     */     
/* 311 */     int i = this.iChronology.getYear(paramLong);
/*     */     
/* 313 */     int j = this.iChronology.getDayOfMonth(paramLong, i);
/* 314 */     int k = this.iChronology.getDaysInYearMonth(i, paramInt);
/* 315 */     if (j > k)
/*     */     {
/* 317 */       j = k;
/*     */     }
/*     */     
/* 320 */     return this.iChronology.getYearMonthDayMillis(i, paramInt, j) + this.iChronology.getMillisOfDay(paramLong);
/*     */   }
/*     */   
/*     */ 
/*     */   public DurationField getRangeDurationField()
/*     */   {
/* 326 */     return this.iChronology.years();
/*     */   }
/*     */   
/*     */   public boolean isLeap(long paramLong)
/*     */   {
/* 331 */     int i = this.iChronology.getYear(paramLong);
/* 332 */     if (this.iChronology.isLeapYear(i)) {
/* 333 */       return this.iChronology.getMonthOfYear(paramLong, i) == this.iLeapMonth;
/*     */     }
/* 335 */     return false;
/*     */   }
/*     */   
/*     */   public int getLeapAmount(long paramLong)
/*     */   {
/* 340 */     return isLeap(paramLong) ? 1 : 0;
/*     */   }
/*     */   
/*     */   public DurationField getLeapDurationField()
/*     */   {
/* 345 */     return this.iChronology.days();
/*     */   }
/*     */   
/*     */   public int getMinimumValue()
/*     */   {
/* 350 */     return 1;
/*     */   }
/*     */   
/*     */   public int getMaximumValue()
/*     */   {
/* 355 */     return this.iMax;
/*     */   }
/*     */   
/*     */   public long roundFloor(long paramLong)
/*     */   {
/* 360 */     int i = this.iChronology.getYear(paramLong);
/* 361 */     int j = this.iChronology.getMonthOfYear(paramLong, i);
/* 362 */     return this.iChronology.getYearMonthMillis(i, j);
/*     */   }
/*     */   
/*     */   public long remainder(long paramLong)
/*     */   {
/* 367 */     return paramLong - roundFloor(paramLong);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private Object readResolve()
/*     */   {
/* 375 */     return this.iChronology.monthOfYear();
/*     */   }
/*     */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\org\joda\time\chrono\BasicMonthOfYearDateTimeField.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */