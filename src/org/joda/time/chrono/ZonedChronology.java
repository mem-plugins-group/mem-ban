/*     */ package org.joda.time.chrono;
/*     */ 
/*     */ import java.util.HashMap;
/*     */ import java.util.Locale;
/*     */ import org.joda.time.Chronology;
/*     */ import org.joda.time.DateTimeField;
/*     */ import org.joda.time.DateTimeZone;
/*     */ import org.joda.time.DurationField;
/*     */ import org.joda.time.IllegalFieldValueException;
/*     */ import org.joda.time.IllegalInstantException;
/*     */ import org.joda.time.ReadablePartial;
/*     */ import org.joda.time.field.BaseDateTimeField;
/*     */ import org.joda.time.field.BaseDurationField;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public final class ZonedChronology
/*     */   extends AssembledChronology
/*     */ {
/*     */   private static final long serialVersionUID = -1079258847191166848L;
/*     */   private static final long NEAR_ZERO = 604800000L;
/*     */   
/*     */   public static ZonedChronology getInstance(Chronology paramChronology, DateTimeZone paramDateTimeZone)
/*     */   {
/*  59 */     if (paramChronology == null) {
/*  60 */       throw new IllegalArgumentException("Must supply a chronology");
/*     */     }
/*  62 */     paramChronology = paramChronology.withUTC();
/*  63 */     if (paramChronology == null) {
/*  64 */       throw new IllegalArgumentException("UTC chronology must not be null");
/*     */     }
/*  66 */     if (paramDateTimeZone == null) {
/*  67 */       throw new IllegalArgumentException("DateTimeZone must not be null");
/*     */     }
/*  69 */     return new ZonedChronology(paramChronology, paramDateTimeZone);
/*     */   }
/*     */   
/*     */ 
/*     */   static boolean useTimeArithmetic(DurationField paramDurationField)
/*     */   {
/*  75 */     return (paramDurationField != null) && (paramDurationField.getUnitMillis() < 43200000L);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private ZonedChronology(Chronology paramChronology, DateTimeZone paramDateTimeZone)
/*     */   {
/*  85 */     super(paramChronology, paramDateTimeZone);
/*     */   }
/*     */   
/*     */   public DateTimeZone getZone() {
/*  89 */     return (DateTimeZone)getParam();
/*     */   }
/*     */   
/*     */   public Chronology withUTC() {
/*  93 */     return getBase();
/*     */   }
/*     */   
/*     */   public Chronology withZone(DateTimeZone paramDateTimeZone) {
/*  97 */     if (paramDateTimeZone == null) {
/*  98 */       paramDateTimeZone = DateTimeZone.getDefault();
/*     */     }
/* 100 */     if (paramDateTimeZone == getParam()) {
/* 101 */       return this;
/*     */     }
/* 103 */     if (paramDateTimeZone == DateTimeZone.UTC) {
/* 104 */       return getBase();
/*     */     }
/* 106 */     return new ZonedChronology(getBase(), paramDateTimeZone);
/*     */   }
/*     */   
/*     */ 
/*     */   public long getDateTimeMillis(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
/*     */     throws IllegalArgumentException
/*     */   {
/* 113 */     return localToUTC(getBase().getDateTimeMillis(paramInt1, paramInt2, paramInt3, paramInt4));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public long getDateTimeMillis(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7)
/*     */     throws IllegalArgumentException
/*     */   {
/* 122 */     return localToUTC(getBase().getDateTimeMillis(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public long getDateTimeMillis(long paramLong, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
/*     */     throws IllegalArgumentException
/*     */   {
/* 132 */     return localToUTC(getBase().getDateTimeMillis(paramLong + getZone().getOffset(paramLong), paramInt1, paramInt2, paramInt3, paramInt4));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private long localToUTC(long paramLong)
/*     */   {
/* 142 */     if (paramLong == Long.MAX_VALUE)
/* 143 */       return Long.MAX_VALUE;
/* 144 */     if (paramLong == Long.MIN_VALUE) {
/* 145 */       return Long.MIN_VALUE;
/*     */     }
/* 147 */     DateTimeZone localDateTimeZone = getZone();
/* 148 */     int i = localDateTimeZone.getOffsetFromLocal(paramLong);
/* 149 */     long l = paramLong - i;
/* 150 */     if ((paramLong > 604800000L) && (l < 0L))
/* 151 */       return Long.MAX_VALUE;
/* 152 */     if ((paramLong < -604800000L) && (l > 0L)) {
/* 153 */       return Long.MIN_VALUE;
/*     */     }
/* 155 */     int j = localDateTimeZone.getOffset(l);
/* 156 */     if (i != j) {
/* 157 */       throw new IllegalInstantException(paramLong, localDateTimeZone.getID());
/*     */     }
/* 159 */     return l;
/*     */   }
/*     */   
/*     */ 
/*     */   protected void assemble(AssembledChronology.Fields paramFields)
/*     */   {
/* 165 */     HashMap localHashMap = new HashMap();
/*     */     
/*     */ 
/*     */ 
/* 169 */     paramFields.eras = convertField(paramFields.eras, localHashMap);
/* 170 */     paramFields.centuries = convertField(paramFields.centuries, localHashMap);
/* 171 */     paramFields.years = convertField(paramFields.years, localHashMap);
/* 172 */     paramFields.months = convertField(paramFields.months, localHashMap);
/* 173 */     paramFields.weekyears = convertField(paramFields.weekyears, localHashMap);
/* 174 */     paramFields.weeks = convertField(paramFields.weeks, localHashMap);
/* 175 */     paramFields.days = convertField(paramFields.days, localHashMap);
/*     */     
/* 177 */     paramFields.halfdays = convertField(paramFields.halfdays, localHashMap);
/* 178 */     paramFields.hours = convertField(paramFields.hours, localHashMap);
/* 179 */     paramFields.minutes = convertField(paramFields.minutes, localHashMap);
/* 180 */     paramFields.seconds = convertField(paramFields.seconds, localHashMap);
/* 181 */     paramFields.millis = convertField(paramFields.millis, localHashMap);
/*     */     
/*     */ 
/*     */ 
/* 185 */     paramFields.year = convertField(paramFields.year, localHashMap);
/* 186 */     paramFields.yearOfEra = convertField(paramFields.yearOfEra, localHashMap);
/* 187 */     paramFields.yearOfCentury = convertField(paramFields.yearOfCentury, localHashMap);
/* 188 */     paramFields.centuryOfEra = convertField(paramFields.centuryOfEra, localHashMap);
/* 189 */     paramFields.era = convertField(paramFields.era, localHashMap);
/* 190 */     paramFields.dayOfWeek = convertField(paramFields.dayOfWeek, localHashMap);
/* 191 */     paramFields.dayOfMonth = convertField(paramFields.dayOfMonth, localHashMap);
/* 192 */     paramFields.dayOfYear = convertField(paramFields.dayOfYear, localHashMap);
/* 193 */     paramFields.monthOfYear = convertField(paramFields.monthOfYear, localHashMap);
/* 194 */     paramFields.weekOfWeekyear = convertField(paramFields.weekOfWeekyear, localHashMap);
/* 195 */     paramFields.weekyear = convertField(paramFields.weekyear, localHashMap);
/* 196 */     paramFields.weekyearOfCentury = convertField(paramFields.weekyearOfCentury, localHashMap);
/*     */     
/* 198 */     paramFields.millisOfSecond = convertField(paramFields.millisOfSecond, localHashMap);
/* 199 */     paramFields.millisOfDay = convertField(paramFields.millisOfDay, localHashMap);
/* 200 */     paramFields.secondOfMinute = convertField(paramFields.secondOfMinute, localHashMap);
/* 201 */     paramFields.secondOfDay = convertField(paramFields.secondOfDay, localHashMap);
/* 202 */     paramFields.minuteOfHour = convertField(paramFields.minuteOfHour, localHashMap);
/* 203 */     paramFields.minuteOfDay = convertField(paramFields.minuteOfDay, localHashMap);
/* 204 */     paramFields.hourOfDay = convertField(paramFields.hourOfDay, localHashMap);
/* 205 */     paramFields.hourOfHalfday = convertField(paramFields.hourOfHalfday, localHashMap);
/* 206 */     paramFields.clockhourOfDay = convertField(paramFields.clockhourOfDay, localHashMap);
/* 207 */     paramFields.clockhourOfHalfday = convertField(paramFields.clockhourOfHalfday, localHashMap);
/* 208 */     paramFields.halfdayOfDay = convertField(paramFields.halfdayOfDay, localHashMap);
/*     */   }
/*     */   
/*     */   private DurationField convertField(DurationField paramDurationField, HashMap<Object, Object> paramHashMap) {
/* 212 */     if ((paramDurationField == null) || (!paramDurationField.isSupported())) {
/* 213 */       return paramDurationField;
/*     */     }
/* 215 */     if (paramHashMap.containsKey(paramDurationField)) {
/* 216 */       return (DurationField)paramHashMap.get(paramDurationField);
/*     */     }
/* 218 */     ZonedDurationField localZonedDurationField = new ZonedDurationField(paramDurationField, getZone());
/* 219 */     paramHashMap.put(paramDurationField, localZonedDurationField);
/* 220 */     return localZonedDurationField;
/*     */   }
/*     */   
/*     */   private DateTimeField convertField(DateTimeField paramDateTimeField, HashMap<Object, Object> paramHashMap) {
/* 224 */     if ((paramDateTimeField == null) || (!paramDateTimeField.isSupported())) {
/* 225 */       return paramDateTimeField;
/*     */     }
/* 227 */     if (paramHashMap.containsKey(paramDateTimeField)) {
/* 228 */       return (DateTimeField)paramHashMap.get(paramDateTimeField);
/*     */     }
/* 230 */     ZonedDateTimeField localZonedDateTimeField = new ZonedDateTimeField(paramDateTimeField, getZone(), convertField(paramDateTimeField.getDurationField(), paramHashMap), convertField(paramDateTimeField.getRangeDurationField(), paramHashMap), convertField(paramDateTimeField.getLeapDurationField(), paramHashMap));
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 235 */     paramHashMap.put(paramDateTimeField, localZonedDateTimeField);
/* 236 */     return localZonedDateTimeField;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean equals(Object paramObject)
/*     */   {
/* 249 */     if (this == paramObject) {
/* 250 */       return true;
/*     */     }
/* 252 */     if (!(paramObject instanceof ZonedChronology)) {
/* 253 */       return false;
/*     */     }
/* 255 */     ZonedChronology localZonedChronology = (ZonedChronology)paramObject;
/* 256 */     return (getBase().equals(localZonedChronology.getBase())) && (getZone().equals(localZonedChronology.getZone()));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int hashCode()
/*     */   {
/* 268 */     return 326565 + getZone().hashCode() * 11 + getBase().hashCode() * 7;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String toString()
/*     */   {
/* 277 */     return "ZonedChronology[" + getBase() + ", " + getZone().getID() + ']';
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   static class ZonedDurationField
/*     */     extends BaseDurationField
/*     */   {
/*     */     private static final long serialVersionUID = -485345310999208286L;
/*     */     
/*     */     final DurationField iField;
/*     */     
/*     */     final boolean iTimeField;
/*     */     
/*     */     final DateTimeZone iZone;
/*     */     
/*     */ 
/*     */     ZonedDurationField(DurationField paramDurationField, DateTimeZone paramDateTimeZone)
/*     */     {
/* 296 */       super();
/* 297 */       if (!paramDurationField.isSupported()) {
/* 298 */         throw new IllegalArgumentException();
/*     */       }
/* 300 */       this.iField = paramDurationField;
/* 301 */       this.iTimeField = ZonedChronology.useTimeArithmetic(paramDurationField);
/* 302 */       this.iZone = paramDateTimeZone;
/*     */     }
/*     */     
/*     */     public boolean isPrecise() {
/* 306 */       return (this.iField.isPrecise()) && (this.iZone.isFixed()) ? true : this.iTimeField ? this.iField.isPrecise() : false;
/*     */     }
/*     */     
/*     */     public long getUnitMillis() {
/* 310 */       return this.iField.getUnitMillis();
/*     */     }
/*     */     
/*     */     public int getValue(long paramLong1, long paramLong2) {
/* 314 */       return this.iField.getValue(paramLong1, addOffset(paramLong2));
/*     */     }
/*     */     
/*     */     public long getValueAsLong(long paramLong1, long paramLong2) {
/* 318 */       return this.iField.getValueAsLong(paramLong1, addOffset(paramLong2));
/*     */     }
/*     */     
/*     */     public long getMillis(int paramInt, long paramLong) {
/* 322 */       return this.iField.getMillis(paramInt, addOffset(paramLong));
/*     */     }
/*     */     
/*     */     public long getMillis(long paramLong1, long paramLong2) {
/* 326 */       return this.iField.getMillis(paramLong1, addOffset(paramLong2));
/*     */     }
/*     */     
/*     */     public long add(long paramLong, int paramInt) {
/* 330 */       int i = getOffsetToAdd(paramLong);
/* 331 */       paramLong = this.iField.add(paramLong + i, paramInt);
/* 332 */       return paramLong - (this.iTimeField ? i : getOffsetFromLocalToSubtract(paramLong));
/*     */     }
/*     */     
/*     */     public long add(long paramLong1, long paramLong2) {
/* 336 */       int i = getOffsetToAdd(paramLong1);
/* 337 */       paramLong1 = this.iField.add(paramLong1 + i, paramLong2);
/* 338 */       return paramLong1 - (this.iTimeField ? i : getOffsetFromLocalToSubtract(paramLong1));
/*     */     }
/*     */     
/*     */     public int getDifference(long paramLong1, long paramLong2) {
/* 342 */       int i = getOffsetToAdd(paramLong2);
/* 343 */       return this.iField.getDifference(paramLong1 + (this.iTimeField ? i : getOffsetToAdd(paramLong1)), paramLong2 + i);
/*     */     }
/*     */     
/*     */ 
/*     */     public long getDifferenceAsLong(long paramLong1, long paramLong2)
/*     */     {
/* 349 */       int i = getOffsetToAdd(paramLong2);
/* 350 */       return this.iField.getDifferenceAsLong(paramLong1 + (this.iTimeField ? i : getOffsetToAdd(paramLong1)), paramLong2 + i);
/*     */     }
/*     */     
/*     */ 
/*     */     private int getOffsetToAdd(long paramLong)
/*     */     {
/* 356 */       int i = this.iZone.getOffset(paramLong);
/* 357 */       long l = paramLong + i;
/*     */       
/* 359 */       if (((paramLong ^ l) < 0L) && ((paramLong ^ i) >= 0L)) {
/* 360 */         throw new ArithmeticException("Adding time zone offset caused overflow");
/*     */       }
/* 362 */       return i;
/*     */     }
/*     */     
/*     */     private int getOffsetFromLocalToSubtract(long paramLong) {
/* 366 */       int i = this.iZone.getOffsetFromLocal(paramLong);
/* 367 */       long l = paramLong - i;
/*     */       
/* 369 */       if (((paramLong ^ l) < 0L) && ((paramLong ^ i) < 0L)) {
/* 370 */         throw new ArithmeticException("Subtracting time zone offset caused overflow");
/*     */       }
/* 372 */       return i;
/*     */     }
/*     */     
/*     */     private long addOffset(long paramLong) {
/* 376 */       return this.iZone.convertUTCToLocal(paramLong);
/*     */     }
/*     */     
/*     */     public boolean equals(Object paramObject)
/*     */     {
/* 381 */       if (this == paramObject)
/* 382 */         return true;
/* 383 */       if ((paramObject instanceof ZonedDurationField)) {
/* 384 */         ZonedDurationField localZonedDurationField = (ZonedDurationField)paramObject;
/* 385 */         return (this.iField.equals(localZonedDurationField.iField)) && (this.iZone.equals(localZonedDurationField.iZone));
/*     */       }
/*     */       
/* 388 */       return false;
/*     */     }
/*     */     
/*     */     public int hashCode()
/*     */     {
/* 393 */       return this.iField.hashCode() ^ this.iZone.hashCode();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   static final class ZonedDateTimeField
/*     */     extends BaseDateTimeField
/*     */   {
/*     */     private static final long serialVersionUID = -3968986277775529794L;
/*     */     
/*     */     final DateTimeField iField;
/*     */     
/*     */     final DateTimeZone iZone;
/*     */     
/*     */     final DurationField iDurationField;
/*     */     
/*     */     final boolean iTimeField;
/*     */     
/*     */     final DurationField iRangeDurationField;
/*     */     
/*     */     final DurationField iLeapDurationField;
/*     */     
/*     */ 
/*     */     ZonedDateTimeField(DateTimeField paramDateTimeField, DateTimeZone paramDateTimeZone, DurationField paramDurationField1, DurationField paramDurationField2, DurationField paramDurationField3)
/*     */     {
/* 419 */       super();
/* 420 */       if (!paramDateTimeField.isSupported()) {
/* 421 */         throw new IllegalArgumentException();
/*     */       }
/* 423 */       this.iField = paramDateTimeField;
/* 424 */       this.iZone = paramDateTimeZone;
/* 425 */       this.iDurationField = paramDurationField1;
/* 426 */       this.iTimeField = ZonedChronology.useTimeArithmetic(paramDurationField1);
/* 427 */       this.iRangeDurationField = paramDurationField2;
/* 428 */       this.iLeapDurationField = paramDurationField3;
/*     */     }
/*     */     
/*     */     public boolean isLenient() {
/* 432 */       return this.iField.isLenient();
/*     */     }
/*     */     
/*     */     public int get(long paramLong) {
/* 436 */       long l = this.iZone.convertUTCToLocal(paramLong);
/* 437 */       return this.iField.get(l);
/*     */     }
/*     */     
/*     */     public String getAsText(long paramLong, Locale paramLocale) {
/* 441 */       long l = this.iZone.convertUTCToLocal(paramLong);
/* 442 */       return this.iField.getAsText(l, paramLocale);
/*     */     }
/*     */     
/*     */     public String getAsShortText(long paramLong, Locale paramLocale) {
/* 446 */       long l = this.iZone.convertUTCToLocal(paramLong);
/* 447 */       return this.iField.getAsShortText(l, paramLocale);
/*     */     }
/*     */     
/*     */     public String getAsText(int paramInt, Locale paramLocale) {
/* 451 */       return this.iField.getAsText(paramInt, paramLocale);
/*     */     }
/*     */     
/*     */     public String getAsShortText(int paramInt, Locale paramLocale) {
/* 455 */       return this.iField.getAsShortText(paramInt, paramLocale);
/*     */     }
/*     */     
/*     */     public long add(long paramLong, int paramInt) {
/* 459 */       if (this.iTimeField) {
/* 460 */         int i = getOffsetToAdd(paramLong);
/* 461 */         long l2 = this.iField.add(paramLong + i, paramInt);
/* 462 */         return l2 - i;
/*     */       }
/* 464 */       long l1 = this.iZone.convertUTCToLocal(paramLong);
/* 465 */       l1 = this.iField.add(l1, paramInt);
/* 466 */       return this.iZone.convertLocalToUTC(l1, false, paramLong);
/*     */     }
/*     */     
/*     */     public long add(long paramLong1, long paramLong2)
/*     */     {
/* 471 */       if (this.iTimeField) {
/* 472 */         int i = getOffsetToAdd(paramLong1);
/* 473 */         long l2 = this.iField.add(paramLong1 + i, paramLong2);
/* 474 */         return l2 - i;
/*     */       }
/* 476 */       long l1 = this.iZone.convertUTCToLocal(paramLong1);
/* 477 */       l1 = this.iField.add(l1, paramLong2);
/* 478 */       return this.iZone.convertLocalToUTC(l1, false, paramLong1);
/*     */     }
/*     */     
/*     */     public long addWrapField(long paramLong, int paramInt)
/*     */     {
/* 483 */       if (this.iTimeField) {
/* 484 */         int i = getOffsetToAdd(paramLong);
/* 485 */         long l2 = this.iField.addWrapField(paramLong + i, paramInt);
/* 486 */         return l2 - i;
/*     */       }
/* 488 */       long l1 = this.iZone.convertUTCToLocal(paramLong);
/* 489 */       l1 = this.iField.addWrapField(l1, paramInt);
/* 490 */       return this.iZone.convertLocalToUTC(l1, false, paramLong);
/*     */     }
/*     */     
/*     */     public long set(long paramLong, int paramInt)
/*     */     {
/* 495 */       long l1 = this.iZone.convertUTCToLocal(paramLong);
/* 496 */       l1 = this.iField.set(l1, paramInt);
/* 497 */       long l2 = this.iZone.convertLocalToUTC(l1, false, paramLong);
/* 498 */       if (get(l2) != paramInt) {
/* 499 */         IllegalInstantException localIllegalInstantException = new IllegalInstantException(l1, this.iZone.getID());
/* 500 */         IllegalFieldValueException localIllegalFieldValueException = new IllegalFieldValueException(this.iField.getType(), Integer.valueOf(paramInt), localIllegalInstantException.getMessage());
/* 501 */         localIllegalFieldValueException.initCause(localIllegalInstantException);
/* 502 */         throw localIllegalFieldValueException;
/*     */       }
/* 504 */       return l2;
/*     */     }
/*     */     
/*     */     public long set(long paramLong, String paramString, Locale paramLocale)
/*     */     {
/* 509 */       long l = this.iZone.convertUTCToLocal(paramLong);
/* 510 */       l = this.iField.set(l, paramString, paramLocale);
/* 511 */       return this.iZone.convertLocalToUTC(l, false, paramLong);
/*     */     }
/*     */     
/*     */     public int getDifference(long paramLong1, long paramLong2) {
/* 515 */       int i = getOffsetToAdd(paramLong2);
/* 516 */       return this.iField.getDifference(paramLong1 + (this.iTimeField ? i : getOffsetToAdd(paramLong1)), paramLong2 + i);
/*     */     }
/*     */     
/*     */ 
/*     */     public long getDifferenceAsLong(long paramLong1, long paramLong2)
/*     */     {
/* 522 */       int i = getOffsetToAdd(paramLong2);
/* 523 */       return this.iField.getDifferenceAsLong(paramLong1 + (this.iTimeField ? i : getOffsetToAdd(paramLong1)), paramLong2 + i);
/*     */     }
/*     */     
/*     */ 
/*     */     public final DurationField getDurationField()
/*     */     {
/* 529 */       return this.iDurationField;
/*     */     }
/*     */     
/*     */     public final DurationField getRangeDurationField() {
/* 533 */       return this.iRangeDurationField;
/*     */     }
/*     */     
/*     */     public boolean isLeap(long paramLong) {
/* 537 */       long l = this.iZone.convertUTCToLocal(paramLong);
/* 538 */       return this.iField.isLeap(l);
/*     */     }
/*     */     
/*     */     public int getLeapAmount(long paramLong) {
/* 542 */       long l = this.iZone.convertUTCToLocal(paramLong);
/* 543 */       return this.iField.getLeapAmount(l);
/*     */     }
/*     */     
/*     */     public final DurationField getLeapDurationField() {
/* 547 */       return this.iLeapDurationField;
/*     */     }
/*     */     
/*     */     public long roundFloor(long paramLong) {
/* 551 */       if (this.iTimeField) {
/* 552 */         int i = getOffsetToAdd(paramLong);
/* 553 */         paramLong = this.iField.roundFloor(paramLong + i);
/* 554 */         return paramLong - i;
/*     */       }
/* 556 */       long l = this.iZone.convertUTCToLocal(paramLong);
/* 557 */       l = this.iField.roundFloor(l);
/* 558 */       return this.iZone.convertLocalToUTC(l, false, paramLong);
/*     */     }
/*     */     
/*     */     public long roundCeiling(long paramLong)
/*     */     {
/* 563 */       if (this.iTimeField) {
/* 564 */         int i = getOffsetToAdd(paramLong);
/* 565 */         paramLong = this.iField.roundCeiling(paramLong + i);
/* 566 */         return paramLong - i;
/*     */       }
/* 568 */       long l = this.iZone.convertUTCToLocal(paramLong);
/* 569 */       l = this.iField.roundCeiling(l);
/* 570 */       return this.iZone.convertLocalToUTC(l, false, paramLong);
/*     */     }
/*     */     
/*     */     public long remainder(long paramLong)
/*     */     {
/* 575 */       long l = this.iZone.convertUTCToLocal(paramLong);
/* 576 */       return this.iField.remainder(l);
/*     */     }
/*     */     
/*     */     public int getMinimumValue() {
/* 580 */       return this.iField.getMinimumValue();
/*     */     }
/*     */     
/*     */     public int getMinimumValue(long paramLong) {
/* 584 */       long l = this.iZone.convertUTCToLocal(paramLong);
/* 585 */       return this.iField.getMinimumValue(l);
/*     */     }
/*     */     
/*     */     public int getMinimumValue(ReadablePartial paramReadablePartial) {
/* 589 */       return this.iField.getMinimumValue(paramReadablePartial);
/*     */     }
/*     */     
/*     */     public int getMinimumValue(ReadablePartial paramReadablePartial, int[] paramArrayOfInt) {
/* 593 */       return this.iField.getMinimumValue(paramReadablePartial, paramArrayOfInt);
/*     */     }
/*     */     
/*     */     public int getMaximumValue() {
/* 597 */       return this.iField.getMaximumValue();
/*     */     }
/*     */     
/*     */     public int getMaximumValue(long paramLong) {
/* 601 */       long l = this.iZone.convertUTCToLocal(paramLong);
/* 602 */       return this.iField.getMaximumValue(l);
/*     */     }
/*     */     
/*     */     public int getMaximumValue(ReadablePartial paramReadablePartial) {
/* 606 */       return this.iField.getMaximumValue(paramReadablePartial);
/*     */     }
/*     */     
/*     */     public int getMaximumValue(ReadablePartial paramReadablePartial, int[] paramArrayOfInt) {
/* 610 */       return this.iField.getMaximumValue(paramReadablePartial, paramArrayOfInt);
/*     */     }
/*     */     
/*     */     public int getMaximumTextLength(Locale paramLocale) {
/* 614 */       return this.iField.getMaximumTextLength(paramLocale);
/*     */     }
/*     */     
/*     */     public int getMaximumShortTextLength(Locale paramLocale) {
/* 618 */       return this.iField.getMaximumShortTextLength(paramLocale);
/*     */     }
/*     */     
/*     */     private int getOffsetToAdd(long paramLong) {
/* 622 */       int i = this.iZone.getOffset(paramLong);
/* 623 */       long l = paramLong + i;
/*     */       
/* 625 */       if (((paramLong ^ l) < 0L) && ((paramLong ^ i) >= 0L)) {
/* 626 */         throw new ArithmeticException("Adding time zone offset caused overflow");
/*     */       }
/* 628 */       return i;
/*     */     }
/*     */     
/*     */     public boolean equals(Object paramObject)
/*     */     {
/* 633 */       if (this == paramObject)
/* 634 */         return true;
/* 635 */       if ((paramObject instanceof ZonedDateTimeField)) {
/* 636 */         ZonedDateTimeField localZonedDateTimeField = (ZonedDateTimeField)paramObject;
/* 637 */         return (this.iField.equals(localZonedDateTimeField.iField)) && (this.iZone.equals(localZonedDateTimeField.iZone)) && (this.iDurationField.equals(localZonedDateTimeField.iDurationField)) && (this.iRangeDurationField.equals(localZonedDateTimeField.iRangeDurationField));
/*     */       }
/*     */       
/*     */ 
/*     */ 
/* 642 */       return false;
/*     */     }
/*     */     
/*     */     public int hashCode()
/*     */     {
/* 647 */       return this.iField.hashCode() ^ this.iZone.hashCode();
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\org\joda\time\chrono\ZonedChronology.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */