/*     */ package org.joda.time.chrono;
/*     */ 
/*     */ import java.util.HashMap;
/*     */ import java.util.Locale;
/*     */ import org.joda.time.Chronology;
/*     */ import org.joda.time.DateTime;
/*     */ import org.joda.time.DateTimeField;
/*     */ import org.joda.time.DateTimeZone;
/*     */ import org.joda.time.DurationField;
/*     */ import org.joda.time.MutableDateTime;
/*     */ import org.joda.time.ReadableDateTime;
/*     */ import org.joda.time.field.DecoratedDateTimeField;
/*     */ import org.joda.time.field.DecoratedDurationField;
/*     */ import org.joda.time.field.FieldUtils;
/*     */ import org.joda.time.format.DateTimeFormatter;
/*     */ import org.joda.time.format.ISODateTimeFormat;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public final class LimitChronology
/*     */   extends AssembledChronology
/*     */ {
/*     */   private static final long serialVersionUID = 7670866536893052522L;
/*     */   final DateTime iLowerLimit;
/*     */   final DateTime iUpperLimit;
/*     */   private transient LimitChronology iWithUTC;
/*     */   
/*     */   public static LimitChronology getInstance(Chronology paramChronology, ReadableDateTime paramReadableDateTime1, ReadableDateTime paramReadableDateTime2)
/*     */   {
/*  67 */     if (paramChronology == null) {
/*  68 */       throw new IllegalArgumentException("Must supply a chronology");
/*     */     }
/*     */     
/*  71 */     paramReadableDateTime1 = paramReadableDateTime1 == null ? null : paramReadableDateTime1.toDateTime();
/*  72 */     paramReadableDateTime2 = paramReadableDateTime2 == null ? null : paramReadableDateTime2.toDateTime();
/*     */     
/*  74 */     if ((paramReadableDateTime1 != null) && (paramReadableDateTime2 != null) && (!paramReadableDateTime1.isBefore(paramReadableDateTime2))) {
/*  75 */       throw new IllegalArgumentException("The lower limit must be come before than the upper limit");
/*     */     }
/*     */     
/*     */ 
/*  79 */     return new LimitChronology(paramChronology, (DateTime)paramReadableDateTime1, (DateTime)paramReadableDateTime2);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private LimitChronology(Chronology paramChronology, DateTime paramDateTime1, DateTime paramDateTime2)
/*     */   {
/*  97 */     super(paramChronology, null);
/*     */     
/*  99 */     this.iLowerLimit = paramDateTime1;
/* 100 */     this.iUpperLimit = paramDateTime2;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public DateTime getLowerLimit()
/*     */   {
/* 109 */     return this.iLowerLimit;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public DateTime getUpperLimit()
/*     */   {
/* 118 */     return this.iUpperLimit;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Chronology withUTC()
/*     */   {
/* 127 */     return withZone(DateTimeZone.UTC);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Chronology withZone(DateTimeZone paramDateTimeZone)
/*     */   {
/* 136 */     if (paramDateTimeZone == null) {
/* 137 */       paramDateTimeZone = DateTimeZone.getDefault();
/*     */     }
/* 139 */     if (paramDateTimeZone == getZone()) {
/* 140 */       return this;
/*     */     }
/*     */     
/* 143 */     if ((paramDateTimeZone == DateTimeZone.UTC) && (this.iWithUTC != null)) {
/* 144 */       return this.iWithUTC;
/*     */     }
/*     */     
/* 147 */     DateTime localDateTime = this.iLowerLimit;
/* 148 */     if (localDateTime != null) {
/* 149 */       localObject1 = localDateTime.toMutableDateTime();
/* 150 */       ((MutableDateTime)localObject1).setZoneRetainFields(paramDateTimeZone);
/* 151 */       localDateTime = ((MutableDateTime)localObject1).toDateTime();
/*     */     }
/*     */     
/* 154 */     Object localObject1 = this.iUpperLimit;
/* 155 */     if (localObject1 != null) {
/* 156 */       localObject2 = ((DateTime)localObject1).toMutableDateTime();
/* 157 */       ((MutableDateTime)localObject2).setZoneRetainFields(paramDateTimeZone);
/* 158 */       localObject1 = ((MutableDateTime)localObject2).toDateTime();
/*     */     }
/*     */     
/* 161 */     Object localObject2 = getInstance(getBase().withZone(paramDateTimeZone), localDateTime, (ReadableDateTime)localObject1);
/*     */     
/*     */ 
/* 164 */     if (paramDateTimeZone == DateTimeZone.UTC) {
/* 165 */       this.iWithUTC = ((LimitChronology)localObject2);
/*     */     }
/*     */     
/* 168 */     return (Chronology)localObject2;
/*     */   }
/*     */   
/*     */ 
/*     */   public long getDateTimeMillis(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
/*     */     throws IllegalArgumentException
/*     */   {
/* 175 */     long l = getBase().getDateTimeMillis(paramInt1, paramInt2, paramInt3, paramInt4);
/* 176 */     checkLimits(l, "resulting");
/* 177 */     return l;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public long getDateTimeMillis(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7)
/*     */     throws IllegalArgumentException
/*     */   {
/* 185 */     long l = getBase().getDateTimeMillis(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7);
/*     */     
/*     */ 
/* 188 */     checkLimits(l, "resulting");
/* 189 */     return l;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public long getDateTimeMillis(long paramLong, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
/*     */     throws IllegalArgumentException
/*     */   {
/* 197 */     checkLimits(paramLong, null);
/* 198 */     paramLong = getBase().getDateTimeMillis(paramLong, paramInt1, paramInt2, paramInt3, paramInt4);
/*     */     
/* 200 */     checkLimits(paramLong, "resulting");
/* 201 */     return paramLong;
/*     */   }
/*     */   
/*     */ 
/*     */   protected void assemble(AssembledChronology.Fields paramFields)
/*     */   {
/* 207 */     HashMap localHashMap = new HashMap();
/*     */     
/*     */ 
/*     */ 
/* 211 */     paramFields.eras = convertField(paramFields.eras, localHashMap);
/* 212 */     paramFields.centuries = convertField(paramFields.centuries, localHashMap);
/* 213 */     paramFields.years = convertField(paramFields.years, localHashMap);
/* 214 */     paramFields.months = convertField(paramFields.months, localHashMap);
/* 215 */     paramFields.weekyears = convertField(paramFields.weekyears, localHashMap);
/* 216 */     paramFields.weeks = convertField(paramFields.weeks, localHashMap);
/* 217 */     paramFields.days = convertField(paramFields.days, localHashMap);
/*     */     
/* 219 */     paramFields.halfdays = convertField(paramFields.halfdays, localHashMap);
/* 220 */     paramFields.hours = convertField(paramFields.hours, localHashMap);
/* 221 */     paramFields.minutes = convertField(paramFields.minutes, localHashMap);
/* 222 */     paramFields.seconds = convertField(paramFields.seconds, localHashMap);
/* 223 */     paramFields.millis = convertField(paramFields.millis, localHashMap);
/*     */     
/*     */ 
/*     */ 
/* 227 */     paramFields.year = convertField(paramFields.year, localHashMap);
/* 228 */     paramFields.yearOfEra = convertField(paramFields.yearOfEra, localHashMap);
/* 229 */     paramFields.yearOfCentury = convertField(paramFields.yearOfCentury, localHashMap);
/* 230 */     paramFields.centuryOfEra = convertField(paramFields.centuryOfEra, localHashMap);
/* 231 */     paramFields.era = convertField(paramFields.era, localHashMap);
/* 232 */     paramFields.dayOfWeek = convertField(paramFields.dayOfWeek, localHashMap);
/* 233 */     paramFields.dayOfMonth = convertField(paramFields.dayOfMonth, localHashMap);
/* 234 */     paramFields.dayOfYear = convertField(paramFields.dayOfYear, localHashMap);
/* 235 */     paramFields.monthOfYear = convertField(paramFields.monthOfYear, localHashMap);
/* 236 */     paramFields.weekOfWeekyear = convertField(paramFields.weekOfWeekyear, localHashMap);
/* 237 */     paramFields.weekyear = convertField(paramFields.weekyear, localHashMap);
/* 238 */     paramFields.weekyearOfCentury = convertField(paramFields.weekyearOfCentury, localHashMap);
/*     */     
/* 240 */     paramFields.millisOfSecond = convertField(paramFields.millisOfSecond, localHashMap);
/* 241 */     paramFields.millisOfDay = convertField(paramFields.millisOfDay, localHashMap);
/* 242 */     paramFields.secondOfMinute = convertField(paramFields.secondOfMinute, localHashMap);
/* 243 */     paramFields.secondOfDay = convertField(paramFields.secondOfDay, localHashMap);
/* 244 */     paramFields.minuteOfHour = convertField(paramFields.minuteOfHour, localHashMap);
/* 245 */     paramFields.minuteOfDay = convertField(paramFields.minuteOfDay, localHashMap);
/* 246 */     paramFields.hourOfDay = convertField(paramFields.hourOfDay, localHashMap);
/* 247 */     paramFields.hourOfHalfday = convertField(paramFields.hourOfHalfday, localHashMap);
/* 248 */     paramFields.clockhourOfDay = convertField(paramFields.clockhourOfDay, localHashMap);
/* 249 */     paramFields.clockhourOfHalfday = convertField(paramFields.clockhourOfHalfday, localHashMap);
/* 250 */     paramFields.halfdayOfDay = convertField(paramFields.halfdayOfDay, localHashMap);
/*     */   }
/*     */   
/*     */   private DurationField convertField(DurationField paramDurationField, HashMap<Object, Object> paramHashMap) {
/* 254 */     if ((paramDurationField == null) || (!paramDurationField.isSupported())) {
/* 255 */       return paramDurationField;
/*     */     }
/* 257 */     if (paramHashMap.containsKey(paramDurationField)) {
/* 258 */       return (DurationField)paramHashMap.get(paramDurationField);
/*     */     }
/* 260 */     LimitDurationField localLimitDurationField = new LimitDurationField(paramDurationField);
/* 261 */     paramHashMap.put(paramDurationField, localLimitDurationField);
/* 262 */     return localLimitDurationField;
/*     */   }
/*     */   
/*     */   private DateTimeField convertField(DateTimeField paramDateTimeField, HashMap<Object, Object> paramHashMap) {
/* 266 */     if ((paramDateTimeField == null) || (!paramDateTimeField.isSupported())) {
/* 267 */       return paramDateTimeField;
/*     */     }
/* 269 */     if (paramHashMap.containsKey(paramDateTimeField)) {
/* 270 */       return (DateTimeField)paramHashMap.get(paramDateTimeField);
/*     */     }
/* 272 */     LimitDateTimeField localLimitDateTimeField = new LimitDateTimeField(paramDateTimeField, convertField(paramDateTimeField.getDurationField(), paramHashMap), convertField(paramDateTimeField.getRangeDurationField(), paramHashMap), convertField(paramDateTimeField.getLeapDurationField(), paramHashMap));
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 277 */     paramHashMap.put(paramDateTimeField, localLimitDateTimeField);
/* 278 */     return localLimitDateTimeField;
/*     */   }
/*     */   
/*     */   void checkLimits(long paramLong, String paramString) {
/*     */     DateTime localDateTime;
/* 283 */     if (((localDateTime = this.iLowerLimit) != null) && (paramLong < localDateTime.getMillis())) {
/* 284 */       throw new LimitException(paramString, true);
/*     */     }
/* 286 */     if (((localDateTime = this.iUpperLimit) != null) && (paramLong >= localDateTime.getMillis())) {
/* 287 */       throw new LimitException(paramString, false);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean equals(Object paramObject)
/*     */   {
/* 301 */     if (this == paramObject) {
/* 302 */       return true;
/*     */     }
/* 304 */     if (!(paramObject instanceof LimitChronology)) {
/* 305 */       return false;
/*     */     }
/* 307 */     LimitChronology localLimitChronology = (LimitChronology)paramObject;
/* 308 */     return (getBase().equals(localLimitChronology.getBase())) && (FieldUtils.equals(getLowerLimit(), localLimitChronology.getLowerLimit())) && (FieldUtils.equals(getUpperLimit(), localLimitChronology.getUpperLimit()));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int hashCode()
/*     */   {
/* 321 */     int i = 317351877;
/* 322 */     i += (getLowerLimit() != null ? getLowerLimit().hashCode() : 0);
/* 323 */     i += (getUpperLimit() != null ? getUpperLimit().hashCode() : 0);
/* 324 */     i += getBase().hashCode() * 7;
/* 325 */     return i;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String toString()
/*     */   {
/* 334 */     return "LimitChronology[" + getBase().toString() + ", " + (getLowerLimit() == null ? "NoLimit" : getLowerLimit().toString()) + ", " + (getUpperLimit() == null ? "NoLimit" : getUpperLimit().toString()) + ']';
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private class LimitException
/*     */     extends IllegalArgumentException
/*     */   {
/*     */     private static final long serialVersionUID = -5924689995607498581L;
/*     */     
/*     */ 
/*     */     private final boolean iIsLow;
/*     */     
/*     */ 
/*     */     LimitException(String paramString, boolean paramBoolean)
/*     */     {
/* 350 */       super();
/* 351 */       this.iIsLow = paramBoolean;
/*     */     }
/*     */     
/*     */     public String getMessage() {
/* 355 */       StringBuffer localStringBuffer = new StringBuffer(85);
/* 356 */       localStringBuffer.append("The");
/* 357 */       String str = super.getMessage();
/* 358 */       if (str != null) {
/* 359 */         localStringBuffer.append(' ');
/* 360 */         localStringBuffer.append(str);
/*     */       }
/* 362 */       localStringBuffer.append(" instant is ");
/*     */       
/* 364 */       DateTimeFormatter localDateTimeFormatter = ISODateTimeFormat.dateTime();
/* 365 */       localDateTimeFormatter = localDateTimeFormatter.withChronology(LimitChronology.this.getBase());
/* 366 */       if (this.iIsLow) {
/* 367 */         localStringBuffer.append("below the supported minimum of ");
/* 368 */         localDateTimeFormatter.printTo(localStringBuffer, LimitChronology.this.getLowerLimit().getMillis());
/*     */       } else {
/* 370 */         localStringBuffer.append("above the supported maximum of ");
/* 371 */         localDateTimeFormatter.printTo(localStringBuffer, LimitChronology.this.getUpperLimit().getMillis());
/*     */       }
/*     */       
/* 374 */       localStringBuffer.append(" (");
/* 375 */       localStringBuffer.append(LimitChronology.this.getBase());
/* 376 */       localStringBuffer.append(')');
/*     */       
/* 378 */       return localStringBuffer.toString();
/*     */     }
/*     */     
/*     */     public String toString() {
/* 382 */       return "IllegalArgumentException: " + getMessage();
/*     */     }
/*     */   }
/*     */   
/*     */   private class LimitDurationField extends DecoratedDurationField {
/*     */     private static final long serialVersionUID = 8049297699408782284L;
/*     */     
/*     */     LimitDurationField(DurationField paramDurationField) {
/* 390 */       super(paramDurationField.getType());
/*     */     }
/*     */     
/*     */     public int getValue(long paramLong1, long paramLong2) {
/* 394 */       LimitChronology.this.checkLimits(paramLong2, null);
/* 395 */       return getWrappedField().getValue(paramLong1, paramLong2);
/*     */     }
/*     */     
/*     */     public long getValueAsLong(long paramLong1, long paramLong2) {
/* 399 */       LimitChronology.this.checkLimits(paramLong2, null);
/* 400 */       return getWrappedField().getValueAsLong(paramLong1, paramLong2);
/*     */     }
/*     */     
/*     */     public long getMillis(int paramInt, long paramLong) {
/* 404 */       LimitChronology.this.checkLimits(paramLong, null);
/* 405 */       return getWrappedField().getMillis(paramInt, paramLong);
/*     */     }
/*     */     
/*     */     public long getMillis(long paramLong1, long paramLong2) {
/* 409 */       LimitChronology.this.checkLimits(paramLong2, null);
/* 410 */       return getWrappedField().getMillis(paramLong1, paramLong2);
/*     */     }
/*     */     
/*     */     public long add(long paramLong, int paramInt) {
/* 414 */       LimitChronology.this.checkLimits(paramLong, null);
/* 415 */       long l = getWrappedField().add(paramLong, paramInt);
/* 416 */       LimitChronology.this.checkLimits(l, "resulting");
/* 417 */       return l;
/*     */     }
/*     */     
/*     */     public long add(long paramLong1, long paramLong2) {
/* 421 */       LimitChronology.this.checkLimits(paramLong1, null);
/* 422 */       long l = getWrappedField().add(paramLong1, paramLong2);
/* 423 */       LimitChronology.this.checkLimits(l, "resulting");
/* 424 */       return l;
/*     */     }
/*     */     
/*     */     public int getDifference(long paramLong1, long paramLong2) {
/* 428 */       LimitChronology.this.checkLimits(paramLong1, "minuend");
/* 429 */       LimitChronology.this.checkLimits(paramLong2, "subtrahend");
/* 430 */       return getWrappedField().getDifference(paramLong1, paramLong2);
/*     */     }
/*     */     
/*     */     public long getDifferenceAsLong(long paramLong1, long paramLong2) {
/* 434 */       LimitChronology.this.checkLimits(paramLong1, "minuend");
/* 435 */       LimitChronology.this.checkLimits(paramLong2, "subtrahend");
/* 436 */       return getWrappedField().getDifferenceAsLong(paramLong1, paramLong2);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   private class LimitDateTimeField
/*     */     extends DecoratedDateTimeField
/*     */   {
/*     */     private static final long serialVersionUID = -2435306746995699312L;
/*     */     
/*     */     private final DurationField iDurationField;
/*     */     
/*     */     private final DurationField iRangeDurationField;
/*     */     private final DurationField iLeapDurationField;
/*     */     
/*     */     LimitDateTimeField(DateTimeField paramDateTimeField, DurationField paramDurationField1, DurationField paramDurationField2, DurationField paramDurationField3)
/*     */     {
/* 453 */       super(paramDateTimeField.getType());
/* 454 */       this.iDurationField = paramDurationField1;
/* 455 */       this.iRangeDurationField = paramDurationField2;
/* 456 */       this.iLeapDurationField = paramDurationField3;
/*     */     }
/*     */     
/*     */     public int get(long paramLong) {
/* 460 */       LimitChronology.this.checkLimits(paramLong, null);
/* 461 */       return getWrappedField().get(paramLong);
/*     */     }
/*     */     
/*     */     public String getAsText(long paramLong, Locale paramLocale) {
/* 465 */       LimitChronology.this.checkLimits(paramLong, null);
/* 466 */       return getWrappedField().getAsText(paramLong, paramLocale);
/*     */     }
/*     */     
/*     */     public String getAsShortText(long paramLong, Locale paramLocale) {
/* 470 */       LimitChronology.this.checkLimits(paramLong, null);
/* 471 */       return getWrappedField().getAsShortText(paramLong, paramLocale);
/*     */     }
/*     */     
/*     */     public long add(long paramLong, int paramInt) {
/* 475 */       LimitChronology.this.checkLimits(paramLong, null);
/* 476 */       long l = getWrappedField().add(paramLong, paramInt);
/* 477 */       LimitChronology.this.checkLimits(l, "resulting");
/* 478 */       return l;
/*     */     }
/*     */     
/*     */     public long add(long paramLong1, long paramLong2) {
/* 482 */       LimitChronology.this.checkLimits(paramLong1, null);
/* 483 */       long l = getWrappedField().add(paramLong1, paramLong2);
/* 484 */       LimitChronology.this.checkLimits(l, "resulting");
/* 485 */       return l;
/*     */     }
/*     */     
/*     */     public long addWrapField(long paramLong, int paramInt) {
/* 489 */       LimitChronology.this.checkLimits(paramLong, null);
/* 490 */       long l = getWrappedField().addWrapField(paramLong, paramInt);
/* 491 */       LimitChronology.this.checkLimits(l, "resulting");
/* 492 */       return l;
/*     */     }
/*     */     
/*     */     public int getDifference(long paramLong1, long paramLong2) {
/* 496 */       LimitChronology.this.checkLimits(paramLong1, "minuend");
/* 497 */       LimitChronology.this.checkLimits(paramLong2, "subtrahend");
/* 498 */       return getWrappedField().getDifference(paramLong1, paramLong2);
/*     */     }
/*     */     
/*     */     public long getDifferenceAsLong(long paramLong1, long paramLong2) {
/* 502 */       LimitChronology.this.checkLimits(paramLong1, "minuend");
/* 503 */       LimitChronology.this.checkLimits(paramLong2, "subtrahend");
/* 504 */       return getWrappedField().getDifferenceAsLong(paramLong1, paramLong2);
/*     */     }
/*     */     
/*     */     public long set(long paramLong, int paramInt) {
/* 508 */       LimitChronology.this.checkLimits(paramLong, null);
/* 509 */       long l = getWrappedField().set(paramLong, paramInt);
/* 510 */       LimitChronology.this.checkLimits(l, "resulting");
/* 511 */       return l;
/*     */     }
/*     */     
/*     */     public long set(long paramLong, String paramString, Locale paramLocale) {
/* 515 */       LimitChronology.this.checkLimits(paramLong, null);
/* 516 */       long l = getWrappedField().set(paramLong, paramString, paramLocale);
/* 517 */       LimitChronology.this.checkLimits(l, "resulting");
/* 518 */       return l;
/*     */     }
/*     */     
/*     */     public final DurationField getDurationField() {
/* 522 */       return this.iDurationField;
/*     */     }
/*     */     
/*     */     public final DurationField getRangeDurationField() {
/* 526 */       return this.iRangeDurationField;
/*     */     }
/*     */     
/*     */     public boolean isLeap(long paramLong) {
/* 530 */       LimitChronology.this.checkLimits(paramLong, null);
/* 531 */       return getWrappedField().isLeap(paramLong);
/*     */     }
/*     */     
/*     */     public int getLeapAmount(long paramLong) {
/* 535 */       LimitChronology.this.checkLimits(paramLong, null);
/* 536 */       return getWrappedField().getLeapAmount(paramLong);
/*     */     }
/*     */     
/*     */     public final DurationField getLeapDurationField() {
/* 540 */       return this.iLeapDurationField;
/*     */     }
/*     */     
/*     */     public long roundFloor(long paramLong) {
/* 544 */       LimitChronology.this.checkLimits(paramLong, null);
/* 545 */       long l = getWrappedField().roundFloor(paramLong);
/* 546 */       LimitChronology.this.checkLimits(l, "resulting");
/* 547 */       return l;
/*     */     }
/*     */     
/*     */     public long roundCeiling(long paramLong) {
/* 551 */       LimitChronology.this.checkLimits(paramLong, null);
/* 552 */       long l = getWrappedField().roundCeiling(paramLong);
/* 553 */       LimitChronology.this.checkLimits(l, "resulting");
/* 554 */       return l;
/*     */     }
/*     */     
/*     */     public long roundHalfFloor(long paramLong) {
/* 558 */       LimitChronology.this.checkLimits(paramLong, null);
/* 559 */       long l = getWrappedField().roundHalfFloor(paramLong);
/* 560 */       LimitChronology.this.checkLimits(l, "resulting");
/* 561 */       return l;
/*     */     }
/*     */     
/*     */     public long roundHalfCeiling(long paramLong) {
/* 565 */       LimitChronology.this.checkLimits(paramLong, null);
/* 566 */       long l = getWrappedField().roundHalfCeiling(paramLong);
/* 567 */       LimitChronology.this.checkLimits(l, "resulting");
/* 568 */       return l;
/*     */     }
/*     */     
/*     */     public long roundHalfEven(long paramLong) {
/* 572 */       LimitChronology.this.checkLimits(paramLong, null);
/* 573 */       long l = getWrappedField().roundHalfEven(paramLong);
/* 574 */       LimitChronology.this.checkLimits(l, "resulting");
/* 575 */       return l;
/*     */     }
/*     */     
/*     */     public long remainder(long paramLong) {
/* 579 */       LimitChronology.this.checkLimits(paramLong, null);
/* 580 */       long l = getWrappedField().remainder(paramLong);
/* 581 */       LimitChronology.this.checkLimits(l, "resulting");
/* 582 */       return l;
/*     */     }
/*     */     
/*     */     public int getMinimumValue(long paramLong) {
/* 586 */       LimitChronology.this.checkLimits(paramLong, null);
/* 587 */       return getWrappedField().getMinimumValue(paramLong);
/*     */     }
/*     */     
/*     */     public int getMaximumValue(long paramLong) {
/* 591 */       LimitChronology.this.checkLimits(paramLong, null);
/* 592 */       return getWrappedField().getMaximumValue(paramLong);
/*     */     }
/*     */     
/*     */     public int getMaximumTextLength(Locale paramLocale) {
/* 596 */       return getWrappedField().getMaximumTextLength(paramLocale);
/*     */     }
/*     */     
/*     */     public int getMaximumShortTextLength(Locale paramLocale) {
/* 600 */       return getWrappedField().getMaximumShortTextLength(paramLocale);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemBan-1.0-SNAPSHOT.jar!\org\joda\time\chrono\LimitChronology.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */